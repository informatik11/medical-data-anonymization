#include "buckettree.h"


bool BucketTree::BucketTree_node_t::constraintHolds(double maxCost,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    if(isnan(maxCost))
    {
        return false;
    }
    if(isnan(costCache))
    {
        costCache = bucket->calcMaxCost(groundDistanceCache);
    }
    return (costCache <= maxCost);
}

bool BucketTree::BucketTree_node_t::constraintHoldsAlways(double maxCost,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    if(constraintHolds(maxCost,groundDistanceCache))
    {
        if(isLeaf())
        {
            return true;
        }else{
            return (left->constraintHoldsAlways(maxCost,groundDistanceCache) &&  right->constraintHoldsAlways(maxCost,groundDistanceCache));
        }
    }else{
        return false;
    }
}

QList<Bucket *> BucketTree::BucketTree_node_t::getLeafBuckets()
{
    QList<Bucket*> ret;
    if(isLeaf())
    {
        ret.append(this->bucket);
        return ret;
    }else{
        QList<BucketTree_node_t*> leftLeafs = left->getLeafNodes();
        QList<BucketTree_node_t*> rightLeafs = right->getLeafNodes();

        for(BucketTree_node_t* node : leftLeafs)
        {
            ret.append(node->bucket);
        }

        for(BucketTree_node_t* node : rightLeafs)
        {
            ret.append(node->bucket);
        }

        return ret;
    }
}

QList<BucketTree::BucketTree_node_t *> BucketTree::BucketTree_node_t::getLeafNodes()
{
    QList<BucketTree_node_t*> ret;
    if(isLeaf())
    {
        ret.append(this);
        return ret;
    }else{
        QList<BucketTree_node_t*> leftLeafs = left->getLeafNodes();
        QList<BucketTree_node_t*> rightLeafs = right->getLeafNodes();

        for(BucketTree_node_t* node : leftLeafs)
        {
            ret.append(node);
        }

        for(BucketTree_node_t* node : rightLeafs)
        {
            ret.append(node);
        }

        return ret;
    }
}

QList<BucketTree::BucketTree_node_t *> BucketTree::BucketTree_node_t::getNodes(int depth)
{
    QList<BucketTree_node_t *>  ret;
    if(depth < 0)
    {
        return ret;
    }else if(depth == 0){
        ret.append(this);
        return ret;
    }else{//still need to recurse
        if(isLeaf())
        {
            return ret;
        }else{
            QList<BucketTree_node_t *> lRet = left->getNodes(depth -1);
            QList<BucketTree_node_t *> rRet = right->getNodes(depth -1);
            if(lRet.isEmpty() ||  rRet.isEmpty())//if depth cant be reached in one branch nothing is returned!
            {
                return ret;
            }else{
                for(BucketTree_node_t* node : lRet)
                {
                    ret.append(node);
                }

                for(BucketTree_node_t* node : rRet)
                {
                    ret.append(node);
                }

                return ret;
            }
        }
    }
}

QList<Bucket *> BucketTree::BucketTree_node_t::getBuckets(int depth)
{
    QList<Bucket *>  ret;
    if(depth < 0)
    {
        return ret;
    }else if(depth == 0){
        ret.append(this->bucket);
        return ret;
    }else{//still need to recurse
        if(isLeaf())
        {
            return ret;
        }else{
            QList<BucketTree_node_t *> lRet = left->getNodes(depth -1);
            QList<BucketTree_node_t *> rRet = right->getNodes(depth -1);
            if(lRet.isEmpty() ||  rRet.isEmpty())//if depth cant be reached in one branch nothing is returned!
            {
                return ret;
            }else{
                for(BucketTree_node_t* node : lRet)
                {
                    ret.append(node->bucket);
                }

                for(BucketTree_node_t* node : rRet)
                {
                    ret.append(node->bucket);
                }

                return ret;
            }
        }
    }
}

void BucketTree::BucketTree_node_t::deleteTree(bool delBuckets)
{
    if(left != nullptr )
    {
        left->deleteTree(delBuckets);
        delete left;
    }
    if(right != nullptr)
    {
        right->deleteTree(delBuckets);
        delete right;
    }
    if(delBuckets)
    {
        delete bucket;
    }
}

QString BucketTree::BucketTree_node_t::toString()
{
    QString currStr = QString("S:%1,F:%2").arg(bucket->size()).arg(bucket->getFullCount());
    QString lStr;
    if(left != nullptr)
    {
        lStr = "-L-"+left->toString();
    }else{
        lStr = "--<EMPTY>";
    }
    QString rStr;
    if(right != nullptr)
    {
        rStr = "-R-"+right->toString();
    }else{
        rStr = "<EMPTY>";
    }
    return currStr + lStr + rStr;//FIXME better visualization
}
double BucketTree::splitLeafsUntil(BucketTree_node_t *node, double costThres,double minBucketFactor,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    if(node->isLeaf())
    {
        return splitUntil(node,costThres,minBucketFactor,groundDistanceCache);
    }else{
        return (splitLeafsUntil(node->left,costThres,minBucketFactor,groundDistanceCache) + splitLeafsUntil(node->right,costThres,minBucketFactor,groundDistanceCache))/2.0;
    }
}

QString BucketTree::toString(){
    if(root != nullptr)
    {
        return root->toString();
    }else{
        return "<EMPTY>";
    }
}

QList<BucketTree::BucketTree_node_t *> BucketTree::getLeafNodes()
{
    return root->getLeafNodes();
}

QList<Bucket *> BucketTree::getLeafBuckets()
{
    return root->getLeafBuckets();
}

QList<Bucket *> BucketTree::getBuckets(int depth)
{
    return root->getBuckets(depth);
}

QList<BucketTree::BucketTree_node_t *> BucketTree::getNodes(int depth)
{
    return root->getNodes(depth);
}

bool BucketTree::isLevelEmpty(int depth)
{
    return isLevelEmpty(getBuckets(depth));
}

bool BucketTree::isLevelEmpty(QList<Bucket *> buckets)
{
    for(Bucket* bucket : buckets)
    {
        if(bucket->isEmpty() == false)
        {
            return false;
        }
    }
    return true;
}

BucketTree *BucketTree::genTree(QList<DataContainer *> data, Distribution *distr, DataDistanceMetric *metric,double minBucketFactor)
{
    if(!data.isEmpty() && distr != nullptr && metric != nullptr && !isnan(minBucketFactor))
    {
        QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = new QHash<DataContainer*,QHash<DataContainer*,double>*>();
        Bucket* rootBucket = new Bucket(INFINITY,metric,distr);//create bucket with all data in it, no matter how bad
        for(DataContainer* cont : data)
        {
            if(rootBucket->add(cont,groundDistanceCache,true) == false){
                qCritical() << QString("Data could not be added to root-bucket, the maxCost  was to low. Try again.");
                QString msg = QString("Die Daten konnten nicht in den Wurzeleimer eingefügt werden, die eingestellten maximalen Kosten waren zu niedrig. Versuchen Sie es erneut");
                QMessageBox bx;
                bx.setText(msg);
                bx.exec();
                return nullptr;
            }
        }
        BucketTree::BucketTree_node_t* root = new BucketTree::BucketTree_node_t();
        root->bucket = rootBucket;
        root->costCache = root->bucket->calcMaxCost(groundDistanceCache);
        double costThres = BucketTree::splitUntil(root,1,minBucketFactor,groundDistanceCache);
        if(!isnan(costThres))
        {
            unsigned int maxDepth = BucketTree::getMaxDepth(root);
            unsigned int minDepth = BucketTree::getMinDepth(root);
            if(maxDepth != minDepth)
            {
                double maxReachedCost = BucketTree::splitDepth(root,maxDepth,minBucketFactor,groundDistanceCache);
                if(maxReachedCost < 0)
                {
                    qWarning() << "Could not split to the same cost level on all branches!";
                }
                return new BucketTree(root,maxReachedCost);

            }else{

                return new BucketTree(root,costThres);
            }
        }else{
            root->deleteTree(true);
            delete root;
            return nullptr;
        }

    }else{
        return nullptr;
    }
}

void BucketTree::deleteTree(bool delBuckets)
{
    if(root != nullptr)
    {
        root->deleteTree(delBuckets);
    }
}

int BucketTree::getDepth()
{
    if(root != nullptr)
    {
        return root->getDepth();
    }else{
        return -1;
    }
}

int BucketTree::getMinDepth(double c)
{
    if(root != nullptr)
    {
        return root->getMinDepth(c);
    }else{
        return -3;
    }
}

void BucketTree::clearChache()
{
    if(root != nullptr)
    {
        root->clearChache();
    }
}

double BucketTree::getMaxCost() const
{
    return maxCost;
}

Distribution *BucketTree::genDistrib(unsigned int depth)
{
    if(root == nullptr)
    {
        return nullptr;
    }else{
        return new Distribution(root->genDistribHelper(depth));
    }
}

double BucketTree::getMaxCost(unsigned int depth)
{
    if( root != nullptr)
    {
        return root->getMaxCost(depth);
    }else{
        return  nan("");
    }
}

void BucketTree::reset()
{
    if(root != nullptr)
    {
        root->reset();
    }
}

int BucketTree::BucketTree_node_t::getDepth()
{
    if(parent == nullptr)
    {
        return 0;
    }else{
        return parent->getDepth() + 1;
    }
}

int BucketTree::BucketTree_node_t::getMinDepth(double c)
{
    return getMinDepthHelper(c,getDepth());
}

void BucketTree::BucketTree_node_t::clearChache()
{
    costCache = nan("");
    if(!isLeaf())
    {
        left->clearChache();
        right->clearChache();
    }
}

int BucketTree::BucketTree_node_t::getMinDepthHelper(double maxCost, int currDepth,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    if(isnan(maxCost) || maxCost < 0)
    {
        return -1;
    }else if (bucket == nullptr){
        return -2;
    }else{
        if(isnan(costCache) || isinf(costCache))
        {
            costCache = bucket->calcMaxCost(groundDistanceCache);
        }
        if(costCache <= maxCost){
            return currDepth;
        }else if(left != nullptr && right != nullptr){
            int leftDepth = left->getMinDepthHelper(maxCost,currDepth +1,groundDistanceCache);
            if(leftDepth < 0)
            {
                return leftDepth;

            }//Calc right only if left is OK

            int rightDepth = right->getMinDepthHelper(maxCost,currDepth +1,groundDistanceCache);
            if(rightDepth < 0)
            {
                return rightDepth;
            }
            //both branches OK
            return std::max(leftDepth,rightDepth);

        }else{
            return -3;
        }

    }
}

Distribution BucketTree::BucketTree_node_t::genDistribHelper(unsigned int level)
{
    if(level > 0 && (left != nullptr && right != nullptr))
    {
        return (left->genDistribHelper(level -1) + right->genDistribHelper(level - 1));
    }else{//level == 0 or any child nullptr (should only happen if both are nullptr, i.e. leaf reached)
        QHash<DataContainer*,unsigned int> distribVals;
        distribVals.insert(bucket->getRefKey(),bucket->getFullCount());
        Distribution* distr = bucket->getDistribution();
        return  Distribution(distr->getType(),distr->isInterpolateable(),distribVals,distr->getDistMetric(),distr->getV());

    }

}

double BucketTree::BucketTree_node_t::getMaxCost(unsigned int depth)
{
    if(depth > 0 && left != nullptr && right != nullptr)
    {
        return std::max(left->getMaxCost(depth -1),right->getMaxCost(depth -1));
    }else if (depth == 0){
        if(isnan(costCache))
        {
            qWarning() << "Calculating bucket-max-cost without cache! This should happen very rarely, otherwise there might be an error.";
            costCache = bucket->calcMaxCost();//FIXME inefficient, as not chached, but should happen rarely
        }
        return costCache;
    }else{
        return nan("");
    }

}

void BucketTree::BucketTree_node_t::reset()
{
    this->bucket->reset();
    if(this->left != nullptr)
    {
        this->left->reset();
    }

    if(this->right != nullptr)
    {
        this->right->reset();
    }
}
double BucketTree::splitNode(BucketTree_node_t *node,double minBucketFactor, bool overwriteChildren,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    if(node != nullptr && node->bucket != nullptr)
    {
        if(overwriteChildren == true || (node->left == nullptr  && node->right == nullptr))
        {
            Bucket* currBucket = node->bucket;
            //create empty bucket &  copy of current bucket
            Bucket* bestLeftBucket = nullptr;
            Bucket* leftBucket = new Bucket(currBucket,false);//empty left
            Bucket* bestRightBucket = nullptr;
            Bucket* rightBucket = new Bucket(currBucket,true);//full right
            double bestLeftCost = nan("");
            double bestRightCost = nan("");
            double bestSumCost = currBucket->calcMaxCost(groundDistanceCache);
            for(int i = 0;i< currBucket->size() -1 ; i++)//try every position for split FIXME can this be done better?
            {

                QApplication::processEvents(QEventLoop::AllEvents,1);
                DataContainer* currKey = currBucket->at(i);
                bool err = leftBucket->add(currKey,groundDistanceCache);
                err |= rightBucket->remove(currKey);
                if(err == false)
                {
                    qCritical() << ("Error in bucketization, add/rm failed, maybe one bucket is already empty!");
                    delete leftBucket;
                    delete rightBucket;
                    return nan("");
                }else{
                    double lCost = leftBucket->calcMaxCost(groundDistanceCache);
                    double rCost = rightBucket->calcMaxCost(groundDistanceCache);
                    if( isnan(lCost) || isnan(rCost) || bestSumCost >  (lCost + rCost) || bestLeftBucket == nullptr || bestRightBucket == nullptr )//Split is better
                    {
                        bestLeftCost = lCost;
                        bestRightCost = rCost;
                        bestSumCost = (lCost + rCost);
                        bestLeftBucket  = leftBucket;
                        bestRightBucket = rightBucket;
                    }else{
                        qInfo() << "Cost to high for split";
                        rightBucket->add(currKey,groundDistanceCache);
                        leftBucket->remove(currKey);
                    }
                }
            }


            if(bestLeftBucket == nullptr || bestRightBucket == nullptr )
            {
                delete leftBucket;
                delete rightBucket;
                return nan("");
            }
            if(!isnan(bestLeftCost) && !isnan(bestRightCost))//a split was performed
            {
                double sizePercentage;
                int size1 = leftBucket->size();
                int size2 = rightBucket->size();
                if(size1 >= size2)
                {
                    sizePercentage = (double)size2/(double)size1;
                }else{

                    sizePercentage = (double)size1/(double)size2;
                }
                Bucket* largeBucket = rightBucket;
                Bucket* smallBucket = leftBucket;
                if(leftBucket->size() >= rightBucket->size())
                {
                    largeBucket = leftBucket;
                    smallBucket = rightBucket;
                }
                while(sizePercentage < minBucketFactor)
                {

                    double bestCost = nan("");
                    DataContainer* bestCont = nullptr;
                    for(DataContainer* cont : largeBucket->getData().keys())//TODO improve, save costs for outer loop
                    {
                        smallBucket->add(cont,groundDistanceCache);
                        double cost = smallBucket->calcMaxCost(groundDistanceCache);
                        if(isnan(bestCost) || cost < bestCost)
                        {
                            bestCost = cost;
                            bestCont = cont;
                        }
                        smallBucket->remove(cont);//TODO better possibility then add&remove?
                    }
                    if(bestCont != nullptr)
                    {
                        smallBucket->add(bestCont);
                        largeBucket->remove(bestCont);
                    }else{
                        qWarning() << "No improvment for bucket percentage found! This might cause issues!";
                        break;
                    }

                    size1 = largeBucket->size();
                    size2 = smallBucket->size();
                    sizePercentage = (double)size2/(double)size1;
                }
                BucketTree_node_t* leftNode = new BucketTree_node_t;
                BucketTree_node_t* rightNode = new BucketTree_node_t;

                leftNode->bucket = bestLeftBucket;
                leftNode->costCache = bestLeftCost;
                leftNode->parent = node;

                rightNode->bucket = bestRightBucket;
                rightNode->costCache = bestRightCost;
                rightNode->parent = node;

                node->left = leftNode;
                node->right = rightNode;

                return bestSumCost;
            }else{
                qInfo() << "No split performed";
                node->costCache = bestSumCost;
                return - bestSumCost;
            }
        }else{
            return nan("");
        }
    }else{
        return nan("");
    }
}


bool BucketTree::constraintHolds(QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    return root->constraintHolds(maxCost,groundDistanceCache);
}

bool BucketTree::constraintHoldsAlways(QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    return root->constraintHoldsAlways(maxCost,groundDistanceCache);
}

bool BucketTree::insert(Bucket* bucket,BucketTree_node_t *parent)
{
    if(bucket == nullptr)
    {
        return false;
    }
    BucketTree_node_t *p=new BucketTree_node_t;
    p->bucket = bucket;
    p->left = nullptr;
    p->right = nullptr;
    if(isEmpty())
    {
        root = p;
        return true;
    }else{
        if(parent->left == nullptr)
        {
            parent->left = p;
            return true;
        }else if(parent-> right == nullptr)
        {
            parent->right = p;
            return true;
        }else{
            delete p;
            return false;
        }
    }
}

double BucketTree::splitUntil(BucketTree_node_t *node, double costThres,double minBucketFactor, QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    if(costThres <= 0)
    {
        return nan("");
    }

    QApplication::processEvents(QEventLoop::AllEvents,5);
    double currCost = splitNode(node,minBucketFactor,true,groundDistanceCache);
    if(isnan(currCost))
    {
        return node->costCache;
    }else{
        if(currCost > costThres)
        {
            if(node->left != nullptr && node->right != nullptr)
            {
                double leftCost = splitUntil(node->left,costThres,minBucketFactor,groundDistanceCache);
                double rightCost = splitUntil(node->right,costThres,minBucketFactor,groundDistanceCache);
                if(isnan(leftCost))
                {
                    qInfo() << "Leftcost was NaN in buckettree";
                    leftCost = node->left->costCache;
                }if(isnan(rightCost))
                {
                    qInfo() << "Rightcost was NaN in buckettree";
                    rightCost = node->right->costCache;
                }
                if(leftCost < 0 || rightCost < 0)//negative propagates!
                {
                    return (-1)*std::max(abs(leftCost),abs(rightCost));
                }else{
                    return std::max(leftCost, rightCost);
                }
            }else{
                qCritical() << "Nullptr child in split, where it was not expected!";
                return nan("");
            }
        }else if(!isnan(currCost)){
            if(currCost < 0)
            {
                qInfo() << "No split was the best split with cost "  << currCost;
            }
            if(abs(currCost) < costThres)
            {
                qDebug() << "Split thres reached in buckettree";
            }else{
                qDebug() << "Split thres could not be reached in buckettree!";
            }
            return currCost;
        }else{//error happened
            return nan("");
        }
    }
}

double BucketTree::splitDepth(BucketTree::BucketTree_node_t *node,unsigned int depth,double minBucketFactor, QHash<DataContainer *, QHash<DataContainer *, double> *> *groundDistanceCache)
{
    if(node != nullptr)
    {
        double currCost;
        if(depth >= 1 && (node->left == nullptr || node->right == nullptr))
        {
            if(node->bucket->size() <= 1)
            {
                qWarning() << "Buckettree could not be made equally deep!";
                return (-1)*node->costCache;
            }
            currCost = splitNode(node,minBucketFactor,true,groundDistanceCache);
        }else{
            currCost = node->costCache;
        }
        if(isnan(currCost))
        {
            return nan("");
        }else if(currCost <= 0)
        {
            qInfo() << "No split was the best split with cost "  << currCost;
            return currCost;
        }else{//valid split performed
            if(depth >= 1)
            {
                if(node->left != nullptr && node->right != nullptr)
                {
                    double leftCost = splitDepth(node->left,depth -1,minBucketFactor,groundDistanceCache);
                    double rightCost = splitDepth(node->right,depth -1,minBucketFactor,groundDistanceCache);
                    if(isnan(leftCost) || isnan(rightCost))
                    {
                        return nan("");
                    }else if ( leftCost < 0 || rightCost < 0)
                    {
                        return (-1)*std::max((abs(leftCost)),abs(rightCost));
                    }else{
                        return std::max(leftCost,rightCost);
                    }
                }else{
                    qCritical() << "Nullptr child in split, where it was not expected!";
                    return nan("");
                }
            }else{
                return currCost;
            }
        }
    }else{
        return nan("");
    }
}

unsigned int BucketTree::getMaxDepth(BucketTree::BucketTree_node_t *node)
{
    if(node == nullptr || (node->left == nullptr && node->right == nullptr))
    {
        return 0;
    }else if(node->left != nullptr && node->right == nullptr)
    {
        return 1 + getMaxDepth(node->left);
    }else if(node->right != nullptr && node->left == nullptr)
    {
        return 1 + getMaxDepth(node->right);
    }else{
        unsigned int leftDepth = getMaxDepth(node->left);
        unsigned int rightDepth = getMaxDepth(node->right);
        return 1 + std::max(leftDepth,rightDepth);
    }
}

unsigned int BucketTree::getMaxDepth()
{
    return BucketTree::getMaxDepth(this->root);
}

unsigned int BucketTree::getMinDepth(BucketTree::BucketTree_node_t *node)
{
    if(node == nullptr || (node->left == nullptr && node->right == nullptr))
    {
        return 0;
    }else if(node->left != nullptr && node->right == nullptr)
    {
        return 1 + getMinDepth(node->left);
    }else if(node->right != nullptr && node->left == nullptr)
    {
        return 1 + getMinDepth(node->right);
    }else{
        unsigned int leftDepth = getMinDepth(node->left);
        unsigned int rightDepth = getMinDepth(node->right);
        return 1 + std::min(leftDepth,rightDepth);
    }
}

unsigned int BucketTree::getMinDepth()
{
    return BucketTree::getMinDepth(this->root);
}
BucketTree *BucketTree::genTree(QList<DataContainer *> data, Distribution* distr,double maxCost, DataDistanceMetric* metric,double minBucketFactor)
{
    if(!isnan(maxCost) && !data.isEmpty() && distr != nullptr && metric != nullptr)
    {
        QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = new QHash<DataContainer*,QHash<DataContainer*,double>*>();
        Bucket* rootBucket = new Bucket(INFINITY,metric,distr);//create bucket with all data in it, no matter how bad
        for(DataContainer* cont : data)
        {
            if(rootBucket->add(cont,groundDistanceCache,true) == false){
                //FIXME can no longer happen remove error
                qCritical() << QString("Data could not be added to root-bucket, the maxCost (%1) was to low. Try again.").arg(maxCost);
                QString msg = QString("Die Daten konnten nicht in den Wurzeleimer eingefügt werden, die eingestellten maximalen Kosten (%1) waren zu niedrig. Versuchen Sie es erneut").arg(maxCost);
                QMessageBox bx;
                bx.setText(msg);
                bx.exec();
                return nullptr;
            }
        }
        BucketTree::BucketTree_node_t* root = new BucketTree::BucketTree_node_t();
        root->bucket = rootBucket;
        root->costCache = root->bucket->calcMaxCost(groundDistanceCache);
        double costThres = BucketTree::splitUntil(root,maxCost,minBucketFactor,groundDistanceCache);
        if(!isnan(costThres))
        {
            unsigned int maxDepth = BucketTree::getMaxDepth(root);
            unsigned int minDepth = BucketTree::getMinDepth(root);
            if(maxDepth != minDepth)
            {
                double maxReachedCost = BucketTree::splitDepth(root,maxDepth,minBucketFactor,groundDistanceCache);
                if(maxReachedCost < 0)
                {
                    qWarning() << "Could not split to the same cost level on all branches!";
                }
                if(isnan(maxReachedCost) || abs(maxReachedCost) > maxCost)
                {

                    root->deleteTree(true);
                    delete root;
                    return nullptr;
                }else{
                    return new BucketTree(root,maxCost);
                }
            }else{

                return new BucketTree(root,maxCost);
            }
        }else{
            root->deleteTree(true);
            delete root;
            return nullptr;
        }

    }else{
        return nullptr;
    }
}
