#ifndef EXTENDEDGENERALLOSSMETRIC_H
#define EXTENDEDGENERALLOSSMETRIC_H

#include <QDebug>
#include <cmath>
#include "informationlossmetric.h"
#include "wave.h"
#define HOLD_VAL_LOSS true // if this is true the value is not interpolated beyond the start/end, but held


class ExtendedGeneralLossMetric : public InformationlossMetric
{
public:
    ExtendedGeneralLossMetric();

    /**
     * @brief calcLoss calculates the informationloss in the eqc, based on its hull
     * @param eqcHull
     * @param eqcMinVal the minimal (smallest) value in the EQC. Should not be NaN, must be smaller then eqcMaxVal
     * @param  eqcMaxVal the maximal (largest) value in the EQC. Should not be NaN, must be larger then eqcMinVal
     * @param  resolution how the hull is sampled. In ms. if it is 0 the data points of the hull are the only measurements
     * @return 0 if the hull is empty, NaN if an error occured
     */
    InformationLoss_t calcLoss(QList<QPair<DataPoint *, DataPoint *> *> eqcHull,double eqcMinVal,double eqcMaxVal,unsigned int resolution = 0) override;

    /**
     * @brief calcLoss calculates the informationloss eqc,based on its hull, with the additional container
     * Iterates the whole time, calculating min-Max (i.e. hull) comparing it with the container. Skipping invalid datapoints. NOT DELTA TIME SENSITVE!
     * @param eqcHull
     * @param eqcMinVal the minimal (smallest) value in the EQC. Should not be NaN, must be smaller then eqcMaxVal
     * @param  eqcMaxVal the maximal (largest) value in the EQC. Should not be NaN, must be larger then eqcMinVal
     * @param add
     * @param  resolution how the hull is sampled. In ms. if it is 0 the data points of the hull are the only measurements
     * @return 0 if the hull is empty, NaN if an error occured
     */
    InformationLoss_t calcLoss(QList<QPair<DataPoint *, DataPoint *> *> eqcHull,double eqcMinVal,double eqcMaxVal,DataContainer add,unsigned int resolution = 0) override;

private:

    /**
     * @brief lossHelper calculates the local information loss. All parameters MUST be valid (i.e. not null or NaN ) this is not checked!!!
     * @param p the current datapoints (low & up )
     * @param eqcMinVal the minimal (smallest) value in the EQC. Should not be NaN, must be smaller then eqcMaxVal
     * @param  eqcMaxVal the maximal (largest) value in the EQC. Should not be NaN, must be larger then eqcMinVal
     * @param ret the informationloss struct
     * @param ptCount number of points iterated
     */
    void lossHelper(QPair<DataPoint*,DataPoint*>* p,double eqcMaxVal,double eqcMinVal, InformationLoss_t* ret,unsigned int* ptCount);
    /**
     * @brief lossHelper calculates the local information loss , with the additional container . All parameters MUST be valid (i.e. not null or NaN ) this is not checked!!!
     * @param p the current datapoints (low & up )
     * @param eqcMinVal the minimal (smallest) value in the EQC. Should not be NaN, must be smaller then eqcMaxVal
     * @param  eqcMaxVal the maximal (largest) value in the EQC. Should not be NaN, must be larger then eqcMinVal
     * @param ret the informationloss struct
     * @param ptCount number of points iterated
     */
    void lossHelper(QPair<DataPoint*,DataPoint*>* p,double eqcMaxVal,double eqcMinVal, InformationLoss_t* ret,unsigned int* ptCount,DataContainer add);
};

#endif // EXTENDEDGENERALLOSSMETRIC_H
