#ifndef POLYFIT_H
#define POLYFIT_H

#include <QDebug>

#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <cmath>
#include <iomanip>


#define POLYFIT_MAXIT 100
#define POLYFIT_EPS 3.0e-7
#define POLYFIT_FPMIN 1.0e-30
#define POLYFIT_STOP 1.0e-8
#define POLYFIT_TINY 1.0e-30

using namespace std;

/**
 * @brief The PolyFit class contains all (except display & main) functions for polynomial-fit by NASA
 * Can be usefull for all kind of matrix-calculations
 * Almost no docs available...
 */
class PolyFit{


public:
    static long double incbeta(long double a, long double b, long double x);

    static long double invincbeta(long double y,long double alpha, long double beta);

    static long double CalculateTValueStudent(const long double nu, const long double alpha);

    static long double cdfStudent(const long double nu, const long double t);

    static long double cdfFisher(const long double df1, const long double df2, const long double x);

    static long double **Make2DArray(const size_t rows, const size_t cols);

    static long double **MatTrans(long double **array, const size_t rows, const size_t cols);

    static long double **MatMul(const size_t m1, const size_t m2, const size_t m3, long double **A, long double **B);

    static void MatVectMul(const size_t m1, const size_t m2, long double **A, long double *v, long double *Av);

    static long double determinant(long double **a, const size_t k);

    static void transpose(long double **num, long double **fac, long double **inverse, const size_t r);

    static void cofactor(long double **num, long double **inverse, const size_t f);

    static long double CalculateRSS(const long double *x, const long double *y, const long double *a, long double **Weights,
                               const bool fixed, const size_t N, const size_t n);

    static long double CalculateTSS(const long double *x, const long double *y, const long double *a, long double **Weights,
                               const bool fixed, const size_t N, const size_t n);

    static long double CalculateR2COD(const long double *x, const long double *y, const long double *a, long double **Weights,
                                 const bool fixed, const size_t N, const size_t n);

    static long double CalculateR2Adj(const long double *x, const long double *y, const long double *a, long double **Weights,
                                 const bool fixed,const size_t N, const size_t n);

    static void fit(const long double *x, long double *y, const size_t n, const size_t k, const bool fixedinter,
                        const long double fixedinterval, long double *beta, long double **Weights, long double **XTWXInv);

    static long double calculatePoly(const long double x, const long double *a, const size_t n);

    static void WriteCIBands(const long double *x, const long double *coefbeta, long double **XTXInv,
                             const long double tstudentval, const long double SE, const size_t n, const size_t k,long double* x1,long double* y0, long double* y1,long double* y2,long double* y3, long double* y4);

    static void CalculateWeights(const long double *erry, long double **Weights, const size_t n,
                                 const int type);

    static void CalculateSERRBeta(const bool fixedinter, const long double SE, size_t k, long double *serbeta, long double **XTWXInv);
private:
    PolyFit();
};

#endif // POLYFIT_H
