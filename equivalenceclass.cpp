#include "equivalenceclass.h"



InformationlossMetric* EquivalenceClass::getInfoMetric() const
{
    return infoMetric;
}


unsigned int EquivalenceClass::getTargetSize() const
{
    return targetSize;
}

void EquivalenceClass::setTargetSize(unsigned int value)
{
    targetSize = value;
}


EquivalenceClass::EquivalenceClass(unsigned int k, double t, double h,Distribution *popDistr, DataDistanceMetric *distM,DistributionDistanceMetric* distrDistM, InformationlossMetric *infoM, unsigned int targetSize,double maxValue,double minValue,int infoLossRes)
{
    this->hull_stepsize = h;
    this->kMin = k;
    this->tMax = t;
    this->distrPopulation = popDistr;
    this->distMetric  = distM;
    this->distribDistMetric = distrDistM;
    this->infoMetric = infoM;
    this->targetSize = targetSize;
    this->maxValue = maxValue;
    this->minValue = minValue;
    this->infoLossRes = infoLossRes;
    this->distrEqc = new Distribution(*popDistr,false);

}

EquivalenceClass::EquivalenceClass(EquivalenceClass *copy)
{

    this->hull_stepsize = copy->getHull_stepsize();
    this->kMin = copy->getKMin();
    this->kReal = copy->getKReal();
    this->tMax = copy->getTMax();
    this->tReal = copy->getTReal();
    this->distrPopulation = new Distribution(*copy->getDistrPopulation(),true);
    this->distrEqc = new Distribution(*copy->getDistrEqc(),true);
    this->distMetric  = copy->getDistMetric();
    this->distribDistMetric = copy->getDistribDistMetric();
    this->infoMetric = copy->getInfoMetric();
    this->targetSize = copy->getTargetSize();
    this->maxValue = copy->getMaxValue();
    this->minValue = copy->getMinValue();
    this->startTime = copy->getStartTime();
    this->endTime = copy->getEndTime();
    this->hulldata = copy->getHull();
    this->droppedData = copy->getDroppedData();
    this->rawData = copy->getRawData();
    this->infoLossRes = copy->getInfoLossRes();
}

double EquivalenceClass::calcMRE()
{
    if(hasHull() == true)
    {
        QList<QPair<DataPoint *, DataPoint *> *> hull = getHull();
        QList<DataContainer *> rawData = getRawData();
        if(hull.isEmpty() || rawData.isEmpty())
        {
            return nan("");
        }else{

            QList<double> error;//FIXME improvment return map, allowing better eval?
            //calc error
            for(QPair<DataPoint *, DataPoint *> *pair : hull)
            {
                DataPoint* low = pair->first;
                DataPoint* high = pair->second;
                double eqcErr = 0;
                int eqcErrCnt = 0;
                for(DataContainer* cont: rawData)
                {

                    double val = cont->valueAt(low->getTime());
                    double diff = min(abs(low->getValue() - val),abs(high->getValue()-val));
                    eqcErr += diff;
                    eqcErrCnt ++;
                }
                eqcErr /= (double)eqcErrCnt;
                error.append(eqcErr);
            }

            std::sort(error.begin(), error.end());
            double median = error.size()%2
                ? error[error.size() / 2]
                : ((double)error[error.size() / 2 - 1] + error[error.size() / 2]) * .5;
            return median;
        }

    }else{
        return nan("");
    }
}

QList<QPair<DataPoint *, DataPoint *> *> EquivalenceClass::getHull() const
{
    return hulldata;
}

bool EquivalenceClass::takeOneConsiderateHelper(Bucket *bucket, bool overwriteRevert)
{
    if(bucket == nullptr || bucket->size() <= 0)
    {
        return false;
    }else{
        QPair<DataContainer *, InformationlossMetric::InformationLoss_t> *best = getSimilar(bucket);
        if(best == nullptr)
        {
            return  false;
        }else{
            add(best->first);
            if(overwriteRevert == true)
            {
                bucket->resetLastTaken();
            }
            bucket->take(best->first);
            return true;
        }
    }
}

bool EquivalenceClass::takeOneGreedyHelper(Bucket *bucket, bool overwriteRevert)
{
    if(bucket == nullptr || bucket->size() <= 0)
    {
        return false;
    }else{
        QPair<DataContainer *, InformationlossMetric::InformationLoss_t> *best = getBest(bucket);
        if(best == nullptr)
        {
            return  false;
        }else{
            add(best->first);
            if(overwriteRevert == true)
            {
                bucket->resetLastTaken();
            }
            bucket->take(best->first);
            return true;
        }
    }
}

QPair<DataContainer *, InformationlossMetric::InformationLoss_t> *EquivalenceClass::getBest(Bucket *bucket)
{
    QList<DataContainer *> containers = bucket->getData().keys();
    DataContainer* bestCont = nullptr;
    InformationlossMetric::InformationLoss_t bestLoss = {nan(""),nan(""),nan("")};
    for(DataContainer* cont : containers)
    {
        if(!this->hasHull())
        {
            this->calcHull(getHull_stepsize());//FIXME IMPROVMENT CACHE UNTIL CHANGE
        }
        InformationlossMetric::InformationLoss_t currLoss = getInfoMetric()->calcLoss(this->getHull(),this->getMinValue(),this->getMaxValue(),*cont,getInfoLossRes());
        if(isnan(bestLoss.averageLoss) || currLoss.averageLoss < bestLoss.averageLoss)
        {
            bestCont = cont;
            bestLoss = currLoss;
        }
    }

    if(bestCont == nullptr)
    {
        return nullptr;
    }else{
        return new QPair<DataContainer*,InformationlossMetric::InformationLoss_t>(bestCont,bestLoss);
    }
}

QPair<DataContainer *, InformationlossMetric::InformationLoss_t> *EquivalenceClass::getSimilar(Bucket *bucket)
{
    QList<DataContainer *> containers = bucket->getData().keys();
    DataContainer* similarCont = nullptr;
    InformationlossMetric::InformationLoss_t similarLoss = {nan(""),nan(""),nan("")};
    DataContainer* bestCont = nullptr;
    InformationlossMetric::InformationLoss_t bestLoss = {nan(""),nan(""),nan("")};

    if(!this->hasHull())
    {
        this->calcHull(getHull_stepsize());//FIXME IMPROVMENT CACHE UNTIL CHANGE
    }
    InformationlossMetric::InformationLoss_t eqcLoss = getInfoMetric()->calcLoss(this->getHull(),this->getMinValue(),this->getMaxValue(),getInfoLossRes());
    for(DataContainer* cont : containers)
    {
        InformationlossMetric::InformationLoss_t currLoss = getInfoMetric()->calcLoss(this->getHull(),this->getMinValue(),this->getMaxValue(),*cont,getInfoLossRes());
        if(isnan(bestLoss.averageLoss) || currLoss.averageLoss < bestLoss.averageLoss)//backup strategy if no improvement is possible
        {
            bestCont = cont;
            bestLoss = currLoss;
        }
        if(isnan(similarLoss.averageLoss) || (currLoss.averageLoss < similarLoss.averageLoss && currLoss.averageLoss <= eqcLoss.averageLoss))
        {
            similarCont = cont;
            similarLoss = currLoss;
        }
    }

    if(similarCont == nullptr && bestCont == nullptr)
    {
        return nullptr;
    }else if (similarCont == nullptr && bestCont != nullptr){
        return new QPair<DataContainer*,InformationlossMetric::InformationLoss_t>(bestCont,bestLoss);
    }else{
        return new QPair<DataContainer*,InformationlossMetric::InformationLoss_t>(similarCont,similarLoss);
    }
}

double EquivalenceClass::getMinValue() const
{
    return minValue;
}

double EquivalenceClass::getMaxValue() const
{
    return maxValue;
}



QList<DataContainer *> EquivalenceClass::getDroppedData() const
{
    return droppedData;
}

void EquivalenceClass::setInfoLossMetric(InformationlossMetric *value)
{
    if(value != nullptr)
    {
        infoMetric = value;
    }
}

QList<DataContainer *> EquivalenceClass::getRawData() const
{
    return rawData;
}

bool EquivalenceClass::removeDroppedContainer(DataContainer *dat)
{
    if(droppedData.contains(dat))
    {
        droppedData.removeAll(dat);
        return true;
    }else{
        return false;
    }
}

Distribution *EquivalenceClass::getDistrEqc() const
{
    return distrEqc;
}

Distribution *EquivalenceClass::getDistrPopulation() const
{
    return distrPopulation;
}

double EquivalenceClass::getTReal() const
{
    return tReal;
}

double EquivalenceClass::getTMax() const
{
    return tMax;
}


unsigned int EquivalenceClass::getKReal() const
{
    return kReal;
}

unsigned int EquivalenceClass::getKMin() const
{
    return kMin;
}

int EquivalenceClass::takeGreedy(Bucket *bucket,double maxInfoLoss,double d )
{
    if(bucket == nullptr || isnan(maxInfoLoss) || maxInfoLoss <= 0)
    {
        return -1;
    }else{
        double exactTakeSize =  (getTargetSize() * bucket->getDistribution()->getProp(bucket->getRefKey()));

        bucket->resetLastTaken();
        int counter = 0;
        EquivalenceClass* tmp = new EquivalenceClass(*this);
        QPair<DataContainer *, InformationlossMetric::InformationLoss_t> *best = nullptr;
        do{

            if(best != nullptr)
            {
                *this = *tmp;
                counter++;
            }
            bool drop = false;
            tmp->calcHull();
            best = getBest(bucket);//FIXME work here fix NaN as loss
            if(best != nullptr && !isinf(d) && !isnan(d))
            {
                drop = best->second.maxLoss >= d;
            }
            if(best != nullptr)
            {
                bucket->take(best->first);
                tmp->add(best->first,drop);
            }


        }while(best != nullptr && best->second.maxLoss < maxInfoLoss  && !(bucket->isEmpty()) && tmp->kReal  < exactTakeSize);
        //FIXME CHECK PROPORTIONALLITY / same in takeSimilar

        if(best == nullptr || best->second.maxLoss < maxInfoLoss)//all distributed, add last candidate
        {

            *this = *tmp;
            delete tmp;
            if(best != nullptr)
            {
                counter++;
            }
        }else{
            delete tmp;
        }
        calcSpecs();
        return counter;
    }
}



int EquivalenceClass::takeConsiderate(Bucket *bucket,double maxInfoLoss,double d )
{
    if(bucket == nullptr || isnan(maxInfoLoss) || maxInfoLoss <= 0)
    {
        return -1;
    }else{
        double exactTakeSize =  (getTargetSize() * bucket->getDistribution()->getProp(bucket->getRefKey()));

        bucket->resetLastTaken();
        int counter = 0;
        EquivalenceClass* tmp = new EquivalenceClass(*this);
        QPair<DataContainer *, InformationlossMetric::InformationLoss_t> *similar = nullptr;
        do{
            if(similar != nullptr)
            {
                *this = *tmp;
                counter++;
            }
            bool drop = false;

            tmp->calcHull();
            similar = getSimilar(bucket);
            if(similar != nullptr && !isinf(d) && !isnan(d))
            {
                drop = similar->second.maxLoss >= d;//FIXME max or average?
            }
            if(similar != nullptr)
            {
                bucket->take(similar->first);
                tmp->add(similar->first,drop);
            }

        }while(similar != nullptr && similar->second.maxLoss < maxInfoLoss  && !(bucket->isEmpty()) && tmp->kReal  < exactTakeSize);

        if(similar == nullptr || similar->second.maxLoss < maxInfoLoss)//all distributed, add last candidate
        {

            *this = *tmp;
            delete tmp;
            counter++;
        }else{
            delete tmp;
        }
        calcSpecs();
        return counter;
    }
}

int EquivalenceClass::takeGreedy(Bucket *bucket)
{
    double exactTakeSize =  (getTargetSize() * bucket->getDistribution()->getProp(bucket->getRefKey()));
    int taken = 0;

    bucket->resetLastTaken();
    do{
        if(takeOneGreedyHelper(bucket,false) == true)
        {
            taken++;
        }else{
            calcSpecs();
            return taken;
        }
    }while(taken < exactTakeSize);

    calcSpecs();
    return taken;
}

bool EquivalenceClass::takeOneGreedy(Bucket *bucket)
{
    return takeOneGreedyHelper(bucket);
}

bool EquivalenceClass::takeOneConsiderate(Bucket *bucket)
{
    return takeOneConsiderateHelper(bucket);
}

int EquivalenceClass::takeConsiderate(Bucket *bucket)
{

    double exactTakeSize =  (getTargetSize() * bucket->getDistribution()->getProp(bucket->getRefKey()));
    int taken = 0;

    bucket->resetLastTaken();
    do{
        if(takeOneConsiderateHelper(bucket,false) == true)
        {
            taken++;
        }else{

            calcSpecs();
            return taken;
        }
    }while(taken < exactTakeSize);

    calcSpecs();
    return taken;
}

EquivalenceClass *EquivalenceClass::merge(EquivalenceClass *eqc1, EquivalenceClass *eqc2)
{
    int kMinEqc3 = std::max(eqc1->getKMin(), eqc2->getKMin());
    int kRealEqc3 = eqc1->getKReal() + eqc2->kReal;
    double tMaxEqc3 = std::min(eqc1->getTMax(),eqc2->getTMax());

    Distribution *eqc3Distr = Distribution::merge(eqc1->getDistrEqc(),eqc2->getDistrEqc());
    Distribution *popDistr;
    if(*eqc1->getDistrPopulation() == *eqc2->getDistrPopulation())
    {
        popDistr = eqc1->getDistrPopulation();
    }else{
        popDistr = Distribution::merge(eqc1->getDistrPopulation(),eqc2->getDistrPopulation());
    }
    if(eqc3Distr == nullptr)
    {
        return nullptr;
    }else{
        double tRealEqc3 = eqc1->getDistribDistMetric()->distance(eqc3Distr,popDistr,DISTR_IGNORE_NON_EXIST);
        if(tRealEqc3 <= tMaxEqc3 && kRealEqc3 <= kMinEqc3)
        {
            double minVal = min(eqc1->getMinValue(),eqc2->getMinValue());
            double maxVal = max(eqc1->getMaxValue(),eqc2->getMaxValue());
            double eqc3HullStepS = min(eqc1->getHull_stepsize(),eqc2->getHull_stepsize());
            int minILR = min(eqc1->getInfoLossRes(),eqc2->getInfoLossRes());
            EquivalenceClass* eqc3 = new EquivalenceClass(kMinEqc3,kMinEqc3,eqc3HullStepS,popDistr,eqc1->getDistMetric(),eqc1->getDistribDistMetric(),eqc1->infoMetric,eqc1->getTargetSize() + eqc2->targetSize,maxVal,minVal,minILR);
            eqc3->add(eqc1->getRawData());
            eqc3->add(eqc2->getRawData());
            eqc3->add(eqc1->getDroppedData());
            eqc3->add(eqc2->getDroppedData());
            return eqc3;

        }else{
            return  nullptr;
        }
    }
}


int EquivalenceClass::takeGreedy(EquivalenceClass *eqc,double maxInfoLoss,double d )
{
    if(eqc == nullptr || isnan(maxInfoLoss) || maxInfoLoss <= 0)
    {
        return -1;
    }else{
        bool drop = false;
        int counter = 0;
        EquivalenceClass* tmp = new EquivalenceClass(*eqc);
        QPair<DataContainer *, InformationlossMetric::InformationLoss_t> *best = nullptr;
        do{
            if(best != nullptr)
            {
                *eqc = *tmp;
                counter++;
            }
            best = getBest(eqc);

            drop = false;
            if(best != nullptr && !isinf(d) && !isnan(d))
            {
                drop = best->second.averageLoss >= d;
            }
            if(best != nullptr)
            {
                tmp->add(best->first,drop);
                this->remove(best->first);
            }
        }while(best != nullptr && best->second.averageLoss < maxInfoLoss && tmp->fullFillsConstraints() && !(rawData.isEmpty()));

        if( (best == nullptr && tmp->fullFillsConstraints()) ||  ( best != nullptr && (best->second.averageLoss < maxInfoLoss   || drop )))//all distributed, add last candidate
        {

            *eqc = *tmp;
            delete tmp;
            counter++;
        }else{
            delete tmp;
        }
        calcSpecs();
        return counter;
    }
}


QPair<DataContainer*,InformationlossMetric::InformationLoss_t>* EquivalenceClass::getBest(EquivalenceClass *eqc)
{
    DataContainer* bestCont = nullptr;
    InformationlossMetric::InformationLoss_t bestLoss = {nan(""),nan(""),nan("")};

    if(!this->hasHull())
    {
        this->calcHull(getHull_stepsize());//FIXME IMPROVMENT CACHE UNTIL CHANGE
    }
    for(DataContainer* cont : this->getRawData())
    {
        InformationlossMetric::InformationLoss_t currLoss = getInfoMetric()->calcLoss(eqc->getHull(),eqc->getMinValue(),eqc->getMaxValue(),*cont,getInfoLossRes());
        if(isnan(bestLoss.averageLoss) || currLoss.averageLoss < bestLoss.averageLoss)
        {
            bestCont = cont;
            bestLoss = currLoss;
        }
    }

    if(bestCont == nullptr)
    {
        return nullptr;
    }else{
        return new QPair<DataContainer*,InformationlossMetric::InformationLoss_t>(bestCont,bestLoss);
    }

}

int EquivalenceClass::takeConsiderate(EquivalenceClass *eqc,double maxInfoLoss,double d)
{
    if(eqc == nullptr || isnan(maxInfoLoss) || maxInfoLoss <= 0)
    {
        return -1;
    }else{
        bool drop = false;
        int counter = 0;
        EquivalenceClass* tmp = new EquivalenceClass(*eqc);
        QPair<DataContainer *, InformationlossMetric::InformationLoss_t> *similar = nullptr;;
        do{
            if(similar != nullptr)
            {
                *eqc = *tmp;
                counter++;
            }
            similar = getSimilar(eqc);
            drop = false;
            if(similar != nullptr && !isinf(d) && !isnan(d))
            {
                drop = similar->second.averageLoss >= d;
            }
            if(similar != nullptr)
            {
                tmp->add(similar->first,drop);
                this->remove(similar->first);
            }
        }while(similar != nullptr && similar->second.averageLoss < maxInfoLoss && tmp->fullFillsConstraints() && !(rawData.isEmpty()));

        if( (similar == nullptr && tmp->fullFillsConstraints()) ||  ( similar != nullptr && ( similar->second.averageLoss < maxInfoLoss  || drop )))//all distributed, add last candidate
        {

            *eqc = *tmp;
            delete tmp;
            counter++;
        }else{
            delete tmp;
        }
        calcSpecs();
        return counter;
    }
}

void EquivalenceClass::addHelper(DataContainer *element, bool p_calcSpecs, bool drop)
{
    if(drop == true)
    {
        droppedData.append(element);
    }else{
        if(!startTime.isValid() || element->getStart().msecsTo(startTime) > 0)
        {
            startTime = element->getStart();
        }else if(!endTime.isValid() || element->getEnd().msecsTo(endTime) < 0)
        {
            endTime = element->getEnd();
        }
        rawData.append(element);
        if(p_calcSpecs == true)
        {
            calcSpecs();
        }
    }
}

int EquivalenceClass::getInfoLossRes() const
{
    return infoLossRes;
}

QDateTime EquivalenceClass::getEndTime() const
{
    return endTime;
}

QDateTime EquivalenceClass::getStartTime() const
{
    return startTime;
}

double EquivalenceClass::getHull_stepsize() const
{
    if(!isnan(hull_stepsize))
    {
        return hull_stepsize;
    }else{
        return DEFAULT_HULL_STEPSIZE_MS;
    }
}

void EquivalenceClass::calcSpecs()
{
    kReal = rawData.size();

    if(distrEqc != nullptr)
    {
        distrEqc->reset();
    }else{
        distrEqc = new Distribution(*distrPopulation,false);
    }

    QDateTime currStart;
    QDateTime currEnd;
    for(DataContainer* cont : rawData)
    {
        DataContainer *refKey = distrPopulation->getClosestKey(cont);
        refKey = refKey == nullptr ? cont : refKey;
        distrEqc->addOccurence(refKey);
        QDateTime start = cont->getStart();
        QDateTime end = cont->getEnd();
        if(!currStart.isValid() || (start.isValid() && currStart.msecsTo(start) < 0))
        {
            currStart = start;
        }

        if(!currEnd.isValid() || (end.isValid() && currEnd.msecsTo(end) > 0))
        {
            currEnd = end;
        }
    }

    this->startTime = currStart;
    this->endTime = currEnd;
    tReal = getDistribDistMetric()->distance(distrEqc,distrPopulation,DISTR_IGNORE_NON_EXIST);
    if(tReal > 1)
    {
        qInfo() << "T Real > 1...";
    }
}

bool EquivalenceClass::calcHull(time_t stepSize)
{
    if(!startTime.isValid() || !endTime.isValid() )
    {
        return false;
    }else{
        hulldata.clear();
        QDateTime currTime = startTime;
        time_t dt = currTime.msecsTo(endTime);
        while(dt >= 0)
        {
            DataPoint* currMin = nullptr;
            DataPoint* currMax= nullptr;
            for(int i = 0; i < rawData.size() ; i++)
            {
                double value = rawData.at(i)->valueAt(currTime,true);
                if(!isnan(value) && (currMin == nullptr || currMin->getValue() >= value ))
                {

                    if(currMin != nullptr)
                    {
                        delete currMin;
                    }
                    currMin = new DataPoint(currTime,value);
                }
                if(!isnan(value) && (currMax == nullptr || currMax->getValue() <= value))
                {

                    if(currMax != nullptr)
                    {

                        delete currMax;
                    }
                    currMax = new DataPoint(currTime,value);
                }

            }
            if(currMin != nullptr && currMax != nullptr && currMin->getValue() != currMax->getValue())
            {
                hulldata.append(new QPair<DataPoint*,DataPoint*>(currMin,currMax));
            }
            currTime = currTime.addMSecs(stepSize);
            dt= currTime.msecsTo(endTime);
        }
        return !hulldata.isEmpty();
    }
}

bool EquivalenceClass::calcHull()
{
    return calcHull(this->getHull_stepsize());
}


QPair<DataContainer *, InformationlossMetric::InformationLoss_t> *EquivalenceClass::getSimilar(EquivalenceClass *eqc)
{
    DataContainer* similarCont = nullptr;
    InformationlossMetric::InformationLoss_t similarLoss = {nan(""),nan(""),nan("")};
    DataContainer* bestCont = nullptr;
    InformationlossMetric::InformationLoss_t bestLoss = {nan(""),nan(""),nan("")};
    if(!this->hasHull())
    {
        this->calcHull(getHull_stepsize());//FIXME IMPROVMENT CACHE UNTIL CHANGE
    }
    InformationlossMetric::InformationLoss_t eqcLoss = getInfoMetric()->calcLoss(this->getHull(),this->getMinValue(),this->getMaxValue(),getInfoLossRes());
    for(DataContainer* cont : this->getRawData())
    {
        InformationlossMetric::InformationLoss_t currLoss = getInfoMetric()->calcLoss(eqc->getHull(),eqc->getMinValue(),eqc->getMaxValue(),*cont,getInfoLossRes());
        if(isnan(bestLoss.averageLoss) || currLoss.averageLoss < bestLoss.averageLoss)//backup strategy if no improvement is possible
        {
            bestCont = cont;
            bestLoss = currLoss;
        }
        if(isnan(similarLoss.averageLoss) || (currLoss.averageLoss < similarLoss.averageLoss && currLoss.averageLoss <= eqcLoss.averageLoss))
        {
            similarCont = cont;
            similarLoss = currLoss;
        }
    }

    if(similarCont == nullptr && bestCont == nullptr)
    {
        return nullptr;
    }else if (similarCont == nullptr && bestCont != nullptr){
        return new QPair<DataContainer*,InformationlossMetric::InformationLoss_t>(bestCont,bestLoss);
    }else{
        return new QPair<DataContainer*,InformationlossMetric::InformationLoss_t>(similarCont,similarLoss);
    }
}
bool EquivalenceClass::isValid()
{
    return (kMin > 1) && (distrPopulation != nullptr && distrPopulation->isValid()) && infoMetric != nullptr && distMetric != nullptr;
}

bool EquivalenceClass::fullFillsConstraints()
{
    return isKAnonymized() && isTClose();
}

bool EquivalenceClass::isKAnonymized()
{
    return (kReal >= kMin) && (kMin > 1);
}

bool EquivalenceClass::isTClose()
{
    return (tReal <= tMax);
}

void EquivalenceClass::add(DataContainer *element,bool drop)
{
    addHelper(element,true,drop);
}

void EquivalenceClass::remove(DataContainer *element)
{
    rawData.removeAll(element);
    droppedData.removeAll(element);
}

void EquivalenceClass::add(QList<DataContainer *> elements, bool drop)
{
    for(DataContainer* elem : elements)
    {
        addHelper(elem, false,drop);
    }
    calcSpecs();
}

bool EquivalenceClass::hasHull()
{
    return !hulldata.isEmpty();
}

DataDistanceMetric *EquivalenceClass::getDistMetric()
{
    return distMetric;
}

void EquivalenceClass::setDistMetric(DataDistanceMetric *value)
{
    if(value != nullptr)
    {
        distMetric = value;
    }
}



DistributionDistanceMetric *EquivalenceClass::getDistribDistMetric() const
{
    return distribDistMetric;
}

void EquivalenceClass::setDistribDistMetric(DistributionDistanceMetric *value)
{
    if(value != nullptr)
    {
        distribDistMetric = value;
    }
}
