#ifndef TANGENT_H
#define TANGENT_H

#include <math.h>

#include "datacontainer.h"
#include "datapoint.h"

/**
 * @brief The Trendline class calculates and stores a trendline for the given wave and base-point
 * Is used to find a good splitting point for the waves.
 */
class Tangent
{
public:
    /**
     * @brief Trendline initalizes all values, if the base or wave is null, the base is not part of the wave, or there is no other datapoint in the wave to calculate the slope, then the trendline is invalid
     * @param base must be part of the wave and valid
     * @param slope can not be nan
     * @param parent
     */
    Tangent(DataPoint* base,double slope);

    QDateTime getStartX();

    double getOffset();
    /**
     * @brief isValid
     * @return
     */
    bool isValid();

    /**
     * @brief valueAt
     * @param param must be valid, otherwise NAN is returned (quite NaN from cMath)
     * @return the value at time param
     */
    double valueAt(QDateTime param);

    /**
     * @brief distance defines a distance between tangents by their  distance of their starting point and distance in the slope.
     * The distance is the euclidean distance.
     * I.e. sqrt(((a.startX - b.startX)^2 + (a.offset - b.offset)^2) + (a.slope -b.slope)^2)
     * @param a must be valid, otherwise NAN is returned (quite NaN from cMath)
     * @param b must be valid, otherwise NAN is returned (quite NaN from cMath)
     * @return
     */
    static double distance(Tangent a, Tangent b);

private:
    double slope = nan("");
    double offset= nan("");
    QDateTime startX;
    DataPoint* base = nullptr;
    bool m_isValid = false;
signals:

};

#endif // TANGENT_H
