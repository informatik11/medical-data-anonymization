#ifndef BUCKETTREE_H
#define BUCKETTREE_H
#include <QString>
#include <QtDebug>
#include <QMessageBox>
#include <QApplication>
#include "bucket.h"

#define DEFAULT_MIN_BUCKET_FACTOR 0.3

/*
* @brief The BucketTree class containing all data of the Bucket class tree. Which is a binary tree.
*/
class BucketTree
{
public:

    ///! Representing a node in the bucketTrree.
    typedef struct BucketTree_node_t{
        ///! Assuming either left AND right are nullptr or none of them
        BucketTree_node_t *left = nullptr;
        ///! Assuming either left AND right are nullptr or none of them
        BucketTree_node_t *right  = nullptr;
        ///! The data that is stored in the node
        Bucket *bucket  = nullptr;

        ///! Nullptr iff this is the root
        BucketTree_node_t* parent = nullptr;

        ///! Used to chache costs. If this is NaN this does NOT mean the cost is NaN, only that is was not yet calculated and set!
        double costCache = nan("");

        /**
         * @brief toString
         * @return
         */
        QString toString();


        /**
         * @brief constraintHolds
         * @param maxCost
         * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
         * @return  true if the given constraint is fullfilled at the current node
         */
        bool constraintHolds(double maxCost,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

        /**
         * @brief constraintHoldsAlways
         * @param maxCost
         * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
         * @return true iff the cost is lower then maxCost the for all nodes. False if the maxcost is invalid
         */
        bool constraintHoldsAlways(double maxCost,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);


        /**
         * @brief getLeafBuckets recurses down from the current node and returns all buckets of the leafs
         * @return list of all buckets of leafs from the current node down.  Is invalid if it contains a nullptr.
         */
        QList<Bucket*> getLeafBuckets();

        /**
         * @brief getLeafNodes recurses down from the current node and returns all leafs
         * @return list of all leafnodes from the current node down.
         */
        QList<BucketTree_node_t*> getLeafNodes();

        /**
         * @brief getNodes returns the nodes at the given depth
         * @param depth
         * @return list of all nodes at the given depth from the current node down. Empty if the depth is deeper then the tree (on any branch) or negative
         */
        QList<BucketTree_node_t*> getNodes(int depth);//TODO use for later ajustments in UI without recalculation


        /**
         * @brief getBuckets recurses down from the root node and returns all buckets at the given depth
         * @param depth
         * @return The list of all buckets of the given depth. Empty if the depth is deeper then the tree (on any branch) or negative. Is invalid if it contains a nullptr.
         */
        QList<Bucket*> getBuckets(int depth);

        /**
         * @brief isLeaf
         * @return
         */
        bool isLeaf()
        {
            return (left == nullptr && right == nullptr);
        }
        /**
         * @brief deleteTree recursively deletes the all children
         * Only the root has to be deleted manually at the end
         * @param delBuckets true if the buckets of the nodes should be deleted
         */
        void deleteTree(bool delBuckets = true);

        /**
         * @brief getDepth
         * @return the depth of the current node. 0 is root.
         */
        int getDepth();

        /**
         * @brief getMinDepth returns the depth where the given constraint is fullfilled.
         * Calculates the cost where it is not chached.
         * @param c must be positive
         * @return the depth where all nodes fullfill the given maxCost constraint, -1 if the maxCost was invalid, -2 if a bucket on the way was nullptr, -3 if the needed depth could not be reached.
         * If multiple error occure the left-most error will propagate.
         */
        int getMinDepth(double c);

        /**
         * @brief clearChache recurses down the tree and sets the costcache to nan.
         * Should be done on larger changes of the bucketization.
         * However the recalculation is expensive, for large depths, so keep the clears as local as possible!
         */
        void clearChache();

        /**
         * @brief genDistribHelper does the recursion for genDistrib.
         * Goes as deep as level allows, or takes the current node if any of the children is nullptr.
         * This should only happen if both are nullptr, i.e. the node is a leaf.
         * @param level how deep the generation should go down (at most). The deepest possible level is taken for the generation.
         * @return the generated distribution. Is invalid if the given node is a nullptr.
         */
        Distribution genDistribHelper(unsigned int level);


        /**
         * @brief getMaxCost
         * @param depth
         * @return the maximum cost at the given depth, NaN if the depth cant be reached
         */
        double getMaxCost(unsigned int depth);

        /**
         * @brief reset resets every bucket on in every depth
         */
        void reset();

    private:
        /**
         * @brief getMinDepthHelper keeps track of the currentdepth for getDepth(maxCost)
         * returns the depth where the given constraint is fullfilled.
         * Calculates the cost where it is not chached.
         * @param maxCost must be positive
         * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
         * @return the depth where all nodes fullfill the given maxCost constraint, -1 if the maxCost was invalid, -2 if a bucket on the way was nullptr, -3 if the needed depth could not be reached.
         * If multiple error occure the left-most error will propagate.
         */
        int getMinDepthHelper(double maxCost, int currDepth,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);



    }BucketTree_node_t;

    BucketTree_node_t *root;
    //FIXME improvment calcRealMaxCost (traverse tree, check cost)
    /**
     * @brief BucketTree an empty Tree is created
     * @param maxCost for splitting
     */
    BucketTree(double maxCost){
        root = nullptr;
        this->maxCost = maxCost;
    }

    /**
     * @brief BucketTree Tree is created with the given root
     * @param root
     * @param maxCost for splitting
     */
    BucketTree(BucketTree_node_t* root,double maxCost){
        this->root = root;
        this->maxCost = maxCost;
    }

    /**
     * @brief constraintHolds calculates the cost where needed
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return true iff the cost is lower then maxCost the root. False if the maxcost is invalid
     */
    bool constraintHolds(QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    /**
     * @brief constraintHoldsAlways calculates the cost where needed
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return true iff the cost is lower then maxCost the for all nodes. False if the maxcost is invalid
     */
    bool constraintHoldsAlways(QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    /**
     * @brief isEmpty
     * @return true iff the root is null
     */
    bool isEmpty() {
        return (root == nullptr);
    }

    /**
     * @brief insert inserts the given bucket into the given parent node.
     * The parent-node is filled left to right. If no child is nullptr nothing is inserted
     * @param bucket can not be nullptr
     * @param parent can be nullptr IFF the inserted node is the root
     * @return true iff the given data was successfully inserted.
     */
    bool insert(Bucket* bucket,BucketTree_node_t *parent);

    /**
     * @brief toString represents the EQCTree as a string. usfull for debugging or a simple GUI.
     * @return
     */
    QString toString();//FIXME make more readable


    //FIXME MAKE depth uint !!!!!!!
    //FIXME check cost calc @ split...
    //TODO improvement add split... without node like in eqcsizetree
    /**
     * @brief splitLeafsUntil splits the given BucketTreeNode, i.e. all leafs are split if the theshold is not yet reached
     * @param node The startingpoint
     * @param costThres the tree should no further be split if the maximum cost is lower then this
    * @param minBucketFactor =  the small bucket size / large bucket size  , which should be upheld. Restricts the splitting a lot! But can help get a balanced tree.
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return the MEAN maximum cost (i.e. sum of all leaf - cost / number of leafs), NaN if an error occured, -1 if no split was performed, because the cost was already reached.
     */
    static double splitLeafsUntil(BucketTree_node_t* node,double costThres,double minBucketFactor,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);
    /**
    * @brief splitUntil splits the given BucketTreeNode from the given node down until the threshold is met, overwriting all existing children.
    * IMPORTANT! This stops if the current depth fullfulls the constraint, therefore the tree might not have the same depth on all branches.
    * splitDepth should be called afterwards!
    * Might fail to meet the threshold, if the best split is no split for enough leafs. So look at the real MEAN maximum cost
    * Can destroy the tree if the result is NaN and some splits are performed and others are not.
    * @param node the startingpoint
    * @param costThres the tree should no further be split if the maximum cost is lower then this
    * @param minBucketFactor =  the small bucket size / large bucket size  , which should be upheld. Restricts the splitting a lot! But can help get a balanced tree.
    * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
    * @return the maximum cost, NaN if an error occured. Negative cost, if no split was performed somewhere.
    */
    static double splitUntil(BucketTree_node_t* node,double costThres,double minBucketFactor = DEFAULT_MIN_BUCKET_FACTOR,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    /**
    * @brief splitDepth splits the given BucketTreeNode until the given depth is reached, overwriting all existing children
    * Might fail, if the depth can not be reached.
    * @param node the startingpoint
    * @param depth the target depth (counting from the current node)
     * @param minBucketFactor =  the small bucket size / large bucket size  , which should be upheld. Restricts the splitting a lot! But can help get a balanced tree.
    * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
    * @return the  maximum cost. negative cost, if somewhere no split was performend! NaN if the depth could not be reached or  a different error occured. Errors will be given up, NaN is stronger then negative
    */
    static double splitDepth(BucketTree_node_t* node,unsigned int depth,double minBucketFactor = DEFAULT_MIN_BUCKET_FACTOR,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    /**
    * @brief getMaxDepth
    * @param node the startingpoint
    * @return the maximum reachable depth, 0 if it is the root or node is nullptr
    */
    static unsigned int getMaxDepth(BucketTree_node_t* node);

    /**
     * @brief getMaxDepth
    * @return the maximum reachable depth, 0 if it is the root or node is nullptr
     */
    unsigned int getMaxDepth();


    /**
    * @brief getMinDepth
    * @param node the startingpoint
    * @return the Minimum reachable depth, 0 if it is the root or node is nullptr
    */
    static unsigned int getMinDepth(BucketTree_node_t* node);


    /**
    * @brief getMinDepth
    * @return the Minimum reachable depth, 0 if it is the root or node is nullptr
    */
    unsigned int getMinDepth();

    /**
    * @brief getLeafNodes recurses down from the root node and returns all leafs
    * @return list of all leafnodes from the current node down.
    */
    QList<BucketTree_node_t*> getLeafNodes();

    /**
    * @brief getLeafBuckets recurses down from the root and returns all buckets of the leafs
    * @return list of all buckets of leafs from the root down.  Is invalid if it contains a nullptr.
    */
    QList<Bucket*> getLeafBuckets();

    /**
    * @brief getBuckets recurses down from the root node and returns all buckets at the given depth
    * @param depth
    * @returnThe list of all buckets of the given depth. Empty if the depth is deeper then the tree (on any branch) or negative.  Is invalid if it contains a nullptr.
    */
    QList<Bucket*> getBuckets(int depth);

    /**
    * @brief getLeafs recurses down from the root node and returns all nodes at the given depth
    * @param depth
    * @return list of all nodes at the given depth from the current node down. Empty if the depth is deeper then the tree (on any branch) or negative
    */
    QList<BucketTree_node_t*> getNodes(int depth);

    /**
    * @brief isLevelEmpty
    * @param depth
    * @return true if every bucket on the given depth has no element remaining.
    */
    bool isLevelEmpty(int depth);


    /**
    * @brief deleteTree recursively deletes the all children
    * Only the root has to be deleted manually at the end
    * @param delBuckets true if the buckets of the nodes should be deleted
    */
    void deleteTree(bool delBuckets = true);

    /**
    * @brief getDepth
    * @return the depth of the current node. 0 is root. -1 if the root was nullptr
    */
    int getDepth();

    /**
    * @brief getMinDepth returns the depth where the given constraint is fullfilled.
    * Calculates the cost where it is not chached.
    * @param  c must be positive
    * @return the depth where all nodes fullfill the given maxCost constraint, -1 if the maxCost was invalid, -2 if a bucket on the way was nullptr, -3 if the needed depth could not be reached.
    * If multiple error occure the left-most error will propagate.
    */
    int getMinDepth(double c);

    /**
    * @brief clearChache recurses down the tree and sets the costcache to nan.
    * Should be done on larger changes of the bucketization.
    * However the recalculation is expensive, for large depths, so keep the clears as local as possible!
    */
    void clearChache();

    /**
    * @brief getMaxCost
    * @return
    */
    double getMaxCost() const;


    /**
    * @brief genDistrib generates a distribution for the buckets at the given level
    * @param depth how deep the generation should go down (at most). The deepest possible level is taken for the generation.
    * @return the generated distribution. Or nullptr if an error occured.
    */
    Distribution* genDistrib(unsigned int depth);


    /**
     * @brief getMaxCost
     * @param depth
     * @return the maximum cost at the given depth, nan if the depth cant be reached
     */
    double getMaxCost(unsigned int depth);


    /**
     * @brief reset Resets every bucket on every level of the tree!
     */
    void reset();

    /**
    * @brief isLevelEmpty
    * @param buckets allows to chache the buckets
    * @return true if every bucket on the given depth has no element remaining.
    */
    static bool isLevelEmpty(QList<Bucket*> buckets);


    /**
  * @brief genTree splits the data into buckets according to the given distribution.
  * I.e. semantically similar datacontainers are grouped into buckets and treated as if they were the same w.r.t. the distribution and the porportionallity constraint.
  * The Tree is split as far as possible. It might not be balanced! Check min and maxDepth!
  * @param data
  * @param distr The distribution must be changed according to the buckets. Use BucketTree::genDistrib(...) for this. I.e. if two values v1 and v2 are both in the same bucket, their value in the distribution is also merged. They are then identified by their reference value of the bucket.
  * @param maxCost the maximum distance between the containers to be considered as equal. I.e. they are put in the same bucket.
  * @param metric how the cost is measured  (semantic distance is used)
  * @param minBucketFactor =  the small bucket size / large bucket size  , which should be upheld. Restricts the splitting a lot! But can help get a balanced tree.
  * @return The tree of containing all sizes of and buckets of the given data and distribution, or nullptr if an error occured .
  */
    static BucketTree * genTree(QList<DataContainer*> data, Distribution* distr,DataDistanceMetric* metric,double minBucketFactor = DEFAULT_MIN_BUCKET_FACTOR);

    /**
   * @brief genTree splits the data into buckets according to the given distribution.
   * I.e. semantically similar datacontainers are grouped into buckets and treated as if they were the same w.r.t. the distribution and the porportionallity constraint.
   * The Tree might not be balanced. Check min and maxDepth!
   * @param data
   * @param distr The distribution must be changed according to the buckets. Use BucketTree::genDistrib(...) for this. I.e. if two values v1 and v2 are both in the same bucket, their value in the distribution is also merged. They are then identified by their reference value of the bucket.
   * @param maxCost the maximum distance between the containers to be considered as equal. I.e. they are put in the same bucket.
   * @param metric how the cost is measured  (semantic distance is used)
   * @param minBucketFactor =  the small bucket size / large bucket size  , which should be upheld. Restricts the splitting a lot! But can help get a balanced tree.
   * @return The tree of containing all sizes of and buckets of the given data and distribution, or nullptr if an error occured, e.g. the maxCost constraint was broken.
   */
    static BucketTree * genTree(QList<DataContainer*> data, Distribution* distr,double maxCost,DataDistanceMetric* metric,double minBucketFactor = DEFAULT_MIN_BUCKET_FACTOR);


private:

    ///! Maximum extended EMD
    double maxCost;

    /**
     * @brief splitNode splits the given node and overwrites its current children.
     * Sets the costcache of the new children.
     * @param node
     * @param minBucketFactor =  the small bucket size / large bucket size  , which should be upheld. Restricts the splitting a lot! But can help get a balanced tree.
     * @param overwriteChildren if this is true, the children are overwritten, otherwise no split is performed if the children are not nullptr.
     * !! DANGER OF MEMLEAK!! You should know wat your are doing if you pass a node with children and this set to true
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return the cost of the resulting split. or NaN if an error occured (children set and not overwritable or nullptr node). The negative cost, if the best split is no split.
     */
    static double splitNode(BucketTree_node_t* node,double minBucketFactor = DEFAULT_MIN_BUCKET_FACTOR, bool overwriteChildren = false,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);
};

#endif // BUCKETTREE_H
