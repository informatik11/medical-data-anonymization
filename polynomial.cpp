#include "polynomial.h"


Polynomial::Polynomial(DataContainer *refData, DataPoint *refZero, uint8_t order, QObject *parent)
{
    this->refData = refData;
    this->maxOrder = order;
    this->data = refData->getData();
    this->refZero = refZero;
    if(refZero == nullptr ||  refData == nullptr || !refZero->getTime().isValid() )
    {
        valid = false;
    }
    calcStartEnd();
}

QPair<long double*,uint8_t> Polynomial::getCoefficients()
{
    int16_t ord = getRealOrder();
    QPair<long double*,uint8_t> ret;
    long double coeffArr[ord];
    if(ord >= 0)
    {
        for( int i = 0; i < ord; i++)
        {
            QMap<uint8_t,long double>::const_iterator currCoeffIt = coeff.find(i);
            if(currCoeffIt == coeff.end())
            {
                coeffArr[i] = 0;//coefficient of the current order not in the map=> is 0
            }else{
                coeffArr[i] = currCoeffIt.value();
            }
        }
        ret.first = coeffArr;
        ret.second = (uint8_t) ord;
        return ret;
    }else{
        ret.first = nullptr;
        return ret;
    }
}

long  double Polynomial::getCoefficient(uint8_t order)
{
    QMap<uint8_t,long  double>::const_iterator it = coeff.find(order);
    if(it == coeff.end())
    {
        return nan("");
    }else{
        return it.value();
    }
}

bool Polynomial::setCoefficient(uint8_t order, long double coeff, bool increaseOrder)
{
    if(order > maxOrder && increaseOrder == false)
    {
        return false;
    }else{
        this->coeff.insert(order,coeff);
        if(order > maxOrder)
        {
            maxOrder = order;
        }
        return true;
    }
}

int16_t Polynomial::getRealOrder()
{
    QList<uint8_t> keys = coeff.keys();
    QList<long double> vals = coeff.values();

    for(uint8_t i = keys.size() -1 ; i >= 0; i++)// FIXME: infinite loop, iterates the wrong direction
    {
        if(vals.at(i) != 0 && !isnan(vals.at(i)))
        {
            return keys.at(i);
        }
    }
    // FIXME: return something in case inner if is never true? 0 for now
    return 0;
}

uint8_t Polynomial::getMaxOrder() const
{
    return maxOrder;
}

void Polynomial::setMaxOrder(uint8_t newMaxOrder,bool recalc)
{
    maxOrder = newMaxOrder;
    if(recalc == false)
    {
        while(coeff.keys().last() > newMaxOrder)
        {
            coeff.remove(coeff.keys().last());
        }
    }else{
        valid = calcPolynomial();
    }
}

void Polynomial::dataChanged()
{
    this->data = refData->getData();
    calcStartEnd();
    valid = calcPolynomial();
}

bool Polynomial::calcPolynomial()
{
    if(refData == nullptr || refZero == nullptr || !refZero->getTime().isValid())
    {
        return false;
    }

    size_t k = maxOrder;
    bool fixedinter = false;                         // Fixed the intercept (coefficient A0)
    int wtype = 0;                                   // Weight: 0 = none (default), 1 = sigma, 2 = 1/sigma^2
    long double fixedinterval = 0.;                       // The fixed intercept value (if applicable)
    //double alphaval = 0.05;                          // Critical alpha value FIXME adapt?

    long double x [data.size()];
    long double y [data.size()];
    long double erry[data.size()];
    //TODO use weights together with error

    for(int i = 0; i < data.size() ; i++)
    {
        erry[i] = 0;
        x[i]    = refZero->getTime().toMSecsSinceEpoch() - data.keys().at(i).toMSecsSinceEpoch();//FIXME check precision!
        y[i]    = data.values().at(i)->getValue();
    }



    size_t n = data.size();                           // Number of data points
    size_t nstar = n-1;                                // equal to n (fixed intercept) or (n-1) not fixed
    if(fixedinter == true)
    {
        nstar = n;
    }

    if( k > nstar)
    {
        qCritical() << QString("Polynomial could not be calculated k > n* ( %1 > %2) !").arg(k).arg(nstar);
        return false;
    }
    long double coefbeta[k+1];                            // Coefficients of the polynomial
    //  double serbeta[k+1];                             // Standard error on coefficients
    //   double tstudentval = 0.;                         // Student t value
    //   double SE = 0.;                                  // Standard error

    long  double **XTWXInv;                                // Matrix XTWX Inverse [k+1,k+1]
    long  double **Weights;                                // Matrix Weights [n,n]


    XTWXInv = PolyFit::Make2DArray(k+1,k+1);
    Weights = PolyFit::Make2DArray(n,n);
    this->weights = Weights;


    PolyFit::CalculateWeights(erry, Weights, n, wtype);


    if (wtype != 0 && PolyFit::determinant(Weights,n) == 0.) {
        qCritical() << "Weighted polynomial not possible, determinante of weights was zero!";
        return false;
    }

    PolyFit::fit(x,y,n,k,fixedinter,fixedinterval,coefbeta,Weights,XTWXInv);

    coeff.clear();
    for(int i = 0; i < maxOrder; i++)
    {
        coeff.insert(i,coefbeta[i]);
    }
    //FIXME XTWInv needed outside?
    return true;
}



long double **Polynomial::getWeights() const
{
    return weights;
}

DataContainer *Polynomial::getRefData()
{
    return refData;
}

int Polynomial::getHorizon()
{
    return refData->getData().size();
}

void Polynomial::setRefZero(DataPoint *newRefZero, bool reCalc)
{
    refZero = newRefZero;
    if(reCalc == true)
    {
        calcPolynomial();
    }
}

DataPoint Polynomial::getRefZero() const
{
    return *refZero;
}

/**
Wave_Trend_Fluctuation_t Polynomial::getFluct() const
{
    return fluct;
}

void Polynomial::setFluct(Wave::Wave_Trend_Fluctuation_t newFluct)
{
    fluct = newFluct;
    valid = calcPolynomial();
}
*/
double Polynomial::valueAt(QDateTime x)
{
    long double val = 0;
    qint64 normX = refZero->getTime().toMSecsSinceEpoch() - x.toMSecsSinceEpoch();
    for(int i = 0 ; i < coeff.size(); i++)
    {
        uint8_t currOrd   = coeff.keys().at(i);
        long double  currCoeff = coeff.values().at(i);
        val += pow(normX,currOrd) * currCoeff;//FIXME check precision/overflow!
    }
    return (double) val;//FIXME check cast
}

double Polynomial::similarity(Polynomial *p1, Polynomial* p2)
{
    // Similarity score R2Adj (increases when R2 increases by more then just "luck") between two polys.
    //=> take x,y of one and fit with coeff of other and vice versa
    // R2Adj(x_1,y_1,coeff_2) + R2Adj(x_2,y_2,coeff_1)
    QPair<long double*,uint8_t> pair1 = p2->getCoefficients();
    QPair<long double*,uint8_t> pair2 = p1->getCoefficients();

    if(pair1.first == nullptr || pair2.first == nullptr)
    {
        return nan("");
    }

    long double* c1 = pair1.first;
    uint8_t c1Size = pair1.second;


    long double* c2 = pair2.first;
    uint8_t c2Size = pair2.second;

    uint8_t n1 = (uint8_t) min((int)c1Size,p1->data.size());
    uint8_t k1 = c1Size + 1;
    uint8_t n2 = (uint8_t) min((int)c2Size,p2->data.size());
    uint8_t k2 = c2Size + 1;


    long double x1[n1];
    long double y1[n1];

    for(int i = 0; i< n1; i++)
    {
        DataPoint* val = p1->data.values().at(i);
        y1[i] = val->getValue();
        x1[i] = p1->getRefZero().getTime().toMSecsSinceEpoch() - val->getTime().toMSecsSinceEpoch();
    }


    long double x2 [n2];
    long double y2[n2];


    for(int i = 0; i< n2; i++)
    {
        DataPoint* val = p2->data.values().at(i);
        y2[i] = val->getValue();
        x2[i] = p2->getRefZero().getTime().toMSecsSinceEpoch() - val->getTime().toMSecsSinceEpoch();
    }

    //FIXME check indices ( should calc datapoints of one polynomial against coeffcients of the other.)
    long double r2Adj1 = PolyFit::CalculateR2Adj(x1,y1,c1,p2->getWeights(),false,n1,k1);

    long double r2Adj2 = PolyFit::CalculateR2Adj(x2,y2,c2,p1->getWeights(),false,n2,k2);

    return r2Adj1 + r2Adj2;
    //FIXME normalize?!
}

bool Polynomial::isValid() const
{
    return valid;
}

