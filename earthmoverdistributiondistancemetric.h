#ifndef EUCLIDEANDISTRIBUTIONDISTANCEMETRIC_H
#define EUCLIDEANDISTRIBUTIONDISTANCEMETRIC_H

#include "distributiondistancemetric.h"

/**
 * @brief The EarthMoverDistance class implements DistributionDistanceMetric with an euclidean gorund distance
 */
class EarthMoverDistance : public DistributionDistanceMetric
{
public:
    EarthMoverDistance();


    /**
     * @brief error how precise is the distance
     * @return
     */
    double error() override;


    /**
      * @brief distance calculates the distance between the distributions.
      * It ignores all
      * @param w1
      * @param w2
      * @param ignoreNonExist true if only keys that are present in both distributions should be used.
      * Otherwise a non existing key is the same as a key with 0  occurences.
      * @return the distance between all  datapoints in the given distributions. NaN if a distribution is invalid
     */
    double distance(Distribution* d1, Distribution* d2, bool ignoreNonExist = false) override;
};

#endif // EUCLIDEANDISTRIBUTIONDISTANCEMETRIC_H
