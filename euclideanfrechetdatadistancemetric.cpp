#include "euclideanfrechetdatadistancemetric.h"

EuclideanFrechetDataDistanceMetric::EuclideanFrechetDataDistanceMetric()
{
    name = "Euclidean Frechet Data Distance";
}

double EuclideanFrechetDataDistanceMetric::error()
{
    return 0;
}

double EuclideanFrechetDataDistanceMetric::valueDistance(DataPoint p1, DataPoint p2)
{

    return abs(p1.getValue() - p2.getValue());
}

double EuclideanFrechetDataDistanceMetric::valueDistance(DataContainer d1, DataContainer d2)
{
    if(d1.getData().size() == d2.getData().size())
    {
        int dist = 0;
        for(int i=0;i<d1.getData().size();i++)//FIXME alignment of waves?
        {
            dist += valueDistance(*d1.getData().values().at(i),*d2.getData().values().at(i));
        }
        return dist;
    }else if(d1.getData().size() < d2.getData().size())
    {
        int diff = d2.getData().size() - d1.getData().size();
        int dist1 = 0;
        for(int i=0;i<d1.getData().size();i++)//FIXME alignment of waves?
        {
            dist1 += valueDistance(*d1.getData().values().at(i),*d2.getData().values().at(i));
        }
        int dist2 = 0;
        for(int i=d2.getData().size() -1 ;i>=diff;i--)//FIXME alignment of waves?
        {
            dist2 += valueDistance(*d1.getData().values().at(i - diff ),*d2.getData().values().at(i));
        }

        if(dist1 < dist2)
        {
            return dist1;
        }else{
            return dist2;
        }
    }else if(d1.getData().size() > d2.getData().size())
    {
        int diff = d1.getData().size() - d2.getData().size();
        int dist1 = 0;
        for(int i=0;i<d2.getData().size();i++)//FIXME alignment of waves?
        {
            dist1 += valueDistance(*d1.getData().values().at(i),*d2.getData().values().at(i));
        }
        int dist2 = 0;
        for(int i=d2.getData().size() -1 ;i>=diff;i--)//FIXME alignment of waves?
        {
            dist2 += valueDistance(*d2.getData().values().at(i - diff ),*d1.getData().values().at(i));
        }

        if(dist1 < dist2)
        {
            return dist1;
        }else{
            return dist2;
        }
    }else{
        return INT32_MAX;//should never happen
    }
}

double EuclideanFrechetDataDistanceMetric::timeDistance(DataPoint p1, DataPoint p2)
{

    return abs(p1.getTime().msecsTo(p2.getTime()));
}

double EuclideanFrechetDataDistanceMetric::timeDistance(DataContainer d1, DataContainer d2)
{
    if(d1.getData().size() == d2.getData().size())
    {
        int dist = 0;
        for(int i=0;i<d1.getData().size();i++)//FIXME alignment of waves?
        {
            dist += timeDistance(*d1.getData().values().at(i),*d2.getData().values().at(i));
        }
        return dist;
    }else if(d1.getData().size() < d2.getData().size())
    {
        int diff = d2.getData().size() - d1.getData().size();
        int dist1 = 0;
        for(int i=0;i<d1.getData().size();i++)//FIXME alignment of waves?
        {
            dist1 += timeDistance(*d1.getData().values().at(i),*d2.getData().values().at(i));
        }
        int dist2 = 0;
        for(int i=d2.getData().size() -1 ;i>=diff;i--)//FIXME alignment of waves?
        {
            dist2 += timeDistance(*d1.getData().values().at(i - diff ),*d2.getData().values().at(i));
        }

        if(dist1 < dist2)
        {
            return dist1;
        }else{
            return dist2;
        }
    }else if(d1.getData().size() > d2.getData().size())
    {
        int diff = d1.getData().size() - d2.getData().size();
        int dist1 = 0;
        for(int i=0;i<d2.getData().size();i++)//FIXME alignment of waves?
        {
            dist1 += timeDistance(*d1.getData().values().at(i),*d2.getData().values().at(i));
        }
        int dist2 = 0;
        for(int i=d2.getData().size() -1 ;i>=diff;i--)//FIXME alignment of waves?
        {
            dist2 += timeDistance(*d2.getData().values().at(i - diff ),*d1.getData().values().at(i));
        }

        if(dist1 < dist2)
        {
            return dist1;
        }else{
            return dist2;
        }
    }else{
        return INT32_MAX;//should never happen
    }
}

double EuclideanFrechetDataDistanceMetric::distance(DataPoint p1, DataPoint p2)
{
    return sqrt(pow((p1.getValue() - p2.getValue()),2) +pow((p1.getTime().msecsTo(p2.getTime())),2) );
}

double EuclideanFrechetDataDistanceMetric::distance(DataContainer d1, DataContainer d2)
{
    if(d1.getData().size() == d2.getData().size())
    {
        int dist = 0;
        for(int i=0;i<d1.getData().size();i++)//FIXME alignment of waves?
        {
            dist += distance(*d1.getData().values().at(i),*d2.getData().values().at(i));
        }
        return dist;
    }else if(d1.getData().size() < d2.getData().size())
    {
        int diff = d2.getData().size() - d1.getData().size();
        int dist1 = 0;
        for(int i=0;i<d1.getData().size();i++)//FIXME alignment of waves?
        {
            dist1 += distance(*d1.getData().values().at(i),*d2.getData().values().at(i));
        }
        int dist2 = 0;
        for(int i=d2.getData().size() -1 ;i>=diff;i--)//FIXME alignment of waves?
        {
            dist2 += distance(*d1.getData().values().at(i - diff ),*d2.getData().values().at(i));
        }

        if(dist1 < dist2)
        {
            return dist1;
        }else{
            return dist2;
        }
    }else if(d1.getData().size() > d2.getData().size())
    {
        int diff = d1.getData().size() - d2.getData().size();
        int dist1 = 0;
        for(int i=0;i<d2.getData().size();i++)//FIXME alignment of waves?
        {
            dist1 += distance(*d1.getData().values().at(i),*d2.getData().values().at(i));
        }
        int dist2 = 0;
        for(int i=d2.getData().size() -1 ;i>=diff;i--)//FIXME alignment of waves?
        {
            dist2 += distance(*d2.getData().values().at(i - diff ),*d1.getData().values().at(i));
        }

        if(dist1 < dist2)
        {
            return dist1;
        }else{
            return dist2;
        }
    }else{
        return INT32_MAX;//should never happen
    }
}


double EuclideanFrechetDataDistanceMetric::semanticDistance(DataContainer d1, DataContainer d2)
{
    const QDateTime start1 = d1.getStart();
    const QDateTime end1 = d1.getEnd();
    const QDateTime start2 = d2.getStart();
    const QDateTime end2 = d2.getEnd();
    //determine common range
    time_t maxStart ;
    if(start1.msecsTo(start2) < 0 )//d1 starts earlier
    {
        maxStart = start2.toMSecsSinceEpoch();
    }else{//d2 starts earlier(or equal)
        maxStart = start1.toMSecsSinceEpoch();
    }


    time_t minEnd ;
    if(end1.msecsTo(end2) < 0 )//d1 ends earlier
    {
        minEnd = end1.toMSecsSinceEpoch();
    }else{//d2 ends earlier(or equal)
        minEnd = end2.toMSecsSinceEpoch();
    }

    const QMap<QDateTime, DataPoint *> data1 = d1.getData();
    QMap<QDateTime, DataPoint *> commonData1;
    const QMap<QDateTime, DataPoint *> data2 = d2.getData();
    QMap<QDateTime, DataPoint *> commonData2;

    for(QDateTime dt : data1.keys())
    {
        time_t ms = dt.toMSecsSinceEpoch();
        if( ms- maxStart >= 0 && ms- minEnd <= 0)//in common range
        {
            commonData1.insert(dt,data1.value(dt));
        }
    }
    if(commonData1.isEmpty())
    {
        qWarning() << "No data in common for distance calculation!";
        return nan("");
    }
    DataContainer commonD1(commonData1,d1.isInterpolateable());

    for(QDateTime dt : data2.keys())
    {
        time_t ms = dt.toMSecsSinceEpoch();
        if( ms- maxStart >= 0 && ms- minEnd <= 0)//in common range
        {
            commonData2.insert(dt,data2.value(dt));
        }
    }


    if(commonData2.isEmpty())
    {
        qWarning() << "No data in common for distance calculation!";
        return nan("");
    }
    DataContainer commonD2(commonData2,d2.isInterpolateable());


    Curve c1 = commonD1.toFrechetCurve();
    Curve c2 = commonD2.toFrechetCurve();


    Frechet::Discrete::Distance dist = Frechet::Discrete::distance(c1,c2);

    return dist.value;
}

