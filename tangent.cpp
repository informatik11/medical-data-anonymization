#include "tangent.h"

Tangent::Tangent(DataPoint *base, double slope)
{
    if( base  == nullptr || isnan(slope) || !base->isValidValue() )
    {
        m_isValid = false;
    }else{
        m_isValid = true;
        this->base = base;
        this->slope = slope;
        offset = base->getValue();
        startX = base->getTime();
    }
}

QDateTime Tangent::getStartX()
{
    return startX;
}

double Tangent::getOffset()
{
    return offset;
}

bool Tangent::isValid()
{
    return m_isValid;
}

double Tangent::valueAt(QDateTime param)
{
    if(param.isValid())
    {
        double dist = startX.msecsTo(param);
        return offset + dist*slope;//FIXME check
    }else{
        return nan("");
    }
}

double Tangent::distance(Tangent a, Tangent b)
{
    if(a.isValid() && b.isValid())
    {
        double xDistP2 = pow((a.startX.msecsTo(b.startX)),2);
        double yDistP2 = pow((a.offset - b.offset),2);
        double slopeDistP2 = pow((a.slope - b.slope),2);
        return sqrt(xDistP2 + yDistP2 + slopeDistP2);
    }else{
        return nan("");
    }
}
