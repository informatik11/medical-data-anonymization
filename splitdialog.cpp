#include "splitdialog.h"
#include "qcustomplot.h"
#include "ui_splitdialog.h"

SplitDialog::SplitDialog(QList<QHash<DataContainer*,QCPGraph*>*>* rawCurves,QCustomPlot* parentPlot, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SplitDialog)
{
    if(parent != nullptr)
    {
        parent->setEnabled(false);
        this->parent = parent;
        this->setEnabled(true);
    }
    ui->setupUi(this);
    ui->graphPlotWidget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend | QCP::iSelectPlottables);
    this->setWindowTitle("NANNI-Splitter");
    this->rawCurves = rawCurves;
    this->parentPlot = parentPlot;
    ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(false);
    ui->buttonBox->button(QDialogButtonBox::Apply)->setEnabled(false);
    ui->buttonBox->button(QDialogButtonBox::Discard)->setEnabled(true);
    segments = new QList<QList<WaveSegment*>*>();

    ui->graphPlotWidget->xAxis->setLabel("Zeit (s)");
    //FIXME add yAxis label? if rawCurves.size > 1 cast to wavesegment & get unit, else Wave
    if(rawCurves != nullptr && !rawCurves->isEmpty() && parentPlot != nullptr)
    {

        for(QHash<DataContainer*,QCPGraph*>* hash : *rawCurves)
        {
            for(DataContainer* cont : hash->keys())
            {
                convertWave(cont,ui->graphPlotWidget);
                ui->graphPlotWidget->replot();
            }
        }

        range = calcDataRange();
        segCnt = round(1.0/HULL_STEPSIZE_FACTOR);
        wndSze = (int) (range * HULL_STEPSIZE_FACTOR);
        stepSze = round(wndSze / 2.0);
        intervalSze = (int) (range * HULL_STEPSIZE_FACTOR);
        ui->intervalSpinbox->setMinimum(1);
        ui->intervalSpinbox->setMaximum(range-1);
        ui->intervalSpinbox->setValue(intervalSze);
        ui->segCntSpinbox->setMinimum(1);
        ui->segCntSpinbox->setMaximum(range-1);
        ui->segCntSpinbox->setValue(segCnt);

        ui->wndSzeSpinbox->setMinimum(1);
        ui->wndSzeSpinbox->setMaximum(floor(range/(double)segCnt));
        ui->wndSzeSpinbox->setValue(wndSze);

        ui->stepSzeSpinbox->setMinimum(1);
        ui->stepSzeSpinbox->setMaximum(wndSze +1);
        ui->stepSzeSpinbox->setValue(stepSze);


        ui->segCntLabel->setVisible(true);
        ui->segCntSpinbox->setVisible(true);
        ui->wndSzeLabel->setVisible(true);
        ui->wndSzeSpinbox->setVisible(true);
        ui->stepSzeLabel->setVisible(true);
        ui->stepSzeSpinbox->setVisible(true);

        ui->intervalLabel->setVisible(false);
        ui->intervalSpinbox->setVisible(false);

        ui->graphPlotWidget->rescaleAxes();
    }else{
        qWarning() << "Invalid curves or parentplot in SplittingDialog!";
        QMessageBox::warning(this,"Fehler!","Kurven oder Plot waren ungültig!");
    }
}

SplitDialog::~SplitDialog()
{
    clearSegments(this->result() == QDialog::Rejected);//delete segments if not applied
    delete segments;
    if(parent != nullptr)
    {
        parent->setEnabled(true);
    }
    delete ui;
}


void SplitDialog::on_splitButton_clicked()
{
    SplitMode_t mode = SPLIT_MODE_FIXED;
    if(ui->fixedButton->isChecked())
    {
        mode = SPLIT_MODE_FIXED;
    }else if(ui->visualButton->isChecked()){
        mode = SPLIT_MODE_VISUAL;
    }else if(ui->frechetButton->isChecked()){
        mode = SPLIT_MODE_FRECHET;
    }else{
        qFatal("Unkown splitmode!");
    }

    ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(true);
    ui->buttonBox->button(QDialogButtonBox::Apply)->setEnabled(true);

    switch (mode)
    {
    case SPLIT_MODE_VISUAL:
        visualSplit();
        break;
    case SPLIT_MODE_FRECHET:
        frechetSplit();
        break;
    case SPLIT_MODE_FIXED:
        fixedSplit();
        break;
    }
}


void SplitDialog::onSelectMove(QMouseEvent *event)
{
    if(selecting == true && !selectGraphs.isEmpty())
    {
        QCPGraph * gLow = selectGraphs.last()->first;
        QCPGraph * gUp = selectGraphs.last()->second;
        double x = ui->graphPlotWidget->xAxis->pixelToCoord(event->x());
        double xMs = x *1000;
        double lastX =(-1)*INFINITY;
        if( gLow->data().get()->size() > 1)
        {
            int i =  gLow->data().get()->size() -1;
            lastX = gLow->data().get()->at(i)->key;
        }
        if( xMs >= maxStart.toMSecsSinceEpoch() && xMs <= minEnd.toMSecsSinceEpoch() && x > lastX)
        {
            double yLower = ui->graphPlotWidget->yAxis->range().lower - 1.0;
            double yUpper = ui->graphPlotWidget->yAxis->range().upper + 1.0;

            gUp->addData(x,yUpper);
            gLow->addData(x,yLower);
            ui->graphPlotWidget->replot();
        }
    }
}

void SplitDialog::onSelectPress(QMouseEvent *event)
{
    double x = ui->graphPlotWidget->xAxis->pixelToCoord(event->x());
    double xMs = x *1000;
    if( xMs < maxStart.toMSecsSinceEpoch())
    {
        xMs = maxStart.toMSecsSinceEpoch();
        x = xMs/1000;
    }
    bool validStart = xMs <= minEnd.toMSecsSinceEpoch();
    if(!segments->empty() && !segments->last()->empty())
    {
        validStart = xMs >= segments->last()->at(0)->getStart().toMSecsSinceEpoch();//can overlap, but at least start after last start!
    }
    if(validStart)
    {

        QCPGraph* gLow = ui->graphPlotWidget->addGraph(ui->graphPlotWidget->xAxis, ui->graphPlotWidget->yAxis);
        QPair<QCPGraph*,QCPGraph*> *pair = new QPair<QCPGraph*,QCPGraph*>();
        pair->first = gLow;
        QCPGraph* gUp = ui->graphPlotWidget->addGraph(ui->graphPlotWidget->xAxis, ui->graphPlotWidget->yAxis);
        pair->second = gUp;
        selectGraphs.append(pair);

        QColor selCol = QColor(0xaa, 0xaa, 0xff, 0x30);//color as in viewer
        QBrush brush = QBrush(selCol);
        gLow->setPen(QPen(QColor(Qt::black)));
        gUp->setPen(QPen(QColor(Qt::black)));
        gLow->setVisible(true);
        gUp->setVisible(true);
        gLow->setSelectable(QCP::stNone);
        gUp->setSelectable(QCP::stNone);
        gLow->setChannelFillGraph(gUp);
        gUp->setChannelFillGraph(gLow);
        gLow->setBrush(brush);
        gUp->setBrush(brush);
        ui->graphPlotWidget->setCursor(Qt::ClosedHandCursor);
        selecting = true;
        double yLower = ui->graphPlotWidget->yAxis->range().lower - 1.0;
        double yUpper = ui->graphPlotWidget->yAxis->range().upper + 1.0;

        gUp->addData(x,yLower);
        gUp->addData(x,yUpper);
        gLow->addData(x,yUpper);
        gLow->addData(x,yLower);
        visCurrStart = QDateTime::fromMSecsSinceEpoch((int)xMs);
    }
}

void SplitDialog::onSelectRelease(QMouseEvent *event)
{
    if(selecting == true && !selectGraphs.isEmpty())
    {
        QCPGraph * gLow = selectGraphs.last()->first;
        QCPGraph * gUp = selectGraphs.last()->second;

        double x = ui->graphPlotWidget->xAxis->pixelToCoord(event->x());
        double xMs = x *1000;
        if( xMs >= minEnd.toMSecsSinceEpoch())
        {
            xMs = minEnd.toMSecsSinceEpoch();
            x = xMs/1000.0;
            selectFinished = true;
            qInfo() << "Reached right border in visualSplit, stopping selection.";
            QMessageBox::information(this,"Auswahl beendet.",QString("Die Auswahl wurde durch erreichen der rechten Grenze automatisch beendet.\n Die Segmente %1 können nun exporiert werden.").arg(segments->size()));
        }
        int i =  gLow->data().get()->size() -1;
        double lastX = gLow->data().get()->at(i)->key;
        if( x < lastX || xMs < maxStart.toMSecsSinceEpoch())
        {
            xMs = lastX * 1000;
            x = lastX;
        }
        double yLower = ui->graphPlotWidget->yAxis->range().lower - 1.0;
        double yUpper = ui->graphPlotWidget->yAxis->range().upper + 1.0;

        gUp->addData(x,yUpper);//change up and low at end
        gUp->addData(x,yLower);
        gLow->addData(x,yLower);
        gLow->addData(x,yUpper);
        ui->graphPlotWidget->replot();
        visCurrEnd = QDateTime::fromMSecsSinceEpoch((int)xMs);

        ui->graphPlotWidget->setCursor(Qt::OpenHandCursor);
        //calc segments
        segments->append(new QList<WaveSegment*>());
        for(QHash<DataContainer*,QCPGraph*>* hash : *rawCurves)
        {
            for(DataContainer* cont : hash->keys())
            {
                Wave* wve = (Wave*) cont;
                WaveSegment *segRes = splitWave(wve,visCurrStart,visCurrEnd);
                if(segRes != nullptr && segRes->isValid() && !segRes->getData().isEmpty())
                {
                    QList<WaveSegment *>* stepList = segments->at(segments->size()-1);
                    stepList->append(segRes);
                }else{
                    if(segRes != nullptr)
                    {
                        delete segRes;
                    }
                    qWarning() << QString("Invalid Wavsegment was omitted at %1 ms step %2").arg(visCurrStart.toMSecsSinceEpoch()).arg(segments->size());
                }
            }
        }
        selecting = false;
    }
}

void SplitDialog::fixedSplit()
{
    if( range > 0)
    {
        clearSegments();
        clearSelectGraphs();

        this->setEnabled(false);
        calcMinMaxDt();
        int currMs = maxStart.toMSecsSinceEpoch();
        int endMs = minEnd.toMSecsSinceEpoch();
        int stepCount = 0;
        while(currMs <= endMs)
        {
            QDateTime currTime = QDateTime::fromMSecsSinceEpoch(currMs);
            QDateTime currEndTime = QDateTime::fromMSecsSinceEpoch(currMs+intervalSze);
            segments->append(new QList<WaveSegment*>());
            for(QHash<DataContainer*,QCPGraph*>* hash : *rawCurves)
            {
                for(DataContainer* cont : hash->keys())
                {
                    QApplication::processEvents(QEventLoop::AllEvents,10);
                    Wave* wve = (Wave*) cont;
                    QDateTime start = wve->getStart();
                    QDateTime end = wve->getEnd();
                    //unequal first & last split, rest perfect match!
                    QDateTime segStart = currTime.msecsTo(start) <= 0 ? currTime : start;
                    QDateTime segEnd = currEndTime.msecsTo(end) >= 0 ? currEndTime : end;
                    WaveSegment *segRes = splitWave(wve,segStart,segEnd);
                    if(segRes != nullptr && segRes->isValid() && !segRes->getData().isEmpty())
                    {
                        QList<WaveSegment *>* stepList = segments->at(stepCount);
                        stepList->append(segRes);
                        addSplitGraph(segStart,segEnd);
                    }else{
                        if(segRes != nullptr)
                        {
                            delete segRes;
                        }
                        qWarning() << QString("Invalid Wavsegment was omitted at %1 ms step %2").arg(currMs).arg(stepCount);
                    }
                }
            }
            stepCount++;
            currMs += intervalSze;
        }
        this->setEnabled(true);
        qInfo() << QString("Finished splitting with %1 segments!").arg(stepCount);
        QMessageBox::information(this,"Teilen abgeschlossen",QString("Die Daten wurden in %1 Segmente geteilt").arg(stepCount));

    }else{
        qWarning() << "Invalid time-range while splitting!";
        QMessageBox::warning(this,"Fehler: Ungültige Daten!","Der Zeitbereich der Daten ist zu klein.\nEin Teilen ist daher nicht möglich!");
    }
}

void SplitDialog::frechetSplit()
{
    if( range > 0)
    {
        ui->graphPlotWidget->setInteractions( QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend | QCP::iSelectPlottables);
        EuclideanFrechetDataDistanceMetric metric = EuclideanFrechetDataDistanceMetric();
        clearSegments();
        clearSelectGraphs();

        this->setEnabled(false);
        calcMinMaxDt();
        int currMs = maxStart.toMSecsSinceEpoch();
        int endMs = minEnd.toMSecsSinceEpoch();
        int stepCount = 0;
        QMap<QDateTime,double> distances;//start of windows to their mean distance
        while(currMs <= endMs)
        {
            QDateTime currTime = QDateTime::fromMSecsSinceEpoch(currMs);
            QDateTime currEndTime = QDateTime::fromMSecsSinceEpoch(currMs+stepSze);
            QList<WaveSegment*> window;
            for(QHash<DataContainer*,QCPGraph*>* hash : *rawCurves)
            {
                for(DataContainer* cont : hash->keys())
                {
                    QApplication::processEvents(QEventLoop::AllEvents,10);
                    Wave* wve = (Wave*) cont;
                    QDateTime start = wve->getStart();
                    QDateTime end = wve->getEnd();
                    //unequal first & last split, rest perfect match!
                    QDateTime segStart = currTime.msecsTo(start) <= 0 ? currTime : maxStart;
                    QDateTime segEnd = currEndTime.msecsTo(end) >= 0 ? currEndTime : minEnd;
                    WaveSegment *segRes = splitWave(wve,segStart,segEnd);//temporary segment
                    if(segRes != nullptr && segRes->isValid() && !segRes->getData().isEmpty())
                    {
                        window.append(segRes);
                    }else{
                        delete segRes;
                        qWarning() << QString("Invalid Window-Wavsegment was omitted at %1 ms step %2").arg(currMs).arg(stepCount);
                    }
                }
            }
            //calc mean dist
            double meanDist = 0;
            int meanCnt = 0;
            for(WaveSegment* seg1 : window)
            {
                for(WaveSegment* seg2 : window)
                {
                    QApplication::processEvents(QEventLoop::AllEvents,10);
                    if(seg1 == seg2)
                    {
                        continue;
                    }else{
                        double dist = metric.semanticDistance(seg1,seg2);
                        if( !isnan(dist))
                        {
                            meanDist += dist;
                            meanCnt++;
                        }else{
                            qWarning() << "NaN Distance in FrechetSplit!";
                        }
                    }
                }
            }
            meanDist /=(double)meanCnt;
            if(!window.isEmpty()){//should be the same start for every member!
                distances.insert(window.at(0)->getStart(),meanDist);
            }else{
                qCritical() << "Empty window in FrechetSplit!";
            }
            stepCount++;
            currMs += stepSze;
        }
        QMap<QDateTime,double> lowestDistances;//Find segCnt windows with the lowest distances
        for(int i = 0 ; i < segCnt ; i++)
        {
            double currLowest = INFINITY;
            QDateTime currDt;
            for(QDateTime dt : distances.keys())
            {
                QApplication::processEvents(QEventLoop::AllEvents,10);
                double val =  distances.value(dt);
                if(!currDt.isValid() || currLowest > val)
                {
                    currLowest = val;
                    currDt = dt;
                }
            }
            lowestDistances.insert(currDt,currLowest);
            distances.remove(currDt);
        }

        int i = 0;
        QList<WaveSegment*>* startSeg = new QList<WaveSegment*>;
        QList<WaveSegment*>* endSeg = new QList<WaveSegment*>;
        for(QDateTime startDt : lowestDistances.keys())//Generate segments
        {
            QDateTime endDt = startDt.addMSecs(wndSze);
            QList<WaveSegment*> * lst = new QList<WaveSegment*>();
            segments->append(lst);
            for(QHash<DataContainer*,QCPGraph*>* hash : *rawCurves)
            {
                for(DataContainer* cont : hash->keys())
                {
                    QApplication::processEvents(QEventLoop::AllEvents,10);
                    Wave* wve = (Wave*) cont;
                    QDateTime start = wve->getStart();
                    QDateTime end = wve->getEnd();
                    QDateTime segStart = startDt.msecsTo(start) <= 0 ? startDt : maxStart;
                    if(i == 0 && ( maxStart.msecsTo(segStart) > range/100.0|| !startSeg->isEmpty()))//insert start segment if  more then 1% ignored
                    {
                        WaveSegment *segRes = splitWave(wve,maxStart,segStart);//start segment
                        if(segRes != nullptr && segRes->isValid() && !segRes->getData().isEmpty())
                        {
                            startSeg->append(segRes);
                            addSplitGraph(maxStart,segStart);
                        }else{
                            delete segRes;
                            qWarning() << QString("Invalid Start-Wavsegment was omitted at %1 ms at step %2").arg(currMs).arg(i);
                        }
                    }

                    QDateTime segEnd;
                    int nextIdx = lowestDistances.keys().indexOf(startDt) +1;
                    if( nextIdx < lowestDistances.size())//start of next (cheapest) window as end of current segment for all but the last segment
                    {
                        //FIXME add graphs
                        QDateTime nextDt = lowestDistances.keys().at(nextIdx);
                        segEnd = nextDt.msecsTo(end) >= 0 ? nextDt : minEnd;
                    }else{//the end of the current window for the last segment
                        segEnd = endDt.msecsTo(end) >= 0 ? endDt : minEnd;
                    }
                    if(i == lowestDistances.size() -1 && (minEnd.msecsTo(segEnd) || !endSeg->isEmpty()))
                    {
                        WaveSegment *segRes = splitWave(wve,segEnd,minEnd);//end segment
                        if(segRes != nullptr && segRes->isValid() && !segRes->getData().isEmpty())
                        {
                            endSeg->append(segRes);
                            addSplitGraph(segEnd,segEnd);
                        }else{
                            delete segRes;
                            qWarning() << QString("Invalid End-Wavsegment was omitted at %1 ms at step %2").arg(currMs).arg(i);
                        }
                    }
                    WaveSegment *segRes = splitWave(wve,segStart,segEnd);
                    if(segRes != nullptr && segRes->isValid() && !segRes->getData().isEmpty())
                    {
                        lst->append(segRes);
                        addSplitGraph(segStart,segEnd);
                    }else{
                        delete segRes;
                        qWarning() << QString("Invalid Wavsegment was omitted at %1 ms at step %2").arg(currMs).arg(i);
                    }
                }
            }
            i++;
        }
        if(!endSeg->isEmpty())
        {
            segments->append(endSeg);
        }else{
            delete endSeg;
        }
        if(!startSeg->isEmpty())
        {
            segments->prepend(startSeg);
        }else{
            delete startSeg;
        }

        this->setEnabled(true);
    }

    QMessageBox::information(this,"Segmentieren Abgeschlossen.",QString("Die Daten wurden nach Ähnlichkeit segmentiert.\n Die Segmente %1 können nun exporiert werden.").arg(segments->size()));

    ui->graphPlotWidget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend | QCP::iSelectPlottables);
}

void SplitDialog::visualSplit()
{
    calcMinMaxDt();
    selectFinished = false;
    selecting = false;
    clearSegments();
    clearSelectGraphs();

    ui->graphPlotWidget->setInteractions( QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend | QCP::iSelectPlottables);
    connect(ui->graphPlotWidget, &QCustomPlot::mouseMove, this, &SplitDialog::onSelectMove);
    connect(ui->graphPlotWidget, &QCustomPlot::mousePress, this, &SplitDialog::onSelectPress);
    connect(ui->graphPlotWidget, &QCustomPlot::mouseRelease, this, &SplitDialog::onSelectRelease);

    ui->graphPlotWidget->setCursor(Qt::OpenHandCursor);
    while(selectFinished == false || selecting == true)
    {
        QApplication::processEvents(QEventLoop::AllEvents,10);//wait for selections to finish
    }

    ui->graphPlotWidget->unsetCursor();
    disconnect(ui->graphPlotWidget, &QCustomPlot::mouseMove, this, &SplitDialog::onSelectMove);
    disconnect(ui->graphPlotWidget, &QCustomPlot::mousePress, this, &SplitDialog::onSelectPress);
    disconnect(ui->graphPlotWidget, &QCustomPlot::mouseRelease, this, &SplitDialog::onSelectRelease);
    ui->graphPlotWidget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend | QCP::iSelectPlottables);
}

int SplitDialog::calcDataRange()
{
    QDateTime start;
    QDateTime end;
    for(QHash<DataContainer*,QCPGraph*>* hash : *rawCurves)
    {
        for(DataContainer* cont : hash->keys())
        {
            const QDateTime localStart = cont->getStart();
            const QDateTime localEnd = cont->getEnd();
            if(!start.isValid() || start.msecsTo(localStart) < 0)
            {
                start = localStart;
            }
            if(!end.isValid() || end.msecsTo(localEnd) > 0)
            {
                end = localEnd;
            }
        }
    }
    if(start.isValid() && end.isValid())
    {
        return  (start.msecsTo(end));
    }else{
        return -1;
    }
}

void SplitDialog::clearSegments(bool delSegs)
{
    if(segments != nullptr)
    {
        for(QList<WaveSegment*>* lst: *segments)
        {
            if(delSegs == true)
            {
                for(WaveSegment* seg: *lst)
                {
                    delete seg;
                }
            }
            delete lst;
        }
        segments->clear();
    }
}

void SplitDialog::clearSelectGraphs()
{
    for(QPair<QCPGraph*,QCPGraph*>* g : selectGraphs)
    {
        ui->graphPlotWidget->removeGraph(g->first);
        ui->graphPlotWidget->removeGraph(g->second);
    }
    ui->graphPlotWidget->replot();
}

void SplitDialog::addSplitGraph(QDateTime start, QDateTime end)
{
    if(start.isValid() && end.isValid())
    {
        double x1 = start.toMSecsSinceEpoch() / 1000.0;
        double x2 = end.toMSecsSinceEpoch() / 1000.0;
        QCPGraph* gLow = ui->graphPlotWidget->addGraph(ui->graphPlotWidget->xAxis, ui->graphPlotWidget->yAxis);
        QPair<QCPGraph*,QCPGraph*>* pair = new QPair<QCPGraph*,QCPGraph*>();
        pair->first = gLow;
        QCPGraph* gUp = ui->graphPlotWidget->addGraph(ui->graphPlotWidget->xAxis, ui->graphPlotWidget->yAxis);
        pair->second = gUp;
        selectGraphs.append(pair);

        QColor selCol = QColor(0xaa, 0xaa, 0xff, 5);//color as in viewer
        QBrush brush = QBrush(selCol);
        gLow->setPen(QPen(QColor(Qt::black)));
        gUp->setPen(QPen(QColor(Qt::black)));
        gLow->setVisible(true);
        gUp->setVisible(true);
        gLow->setSelectable(QCP::stNone);
        gUp->setSelectable(QCP::stNone);
        gLow->setChannelFillGraph(gUp);
        gUp->setChannelFillGraph(gLow);
        gLow->setBrush(brush);
        gUp->setBrush(brush);
        ui->graphPlotWidget->setCursor(Qt::ClosedHandCursor);
        selecting = true;
        double yLower = ui->graphPlotWidget->yAxis->range().lower - 1.0;
        double yUpper = ui->graphPlotWidget->yAxis->range().upper + 1.0;

        gUp->addData(x1,yLower);
        gUp->addData(x1,yUpper);
        gLow->addData(x1,yUpper);
        gLow->addData(x1,yLower);

        double xD = x1 + abs(x1-x2)/2.0;
        gUp->addData(xD,yUpper);
        gLow->addData(xD,yLower);


        gUp->addData(x2,yUpper);//change up and low at end
        gUp->addData(x2,yLower);
        gLow->addData(x2,yLower);
        gLow->addData(x2,yUpper);
        ui->graphPlotWidget->replot();
    }else{
        qWarning() << "Invalid DT for split-graph! Ingoring it.";
    }
}

WaveSegment *SplitDialog::splitWave(Wave *wave, QDateTime start, QDateTime end)
{
    if(wave == nullptr || !wave->isValidData() || !start.isValid() || !end.isValid())
    {
        return nullptr;
    }else{
        return new WaveSegment(wave,start,end);
    }
}

void SplitDialog::calcMinMaxDt()
{
    maxStart = QDateTime();
    minEnd = QDateTime();
    // get min start & end  of all waves
    for(QHash<DataContainer*,QCPGraph*>* hash : *rawCurves)
    {
        for(DataContainer* cont: hash->keys())
        {
            if(!maxStart.isValid() || maxStart.msecsTo(cont->getStart()) > 0)
            {
                maxStart = cont->getStart();
            }
            if(!minEnd.isValid() || minEnd.msecsTo(cont->getEnd()) < 0)
            {
                minEnd = cont->getEnd();
            }
        }
    }
}
bool SplitDialog::exportSegment(WaveSegment *seg, QString dir,int segmentId,int waveId)
{
    if(!dir.isEmpty())
    {
        if(seg->getParentWave() == nullptr)
        {
            qCritical() << "Nullptr parentwave in export!";
            return false;
        }
        QString fleName = QString("Wve%1-Seg%2-").arg(waveId).arg(segmentId) + seg->getParentWave()->getName() +  ".csv";
        QFile fle(dir+fleName);
        if(fle.open(QIODevice::WriteOnly))
        {
            QUuid id = seg->getParentWave()->getId();
            QString idStr = "<discardedID>";
            if(!id.isNull())
            {
                idStr = id.toString(QUuid::WithoutBraces);
            }
            QTextStream out(&fle);
            //FIXME make seperator configurable!
            QString head = QString("% Patient: %1 ;\nStartdatum: %2;\n%3;%4").arg(idStr).arg(seg->getParentWave()->getStart().toString(Qt::ISODateWithMs)).arg("Zeit [s]").arg(seg->getUnit());
            out <<head ;

            seg->calcData();
            QMap<QDateTime, DataPoint *> data = seg->getData();
            if(data.isEmpty())
            {
                fle.close();
                return false;
            }
            for(QDateTime dt: data.keys()){
                double key = dt.toMSecsSinceEpoch()/1000.0;
                double val = data.value(dt)->getValue();
                QString outStr = QString("\n%1;%2").arg(key).arg(val);
                out << outStr;
            }
            out.flush();
            fle.close();

            return true;
        }else{
            qWarning() << QString("Failed to open file for writing %1. Error: %1").arg(dir+fleName).arg(fle.errorString());
            QMessageBox::warning(this,"Fehler beim Exportieren!","Datei kann nicht geschrieben werden.");
            return false;
        }
    }else{
        qWarning() << QString("Failed to export split to dir %1").arg(dir);
        QMessageBox::warning(this,"Fehler beim Exportieren!","Ungültiges Exportverzeichnis.");
        return false;
    }
}

void SplitDialog::on_buttonBox_clicked(QAbstractButton *button)
{
    if(button == ui->buttonBox->button(QDialogButtonBox::Save))
    {
        exportData();
    }else if(button == ui->buttonBox->button(QDialogButtonBox::Apply))
    {
        QMessageBox::StandardButton warnRes = QMessageBox::warning(this,"Achtung!","Das Übernehmen der Segmente überschreibt die vorher geladenen Kurven im Hauptfenster.\nAußerdem können die Segmente nicht gespeichert werden, wenn diese nicht bereits gespeichert wurden!",QMessageBox::Ok | QMessageBox::Cancel);
        if(warnRes == QMessageBox::Ok)
        {
            this->setEnabled(false);
            applyData();
            this->setEnabled(true);
            QMessageBox::information(this,"Erfolg.","Die Segmente wurden übernommen.");
            this->accept();
        }
    }else if(button == ui->buttonBox->button(QDialogButtonBox::Discard))
    {
        this->reject();
    }else{
        qFatal("Unknown Button in SplitDialog::ButtonBox!");
    }
}

QCPGraph *SplitDialog::convertWave(DataContainer *cont, QCustomPlot *plot)
{
    if(plot == nullptr || cont == nullptr)
    {
        return nullptr;
    }else{
        QMap<QDateTime,DataPoint*> data = cont->getData();
        QCPGraph *g = plot->addGraph();
        for(QDateTime dt : data.keys())
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents,1);
            double key = dt.toMSecsSinceEpoch()/1000.0;
            double val = data.value(dt)->getValue();
            g->addData(key,val);
        }
        if(cont->isInterpolateable() == false)
        {
            g->setLineStyle(QCPGraph::LineStyle::lsStepLeft);
        }else{
            g->setLineStyle(QCPGraph::LineStyle::lsLine);
        }
        return g;
    }
}

QPair<DataContainer *, QCPGraph *> SplitDialog::convertWave(WaveSegment *seg, QCustomPlot *plot)
{
    if(plot == nullptr || seg == nullptr)
    {
        return QPair<DataContainer*,QCPGraph*>();
    }else{
        QMap<QDateTime,DataPoint*> data = seg->getData();
        QCPGraph *g = plot->addGraph();
        for(QDateTime dt : data.keys())
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents,1);
            double key = dt.toMSecsSinceEpoch()/1000.0;
            double val = data.value(dt)->getValue();
            g->addData(key,val);
        }
        QPair<DataContainer*,QCPGraph*> ret;
        ret.first = (DataContainer*) seg;
        ret.second = g;
        return ret;
    }
}

void SplitDialog::applyData()
{
    for(QHash<DataContainer*,QCPGraph*>* hash : *rawCurves)
    {
        for(DataContainer* cont : hash->keys())
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents,1);
            parentPlot->removeGraph(hash->value(cont));
            delete cont;
        }
        delete hash;
    }
    rawCurves->clear();
    int i = 0;
    for(QList<WaveSegment*> *lst : *segments)
    {
        QHash<DataContainer*,QCPGraph*>* hash = new  QHash<DataContainer*,QCPGraph*>();
        for(WaveSegment* seg :*lst)
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents,10);
            QPair<DataContainer *, QCPGraph *> pair = convertWave(seg,parentPlot);
            hash->insert(pair.first,pair.second);
            parentPlot->replot();
        }
        rawCurves->insert(i,hash);
        i++;
    }
    emit segmentsApplied();
}

void SplitDialog::exportData()
{
    if(selectFinished == false)
    {
        selectFinished = true;
    }
    QString targetDir = QFileDialog::getExistingDirectory(this,"Wählen Sie ein Zielverzeichnis für den Export.",QDir::homePath())+ QDir::separator();
    QFileInfo fi(targetDir);
    if(targetDir.isEmpty() || targetDir =="/" ||  !fi.isWritable())
    {
        QMessageBox::warning(this,"Kein Exportverzeichnis gewählt!","Es wurde kein (gültiges) Verzeichnis für den Export der Segmente gewählt!");
        return;
    }
    bool success = !segments->isEmpty();
    int j = 0;
    for(QList<WaveSegment*>* lst :*segments)
    {
        int i = 0;
        for(WaveSegment* seg: *lst)
        {
            bool res =  exportSegment(seg,targetDir,j,i++);;
            success = success && res;
        }
        j++;
    }
    if(success == true)
    {
        qInfo() << "Finished export of segments!";
        QMessageBox::information(this,"Export abgeschlossen.","Der Export der Segmente wurde erfolgreich beendet.");
    }else{
        qWarning() << "Failed to export some segments!";
        QMessageBox::warning(this,"Export Fehlgeschlagen!","Einige Segmente konnten ggf. nicht gespeichert werden!");
    }
}

void SplitDialog::on_frechetButton_toggled(bool checked)
{
        ui->segCntLabel->setVisible(checked);
        ui->segCntSpinbox->setVisible(checked);
        ui->wndSzeLabel->setVisible(checked);
        ui->wndSzeSpinbox->setVisible(checked);
        ui->stepSzeLabel->setVisible(checked);
        ui->stepSzeSpinbox->setVisible(checked);

        ui->intervalLabel->setVisible(!checked);
        ui->intervalSpinbox->setVisible(!checked);
}

void SplitDialog::on_visualButton_toggled(bool checked)
{
    ui->segCntLabel->setVisible(!checked);
    ui->segCntSpinbox->setVisible(!checked);
    ui->wndSzeLabel->setVisible(!checked);
    ui->wndSzeSpinbox->setVisible(!checked);
    ui->stepSzeLabel->setVisible(!checked);
    ui->stepSzeSpinbox->setVisible(!checked);
    ui->intervalLabel->setVisible(!checked);
    ui->intervalSpinbox->setVisible(!checked);
}

void SplitDialog::on_fixedButton_toggled(bool checked)
{

    ui->segCntLabel->setVisible(!checked);
    ui->segCntSpinbox->setVisible(!checked);
    ui->wndSzeLabel->setVisible(!checked);
    ui->wndSzeSpinbox->setVisible(!checked);
    ui->stepSzeLabel->setVisible(!checked);
    ui->stepSzeSpinbox->setVisible(!checked);

    ui->intervalLabel->setVisible(checked);
    ui->intervalSpinbox->setVisible(checked);

}

void SplitDialog::on_segCntSpinbox_valueChanged(int arg1)
{
    segCnt = arg1;
    ui->wndSzeSpinbox->setMaximum(floor(range/(double)segCnt));
    ui->wndSzeSpinbox->setValue(floor(range/(double)segCnt));
}

void SplitDialog::on_wndSzeSpinbox_valueChanged(int arg1)
{
    wndSze = arg1;
    ui->stepSzeSpinbox->setMaximum(wndSze +1);
    ui->stepSzeSpinbox->setValue((int)round(wndSze / 2.0));
}

void SplitDialog::on_stepSzeSpinbox_valueChanged(int arg1)
{
    stepSze = arg1;
}

void SplitDialog::on_intervalSpinbox_valueChanged(int arg1)
{
    intervalSze = arg1;
}
