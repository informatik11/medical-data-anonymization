#include "wave.h"

Wave::Wave(QString name, QString source, Wave::WaveType_t type, QString unit, QList<DataPoint *>* data, QString patId, bool interpolatable, Wave* baseWave, Wave_Trend_Conf_t* trend, bool derivative)
{
    this->name = name;
    mSource = source;
    this->type = type;
    this->unit = unit;
    this->m_isInterpolateable = interpolatable;
    this->trendConf = trend;
    this->m_isDerivative = derivative;
    this->baseWave = baseWave;
    this->id = QUuid::fromString(patId);
    genDataMap(data);
    calcStartEnd();
}

Curve Wave::toFrechetCurve()
{
    Points pts = Points(2);
    QList<QDateTime> keys = data.keys();
    for(int i = 0 ; i< keys.size() ; i++)
    {
        Point tmp(2);
        tmp.set(0,keys.at(i).toMSecsSinceEpoch());//FIXME OVERFLOW /precision ?
        tmp.set(1,data.value(keys.at(i))->getValue());
        pts.add(tmp);
    }
    Curve ret(pts,getName().toStdString());
    return ret;
}

QString Wave::getName() const
{
    return name;
}

Wave::WaveType_t Wave::getType() const
{
    return type;
}

QString Wave::getUnit() const
{
    return unit;
}

QUuid Wave::getId() const
{
    return id;
}


Wave * Wave::calcDerivative()
{
    QList<QDateTime> keys = data.keys();
    QList<DataPoint*>* derivDat = new QList<DataPoint*>();
    for(int i = 0;i< keys.size();i++)
    {
        double slope = slopeAt(keys.at(i));
        DataPoint* dp = new DataPoint(keys[i],slope,!isnan(slope));
        derivDat->append(dp);
    }
    return new Wave(getName() + " dx/dy ",mSource,type,unit +  " dx/dy ",derivDat,this->getId().toString(QUuid::WithBraces),isInterpolateable());
}

Tangent *Wave::calcLinTrend(DataPoint *startPoint)
{
    Tangent* tang = new Tangent(startPoint,this->slopeAt(startPoint->getTime()));
    if(tang->isValid())
    {
        return tang;
    }else{
        delete tang;
        return nullptr;
    }
}

Polynomial *Wave::calcPolyTrend(DataPoint* startPoint, uint16_t horizon, uint8_t order)
{
    QList<DataPoint*> *rawRefData = new QList<DataPoint*>();
    QString polyName = getName() + QString("-PolyTrend H%1 - O%2 - S%3").arg(horizon).arg(order).arg(startPoint->getTime().toString(Qt::ISODateWithMs));
    Wave_Trend_Conf_t* conf = new Wave_Trend_Conf_t();
    conf->fluct = Wave_Trend_Fluctuation_t::WAVE_TREND_FLUCTUATION_NONE;
    conf->type = WAVE_TREND_TYPE_POLYNOMIAL;
    conf->order = order;
    conf->horizon = horizon;

    for(int i = 0 ; i< horizon; i++)
    {

        DataPoint *next = getNext(startPoint);
        if(next != nullptr )
        {
            rawRefData->append(next);
        }else{
            conf->horizon = i+1;
            break;
        }

    }

    Wave* refData = new Wave(polyName,mSource,type,"PolyTrend",rawRefData,getId().toString(QUuid::WithoutBraces),isInterpolateable(),this,conf,false);

    DataPoint* refZero = this->getData().value(this->start);//FIXME check refZero

    return new Polynomial(refData,refZero,order);
}

Wave *Wave::calcRMeanTrend(uint8_t order)
{
    if(order > data.size())
    {
        return nullptr;
    }else{

        QList<DataPoint*>* trendData = new QList<DataPoint*>();

        for(int i = 0 ; i < data.size();i++)
        {
            double valSum = 0;
            qint64 keySum = 0;
            for(int j = 0; j < order; j++)
            {
                double currVal = data.values().at(i + j)->getValue();
                if(isnan(currVal) || !data.values().at(i + j)->getTime().isValid())
                {
                    valSum = nan("");
                    break;
                }else{
                    valSum += currVal;
                    keySum += data.values().at(i + j)->getTime().toMSecsSinceEpoch() - data.values().at(i)->getTime().toMSecsSinceEpoch(); //only delta t from start!
                }
            }
            //FIXME check start/end -- even/odd
            double mean = valSum/ keySum; //FIXME check type stuff!
            uint8_t center = (uint8_t)(order/2) + 1;
            DataPoint* dp = new DataPoint(data.values().at(i + center)->getTime(),mean,!isnan(valSum));//valid if all points of the mean were valid!
            trendData->append(dp);
        }
        Wave_Trend_Conf_t* conf = new Wave_Trend_Conf_t();
        conf->fluct = Wave_Trend_Fluctuation_t::WAVE_TREND_FLUCTUATION_NONE;
        conf->type = WAVE_TREND_TYPE_RUNNING_MEAN;
        conf->order = order;
        conf->horizon = order;
        return new Wave(getName()+ QString("_%1-Mean-Trend").arg(order),mSource,getType(),getUnit(),trendData,getId().toString(QUuid::WithoutBraces),isInterpolateable(),this,conf);
    }
}

Wave *Wave::calcExp()
{
    qFatal("Unimplemented Wave::calcExp()");
//FIXME IMPLEMENT
}

uint8_t Wave::calcDerivOrder()
{
    if(m_isDerivative)
    {
        return (getBaseWave()->calcDerivOrder() +1);
    }else{
        return 0;
    }
}

void Wave::genDataMap(QList<DataPoint *>* p)
{
    for(int i = 0;i< p->size();i++)
    {
        data.insert(p->at(i)->getTime(),p->at(i));
    }

}

Wave::Wave_Trend_Conf_t Wave::getTrendConf() const
{
    return *trendConf;
}

Wave *Wave::getBaseWave() const
{
    return baseWave;
}

bool Wave::isTrend() const
{
    return (trendConf != nullptr);
}

Wave *Wave::getRootWave() const
{
    Wave* wave = getBaseWave();
    if(wave == nullptr)
    {
        return (Wave*) this ;
    }else{
        return wave->getRootWave();
    }
}

bool Wave::isDerivative() const
{
    return m_isDerivative;
}
