#ifndef BUCKET_H
#define BUCKET_H
#include <QMap>
#include <QDebug>
#include "distribution.h"
#include "datacontainer.h"
#include "datadistancemetric.h"




/**
 * @brief The Bucket class all values in a bucket are considered identicall for the sake of the distribution.
 * They are semantically similar, as given be the maximum semantic distance eps
 * Each bucket has a distribution and a reference value. The reference value must be in this distribution.
 * It is the reference point for all other values in the bucket.
 */
class Bucket
{

public:

    /**
     * @brief Bucket use the explicit copy constructor
     * @param copy
     */
    Bucket(Bucket &copy) = delete;

    /**
     * @brief Bucket
     * @param copy
     * @param copyData if this is false everything except the data  is copied from the distribution AND the Bucket itself
     */
    Bucket(Bucket* copy,bool copyData = true);

    /**
     * @brief Bucket creates a new bucket, which contains only the reference value.
     * All pointer should not be null the Eps values can be zero but this is not adviceable.
     * @param refValue The reference value must be in this distribution. It is the reference point for all other values in the bucket. Is added to the distribution if not already present
     * @param maxCost  How far can the containers be from the reference value w.r.t. cost.
     * @param metric How the distance along the time and value axis and possibly semantic is measured
     * @param distr The reference distribution, how the containers should be taken out from the bucket and where the added values to the bucket are inserted (as an obeservation for the reference value)
     * Is copied DOES NOT TAKE OWNERSHIP
     * @param copyDistribData if this is false everything except the data is copied from the distribution
     *
     */
    Bucket(DataContainer* refValue, double maxCost, DataDistanceMetric* metric,Distribution* distr,bool copyDistribData = true);

    /**
     * @brief Bucket creates an EMPTY bucket
     * @param maxCost
     * @param metric
     * @param distr The reference distribution, how the containers should be taken out from the bucket and where the added values to the bucket are inserted (as an obeservation for the reference value)
     * Is copied DOES NOT TAKE OWNERSHIP
     * @param copyDistribData if this is false everything except the data is copied from the distribution
     */
    Bucket(double maxCost, DataDistanceMetric* metric,Distribution* distr,bool copyDistribData = true);



    /**
     * @brief destructor of the bucket, deletes the copy of distr
     **/
    ~Bucket();

    /**
     * @brief isValid checks if the refValue and distribution is not null and no constraint value is NaN
     * @return true if the Bucket can be used
     */
    bool isValid();

    /**
     * @brief add Inserts the given container to the bucket and an observation to the distribution.
     * If the constraints are fullfilled.
     * Sets the val as refKey if the key is not already set
     * @param val
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @param dontChangeDistrib does not add anything to the distribution if this is true, e.g. rootbucket
     * @return true if the container was added to the bucket, false if the constraints were not fullfilled, or the val is already in the bucket
     */
    bool add(DataContainer* val,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr,bool dontChangeDistrib = false);

    /**
     * @brief remove Deletes the given container from the bucket and one observation from the distribution.
     * IS NOT A TAKE FUNCTION! I.e. the container is deleted, like it was never in the bucket, not taken out and keeps beeing taken into consideration w.r.t. the total count!
     * @param val
     * @return true if the container was actually in the bucket and could be removed. False otherwise (e.g. bucket did not contain the given value)
     */
    bool remove(DataContainer* val);

    /**
     * @brief reset adds all taken containers back to the data-list
     * Can be used to recalculate EQCs with new parameters.
     * Overwrites existing data-entries, that were added after one with an identical key was taken.
     */
    void reset();

    /**
     * @brief revert undos the last take, if possible
     * Will overwrite existing data-entries that were added after the last take!
     * THIS WOULD MAKE FULLCOUNT INVALID; SO CHECK THIS BEFOREHAND TODO automate this
     * @return true if there was a take to undo. false if there was nothing to undo
     */
    bool revert(); //TODO make revertbuffer (ring) => searchtree & check if overwriting

    /**
     * @brief clear clears data & takenData
     */
    void clear();

    /**
     * @brief clearData clears the data
     */
    void clearData();

    /**
     * @brief clearTaken clears the taken data
     */
    void clearTaken();

    /**
     * @brief getFullCount How many elements were in the bucket when it was full
     * @return
     */
    uint32_t getFullCount();

    /**
     * @brief size how many elements are currently in the bucket
     * @return
     */
    int size();

    /**
     * @brief getRefKey
     * @return
     */
    DataContainer *getRefKey() const;
    /**
     * @brief calcMaxCost calculates the maximum cost of the virtual bucket (for size calculation)
     * I.e. Earth mover distance for the worst case, which is the sum of the ground distance * flow
     * Might be computational expensive for large buckets. So it is a good idea to cache this value until the bucket is modified.
     * @param data the virtual bucket
     * @param fullCount size of the bucket (all observations)
     * @param distr the corresponding distribution (of the population!)
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return the maximum cost, or NaN if an error occured.
     */
    static double calcMaxCost(QList<DataContainer*> data,unsigned int fullCount, Distribution* distr,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    /**
     * @brief calcMaxCost calculates the maximum cost of the bucket
     * I.e. Earth mover distance for the worst case, which is the sum of the ground distance * flow
     * Might be computational expensive for large buckets. So it is a good idea to cache this value until the bucket is modified.
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return the maximum cost,infinity if the bucket is empty. NaN if an error occured.
     */
    double calcMaxCost(QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    /**
     * @brief getMaxDist the maximum distance between elements of the bucket.
     * @return NaN if the bucket is empty
     */
    double getMaxDist();

    /**
     * @brief isUninitalized
     * @return true if the refKey is not set
     */
    bool isUninitalized();

    /**
     * @brief isEmpty
     * @return true if the refkey is set, but no element is remaining, which could be taken, and there were some taken.
     */
    bool isEmpty();

    /**
     * @brief setRefKey sets the reference key
     * @param newRefKey must be in the bucket
     * @return true if the key was set, false if an error occured. i.e. the key is not in the bucket or nullptr
     */
    bool setRefKey(DataContainer *newRefKey);


    /**
     * @brief getMetric
     * @return
     */
    DataDistanceMetric *getMetric() const;

    /**
     * @brief getDistribution
     * @return
     */
    Distribution* getDistribution() const;

    /**
     * @brief at provides access to the elements for iteration
     * @param i
     * @return the element if the index was correct. Nullptr otherwise
     */
    DataContainer* at(int i);

    /**
     * @brief take
     * @param cont removes the given container from the data set and adds it to the taken data AND LastTaken. Does nothing if the container is not a member of the bucket.
     * You have toe rest the lastTaken manually if your "move" ends
     */
    void take(DataContainer* cont);

    /**
     * @brief resetLastTaken resets the last taken data
     */
    void resetLastTaken();

    /**
     * @brief getCostLimit
     * @return
     */
    double getCostLimit() const;

    /**
     * @brief getData
     * @return
     */
    QHash<DataContainer *, double> getData() const;

    /**
     * @brief genDistrib generates the distribution for the given buckets
     * Each bucket is referenced by its reference key. I.e. two keys k1 and k2 in the same bucket have the same propability in the distribution.
     * The distance metric of the distribution of the first bucket is used. THEY SHOULD HAVE ALL THE SAME!!!
     * V ist the maximum distance within the buckets
     * @param buckets must be distinct buckets, but is not checked. However the resulting distribution might not be sound if buckets occur multiple times, as the older entry in the distribution is overwritten.
     * @return the generated distribution. Or nullptr if an error occured.
     */
    static Distribution* genDistrib(QList<Bucket*>* buckets);

    /**
     * @brief genDistrib generates the distribution for the given virtual buckets
     * @param virtBuckets Is a map from refValue to number of oberservations
     * @param metric the distance metric used for the distribution.
     * @param v the maximum distance for grouping the keys
     * @param interpolatable is the data of the distribution interpolatable
     * @return the generated distribution, non interpolatable virtual distribution. Or nullptr if an error occured.
     */
    static Distribution* genDistrib(QHash<DataContainer*, unsigned int> virtBuckets,DataDistanceMetric* metric,double v,bool interpolatable = false);




private:

    /**
     * @brief calcMaxDist recalculates the maximum distance to the refkey e.g. if a key was removed
     * @return
     */
    double calcMaxDist();

    /**
     * @brief fullFillsConstraints Helper function , checking if the given datacontainer is within the constraints, defined for the bucket
     * The *maxCost parameters can be used to save expensive recalutation of distances between containers!
     * Does not calc the maxCost if the costlimit of the bucket is infinity
     * @param val
     * @param maxCost Is set to the calculated maxCost, if not a nullptr.
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return True if the constraints would hold when adding the container to the bucket
     */
    bool fullFillsConstraints(DataContainer* val,double* maxCost,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    ///! Mapping the datacontainer to the distance to the reference object
    QHash<DataContainer*,double> data;

    ///! Mapping the datacontainer to the distance to the reference object, the already taken data is stored here for reset purposes
    QHash<DataContainer*,double> takenData;

    ///! For reverts TODO make this a ringbuffer to allow multiple reverts
    QList<DataContainer*> lastTakenData;

    ///! The value all items in the bucket are counted to in the distribution
    DataContainer* refKey = nullptr;

    ///! How many items were in the bucket when it was full
    uint32_t fullCount = 0;

    ///! How how large can the cost be at most within the bucket
    double costLimit = 0;

    /// The maximum distance to the reference key
    double maxDist = nan("");

    ///! Reference the corresponding distribution
    Distribution* distr = nullptr;

    ///! Defining how the distance is measured for this bucket, should be the same for all buckets of the same distribution
    DataDistanceMetric *metric = nullptr;


};

#endif // BUCKET_H
