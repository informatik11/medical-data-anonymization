#ifndef DATACONTAINER_H
#define DATACONTAINER_H


#include <QMap>
#include <QObject>
#include <limits>
#include <math.h>
#include <QDebug>
#include <QApplication>
#include "datapoint.h"
#include "tangent.h"
#include "frechet/curve.hpp"


#define CONF_HOLD_VALUE true // holds the old value for not-interpolatable curves until a new arrives. otherwise the closest is taken

/**
 * @brief The DataContainer class all classes which store sets of data are derived from this.
 * This is used to provide an easy way to measure informationloss and distances for all of this children.
 */
class DataContainer
{
public:




    /**
     * @brief findNext
     * @param key
     * @return  the next datapoint to the given key, nullptr if the datacontainer was empty or the key is invalid
     */
    virtual DataPoint* findNext(QDateTime key);
    /**
    * @brief findPrevious
    * @param key
    * @return  the previous datapoint to the given key, nullptr if the datacontainer was empty or the key is invalid
    */
   virtual DataPoint* findPrevious(QDateTime key);

    /**
     * @brief findClosestNeighbor like findClosest, but does not return the value for the given key.
     * @param key
     * @return  the closest w.r.t. time datapoint to the given key, but not itself! nullptr if the datacontainer was empty or the key is invalid
     */
    DataPoint* findClosestNeighbor(QDateTime key);//TODO findclosest, -prev , -next could be one higher order function

    /**
     * @brief findClosest
     * @param key
     * @return  the closest w.r.t. time datapoint to the given key, nullptr if the datacontainer was empty or the key is invalid
     */
     DataPoint* findClosest(QDateTime key);//TODO findclosest, -prev , -next could be one higher order function


     /**
      * @brief remove removes the given DP and updates start/end. Does not delete the DP.
      * @param dp the datapoint which will be removed
      * @return true if the given datapoint was removed. false if the given dp was not in the datacontainer
      */
     bool remove(DataPoint* dp);

    /**
     * @brief find
     * @param key
     * @return the corresponding datapoint, nullptr if none was found or the key is invalid
     */
    virtual DataPoint* find(QDateTime key);
    /**
     * @brief getSize
     * @return
     */
    virtual unsigned int  getSize();

    /**
     * @brief getData
     * @return
     */
    virtual const QMap<QDateTime,DataPoint*> getData();

    /**
     * @brief getStart
     * @return
     */
    virtual const QDateTime getStart();

    /**
     * @brief getEnd
     * @return
     */
    virtual const QDateTime getEnd();


    /**
     * @brief getNext finds current and returns the following datapoint, if there is one
     * @param current the pointer is compared not its value!!
     * @return nullptr if current was not found or there is no next datapoint
     */
    virtual DataPoint* getNext(DataPoint* current);//FIXME more efficient with a datastructure supporting find?

    /**
     * @brief getPrev finds current and returns the previous datapoint, if there is one
     * @param current the pointer is compared not its value!!
     * @return nullptr if current was not found or there is no previous datapoint
     */
    virtual const DataPoint* getPrev(DataPoint* current);//FIXME more efficient with a datastructure supporting find?

    /**
    * @brief reverseFind finds the key for a given value.
    * This works as the value should be unique as each Datapoint shuld be only once in each container!
    * Data must be valid for this to work
    * @param dp
    * @return the empty DateTime if nothing was found
    */
   virtual  QDateTime reverseFind(DataPoint* dp);

    /**
     * @brief isValidData
     * @return true if the data does not contain douplicate datapoints or nullptrs  (as key OR value) and the key matches the time in the value
     */
    virtual bool isValidData();

       /**
      * @brief slopeAt the closest key is taken if there is no exact match!
      * @param key
      * @return the (numerical) derivative at the given point. nan if this is not possible, e.g. the value is not valid at the given key
      */
    virtual double slopeAt(QDateTime key);

     /**
      * @brief valueAt
      * @param key
      * @param stayInRange if this is true, the value is not interpolated beyond start/end-time. Usefull for e.g. hull-calculation
      * @return the value at the given key.  NaN if the key does not correspond to a valid value and the wave is not interpolateable!
      */
     virtual double valueAt(QDateTime key,bool stayInRange = false);

     /**
      * @brief isInterpolateable
      * @return
      */
     bool isInterpolateable() const;

     /**
      * @brief getSource
      * @return
      */
     QString getSource() const;

     /**
      * @brief toFrechetCurve
      * Converts the container to a curve, compatible with the FRED-Frechet API
      * Does not name the curve
      * @return
      */
     virtual Curve toFrechetCurve();

    /**
     * @brief DataContainer creates an empty DataContainer
     * @param isInterpolatable
     */
    DataContainer(bool isInterpolatable = true, QString source = "");

    /**
     * @brief DataContainer
     * @param data calculates start &  end from it
     * @param isInterpolatable
     */
    DataContainer(QMap<QDateTime,DataPoint*> data, bool isInterpolateable, QString source = "");

    /**
     * @brief DataContainer copyconstructor
     * @param cpy
     */
    DataContainer(DataContainer* cpy);

protected:

     /**
     * @brief findClosestHelper Helper function for findClosest and findClosest neighbor
     * @param key
     * @param retKey true if the datapoint for the key should be returned if present, false if the next neighbor is searched
     * @return  the closest w.r.t. time datapoint to the given key, but not itself if retKey = false! nullptr if the datacontainer was empty or the key is invalid
     */
    virtual DataPoint* findClosestHelper(QDateTime key, bool retKey);

    /**
     * @brief calcStartEnd calculates start and end time from the current data
     */
     virtual void calcStartEnd();


    /**
     * @brief indexOf finds the given value and returns its index.
     * @param val the pointer is compared not its value!!
     * @return -1 if the value was not found
     */
    virtual int indexOf(DataPoint* val);


    bool m_isInterpolateable = false;

    ///! Stores the source of the data (e.g. filename)
    QString mSource;

    ///! Stores the data of the container
    QMap<QDateTime,DataPoint*> data;

    ///! Earliest data
    QDateTime start;

    ///! Latest data
    QDateTime end;
signals:

};

#endif // DATACONTAINER_H
