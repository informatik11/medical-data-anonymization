#ifndef DISTRIBUTIONDISTANCEMETRIC_H
#define DISTRIBUTIONDISTANCEMETRIC_H

#include "distribution.h"



/**
 * @brief The DistributionDistanceMetric class defining the interface for metrics between distributions avoiding a circular dependency with the DistanceMetric interface and distribution
 * Is used to measure distance BETWEEN Distributions NOT IN Distributions!!!!
 */
class DistributionDistanceMetric
{
public:

    /**
     * @brief error how precise is the distance
     * @return
     */
    virtual double error() = 0;


    /**
      * @brief distance
      * @param w1
      * @param w2
      * @param ignoreNonExist true if only keys that are present in both distributions should be used.
      * Otherwise a non existing key is the same as a key with 0  occurences.
      * @return the distance between all  datapoints in the given distributions
      */
    virtual double distance(Distribution* d1, Distribution* d2, bool ignoreNonExist = false) = 0;

    /**
     * @brief getName
     * @return
     */
    QString getName() const;

protected:

    /// The name of the distancemetric, for visualization purposes
    QString name = "Distribution distance metric BASECLASS";

};

inline QString DistributionDistanceMetric::getName() const
{
    return name;
}

#endif // DISTRIBUTIONDISTANCEMETRIC_H
