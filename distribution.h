#ifndef DISTRIBUTION_H
#define DISTRIBUTION_H

#include "datacontainer.h"
#include "datadistancemetric.h"

#include <QMap>
#include <QObject>
#include <cmath>

/**
 * @brief The Distribution class represents the distribution of a sensitive attribute, e.g. in a population or equivalence class
 */
class Distribution
{
public:
    ///Holds the data of a distribution cluster, which groups together similar datacontainers for the generation of a distribution
    typedef struct Distribution_cluster_t{
        ///The rawdata
        QList<DataContainer*>* data = new QList<DataContainer*>();
        ///The reference for the cluster
        DataContainer* refKey = nullptr;
        ///The minimal (left) key of the cluster
        DataContainer* minKey = nullptr;
        ///The maximal (right) key of the cluster
        DataContainer* maxKey = nullptr;

        ///The distance from left to right key(min to max key)
        double range = 0;
    }Distribution_cluster_t;

    typedef enum Distribution_type_t{
        DISTRIBUTION_TYPE_VIRTUAL = -2,///! Used to calculate EQC sizes etc.
        DISTRIBUTION_TYPE_INVALID = -1,
        //ints <10 following
        DISTRIBUTION_TYPE_GENERIC_INTEGER= 0,
        DISTRIBUTION_TYPE_DISEASE_ENUM = 1,
        DISTRIBUTION_TYPE_ZIP_CODE = 2,
        DISTRIBUTION_TYPE_AGE= 3,
        //double (>=10) following
        DISTRIBUTION_TYPE_GENERIC_DOUBLE = 10,
        DISTRIBUTION_TYPE_WEIGHT= 11,
        DISTRIBUTION_TYPE_MEV= 12,

        //
        //FIXME populate
        //FIXME adapt and keyIsXXX , getProp getClosest,next,prev accordingly
    }Distribution_type_t;

    /**
     * @brief Distribution copy everything incl. data
     * @param copy
     */
    Distribution(Distribution &copy);

    /**
     * @brief Distribution creates a new distribution of the given type
     * @param type
     * @param p_isInterpolateable can the distribution be interpolated if there is no exact match
     * @param distrData is assumed to match the type. maps the reference keys to their observation count
     * @param metric the distancemetric used to calculate the distance between the reference keys and the given keys
     * @param v the distancen in which a key was presumed equal around the reference key, for the generation of the distribtuion. Only stored, not used after generation of the distribution except for equality checks
     */
    Distribution(Distribution_type_t type, bool p_isInterpolateable,QHash<DataContainer*,unsigned int> distrData,DataDistanceMetric* metric,double v);


    /**
     * @brief Distribution copies the given distribution
     * @param copy
     * @param copyData copy the data or create an empty distribution with the same metric, v, type and interpolation-flag
     */
    Distribution(Distribution &copy, bool copyData);


    ~Distribution();

    /**
     * @brief calcDistrib
     * @param data
     * @return
     */
    // static Distribution* calcDistrib(QList<DataContainer*> data,double  );//FIXME WORK HERE map pair of containers to distance

    /**
     * @brief reset clears all propabilites. Distribution is empty afterwards!
     */
    void reset();

    //TODO getKeyType & enum instead of keyIs...

    /**
     * @brief isValid
     * @return  true iff the type is set and distribution has at least one member
     */
    bool isValid();

    /**
     * @brief getType
     * @return  the type of the distribution, defines the type of the key
     */
    Distribution_type_t getType();


    /**
     * @brief getClosestProp
     * @param key the reference key
     * @return the closest value to the given one. Itself if this is part of the distribution. -1 if the distribution is empty or the given key is a nullptr
     */
    double getClosestProp(DataContainer* key);//TODO closest/next/previous Prop can be done as a higher order function which gets getClosest/next/previous key as a param

    /**
     * @brief getNextProp
     * @param key the reference key
     * @return the next value, i.e. higher key to the given one. Itself if the given key is the upper bound. -1 if the distribution is empty or the given key is a nullptr
     */
    double getNextProp(DataContainer* key);

    /**
     * @brief getPreviousProp
     * @param key the reference key
     * @return the previous value, i.e. higher lower to the given one. Its own value if the given key is the lower bound. -1 if the distribution is empty or the given key is a nullptr
     */
    double getPreviousProp(DataContainer* key);

    /**
     * @brief getClosestKey
     * @param key possibly a reference key , but maybe not
     * @return the closest refernce key to the given one. Itself if this is part of the distribution. nullptr if the distribution is empty or the given key is a nullptr
     */
    DataContainer* getClosestKey(DataContainer* key);//TODO closest, next, previous could be done by the same higher order function with two additional parameters, one for abs/id , one for additional check (distance <= 0; >= 0 , true)

    /**
     * @brief getNext
     * @param key the reference key
     * @return the next value, i.e. higher key to the given one. Itself if the given key is the upper bound. nullptr if the distribution is empty or the given key is a nullptr
     */
    DataContainer* getNextKey(DataContainer* key);

    /**
     * @brief getPrevious
     * @param key the reference key
     * @return the previous value, i.e. lower key to the given one. Itself if the given key is the lower bound. nullptr if the distribution is empty or the given key is a nullptr
     */
    DataContainer* getPreviousKey(DataContainer* key);

    /**
     * @brief getProp
     * @param key the reference key
     * @return the propability of the key, is interpolated if the distribution is interpolateable, -1 if the key does not exist and is not interpolateable
     */
    double getProp(DataContainer* key);

    /**
     * @brief getObservations
     * @param key the reference key
     * @return the number of oberservations of the given key, is a uInt32, is interpolated if the distribution is interpolateble, -1 if the key does not exist and is not interpolatable.
     * Interpolation is rounded.
     */
    int64_t getObservations(DataContainer* key);//TODO uint32 and MAX_VAL on error? catch for addProp

    /**
     * @brief addProp adds the key with the observed occurences. This implicity sets the propability of the distribution
     * Adds a new reference key to the distribution if needed
     * @param key the reference key
     * @param occurences how often the key was observed NOT the propability.
     * @return true if the number of occurences was set, false if the key was already present.
     */
    bool addProp(DataContainer* key,uint32_t occurences);

    /**
     * @brief removeProp deletes the key from the distribution
     * @param key the reference key
     * @param removeClosest the closest container is used if the refenrence key is no exact match
     * @return true if the key was deleted, false if it did not exist.
     */
    bool removeProp(DataContainer* key,bool removeClosest = true );

    /**
     * @brief addOccurence Adds an occurence to the given key. This implicity sets the propability of the distribution.
     * @param key the reference key,the closest container is used if the refenrence key is no exact match
     * @param useClosest add an occurence to the closest reference key, rather then adding a new reference key to the distribution
     * @return true if it was set, false if the key did not exist, or the key was nullptr
     */
    bool addOccurence(DataContainer* key,bool useClosest = true);


    /**
     * @brief removeOccurence Removes an occurence to the given key. This implicity sets the propability of the distribution.
     * @param key the reference key,the closest container is used if the refenrence key is no exact match
     * @param useClosest add an occurence to the closest reference key, rather then adding a new reference key to the distribution
     * @return true if it was set, false if the key did not exist, or the key was nullptr
     */
    bool removeOccurence(DataContainer* key,bool useClosest = true);

    /**
     * @brief setProp sets the occurences with the corresponding key. This implicity sets the propability of the distribution.
     * @param key the reference key
     * @param occurences
     * @return true if it was set, false if the key did not exist.
     */
    bool setProp(DataContainer* key,unsigned int occurences);

    /**
     * @brief merge creates a new distribution from the given two.
     * Must be of the same data & distribution type and have the same metric
     * The maximum of both v-values is stored in the result
     * Interpolatable if both are interpolatable
     * @param distr1
     * @param distr2
     * @return the new distribution, is nullptr if the dataType does not match, or one param is invalid. Interpolatable if both are interpolatable
     */
    static Distribution*  merge(Distribution* distr1, Distribution* distr2);//TODO domain generalization hierarchy for type instead of failure!

    /**
     * @brief operator== checks if the types are identical (both data and distribtution type). And all values, the distancemetric and v are the same, 0 observation values are the same as non-existing.
     *
     * @param x
     * @return
     */
    bool operator==(Distribution x);

    /**
     * @brief operator+ merges the two distributions, see merge.
     *
     * @param x
     * @return
     */
    Distribution operator+(Distribution x);

    /**
     * @brief getRawData
     * @return map of reference key to oberservation-count
     */
    QHash<DataContainer *, unsigned int> getRawData();

    /**
     * @brief isInterpolateable true if the propability between two keys is the interpolated propability of the keys
     * @return
     */
    bool isInterpolateable() const;

    /**
     * @brief getTotalObservations
     * @return
     */
    unsigned int getTotalObservations() const;

    /**
     * @brief getLeastPropKey returns the key with the smalles propability. The first one if there is not unique least element
     * @return the key with the least observations, i.e. the smallest propability. Nullptr if an error occured.
     */
    DataContainer* getLeastPropKey();

    /**
     * @brief getMostPropKey returns the key with the largest propability. The first one if there is no unique largest element
     * @return the key with the most observations, i.e. the largest propability. Nullptr if an error occured.
     */
    DataContainer* getMostPropKey();

    //TODO Least/most as higher order function

    /**
     * @brief getV
     * @return
     */
    double getV() const;

    /**
     * @brief getDistMetric
     * @return
     */
    DataDistanceMetric *getDistMetric() const;

private:
    uint32_t totalObservations = 0;
    Distribution_type_t type = DISTRIBUTION_TYPE_INVALID;
    ///maps the sensitive attribute to its propability
    QHash<DataContainer*,uint32_t>* distrib = nullptr;
    bool m_isInterpolateable = false;

    ///the distancen in which a key was presumed equal around the reference key, for the generation of the distribtuion. Only stored, not used after generation of the distribution
    double v = nan("");

    ///used to calculate the distance between the reference keys and the given keys
    DataDistanceMetric* distMetric = nullptr;
signals:

};

#endif // DISTRIBUTION_H
