#ifndef WAVE_H
#define WAVE_H

#include <QUuid>
#include <qdatetime.h>
#include <qstring.h>
#include <math.h>
#include "polynomial.h"
#include "tangent.h"
#include "datacontainer.h"
#include "datapoint.h"
#include "frechet/curve.hpp"




class Wave: public DataContainer
{
public:
    typedef enum WaveType_t{
        WAVE_TYPE_UNKNOWN = 0,
        WAVE_TYPE_ECG_1 = 1,
        WAVE_TYPE_ECG_2 = 2,
        WAVE_TYPE_ECG_3 = 3,
        WAVE_TYPE_PLETH = 4,
        WAVE_TYPE_PCO2_IN   = 5,
        WAVE_TYPE_PCO2_OUT  = 6,
        WAVE_TYPE_PO2_IN    = 7,
        WAVE_TYPE_PO2_OUT   = 8,
        WAVE_TYPE_HF   = 9,
        WAVE_TYPE_AWF  = 10,
        WAVE_TYPE_AWP  = 11,
        WAVE_TYPE_CO2  = 12,
        WAVE_TYPE_O2   = 13,
        //FIXME WORK HERE SEE VIEWER!
    }WaveType_t;


    //! Defining different types of fluctuation handling for the calculation of the trends TODO CURRENTLY NOT USED
    typedef enum Wave_Trend_Fluctuation_t{
        WAVE_TREND_FLUCTUATION_NONE = 0,
        WAVE_TRENDFLUCTUATION_ADDITIVE = 1,
        WAVE_TRENDFLUCTUATION_MULTIPLICATIVE = 2
    }Wave_Trend_Fluctuation_t;

    //! The different types of a wave trend
    typedef enum Wave_Trend_Type_t{
        WAVE_TREND_TYPE_POLYNOMIAL = 0,
        WAVE_TREND_TYPE_RUNNING_MEAN = 1,
        WAVE_TREND_TYPE_EXPONENTIAL = 2
    } Wave_Trend_Type_t;

    ///! Stores all date about a trendline, i.e. its parameters and type
    typedef struct Wave_Trend_Conf_t{
        Wave_Trend_Type_t type;
        Wave_Trend_Fluctuation_t fluct;
        uint8_t order;///! usesless for exponential!
        uint16_t horizon;///! how many points are taken from the startpoint to calculate the polynomial, same as order for rMean
    }Wave_Trend_Conf_t ;

    /**
     * @brief Wave creates a wave with the given parameters.
     * It is valid iff the given data is valid.
     * @param name
     * @param type usefull for some additional information, but not used internally
     * @param unit
     * @param data
     * @param patId for segmentation. the patientId
     * @param interpolatable
     * @param baseWave if the wave is a trend or derivative, this references the wave it was derived from. Otherwise it is ignored.
     * @param trend NOT nullptr if the wave is a trend line. Storing all information about the trend-type. BaseWave must be valid (not nullptr and isValidData )
     * @param derivative the wave is a derivative, baseWave must be valid (not nullptr and isValidData)
     */
    Wave(QString name, QString source, WaveType_t type, QString unit, QList<DataPoint*>* data,QString patId ="",bool interpolatable = false,Wave* baseWave = nullptr,Wave_Trend_Conf_t* trend = nullptr, bool derivative = false);

    /**
     * @brief getName
     * @return
     */

    QString getName() const;

    /**
     * @brief getType
     * @return
     */
    WaveType_t getType() const;

    /**
     * @brief getUnit
     * @return
     */
    QString getUnit() const;


    /**
     * @brief getId
     * @return
     */
    QUuid getId() const;

    /**
      * @brief calcDerivative if some values are invalid the values in the derivative are also invalid.
      * Appends " dx/dy "  to the name and the unit. Is interpolatable if the base-wave is interpolatable
      * @return the (numerical) derivative of the current curve. might be invalid if an error occured, e.g. the current wave is invalid, has only one point... Does not take ownership of the object. I.e. delete it yourself!
      */
    Wave* calcDerivative();//FIXME check this


    //FIXME all calcX => add fluctuaion & trendConf...

    /**
      * @brief calcLinTrend approximates the wave with a tangents
      * @param startPoint
      * @return the tangent or nullptr if the given startpoint is not part of the wave
      */
     Tangent* calcLinTrend(DataPoint* startPoint);

    /**
     * @brief calcPolyTrend calculates the polynomial trendline für the current wave
     * @param startPoint the point the trend is calculated around
     * @param horizon how many points should be taken to fit the polynomial starting from startPoint.
     * If there are not enough points available, as many as possible are taken. So check the real horizon in the result.
     * @param order what order should the polynmoial have
     * @return the trend polynomial or a nullptr if an error occured, does not take ownership of the trendwave
     */
    Polynomial* calcPolyTrend(DataPoint* startPoint,uint16_t horizon = 5,uint8_t order = 5);
    //FIXME also look back or look both

    /**
     * @brief calcRMeanTrend calculates the running mean trendline für the current wave
     * @param order how many datapoints should be considered
     * @return the trend wave or a nullptr if an error occured, e.g. order > data-size. does not take ownership of the trendwave
     */
    Wave* calcRMeanTrend(uint8_t order = 5);//FIXME check starttime!!

    /**
     * @brief calcExp calculates the exponential trendline für the current wave
     * @return the trend wave or a nullptr if an error occured, does not take ownership of the trendwave
     */
    Wave* calcExp();//FIXME implement

    /**
     * @brief calcDerivOrder
     * @return the order of the current wave, if it is an derivative, otherwise 0
     */
    uint8_t calcDerivOrder();


    //FIXME WORK HERE implement   getExtrema, (add/remove datapoints) ,split,

    /**
     * @brief isDerivative
     * @return
     */
    bool isDerivative() const;

    /**
     * @brief isTrend
     * @return
     */
    bool isTrend() const;


    /**
     * @brief getRootWave
     * @return the calse getBaseWave until its a nullptr, then the current wave is returned.
     */
    Wave *getRootWave() const;

    /**
     * @brief getBaseWave
     * @return
     */
    Wave *getBaseWave() const;

    /**
     * @brief getTrendConf
     * @return
     */
    Wave_Trend_Conf_t getTrendConf() const;

    /**
     * @brief toFrechetCurve
     * Converts the container to a curve, compatible with the FRED-Frechet API
     * Names the curve
     * @return
     */
    Curve toFrechetCurve() override;

signals:

    /**
     * @brief dataPointAdded should be connected to the corresponding slot for all wavesegments
     * @param dp
     */
    void dataPointAdded(DataPoint* dp);

    /**
     * @brief dataPointRemoved should be connected to the corresponding slot for all wavesegments
     * @param dp
     */
    void dataPointRemoved(DataPoint* dp);

private:

    /**
     * @brief genDataMap helperfunction generating the map from the given list and sets it.
     * Might create an invalid datacontainer, can be checked!
     * @param p
     */
    void genDataMap(QList<DataPoint*>* p);

    QString name;
    WaveType_t type = WaveType_t::WAVE_TYPE_UNKNOWN;
    QString unit;
    QUuid id;
    bool m_isDerivative = false;
    Wave_Trend_Conf_t* trendConf = nullptr;
    Wave* baseWave = nullptr;

};

#endif // WAVE_H
