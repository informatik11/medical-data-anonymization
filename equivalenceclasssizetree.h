#ifndef EQUIVALENCECLASSSIZETREE_H
#define EQUIVALENCECLASSSIZETREE_H
#include "datacontainer.h"
#include "distribution.h"
#include "equivalenceclass.h"
#include "buckettree.h"



/**
 * @brief The EquivalenceClassSizeTree class containing all data for the calculation of the EqcSize. It is a binary tree.
 * Is also used to generate equivalence classes, which is the only intended way to get an EQC
 * Is a friend of EquivalenceClass.
 **/
class EquivalenceClassSizeTree
{
public:
    ///! Representing a node in the EQCSizeTree. First the sizes of the eqcs are calculated, then the actual EQCs are generated.
    typedef struct EqcSizeTree_node_t{
        ///! Assuming either left AND right are nullptr or none of them
        EqcSizeTree_node_t *left = nullptr;
        ///! Assuming either left AND right are nullptr or none of them
        EqcSizeTree_node_t *right = nullptr;
        ///! Maps the refvalue of a bucket to its size.
        QHash<DataContainer*,unsigned int> bucketSizes;
        ///! How the values in the buckets are distributed in this node
        Distribution* nodeDistrib = nullptr;
        ///! Nullptr iff this is the root
        EqcSizeTree_node_t* parentNode = nullptr;
        ///! The tree this node is part of
        EquivalenceClassSizeTree* parentTree = nullptr;



        /**
         * @brief getSibilings returns all nodes which are on the same level then the current node, including itself
         * @return  All nodes which are on the same level then the current node, including itself
         */
        QList<EqcSizeTree_node_t*> getSibilings();

        /**
         * @brief getNodesByDepth
         * @param depth
         * @return All nodes on the given level
         */
        QList<EqcSizeTree_node_t*> getNodesByDepth(unsigned int depth);

        /**
         * @brief getMaxLevelSize
         * @return The largest size on the current level / depth
         */
        unsigned int getMaxLevelSize();

        /**
         * @brief getMaxLevelSize
         * @param depth
         * @return The largest size on the given level / depth, 0 if the depth was negative, or the depth could not be reached
         */
        unsigned int getMaxLevelSize(int depth);

        /**
         * @brief calcSize the size of all buckets.
         * If possible the node distribution is used.
         * When this is not possible, it is calculated using bucketSizes, which is more expensive.
         * @return
         */
        unsigned int calcSize();

        /**
         * @brief toString
         * @return
         */
        QString toString();

        /**
         * @brief constraintHolds
         * @param maxSize
         * @return  true if the given constraint is fullfilled at the current node
         */
        bool constraintHolds(unsigned int maxSize);

        /**
         * @brief constraintHoldsAlways
         * @param maxSize
         * @return true iff the cost is lower then maxSize the for all nodes.
         */
        bool constraintHoldsAlways(unsigned int maxSize);


        /**
         * @brief getLeafNodes recurses down from the current node and returns all leafs
         * @return list of all leafnodes from the current node down.
         */
        QList<EqcSizeTree_node_t*> getLeafNodes();

        /**
         * @brief getNodes returns the nodes at the given depth
         * @param depth
         * @return list of all nodes at the given depth from the current node down. Empty if the depth is deeper then the tree (on any branch) or negative
         */
        QList<EqcSizeTree_node_t*> getNodes(int depth);//TODO use for later ajustments in UI without recalculation

        /**
         * @brief isLeaf
         * @return
         */
        bool isLeaf()
        {
            return (left == nullptr || right == nullptr);
        }
        /**
         * @brief deleteTree recursively deletes the all children
         * Only the root has to be deleted manually at the end
         * @param deleteDistr should the distributions of the buckets be deleted?
         */
        void deleteTree(bool deleteDistr = true);

        /**
         * @brief getDepth
         * @return the depth of the current node. 0 is root.
         */
        unsigned int getDepth();

        /**
         * @brief getMinDepth returns the depth where the given constraint is fullfilled.
         * Calculates the cost where it is not chached.
         * @param maxSize
         * @return the depth where all nodes fullfill the given maxSize constraint, -1 if a bucket on the way was nullptr, -2 if the needed depth could not be reached.
         * If multiple error occure the left-most error will propagate.
        * If the value is non negative it is just an unsigned int
         */
        int64_t getMinDepth(unsigned int maxSize);


    private:
        /**
         * @brief getMinDepthHelper keeps track of the currentdepth for getDepth(maxCost)
         * returns the depth where the given constraint is fullfilled.
         * Calculates the cost where it is not chached.
         * @param maxCost must be positive
         * @return the depth where all nodes fullfill the given maxSize constraint, -1 if a bucketlist on the way was empty, -2 if the needed depth could not be reached.
         * If multiple error occure the left-most error will propagate.
         * If the value is non negative it is just an unsigned int
         */
        int64_t getMinDepthHelper(unsigned int maxSize, int currDepth);


    }EqcSizeTree_node_t;

    //FIXME encapsulate
    ///! The root of the tree
    EqcSizeTree_node_t *root;


    /**
     * @brief EqcSizeTree an empty Tree is created. Usually you should use genTree!
     * But can be the inital parameter for generating an eqc
     * @param c for splitting, maximum semantic distance
     * @param metric how the distance is measured between distributions
     * @param popDistr can not be nullptr, or this behaves undefined
     */
    EquivalenceClassSizeTree(double c, DistributionDistanceMetric* metric,Distribution* popDistr);

    /**
     * @brief EqcSizeTree Tree is created with the given root. Usually you should use gentree
     * @param root parentTree is set to this tree
     * @param c for splitting, maximum semantic distance
     * @param metric how the distance is measured between distributions
     * @param popDistr can not be nullptr, or this behaves undefined
     */
    EquivalenceClassSizeTree(EqcSizeTree_node_t* root,double c, DistributionDistanceMetric* metric,Distribution* popDistr);

    /**
     * @brief getSibilings
     * @param depth
     * @return  All nodes on the given level.
     */
    QList<EqcSizeTree_node_t*> getSibilings(unsigned int depth);

    //FIXME improvment depth as uint !

    /**
     * @brief getMaxLevelSize
     * @param depth
     * @return The largest size on the given level / depth, 0 if the tree is empty or depth is negative
     */
    unsigned int getMaxLevelSize(int depth);

    /**
     * @brief genTree creates a sizetree with the given constraints, which is split as far as possible,
     * This is most likely how you want to create an EqcSizeTree!
     * @param buckets the root-buckets
     * @param c for splitting, maximum semantic distance to the distribution
     * @param dataDistMetric how the distance is measured between datacontainers
     * @param distrDistMetric the distance between distributions
     * @param popDistr can not be nullptr
     * @param v maximum distance from the reference key to other values in the distribution to be considered equal to the key
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return a pair of the fully split EqSizeTree, or nullptr if an error occured (e.g. invalid constraints, nullptr or empty parameter) and the maximum size on the last level. Always check the first entry before using the second! If the first is nullptr the second is undefined!
     */
    static QPair<EquivalenceClassSizeTree *, unsigned int> genTree(QHash<DataContainer *, unsigned int> buckets, double c, DataDistanceMetric *dataDistMetric,DistributionDistanceMetric* distribDistMetric, Distribution *popDistr,double v,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    /**
     * @brief genTree creates a sizetree with the given constraints, which is split as far as possible,
     * This is most likely how you want to create an EqcSizeTree!
     * @param bTree the buckettree, are transformed into a set of virtual buckets, which are then used. I.e. the minium depth for t is used to extract the buckets from the tree
     * Also provides the maximum distance (internal estimate) of a bucket to the distribution (max cost estimate)
     * @param c for splitting, maximum semantic distance to the distribution
     * @param dataDistMetric how the distance is measured between datacontainers
     * @param distrDistMetric the distance between distributions
     * @param popDistr can not be nullptr
     * @param v maximum distance from the reference key to other values in the distribution to be considered equal to the key
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return a pair of the fully split EqSizeTree, or nullptr if an error occured (e.g. invalid constraints, nullptr or empty parameter) and the maximum size on the last level. Always check the first entry before using the second! If the first is nullptr the second is undefined!
     */
    static QPair<EquivalenceClassSizeTree*,unsigned int> genTree(BucketTree *bTree, double c, DataDistanceMetric* dataDistMetric,DistributionDistanceMetric* distribDistMetric, Distribution* popDistr,double v,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);


    /**
     * @brief generate takes data from the containers and adds them to an equivalenceclass until the requirements are met if possible.
     * The anonymized data is removed from data.
     * To fully anonymize data use  generateAll, as interatively using generate on a smaller and smaller set of data will most likly result in a high information loss!
     * Alternatively one can use  generateExist to iteratively generate new eqcs while being able to modify existing eqcs where needed
     * Overestimates the TargetSizes of the eqcs, as always the maximum is used.
     * V is taken from the popDistribution
     * Will STRONGLY overestimate the needed size for the eqcs!
     * @param minValue the minimal value for the type. NOT of the data, but the smallest value that would make sense (min of domainrange).
     * @param maxValue the maximal value for the type. NOT of the data, but the largest value that would make sense (max of domainrange).
     * @param data the data to be anonymized as a BucketTree
     * @param popDistr the distribution in the population. Is the reference for t-closeness
     * @param dataDistMetric the distance metric used for the calculation in the eqc between datacontainers
     * @param distrDistMetric the distance between distributions
     * @param infoMetric the informationloss metric used for the calculation in the eqc
     * @param k the minimal number of containers in the eqc
     * @param t the maximum distance of the distribution in the eqc to the population eqc.
     * @param b the depth for the buckettree, must be reachable for all branches!
     * @param h the hull stepsize in ms. The default is used if this is NaN. but this parameter is important for the calculation of the hull and infoloss
     * @param sizeTree is set to the calculated EqcSizeTree for the given parameters. It is only calculated if this parameter is either empty or nullptr. If it is empty it is overwritten and can be reused for further generations. If it is nullptr the calculated tree is deleted before the return.
     * Takes the maxCost from the bucketTree to calculate the sizeTree.
     * @param infoLossRes the resolution of the information loss calculation
     * @param sizeTreeDepth sets the used depth of the sizeTree. If this is nullptr or the given depth is to large, the maximum depth is used.
     * @param greedy true if the data should be taken greedy. Considerate otherwise.
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return an anonymizing eqc if the constraints can be met. The containers in the eqc are removed from data. They will most likely not span the whole data! Nullptr if the constraints could not be met, or some parameters were invalid, in this case data is unchanged.
     */
    static  EquivalenceClass *generate(double minValue, double maxValue,BucketTree *data, Distribution* popDistr, DataDistanceMetric* dataDistMetric,DistributionDistanceMetric* distrDistMetric, InformationlossMetric* infoMetric, unsigned int k, double t,unsigned int c, double  h,int infoLossRes, EquivalenceClassSizeTree* sizeTree = nullptr, unsigned int* sizeTreeDepth = nullptr, bool greedy = true,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);
    //FIXME adapt other generate function!


    /**
     * @brief generateAll takes data from the containers and adds it to an equivalenceclass and generates new ones if the informationloss is to high.
     * If all constraints are fullfilled the list of eqcs is returned, otherwise they are merged until the constraints hold.
     * This should be used to anonymize data completly. Alternatively  generateExist can be used to interatively generate new eqcs while being able to modify existings ones where needed.
     * Iterates the SizeTree and uses the correct targetSizes for the EQCs.
     * @param minValue the minimal value for the type. NOT of the data, but the smallest value that would make sense (min of domainrange).
     * @param maxValue the maximal value for the type. NOT of the data, but the largest value that would make sense (max of domainrange).
     * @param data the data to be anonymized as a BucketTree
     * @param popDistr  the distribution in the population. Is the reference for t-closeness
     * @param dataDistMetric the distance metric used for the calculation in the eqc
     * @param distrDistMetric the distance between distributions
     * @param infoMetric the informationloss metric used for the calculation in the eqc
     * @param k the minimal number of containers in the eqc
     * @param t the maximum distance of the distribution in the eqc to the population eqc
     * @param b the depth for the buckettree, must be reachable for all branches!
     * @param h the hull stepsize in ms. The default is used if this is NaN. but this parameter is important for the calculation of the hull and infoloss
     * @param s the maximum informationloss. This is a soft threshold, i.e. a new EQC is generated before breaking the constraint, however if there are containers left and not droppable, this constraint will be broken rather then t or k.
     * @param infoLossRes the resolution of the information loss calculation
     * @param sizeTree is set to the calculated EqcSizeTree for the given parameters. It is only calculated if this parameter is either empty or nullptr. If it is empty it is overwritten and can be reused for further generations. If it is nullptr the calculated tree is deleted before the return.
     * Takes the maxCost from the bucketTree to calculate the sizeTree
     * @param sizeTreeDepth sets the used depth of the sizeTree. If this is nullptr or the given depth is to large, the maximum depth is used.
     * @param greedy true if the data should be taken greedy. Considerate otherwise.
     * @param d can some points be ignored to improve the result? They are nevetheless assigned to the closes cluster, but as "droppedPoints". If this is infinity no points are dropped! Can not be NaN!
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return a list of anonymizing eqcs if the constraints can be met. Otherwise a nullptr.
     */
    static QList<EquivalenceClass*>* generateAll(double minValue, double maxValue, BucketTree *data, Distribution* popDistr, DataDistanceMetric *dataDistMetric,DistributionDistanceMetric* distrDistMetric, InformationlossMetric *infoMetric, int k, double t,unsigned int b, double  h, double s,int infoLossRes, EquivalenceClassSizeTree* sizeTree = nullptr, unsigned int* sizeTreeDepth = nullptr, bool greedy = true, double d = INFINITY,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    /**
     * @brief generateExist takes data from containers and adds it into a new equivalenceclass. It also distributes data into existing ones, or swaps date between  classes to decrease information loss.
     * At most one new eqc is geneated. Used segments are removed from data
     * @param data the DataContainer that need to be anonymized
     * @param exEqcs the existing eqcs they might be modified to decrease information loss, or allow the successfull termination at all.
     * @param popDistr  the distribution in the population. Is the reference for t-closeness
     * @param k the minimal number of containers in the eqc
     * @param t the maximum distance of the distribution in the eqc to the population eqc
     * @param maxTolerance the maximum distance between points on the time-axis to be considered close.
     * @param maxSemanticDistance how far away can sensitive attributes be to fall into the same bucket for t-closeness
     * @param droppabale can some points be ignored to improve the result? They are nevetheless assigned to the closes cluster, but as "droppedPoints"
     * @return The newly generated eqc. If the best solution would be to not generate a new eqc and distribute the data between the existing ones it is empty. If the constraints could not be fullfilled a nullptr is returned
     */
    static EquivalenceClass* generateExist(QList<DataContainer*>* data,QList<EquivalenceClass*>* exEqcs,Distribution* popDistr,int k, double t,double maxTolerance,double maxSemanticDistance,bool droppabale = false);//FIXME implement

    //FIXME implement generateAlong! (friend!)
    /**
     * @brief constraintHolds calculates the cost where needed
     * @return true iff the cost is lower then maxCost the root. False if the maxcost is invalid
     */
    bool constraintHolds();

    /**
     * @brief constraintHoldsAlways calculates the cost where needed
     * @return true iff the cost is lower then maxCost the for all nodes. False if the maxcost is invalid
     */
    bool constraintHoldsAlways();

    /**
     * @brief isEmpty
     * @return true iff the root is null
     */
    bool isEmpty() {
        return (root == nullptr);
    }

    /**
     * @brief insert inserts the given buckets into the given parent node. Usually you should not insert, just use a fully build Tree from genTree!
     * The parent-node is filled left to right. If no child is nullptr nothing is inserted.
     * The parentTree of the created node is set to this tree.
     * @param buckets can not be empty, each entry represents a mapping from the refvalue to the size of a bucket
     * @param parent can be nullptr IFF the inserted node is the root
     * @param nodeDistrib the distribution within the node (i.e. of the buckets). Can be set later if not needed. Can be generated with Bucket::genDistrib(...)
     * @return true iff the given data was successfully inserted.
     */
    bool insert(QHash<DataContainer*,unsigned int> buckets,EqcSizeTree_node_t *parent,Distribution* nodeDistrib = nullptr);

    /**
     * @brief toString represents the EQCTree as a string. usfull for debugging or a simple GUI.
     * @return
     */
    QString toString();//FIXME make more readable


    /**
     * @brief splitLeafsUntil splits the current EqcSizeTree, i.e. all leafs are split if the theshold (size AND t with maxCost upperbound from the tree) is not yet reached
     * Starts from the root
     * @param sizeThres the tree should no further be split if the size is lower then this
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return the maximum size on the last level, -1 if an error occured
     */
    int splitLeafsUntil(unsigned int sizeThres,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);


    /**
     * @brief splitLeafsUntil splits the given EqcSizeTreeNode, i.e. all leafs are split if the theshold (size AND t with maxCost upperbound from the tree) is not yet reached
     * @param node The startingpoint
     * @param sizeThres the tree should no further be split if the size is lower then this
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return the maximum size on the last level, -1 if an error occured
     */
    static int splitLeafsUntil(EqcSizeTree_node_t* node,unsigned int sizeThres,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);


    /**
     * @brief splitLeafsUntil splits the given EqcSizeTreeNode, i.e. all leafs are split if the threshold (t with maxCost upperbound from the tree) is not yet reached.
     * @param node The startingpoint
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return the maximum size of the last level, -1 if an error ouccred
     */
    static int splitLeafsUntil(EqcSizeTree_node_t* node,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    /**
     * @brief splitLeafsUntil splits the given EqcSizeTree, i.e. all leafs are split if the threshold (t with maxCost upperbound from the tree) is not yet reached.
     * Starts from the root
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return the maximum size of the last level, -1 if an error ouccred
     */
    int splitLeafsUntil(QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    /**
    * @brief splitUntil splits the given EqcSizeTreeNode from the given node down until the threshold (size AND t with maxCost upperbound from the tree) is met, overwriting all existing children.
    * Might fail to meet the threshold, if the best split is no split for enough leafs. So look at the returned value.
    * Can destroy the tree if the result is negative and some splits are performed and others are not.
    * @param node the startingpoint
    * @param sizeThres the tree should no further be split if the size is lower then this
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
    * @return the maximum size on the last leven, -1 if an error occured.
    */
    static int splitUntil(EqcSizeTree_node_t* node,unsigned int sizeThres,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    /**
     * @brief splitUntil splits the given EqcSizeTreeNode from the given node down until the threshold (t with maxCost upperbound from the tree)is met, overwriting all existing children.
     * Might fail to meet the threshold, if the best split is no split for enough leafs. So look at the returned value.
     * Can destroy the tree if the result is negative and some splits are performed and others are not.
     * @param node The startingpoint
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return the maximum size of the last level, -1 if an error occured
     */
    static int splitUntil(EqcSizeTree_node_t* node,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    /**
    * @brief splitUntil splits the given EqcSizeTree from the root node down until the threshold (size AND t with maxCost upperbound from the tree) is met, overwriting all existing children.
    * Might fail to meet the threshold, if the best split is no split for enough leafs. So look at the returned value.
    * Can destroy the tree if the result is negative and some splits are performed and others are not.
    * @param sizeThres the tree should no further be split if the size is lower then this
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
    * @return the maximum size on the last leven, -1 if an error occured.
    */
    int splitUntil(unsigned int sizeThres,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    /**
     * @brief splitUntil splits the given EqcSizeTree from the root down until the threshold (t with maxCost upperbound from the tree)is met, overwriting all existing children.
     * Might fail to meet the threshold, if the best split is no split for enough leafs. So look at the returned value.
     * Can destroy the tree if the result is negative and some splits are performed and others are not.
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return the maximum size of the last level, -1 if an error ouccred
     */
    int splitUntil(QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);

    /**
    * @brief getLeafNodes recurses down from the root node and returns all leafs
    * @return list of all leafnodes from the current node down.
    */
    QList<EqcSizeTree_node_t*> getLeafNodes();

    /**
    * @brief getLeafs recurses down from the root node and returns all nodes at the given depth
    * @param depth
    * @return list of all nodes at the given depth from the current node down. Empty if the depth is deeper then the tree (on any branch) or negative
    */
    QList<EqcSizeTree_node_t*> getNodes(int depth);

    /**
    * @brief deleteTree recursively deletes the all children
    * Only the root has to be deleted manually at the end
    * @param delDistr should the node distributions be deleted
    */
    void deleteTree(bool delDistr = true);

    /**
    * @brief getDepth
    * @return the depth of the current node. 0 is root. -1 if the root was nullptr.
    * If the value is non negative it is just an unsigned int
    */
    int64_t getDepth();

    /**
    * @brief getMinDepth returns the depth where the given constraint is fullfilled.
    * @param maxSize
    * @return the depth where all nodes fullfill the given maxCost constraint, -1  if a bucketlist on the way was empty, -2 if the needed depth could not be reached.
    * If multiple error occure the left-most error will propagate.
    * If the value is non negative it is just an unsigned int
    */
    int64_t getMinDepth(unsigned int maxSize);

    /**
    * @brief isValid
    * @return true if all constraints have a valid bound and distribution, metric is set etc. Root can be null nevertheless !
    */
    bool isValid();

    /**
    * @brief getC
    * @return
    */
    double getC();


    /**
    * @brief getPopDistribution
    * @return
    */
    Distribution* getPopDistribution();

    /**
    * @brief getMetric
    * @return
    */
    DistributionDistanceMetric* getMetric();

    /**
     * @brief calcRefKey calculates the new reference key for a virtual bucket base
     * take the Size -1  values to the left (previous) of the currRefKey, including currRefKey, and take the "middle" of it as reference for left
     * take the remaining Size right (next) keys of the currRefKey (not including currRefKey) and take the "middle" of it as reference  for right
     * @param distr
     * @param size the size of the  virtual bucket which is generated, NOT how many steps should be taken!
     * @param currRefKey the reference key of the FORMER virtual bucket
     * @param next go right (next) or left (previous)
     * @return the refkey for the new virtual bucket or nullptr if a parameter was invalid
     */
    static DataContainer* calcRefKey(Distribution* distr,unsigned int size,DataContainer* currRefKey,bool next);

private:

    /**
     * @brief handleSizeTree helperfunction calculating the tree where needed according to the depth
     * @param sizeTree is calculated if nullptr or empty. If it is empty, it is set to the calculated tree.
     * @param depth the prefered depth to be used, maximum depth if this is a nullptr or deeper then the tree.
     * If it is not nullptr and the depth could not be reached, it is set to the used depth.
     * @param data the BucketTree from the data, is used to get the buckets and maxCost
     * @param c for splitting, maximum semantic distance
     * @param dataDistMetric how the distance is measured between datacontainers
     * @param distrDistMetric the distance between distributions
     * @param popDistr can not be nullptr or invalid!
     * @param v maximum distance from the reference key to other values in the distribution to be considered equal to the key
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return All nodes on the target level, each node giving the size for one eqc. Empty if an error occured
     */
    static QList<EquivalenceClassSizeTree::EqcSizeTree_node_t*> handleSizeTree(EquivalenceClassSizeTree* sizeTree, unsigned int * depth, BucketTree *data, double c, DataDistanceMetric* dataDistMetric,DistributionDistanceMetric* distribDistMetric, Distribution* popDistr,double v,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);


    /**
     * @brief splitNode splits the given node and overwrites its current children.
     * @param node
     * @param overwriteChildren if this is true, the children are overwritten, otherwise no split is performed if the children are not nullptr.
     * !! DANGER OF MEMLEAK!! You should know wat your are doing if you pass a node with children and this set to true
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
     * @return the max size of the resulting split. or -1 if the node or parentTree nullptr or the parenttree was invalid, -2 if the children are set and not overwritable, -3 if a node contains an empty bucket, -4 if a (virtual) distribution could not be generated.
     */
    static int64_t splitNode(EqcSizeTree_node_t* node,bool overwriteChildren = false,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache = nullptr);
    //FIXME improvement: statusflag as param for return (was it split, why not?)

    ///! The distribution used to calculate the size.
    Distribution* popDistr = nullptr;

    ///! How the distance between distributions is measured.
    DistributionDistanceMetric* metric = nullptr;

    ///! Maximum extended cost
    double b = nan("");

};

#endif // EQUIVALENCECLASSSIZETREE_H
