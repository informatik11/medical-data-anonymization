#ifndef DATAPOINT_H
#define DATAPOINT_H

#include <qdatetime.h>
#include <cmath>

/**
 * @brief The DataPoint class holds all data for one point in a container e.g. wave
 */
class DataPoint
{
public:

    /**
     * @brief DataPoint
     * @param time
     * @param value
     * @param valid
     */
    DataPoint(QDateTime time,double value,bool valid = true);



    /**
     * @brief getTime
     * @return the time value of the point
     */
    QDateTime getTime() const;

    /**
     * @brief getValue
     * @return the value of the point
     */
    double getValue() const;

    /**
     * @brief isValidValue
     * @return true iff the value is usable. Says nothing about the QDatetime, this can be invalid eitherway!
     */
    bool isValidValue() const;

    /**
     * @brief operator== checks if the types are identical, same time AND value
     *
     * @param x
     * @return
     */
    bool operator==(DataPoint x);

private:
    double value = nan("");
    QDateTime time;
    bool m_isValid = false;
};

#endif // DATAPOINT_H
