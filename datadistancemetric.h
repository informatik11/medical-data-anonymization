#ifndef DATADISTANCEMETRIC_H
#define DATADISTANCEMETRIC_H

#include "datapoint.h"
#include "datacontainer.h"

/**
 * @brief The DistanceMetrics the interface defining different metrics on Datapoints and  -containers
 */
class  DataDistanceMetric
{
public:

    /**
     * @brief error how precise is the distance
     * @return
     */
    virtual double error() = 0;

    /**
     * @brief valueDistance distance only on the value-axis
     * @param p1
     * @param p2
     * @return the distance between two datapoints on the time axis
     */
    virtual double valueDistance(DataPoint p1,DataPoint p2) =0;

    /**
      * @brief valueDistance calculates the distance between the datapoint, both on value-axis. I.e. the minimum of left to right and right to left, if they are of unequal length.
      * @param w1
      * @param w2
      * @return the distance between all  datapoints in the given containers
      */
    virtual double valueDistance(DataContainer d1, DataContainer d2) =0;

    /**
     * @brief timeDistance distance only on the time-axis
     * @param p1
     * @param p2
     * @return the distance between two datapoints on the time axis
     */
    virtual double timeDistance(DataPoint p1,DataPoint p2) =0;

    /**
      * @brief timeDistance calculates the distance between the datapoint, both on time-axis. I.e. the minimum of left to right and right to left, if they are of unequal length.
      * @param w1
      * @param w2
      * @return the distance between all  datapoints in the given containers
      */
    virtual double timeDistance(DataContainer d1, DataContainer d2) =0;

    /**
     * @brief distance
     * @param p1
     * @param p2
     * @return the distance between two datapoints
     */
    virtual double distance(DataPoint p1,DataPoint p2) = 0;

    /**
      * @brief distance
      * @param w1
      * @param w2
      * @return the distance between all  datapoints in the given containers
      */
    virtual double distance(DataContainer d1, DataContainer d2) = 0;

    /**
     * @brief semanticDistance Calculates the semantic distance, e.g. considering trend or derivatives.
     * Usually very expensive to calculate.
     * @param d1
     * @param d2
     * @return the distance between the containers.
     */
    virtual double semanticDistance(DataContainer d1, DataContainer d2) = 0;

    /**
     * @brief getName
     * @return
     */
    QString getName() const;

protected:

    /// The name of the distancemetric, for visualization purposes
    QString name = "Distance metric BASECLASS";

};

inline QString DataDistanceMetric::getName() const
{
    return name;
}

#endif // DATADISTANCEMETRIC_H
