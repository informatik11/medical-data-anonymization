#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->evaluationButton->setVisible(EVAL_MODE == true);


    ui->segmentSelectScrollbar->setContextMenuPolicy(Qt::NoContextMenu);
    setVisibilityExtendedOptions();
    this->setWindowTitle("NANNI-Anonymizer");
    ui->graphPlotWidget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom  );//FIXME improvment selectable, but not on export!
    ui->distribPlotWidget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom  );

    for(int i: infolossMap.keys())
    {
        ui->infoLossList->addItem(infolossMap.value(i)->getName());
    }


    for(int i: dataDistMap.keys())
    {
        ui->distanceList->addItem(dataDistMap.value(i)->getName());//TODO remove "data"
    }

    ui->distanceList->item(DEFAULT_DISTANCE_INDEX)->setSelected(true);
    ui->infoLossList->item(DEFAULT_INFOLOSS_INDEX)->setSelected(true);
    enableWidget(ui->interpolatableCheckbox,true);
    enableWidget(ui->loadButton,true);

}

MainWindow::~MainWindow()
{
    clearTreeMap();
    deleteAnonData();
    delete distrMap;//FIXME memleak distribution?
    delete distrClusterMap;//fixme memleak list&clusters?
    delete ui;
    delete currentSegmentHighlight;
    delete distrData;
    delete result;
    delete eqcVisMap;
    delete itemStateMap;
    delete anonDistanceCache;
}

InformationlossMetric *MainWindow::getCurrentInfoLossMetric()
{
    if(ui->extendedOptionsCheckbox->isChecked() && ui->infoLossList->currentRow() >= 0)
    {
        return infolossMap.value(ui->infoLossList->currentRow());
    }else{
        return infolossMap.value(DEFAULT_INFOLOSS_INDEX);
    }
}

DistributionDistanceMetric *MainWindow::getCurrentDistribDistanceMetric()
{
    if(ui->extendedOptionsCheckbox->isChecked() && ui->distanceList->currentRow() >= 0)
    {
        return distribDistMap.value(ui->distanceList->currentRow());
    }else{
        return distribDistMap.value(DEFAULT_DISTANCE_INDEX);
    }
}

DataDistanceMetric *MainWindow::getCurrentDataDistanceMetric()
{
    ui->distanceList->setEnabled(false);
    if(dataDistMetric == nullptr)
    {
        if(ui->extendedOptionsCheckbox->isChecked() && ui->distanceList->currentRow() >= 0)
        {
            return dataDistMap.value(ui->distanceList->currentRow());
        }else{
            return dataDistMap.value(DEFAULT_DISTANCE_INDEX);
        }
    }else{
        return dataDistMetric;
    }
}

MainWindow::generationMode_t MainWindow::getMode()
{
    if(ui->extendedOptionsCheckbox->isChecked())
    {
        if(ui->redistModeAll->isChecked())
        {
            return generationMode_ALL;
        }else if(ui->redistModeFill->isChecked())
        {
            return generationMode_FILL;
        }else if(ui->redistModeOne->isChecked())
        {
            return generationMode_ONE;
        }else if(ui->redistModeCase->isChecked())
        {
            return generationMode_CASE;
        }else{//should not happen
            return DEFAULT_GEN_MODE;
        }
    }else{
        return DEFAULT_GEN_MODE;
    }
}

bool MainWindow::getGreedy()
{
    if(ui->extendedOptionsCheckbox->isChecked())
    {
        return ui->greedyCheckbox->isChecked();
    }else{
        return DEFAULT_GREEDY;
    }
}

void MainWindow::setVisibilityExtendedOptions()
{
    bool extended = ui->extendedOptionsCheckbox->isChecked();
    ui->infoLossLabel->setVisible(extended);
    ui->infoLossList->setVisible(extended);
    ui->distanceLabel->setVisible(extended);
    ui->distanceList->setVisible(extended);
    ui->dropDataLabel->setVisible(extended);
    ui->dropDataOutput->setVisible(extended);
    ui->dropDataSlider->setVisible(extended);
    ui->loadDistribButton->setVisible(extended);
    ui->redistModeAll->setVisible(extended);
    ui->redistModeCase->setVisible(extended);
    ui->redistModeFill->setVisible(extended);
    ui->redistModeOne->setVisible(extended);
    ui->redistModeLabel->setVisible(extended);
    ui->greedyCheckbox->setVisible(extended);
    ui->sSlider->setVisible(extended);
    ui->sSliderLabel->setVisible(extended);
    ui->sSliderOutput->setVisible(extended);
    ui->sepDistrCheckbox->setVisible(extended);
    ui->loadDistribButton->setVisible(extended);
    ui->distribSingleValueClusterCheckbox->setVisible(extended);
    ui->evaluationButton->setVisible(extended);
    ui->infoLossResLabel->setVisible(extended);
    ui->infoLossResSpinbox->setVisible(extended);
}


double MainWindow::getT()
{
    return 1.0;
}

double MainWindow::getS()
{
    if(ui->redistModeAll->isChecked() )
    {
        ui->sSlider->setEnabled(true && ui->anonymizationButton->isEnabled());
        ui->sSliderLabel->setEnabled(true&& ui->anonymizationButton->isEnabled());
        ui->sSliderOutput->setEnabled(true&& ui->anonymizationButton->isEnabled());
        ui->sSlider->setToolTip("Wie leicht eine neue EQC eröffnet wird, wenn der Informationsverlust steigt.");
        ui->sSliderLabel->setToolTip("Wie leicht eine neue EQC eröffnet wird, wenn der Informationsverlust steigt.");
        ui->sSliderOutput->setToolTip("Wie leicht eine neue EQC eröffnet wird, wenn der Informationsverlust steigt.");
        double val = (ui->sSlider->value()) / 1000.0;
        ui->sSliderOutput->setText(QString("%1").arg(val));
        return val;
    }else{
        ui->sSlider->setEnabled(false);
        ui->sSliderLabel->setEnabled(false);
        ui->sSliderOutput->setEnabled(false);
        ui->sSlider->setToolTip("Im gewählten Modus ist keine Empfindlichkeit einstellbar.");
        ui->sSliderLabel->setToolTip("Im gewählten Modus ist keine Empfindlichkeit einstellbar.");
        ui->sSliderOutput->setToolTip("Im gewählten Modus ist keine Empfindlichkeit einstellbar.");
        return nan("");
    }
}

double MainWindow::getF()
{
    bool enableFlag = (anonData != nullptr && !anonData->isEmpty());

    ui->fSlider->setEnabled(enableFlag);
    ui->fLabel->setEnabled(enableFlag);
    ui->fOutput->setEnabled(enableFlag);

    if(enableFlag == true)
    {
        double val = ui->fSlider->value() / 100.0;        ui->fOutput->setText(QString("%1").arg(val));
        return val;
    }else{
        ui->fOutput->setText("NaN");
        return nan("");
    }
}


unsigned int MainWindow::getB()
{
    if(!treeMap.isEmpty())
    {
        double val = (ui->bSlider->value());
        double currSegSze = 0;
        double currSegMinSze = INFINITY;
        double currSegMaxSze = 0;
        int segIdx = getCurrentSegmentIdx();
        QList<Bucket *> buckets;
        int minDepth = INFINITY;//FIXME optimize CACHE THIS! <--- Use max int?
        int i = 0;
        for(BucketTree* tree : treeMap.values())
        {
            int treeMd = tree->getMinDepth();
            if(minDepth > treeMd )
            {
                minDepth = treeMd;
            }
            QList<Bucket *> currBuckets = tree->getBuckets(val);
            buckets.append(currBuckets);
            if(i == segIdx)
            {
                for(Bucket* buck : currBuckets )
                {
                    currSegSze += buck->size();
                    if(buck->size() < currSegMinSze)
                    {
                        currSegMinSze = buck->size();
                    }
                    if(buck->size() > currSegMaxSze)
                    {
                        currSegMaxSze = buck->size();
                    }
                }
                currSegSze = currSegSze / currBuckets.size();
            }
            i++;
        }


        ui->bSlider->setMinimum(0);
        ui->bSlider->setMaximum(minDepth);

        ui->bSlider->setEnabled(true);
        ui->bLabel->setEnabled(true);
        ui->bOutput->setEnabled(true);
        ui->bSlider->setToolTip("Der Tiefe im Eimerbaum für die Anonymisierung.");
        ui->bLabel->setToolTip("Der Tiefe im Eimerbaum für die Anonymisierung.");
        ui->bOutput->setToolTip("Der Tiefe im Eimerbaum für die Anonymisierung.");
        ui->bOutput->setToolTip("Durchschnittliche Eimergröße.");
        double sze = 0;
        double minSze = INFINITY;
        double maxSze = 0;
        for(Bucket* buck : buckets )
        {
            sze += buck->size();
            if(buck->size() < minSze)
            {
                minSze = buck->size();
            }
            if(buck->size() > maxSze)
            {
                maxSze = buck->size();
            }
        }
        sze = sze / buckets.size();
        ui->bucketSizeOutput->setText(QString("∅:%1;Min.:%2;Max.:%3").arg(sze).arg(minSze).arg(maxSze));
        ui->bucketSizeCurrSegmentOutput->setText(QString("∅:%1;Min.:%2;Max.:%3").arg(currSegSze).arg(currSegMinSze).arg(currSegMaxSze));
        ui->bOutput->setText(QString("%1").arg(val));
        return val;
    }else{
        ui->bSlider->setMinimum(0);
        ui->bSlider->setMaximum(0);
        ui->bSlider->setEnabled(false);
        ui->bLabel->setEnabled(false);
        ui->bOutput->setEnabled(false);
        ui->bSlider->setToolTip("Der Tiefe im Eimerbaum für die Anonymisierung.");
        ui->bLabel->setToolTip("Der Tiefe im Eimerbaum für die Anonymisierung.");
        ui->bOutput->setToolTip("Der Tiefe im Eimerbaum für die Anonymisierung.");
        ui->bOutput->setToolTip("Durchschnittliche Eimergröße.");
        ui->bOutput->setText(QString("NaN"));
        ui->bucketSizeOutput->setText(QString("NaN"));
        ui->bucketSizeCurrSegmentOutput->setText(QString("NaN"));
        return 0;
    }

}


double MainWindow::getD()
{
    if(ui->dropDataSlider->isEnabled() == false && automaticEval == false)
    {

        ui->dropDataOutput->setText("Übrige");
        ui->dropDataOutput->setToolTip("Im gewählten Modus können Daten \"übrig bleiben\" diese werden dann verworfen.");
        ui->dropDataSlider->setToolTip("Im gewählten Modus können Daten \"übrig bleiben\" diese werden dann verworfen.");
        ui->dropDataLabel->setToolTip("Im gewählten Modus können Daten \"übrig bleiben\" diese werden dann verworfen.");
        return nan("");//disabled, because it does not work in the selected mode!
    }else{
        double val = INFINITY;
        if(!ui->extendedOptionsCheckbox->isChecked()){
            val = DEFAULT_D;
        }
        if(val == ui->dropDataSlider->maximum() || isinf(val))
        {
            ui->dropDataOutput->setText("verbleibende");
            ui->dropDataOutput->setToolTip("Im gewählten Modus können Daten ab dem gewählten Wert verworfen werde um den Informationsverlust zu reduzieren.");
            ui->dropDataSlider->setToolTip("Im gewählten Modus können Daten ab dem gewählten Wert verworfen werde um den Informationsverlust zu reduzieren.");
            ui->dropDataLabel->setToolTip("Im gewählten Modus können Daten ab dem gewählten Wert verworfen werde um den Informationsverlust zu reduzieren.");
            return INFINITY;
        }else{
            val = val / 1000.0;
            ui->dropDataOutput->setText(QString("%1").arg(val));
            ui->dropDataOutput->setToolTip("Im gewählten Modus können Daten ab dem gewählten Wert verworfen werde um den Informationsverlust zu reduzieren.");
            ui->dropDataSlider->setToolTip("Im gewählten Modus können Daten ab dem gewählten Wert verworfen werde um den Informationsverlust zu reduzieren.");
            ui->dropDataLabel->setToolTip("Im gewählten Modus können Daten ab dem gewählten Wert verworfen werde um den Informationsverlust zu reduzieren.");
        }
        return val;

    }
}

double MainWindow::getV()
{
    double maxDist = 0;
    if(distribDistanceChache != nullptr && !distribDistanceChache->isEmpty())
    {
        maxDist = maxDistribDist;
    }else{
        for(int i = 0; i<anonData->size() ; i++)
        {
            if(maxAnonDist.contains(i))
            {
                maxDist = max(maxDist,maxAnonDist.value(i));
            }
        }
    }
    if(!isnan(maxDist) )
    {
        double val = ui->distrSensSlider->value();
        ui->distrSensOutput->setText(QString("%1").arg(val));
        return val;
    }else{
        ui->distrSensOutput->setText(QString("NaN"));
        return nan("");
    }
}

bool MainWindow::dropEnabled()
{
    if(ui->extendedOptionsCheckbox->isChecked() )
    {
        return (ui->dropDataSlider->value() > ui->dropDataSlider->minimum());
    }else{
        return DEFAULT_DROP_ENABLED;
    }
}


bool MainWindow::forceAddSingleDistribClusters()
{
    if(ui->extendedOptionsCheckbox->isChecked())
    {
        return ui->distribSingleValueClusterCheckbox->isChecked();
    }else{
        return DEFAULT_DISTRIB_SINGLE_VAL;
    }
}

double MainWindow::calcQuantil(double factor, QList<double> data)
{
    double qIndD = (data.size()*factor) -1;
    double ret = nan("");
    double val;
    if(modf(qIndD,&val) <= QUANTILE_PRECISION )
    {
        if((int) qIndD < data.size() -1)
        {
            ret =  (data.at((int) qIndD) + data.at((int) qIndD +1))/2.0f;
        }else{
            ret =  data.at((int) qIndD);
        }

    }else if((int) qIndD +1 < data.size())
    {
        ret =  data.at((int) qIndD +1);
    }else{
        ret = data.at(data.size() - 1);
    }
    return ret;
}

void MainWindow::updateEqcVis(QListWidgetItem *item)
{
    if(visEqcMap->find(item) == visEqcMap->end())
    {
        updateLegend();
    }
    EquivalenceClass * eqc = visEqcMap->value(item);
    QPair<QCPGraph *, QCPGraph *> hull = result->value(eqc);
    itemStateMap->insert(item,item->checkState());

    bool vis = item->checkState() == Qt::Checked;


    for(QHash<DataContainer*,QCPGraph*>* hash : *anonData)
    {
        for(DataContainer* cont : eqc->getRawData())
        {
            if(hash->contains(cont))
            {
                hash->value(cont)->setVisible(vis);
            }
        }
        if(ui->graphplotDetailsCheckbox->checkState() == Qt::Checked)
        {
            vis = false;//dont show dropped in this checked mode
        }
        for(DataContainer* cont : eqc->getDroppedData())
        {
            if(hash->contains(cont))
            {
                hash->value(cont)->setVisible(vis);
            }
        }
    }

    hull.first->setVisible(vis);
    hull.second->setVisible(vis);
    ui->graphPlotWidget->replot();
}

void MainWindow::updateLegend()
{
    if(eqcVisMap == nullptr)
    {
        eqcVisMap = new QHash<EquivalenceClass*,QListWidgetItem*>();
    }
    if(visEqcMap == nullptr)
    {
        visEqcMap = new QHash<QListWidgetItem*,EquivalenceClass*>();
    }

    if(itemStateMap == nullptr)
    {
        itemStateMap = new QHash<QListWidgetItem*,Qt::CheckState>();
    }

    if(result == nullptr)
    {
        return;
    }
    int currSeg = getCurrentSegmentIdx();

    if(!eqcMap->contains(currSeg))
    {
        qCritical() << "No EQCs found for current segment in updateLegend";
        return;
    }

    QList<EquivalenceClass *> * currEqcs = eqcMap->value(currSeg);
    for(EquivalenceClass* eqc : *currEqcs)//create items for current segment
    {

        QPair<QCPGraph *, QCPGraph *> hull = result->value(eqc);
        InformationlossMetric::InformationLoss_t loss;
        if(resultLoss.find(eqc) == resultLoss.end())//FIXME adapt to have better resolution
        {
            loss = eqc->getInfoMetric()->calcLoss(eqc->getHull(),eqc->getMinValue(),eqc->getMaxValue(),eqc->getInfoLossRes());//TODO improve was already calculated before, cache this!
            resultLoss.insert(eqc,loss);
        }else{
            loss = resultLoss.value(eqc);
        }
        QColor color = hull.first->pen().color();
        QPixmap px = QPixmap(32,32);
        px.fill(color);
        int idx = getEQCIdx(eqc);
        QString text = QString("#%1 k=%2,t=%3, verw.=%4, ∅ I.V.=%5").arg(idx).arg(eqc->getKReal()).arg(eqc->getTReal()).arg(eqc->getDroppedData().size()).arg(loss.averageLoss);
        QListWidgetItem* item ;

        if(eqcVisMap->find(eqc) == eqcVisMap->end())
        {
            item = new QListWidgetItem(QIcon(px),text);
            item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
            item->setCheckState(Qt::Checked);
            eqcVisMap->insert(eqc,item);
        }else{
            item = eqcVisMap->value(eqc);
        }
        ui->eqcLstWidget->insertItem(0,item);
        if(visEqcMap->find(item) == visEqcMap->end())
        {
            visEqcMap->insert(item,eqc);
        }

        itemStateMap->insert(item,item->checkState());

        bool vis = item->checkState() == Qt::Checked;


        for(QHash<DataContainer*,QCPGraph*>* hash : *anonData)
        {
            for(DataContainer* cont : eqc->getRawData())
            {
                if(hash->contains(cont))
                {
                    hash->value(cont)->setVisible(vis);
                }
            }
            for(DataContainer* cont : eqc->getDroppedData())
            {
                if(hash->contains(cont))
                {
                    hash->value(cont)->setVisible(vis);
                }
            }
        }

        hull.first->setVisible(vis);
        hull.second->setVisible(vis);
    }

    for(EquivalenceClass* eqc : result->keys())
    {
        if(!currEqcs->contains(eqc))
        {//remove entries of other segments
            if(eqcVisMap->find(eqc) != eqcVisMap->end())
            {
                QListWidgetItem * item = eqcVisMap->value(eqc);//Delete removes it from the list IFF it was present but does not segfault otherwise => copy and reinsert
                QListWidgetItem* copy = new QListWidgetItem(*item);
                visEqcMap->remove(item);
                delete item;

                visEqcMap->insert(copy,eqc);
                eqcVisMap->insert(eqc,copy);
                itemStateMap->remove(item);
                itemStateMap->insert(copy,copy->checkState());
            }
        }
    }
}


void MainWindow::clearEqcMap()
{
    if(eqcMap != nullptr)
    {
        for(int i : eqcMap->keys())
        {
            delete eqcMap->value(i);
        }
        eqcMap->clear();
    }
    if(inverseEqcMap != nullptr)
    {
        inverseEqcMap->clear();
    }
}

void MainWindow::resetEqcUILst()
{
    if(visEqcMap != nullptr)
    {
        visEqcMap->clear();
    }
    if(eqcVisMap != nullptr)
    {
        eqcVisMap->clear();
    }
    if(itemStateMap != nullptr)
    {
        itemStateMap->clear();
    }
    ui->eqcLstWidget->clear();
}

void MainWindow::deleteAnonData()
{
    if(anonData != nullptr)
    {
        for(QHash<DataContainer*,QCPGraph*> *hash : *anonData)
        {
            for(DataContainer* cont : hash->keys())
            {
                ui->graphPlotWidget->removeGraph(hash->value(cont));
                delete cont;
            }
            delete hash;
        }
        delete anonData;
    }
}

void MainWindow::clearTreeMap()
{
    for(BucketTree* tree : treeMap.values())
    {
        tree->deleteTree(true);
        delete tree->root;
        delete tree;
    }
    treeMap.clear();
}

void MainWindow::clearDistribs()
{
    clearDistribBarPlots();
    clearDistribBoxPlots();
    clearDistribClusters();
    clearDistribMap();
}

void MainWindow::clearDistribMap()
{
    if(distrMap != nullptr)
    {
        for(int i : distrMap->keys())
        {
            delete distrMap->value(i);
        }
        distrMap->clear();
    }
}

void MainWindow::clearDistribClusters()
{
    if(distrClusterMap != nullptr)
    {
        for(Distribution* distr : distrClusterMap->keys())
        {
            QList<Distribution::Distribution_cluster_t *> *lst = distrClusterMap->value(distr);
            for(Distribution::Distribution_cluster_t* c : *lst)
            {
                delete c;
            }
            delete lst;
        }
        distrClusterMap->clear();
    }
}

void MainWindow::clearDistribBoxPlots()
{
    for(Distribution* distr : boxplots.keys())
    {
        QList<QPair<QCPStatisticalBox*,QCPGraph*>>* lst = boxplots.value(distr);
        for(QPair<QCPStatisticalBox*,QCPGraph*> pair : *lst)
        {
            ui->distribPlotWidget->removePlottable(pair.first);
            ui->distribPlotWidget->removeGraph(pair.second);
        }
        delete lst;
    }
    boxplots.clear();
}

void MainWindow::clearDistribBarPlots()
{
    for(Distribution* distr : barplots.keys())
    {
        QList<QCPGraph*>* lst = barplots.value(distr);
        for(QCPGraph* g : *lst)
        {
            ui->distribPlotWidget->removeGraph(g);
        }
        delete lst;
    }
    barplots.clear();
}

int MainWindow::getCurrentSegmentIdx()
{
    return ui->segmentSelectScrollbar->value();
}

void MainWindow::updateSegmentHighlight(int segment)
{
    QCPGraph* gLow;
    QCPGraph* gUp;
    if(currentSegmentHighlight == nullptr)
    {
        currentSegmentHighlight = new QPair<QCPGraph*,QCPGraph*>();
    }else{
        ui->graphPlotWidget->removeGraph(currentSegmentHighlight->first);
        ui->graphPlotWidget->removeGraph(currentSegmentHighlight->second);
    }
    gLow = ui->graphPlotWidget->addGraph(ui->graphPlotWidget->xAxis, ui->graphPlotWidget->yAxis);
    gUp = ui->graphPlotWidget->addGraph(ui->graphPlotWidget->xAxis, ui->graphPlotWidget->yAxis);
    currentSegmentHighlight->first = gLow;
    currentSegmentHighlight->second = gUp;
    QColor selCol = QColor(0xaa, 0xaa, 0xff, 0x30);//color as in viewer
    QBrush brush = QBrush(selCol);
    gLow->setPen(QPen(QColor(Qt::black)));
    gUp->setPen(QPen(QColor(Qt::black)));
    gLow->setVisible(true);
    gUp->setVisible(true);
    gLow->setSelectable(QCP::stNone);
    gUp->setSelectable(QCP::stNone);
    gLow->setChannelFillGraph(gUp);
    gUp->setChannelFillGraph(gLow);
    gLow->setBrush(brush);
    gUp->setBrush(brush);

    double yLower;
    if(!isnan(minDomVal))
    {
        yLower = minDomVal -1.0;
    }else{
        yLower = ui->graphPlotWidget->yAxis->range().lower - 1.0;//FIXME CHECK IF NEEDED!
    }
    double yUpper;
    if(!isnan(maxDomVal))
    {
        yUpper = maxDomVal +1.0;
    }else{
        yUpper = ui->graphPlotWidget->yAxis->range().upper + 1.0;//FIXME CHECK IF NEEDED!
    }
    QHash<DataContainer *, QCPGraph *> * data = anonData->at(segment);
    double minX = nan("");
    double maxX = nan("");
    for(DataContainer* cont: data->keys())
    {
        if(isnan(minX) || minX > cont->getStart().toMSecsSinceEpoch()/1000.0)
        {
            minX = cont->getStart().toMSecsSinceEpoch() /1000.0;
        }
        if(isnan(maxX) || maxX < cont->getEnd().toMSecsSinceEpoch()/1000.0)
        {
            maxX = cont->getEnd().toMSecsSinceEpoch() /1000.0;
        }
    }

    double centerX = minX + abs(maxX - minX)/2.0;
    gUp->data().clear();
    gLow->data().clear();

    gUp->addData(minX,yLower);//starting point
    gUp->addData(minX,yUpper);
    gLow->addData(minX,yUpper);
    gLow->addData(minX,yLower);

    gUp->addData(centerX,yUpper);
    gLow->addData(centerX,yLower);

    gUp->addData(maxX,yUpper);//change up and low at end
    gUp->addData(maxX,yLower);
    gLow->addData(maxX,yLower);
    gLow->addData(maxX,yUpper);
    if(currentSegmentHighlight != nullptr)
    {
        currentSegmentHighlight->first->setVisible(true && anonData->size() > 1);
        currentSegmentHighlight->second->setVisible(true && anonData->size() > 1);
    }
    ui->graphPlotWidget->replot();
}

int MainWindow::getEQCIdx(EquivalenceClass *eqc)
{
    if(eqcLst == nullptr || eqc == nullptr)
    {
        return -1;
    }else{
        int idx = eqcLst->indexOf(eqc);
        if(idx < 0)
        {
            return -2;
        }else{
            return idx;
        }
    }
}

int MainWindow::getSegmentIdx(EquivalenceClass *eqc)
{
    if(eqcMap == nullptr || eqc == nullptr)
    {
        return -1;
    }else{
        for(int seg : eqcMap->keys())
        {
            QList<EquivalenceClass*>* lst = eqcMap->value(seg);
            if(lst->contains(eqc))
            {
                return seg;
            }
        }
        return -2;
    }
}




void MainWindow::on_anonymizationButton_clicked()
{
    anonymize();
}

void MainWindow::on_kSlider_valueChanged(int value)
{
    ui->kSliderOutput->setText(QString("%1").arg(value));
}

void MainWindow::on_extendedOptionsCheckbox_stateChanged(int )
{
    setVisibilityExtendedOptions();
    getD();
    getS();
}

void MainWindow::on_loadButton_clicked()
{

    ui->bucketSizeCurrSegmentLabel->setVisible(true);
    ui->bucketSizeCurrSegmentOutput->setVisible(true);
    ui->segmentSelectLabel->setVisible(true);
    ui->segmentSelectScrollbar->setVisible(true);

    automaticEval = false;
    loadData();

}

void MainWindow::on_exportButton_clicked()
{
    exportData();
}

void MainWindow::on_dropDataSlider_valueChanged(int )
{
    getD();
}

void MainWindow::on_sSlider_valueChanged(int )
{
    getS();
}

bool MainWindow::parseLine(QMap<double, double> *values, QString line)
{
    if(values == nullptr || line.isEmpty())
    {
        return false;
    }
    line = line.remove('\r');
    line = line.remove('\n');


    QStringList decimalSplit = line.split(SEP);

    if(decimalSplit.size() != 2)
    {
        return false;
    }else{
        QString str = decimalSplit.at(0);
        str.replace(DECIMAL_POINT,'.');
        str.replace('N', ' ');//FIXME why?
        bool ok;
        double val1 = str.toDouble(&ok);
        if(ok == false)
        {
            return false;
        }


        str = decimalSplit.at(1);
        str.replace(DECIMAL_POINT,'.');
        double val2 = str.toDouble(&ok);
        if(ok == false)
        {
            return false;
        }
        values->insert(val1,val2);
        return true;
    }
}

void MainWindow::on_distrSensSlider_valueChanged(int )
{
    getV();
}

QHash<DataContainer *,QCPGraph*> *MainWindow::loadWaves(QString title,bool plot)
{
    QDateTime start = QDateTime::currentDateTime();
    unsigned long sampleCnt = 0;
    ui->progressBar->setTextVisible(true);
    ui->progressBar->setFormat("Lade..");//FIXME improvment real progressbar
    ui->progressBar->setValue(0);
    bool isInterpolatable = ui->interpolatableCheckbox->isChecked();
    ui->interpolatableCheckbox->setEnabled(false);
    QHash<DataContainer *,QCPGraph*> * ret = new QHash<DataContainer *,QCPGraph*>();
    QStringList fileNames = QFileDialog::getOpenFileNames(this,
                                                          title, QDir::homePath(), tr("CSV mit ; Spaltentrennzeichen und . Dezimaltrennzeichen (*.CSV *.csv)"));

    int cnt = 0;
    for(QString fileName : fileNames)
    {
        if(!fileName.isEmpty()  )
        {
            QDateTime fileStart = QDateTime::currentDateTime();
            QFile* file = new QFile(fileName);
            if (!file->open(QIODevice::ReadOnly)) {
                qWarning() << QString("File %1 could not be opened ignoring it").arg(fileName)+file->errorString();
                QMessageBox msg;
                msg.setText(QString("Datei %1 konnte nicht geparsed werden, sie wird übersprungen!").arg(fileName)+file->errorString());
                msg.exec();
                continue;
            }else{
                QString patID = file->readLine().replace("% Patient:","");//read patient-Id for debugging
                patID = patID.trimmed();
                QDateTime startDT = QDateTime::fromString(file->readLine().replace("% StartDatum: ",""),Qt::ISODateWithMs);//read startDate for time-alignment (FIXME future work) and debugging

                QString head = file->readLine();//read head
                QStringList headSplit = head.split(SEP);
                QString type = "unknown";
                QString unit = "";
                //parsing type & unit
                if(headSplit.size() > 1)
                {
                    QStringList typeSplit = headSplit.at(1).split(" [");
                    if(typeSplit.size() > 1)
                    {
                        type = typeSplit.at(0);
                        type = type.trimmed();
                        unit = typeSplit.at(1);
                        unit = unit.replace("[","");
                        unit = unit.replace("]","");
                        unit = unit.trimmed();

                    }else{
                        type = headSplit.at(1);
                        type = type.trimmed();
                    }

                }


                QMap<double,double> valMap;
                int lneCnt = 2;
                while (!file->atEnd() ) {
                    lneCnt++;
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    if(parseLine(&valMap, file->readLine()) == false)
                    {
                        qWarning() << QString("Error while parsing line %1").arg(lneCnt);
                    }
                }
                if(!startDT.isValid())
                {
                    startDT = QDateTime::currentDateTime();
                    qWarning() << "Invalid start datetime, using current DT.";
                }
                QPair<DataContainer *, QCPGraph *> wgPair = convertWave(valMap, fileName, patID, type, unit, startDT, isInterpolatable, plot);

                if(isInterpolatable == false)
                {
                    wgPair.second->setLineStyle(QCPGraph::LineStyle::lsStepLeft);
                }else{
                    wgPair.second->setLineStyle(QCPGraph::LineStyle::lsLine);
                }

                ret->insert(wgPair.first,wgPair.second);
                delete file;
                ui->graphPlotWidget->xAxis->setLabel("Zeit (s)");
                ui->graphPlotWidget->yAxis->setLabel(((Wave*)wgPair.first)->getUnit());
                ui->graphPlotWidget->replot();
                ui->graphPlotWidget->rescaleAxes();
                sampleCnt += valMap.size();
                qInfo() << QString("Parsed wave %1 of %2, with %3 samples in %4 ms").arg(cnt).arg(fileNames.size()).arg(valMap.size()).arg(fileStart.msecsTo(QDateTime::currentDateTime()));

            }
        }else{
            qWarning() << QString("Filename was empty!");
        }

        ui->progressBar->setValue((cnt++)/fileNames.size());
    }
    QDateTime end = QDateTime::currentDateTime();
    qInfo() << QString("Parsed %1 waves with %2 samples in %3 ms").arg(fileNames.size()).arg(sampleCnt).arg(start.msecsTo(end));
    ui->progressBar->setValue(100);
    return ret;
}

QPair<DataContainer*,QCPGraph*> MainWindow::convertWave(QMap<double, double> data, QString source, QString patId, QString type, QString unit, QDateTime startDt, bool isInterpolatable,bool plot)
{
    //FIXME WORK HERE switch WaveType_t for type
    Wave::WaveType_t waveType = Wave::WAVE_TYPE_UNKNOWN;
    QString name = patId+"-"+type+"-"+startDt.toString(Qt::ISODateWithMs)+" ("+source+")";
    QList<DataPoint*>* waveData = new QList<DataPoint*>();
    QCPGraph* g = nullptr;
    if(plot == true)
    {
        g = ui->graphPlotWidget->addGraph();
    }
    for(double key: data.keys())
    {

        QCoreApplication::processEvents(QEventLoop::AllEvents,10);
        double val = data.value(key);
        QDateTime dt = QDateTime();
        bool valid = false;
        if(!isnan(key))
        {
            dt = QDateTime::fromMSecsSinceEpoch(key*1000 + EPOCH_OFFSET_MS);
            if(isnan(val))
            {
                qWarning() << "Invalid (value) Datapoint (NaN) in " +name;
            }else{

                if(plot == true)
                {
                    g->addData(key,val);
                }
                valid = true;
            }
        }else{
            qWarning() << "Invalid (key) Datapoint (NaN) in " +name;
        }

        DataPoint* dp = new DataPoint(dt,val,valid);
        waveData->append(dp);
    }

    return QPair<DataContainer*,QCPGraph*>(new Wave(name,source,waveType,unit,waveData,patId,isInterpolatable),g);
}

void MainWindow::on_loadDistribButton_clicked()
{
    QHash<DataContainer *, QCPGraph *> *localLoadWaves = loadWaves("Daten für die Verteilung Laden. Eine Datei pro Kurve.",false);
    delete distrData;
    distrData = new QList<DataContainer*>(localLoadWaves->keys());
    delete localLoadWaves;
    if(distrData == nullptr || distrData->isEmpty())
    {
        qWarning() << "Distributionwaves could not be loaded!";
        QMessageBox msg;
        msg.setText("Es konnten keine Daten zur Verteilung geladen werden. Versuchen Sie es erneut.");
        msg.exec();
    }else{
        qInfo() << "Distributionwaves loaded successfully!";
        QMessageBox msg;
        msg.setText("Die Daten zur Verteilung wurden erfolgreich geladen.\nSie werden nun verarbeitet, das kann einige Minuten dauern.\nHol dir einen Tee ☕ und bleibe geduldig.");
        //FIXME imrpovement heursitic for time (x sec * waves^2)
        msg.exec();
    }
    ui->progressBar->setValue(0);
    ui->progressBar->setTextVisible(true);
    ui->progressBar->setFormat("Verarbeite...");
    calcDistances();
    if(anonData == nullptr || anonData->isEmpty() )
    {
        ui->anonymizationButton->setToolTip("Daten müssen geladen werden!");
    }

    if(distrData != nullptr && !distrData->isEmpty())
    {
        enableWidget(ui->genDistribButton,true);
    }else{
        disableWidget(ui->genDistribButton);
    }
}

void MainWindow::on_distanceList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
    if(current != previous && (anonDistanceCache != nullptr || distribDistanceChache != nullptr) && previous != nullptr)
    {
        calcDistances();
    }
}

void MainWindow::calcDistances(bool segments)
{
    if(segments == true)
    {
        // QMessageBox::information(this,"Berechne...","Die Abstände der Segmente werden berechnet.\n Das kann sehr lange dauern, wenn es viele Segmente sind.");
    }

    ui->progressBar->setValue(0);
    ui->progressBar->setTextVisible(true);
    ui->progressBar->setFormat("Berechne Abstände...");
    clearDistanceCaches();
    bool success = true;
    double minDist = INFINITY;
    double maxDist = 0;
    if(ui->extendedOptionsCheckbox->isChecked() && ui->sepDistrCheckbox->isChecked() && distrData != nullptr && !distrData->isEmpty())
    {//calc on dedicated distribution data
        calcDistanceHelper(*distrData,false);
        minDist = minDistribDist;
        maxDist = maxDistribDist;
        if(anonData != nullptr && !anonData->isEmpty())
        {
            int i = 0;
            for(QHash<DataContainer*,QCPGraph*> *hash: *anonData)
            {
                calcDistanceHelper(hash->keys(),true,i);
                if(minAnonDist.contains(i) && maxAnonDist.contains(i))
                {
                    minDist = min(minDist,minAnonDist.value(i));
                    maxDist = max(maxDist,maxAnonDist.value(i));
                }else{
                    qWarning() << "Min- or Max-Value for anonymization was not found!";
                }
                i++;
            }
        }
    }else if((!ui->extendedOptionsCheckbox->isChecked() || !ui->sepDistrCheckbox->isChecked()) && anonData != nullptr && !anonData->isEmpty())
    {//calc on anonymization data
        int i = 0;
        for(QHash<DataContainer*,QCPGraph*> *hash: *anonData)
        {
            calcDistanceHelper(hash->keys(),true,i);
            minDist = min(minDist,minAnonDist.value(i));
            maxDist = max(maxDist,maxAnonDist.value(i));
            i++;
        }
    }else{//no (matching) data loaded, do nothing
        success = false;
    }


    //set slider
    success = success && anonDistanceCache != nullptr && !anonDistanceCache->isEmpty() && !isnan(minDist) && !isnan(maxDist);
    ui->distrSensLabel->setEnabled(success);
    ui->distrSensOutput->setEnabled(success);
    ui->distrSensSlider->setEnabled(success);
    ui->dropDataSlider->setEnabled(success);
    if(success == true)
    {
        ui->distrSensSlider->setMinimum(minDist);
        ui->distrSensSlider->setMaximum(maxDist);
        ui->distrSensSlider->setValue(maxDist * DEFAULT_V_FACTOR );
        getD();
        getV();
        getS();
    }else{
        ui->distrSensOutput->setText("NaN");
        ui->dropDataOutput->setText("NaN");
    }

    ui->progressBar->setFormat("Abstände berechnet.");
    ui->progressBar->setValue(100);
}

void MainWindow::clearDistanceCaches()
{
    if(anonDistanceCache != nullptr)
    {
        for(QHash<DataContainer*,QHash<DataContainer*,double>*>* hash : anonDistanceCache->values())
        {
            hash->clear();
            delete hash;
        }
        anonDistanceCache->clear();
    }

    if(distribDistanceChache != nullptr)
    {
        distribDistanceChache->clear();
    }
}

void MainWindow::calcSegDistances()
{
    calcDistances(true);
}

void MainWindow::calcDistanceHelper(QList<DataContainer *> dat,bool anonData,int segment)
{
    //FIXME improvement, if wave waves in plot, collor according to distance.
    // Base color(center?) go l/r
    //or just color randomly
    //or add tooltip on wave with distance-list
    //FIXME improvment measure time
    dataDistMetric = getCurrentDataDistanceMetric();
    QHash<DataContainer*,QHash<DataContainer*,double>*>* distanceChache;
    double minDist = nan("");
    double maxDist = nan("");

    if(anonData == true)//FIXME Improvment maybe ADD INFINITY FOR ALL OTHER SEGMENTS ?
    {
        if(anonDistanceCache == nullptr)
        {
            anonDistanceCache = new QMap<int,QHash<DataContainer*,QHash<DataContainer*,double>*>*>();
        }
        if(anonDistanceCache->contains(segment))
        {
            delete anonDistanceCache->value(segment);
        }
        distanceChache = new QHash<DataContainer*,QHash<DataContainer*,double>*>();
        anonDistanceCache->insert(segment,distanceChache);
    }else{
        delete distribDistanceChache;
        minDistribDist = nan("");
        maxDistribDist = nan("");
        distribDistanceChache = new QHash<DataContainer*,QHash<DataContainer*,double>*>();
        distanceChache = distribDistanceChache;
    }
    //int progressCnt = 0;
    for(DataContainer* currWave : dat)
    {
        QHash<DataContainer*,double>* currWaveMap = new QHash<DataContainer*,double>();
        QList<DataContainer*> iteratDataLst(dat);
        iteratDataLst.removeAll(currWave);
        for(DataContainer* otherWave: iteratDataLst)
        {
            QCoreApplication::processEvents(QEventLoop::AllEvents,3);
            double dist = nan("");
            QHash<DataContainer*,QHash<DataContainer*,double>*>::iterator otherWaveIt = distanceChache->find(otherWave);
            QHash<DataContainer*,double>* otherWaveMap ;
            if(otherWaveIt == distanceChache->end() )
            {//otherWave has no map
                otherWaveMap = new QHash<DataContainer*,double>();
            }else {
                otherWaveMap = otherWaveIt.value();
                QHash<DataContainer*,double>::iterator otherWaveMapIt = otherWaveMap->find(currWave);
                if(otherWaveMapIt == otherWaveMap->end())
                {//otherWave has already a map, but no entry for this wave
                    break;
                }else{//there is already a matching entry for this wave in the otherwave's map, copy it (symmetry)
                    dist = otherWaveMap->value(currWave);
                }
            }
            if(isnan(dist))//calculate if not already calculated before
            {
                dist = dataDistMetric->distance(DataContainer(currWave),DataContainer(otherWave));//would be casted, but better readable IMO
                otherWaveMap->insert(currWave,dist);
            }
            if(isnan(dist)){//an error occured in the calculation
                qCritical() << QString("The distance between %1 and %2 was nan!").arg(((Wave*)currWave)->getName()).arg(((Wave*)otherWave)->getName());
            }else{
                if(isnan(minDist) || dist < minDist)
                {
                    minDist = dist;
                }
                if(isnan(maxDist) || dist > maxDist)
                {
                    maxDist = dist;
                }
            }
            currWaveMap->insert(otherWave,dist);
        }
        if(anonData == true)
        {
            minAnonDist.insert(segment,minDist);
            maxAnonDist.insert(segment,maxDist);
        }else{
            minDistribDist = minDist;
            maxDistribDist = maxDist;
        }
        distanceChache->insert(currWave,currWaveMap);
        // progressCnt++;
        //i->progressBar->setValue(progressCnt/dat.size());
    }
}



void MainWindow::on_sepDistrCheckbox_stateChanged(int )
{

    bool state = ui->extendedOptionsCheckbox->isChecked() && ui->sepDistrCheckbox->isChecked();
    ui->loadDistribButton->setEnabled(state);
    if(state == true && (distrData == nullptr || distrData->isEmpty()))
    {
        ui->anonymizationButton->setEnabled(false);
        ui->anonymizationButton->setToolTip("Verteilungsdaten müssen geladen werden!");
    }
}

Distribution *MainWindow::calcDistrib(DataDistanceMetric *metric,int segment, double v)
{

    ui->progressBar->setValue(0);
    ui->progressBar->setTextVisible(true);
    ui->progressBar->setFormat("Berechne Verteilung...");

    double realV = v;

    if(isnan(v) || metric == nullptr)
    {
        qCritical() << "Could not generate Distribution, maximum distribution cluster-distance (v) was NaN! or metric was null .";
        QMessageBox msg;
        msg.setText("Verteilung konnte nicht erzeugt werden. Da der maximale Verteilungsabstand (v) NaN oder die Metrik NULL war. Versuchen Sie es erneut");
        msg.exec();
        return nullptr;
    }else{
        if((anonDistanceCache == nullptr || anonDistanceCache->isEmpty()) && (distribDistanceChache == nullptr || distribDistanceChache->isEmpty()))
        {
            qCritical() << "Could not generate Distribution, distances could not be calculated!";
            QMessageBox msg;
            msg.setText("Verteilung konnte nicht erzeugt werden. Da keine Abstände berechnet waren. Versuchen Sie es erneut");
            msg.exec();
            return nullptr;
        }else{
            QHash<DataContainer*,QHash<DataContainer*,double>*>* distanceCache = nullptr;
            if(distribDistanceChache == nullptr || distribDistanceChache->isEmpty())
            {

                if(anonDistanceCache->contains(segment))
                {
                    distanceCache = anonDistanceCache->value(segment);
                }else{
                    qCritical() << "No Anon-Distancecache found for segment " + QString::number(segment);
                }
            }else{
                distanceCache = distribDistanceChache;
            }
            ///Calculating mean distance by datacontainer for seeding
            QList<DataContainer *> localKeys = distanceCache->keys();
            QMap<double,QList<DataContainer*>*> meanDistances;
            QList<DataContainer*> fullData;

            for(DataContainer* key: localKeys)
            {
                fullData.append(key);
                double meanDist = 0;
                QList<double> distances = distanceCache->value(key)->values();
                for(double dist : distances)
                {
                    meanDist +=dist;
                }
                meanDist = meanDist/localKeys.size();
                QList<DataContainer*>* vals;
                QMultiMap<double,QList<DataContainer*>*>::iterator localFind = meanDistances.find(meanDist);
                if(localFind != meanDistances.end())
                {
                    vals = localFind.value();
                }else{
                    vals =  new QList<DataContainer*>();
                }
                vals->append(key);
                meanDistances.insert(meanDist,vals);
            }

            int fullSize = fullData.size();
            if(fullSize == 0)
            {
                return nullptr;
            }
            QHash<DataContainer*,Distribution::Distribution_cluster_t*> distribClusterRefKeyMap;
            QHash<DataContainer*,Distribution::Distribution_cluster_t*> distribClusterMinKeyMap;
            QHash<DataContainer*,Distribution::Distribution_cluster_t*> distribClusterMaxKeyMap;
            int progressCnt = 0;
            QHash<DataContainer*,QHash<DataContainer*,double>*> localDistanceCache;
            for(DataContainer* key : distanceCache->keys())
            {
                QHash<DataContainer *, double> *  val = new QHash<DataContainer*,double>(*distanceCache->value(key));
                localDistanceCache.insert(key,val);
            }
            QList<DataContainer*> deletedContainers; //as not all keys are in the same distance-list
            QList<double> mdistKeys = meanDistances.keys();


            for(int i = mdistKeys.size() -1 ; i >= 0 ;i--)
            {
                QList<DataContainer*> *currWaves = meanDistances.value(mdistKeys.at(i));
                for(DataContainer* delCont : deletedContainers)
                {
                    if(currWaves->removeAll(delCont) > 0)
                    {
                        deletedContainers.removeAll(delCont);//should only occure once => save some time
                    }
                }
                for(DataContainer* currWave : *currWaves)
                {
                    if(currWave == nullptr)
                    {
                        qDebug() << "Nullptr wave in calcDistrib SHOULD NOT HAPPEN!";
                    }
                    currWaves->removeAll(currWave);
                    fullData.removeAll(currWave);
                    QHash<DataContainer*,Distribution::Distribution_cluster_t*>::iterator distribClusterIt = distribClusterRefKeyMap.find(currWave);
                    Distribution::Distribution_cluster_t* currCluster = nullptr;
                    if(distribClusterIt != distribClusterRefKeyMap.end())
                    {
                        currCluster = distribClusterIt.value();
                    }else{
                        currCluster = new Distribution::Distribution_cluster_t;
                        currCluster->refKey = currWave;
                        currCluster->minKey = currWave;
                        currCluster->maxKey = currWave;
                        distribClusterRefKeyMap.insert(currWave,currCluster);
                        distribClusterMinKeyMap.insert(currWave,currCluster);
                        distribClusterMaxKeyMap.insert(currWave,currCluster);
                    }
                    currCluster->data->append(currWave);

                    //TODO move to helper
                    QPair<DataContainer*,double>* bestMatch = nullptr;
                    do{
                        bestMatch = calcDistribBestWaveHelper(currWave,fullData,&localDistanceCache);

                        if(bestMatch != nullptr && bestMatch->second <= v)
                        {
                            if(currWaves->removeAll(bestMatch->first) == 0)//best wave not in current list, remove from rest
                            {
                                deletedContainers.append(bestMatch->first);
                            }
                            localDistanceCache.remove(bestMatch->first);
                            for(DataContainer* cont : localDistanceCache.keys())
                            {
                                localDistanceCache.value(cont)->remove(bestMatch->first);
                            }
                            //FIXME WORKE HERE improvement border to border not refkey to border?
                            // + give back clusters to distrib / main for plotting
                            QHash<DataContainer*,Distribution::Distribution_cluster_t*>::iterator bestClusterRefIt = distribClusterRefKeyMap.find(bestMatch->first);
                            QHash<DataContainer*,Distribution::Distribution_cluster_t*>::iterator bestClusterMinIt = distribClusterMinKeyMap.find(bestMatch->first);
                            QHash<DataContainer*,Distribution::Distribution_cluster_t*>::iterator bestClusterMaxIt = distribClusterMaxKeyMap.find(bestMatch->first);
                            if(bestClusterRefIt != distribClusterRefKeyMap.end() || bestClusterMinIt != distribClusterMinKeyMap.end() || bestClusterMaxIt != distribClusterMaxKeyMap.end())//Merge clusters
                            {//merge the clusters if at least border hit
                                QHash<DataContainer*,Distribution::Distribution_cluster_t*> ::iterator bestClusterIt;
                                if(bestClusterRefIt != distribClusterRefKeyMap.end())//TODO not optimal, (left/right imply ref)
                                {
                                    bestClusterIt = bestClusterRefIt;
                                }else if(bestClusterMinIt != distribClusterMinKeyMap.end())
                                {
                                    bestClusterIt = bestClusterMinIt;
                                }else{
                                    bestClusterIt = bestClusterMaxIt;
                                }


                                Distribution::Distribution_cluster_t* bestCluster = bestClusterIt.value();

                                //delete old cluster from maps
                                distribClusterRefKeyMap.remove(bestCluster->refKey);
                                distribClusterMinKeyMap.remove(bestCluster->minKey);
                                distribClusterMaxKeyMap.remove(bestCluster->maxKey);

                                //calc new min/max & range
                                double currDist1 = distanceCache->value(currCluster->refKey)->value(bestCluster->minKey);
                                double currDist2 = distanceCache->value(currCluster->refKey)->value(bestCluster->maxKey);
                                double oldMinDist = distanceCache->value(currCluster->refKey)->value(currCluster->minKey);
                                double oldMaxDist = distanceCache->value(currCluster->refKey)->value(currCluster->maxKey);
                                bool updateRange = false;

                                if(currDist1 < oldMinDist)
                                {
                                    distribClusterMinKeyMap.remove(currCluster->minKey);
                                    currCluster->minKey = bestMatch->first;
                                    distribClusterMinKeyMap.insert(currCluster->minKey,currCluster);

                                    updateRange = true;
                                }
                                if(currDist1 > oldMaxDist)
                                {
                                    distribClusterMaxKeyMap.remove(currCluster->maxKey);
                                    currCluster->maxKey = bestMatch->first;
                                    distribClusterMaxKeyMap.insert(currCluster->maxKey,currCluster);
                                    updateRange = true;
                                }


                                if(currDist2 < oldMinDist)
                                {
                                    distribClusterMinKeyMap.remove(currCluster->minKey);
                                    currCluster->minKey = bestMatch->first;
                                    distribClusterMinKeyMap.insert(currCluster->minKey,currCluster);

                                    updateRange = true;
                                }
                                if(currDist2 > oldMaxDist)
                                {
                                    distribClusterMaxKeyMap.remove(currCluster->maxKey);
                                    currCluster->maxKey = bestMatch->first;
                                    distribClusterMaxKeyMap.insert(currCluster->maxKey,currCluster);
                                    updateRange = true;
                                }

                                if(updateRange)
                                {
                                    currCluster->range = distanceCache->value(currCluster->minKey)->value(currCluster->maxKey);
                                }

                                for(DataContainer* cont : *bestCluster->data)
                                {
                                    if(!currCluster->data->contains(cont))
                                    {
                                        currCluster->data->append(cont);
                                    }else{
                                        qWarning() << "Tried merging a cluster, but already contained some container of the new b!";
                                    }
                                }
                                delete bestCluster->data;
                                delete bestCluster;

                            }else{//add wave to cluster
                                currCluster->data->append(bestMatch->first);

                                //calc new min/max & range
                                double currDist = distanceCache->value(currCluster->refKey)->value(bestMatch->first);
                                double oldMinDist = distanceCache->value(currCluster->refKey)->value(currCluster->minKey);
                                double oldMaxDist = distanceCache->value(currCluster->refKey)->value(currCluster->maxKey);
                                bool updateRange = false;
                                if(currDist < oldMinDist)
                                {
                                    distribClusterMinKeyMap.remove(currCluster->minKey);
                                    currCluster->minKey = bestMatch->first;
                                    distribClusterMinKeyMap.insert(currCluster->minKey,currCluster);

                                    updateRange = true;
                                }
                                if(currDist > oldMaxDist)
                                {
                                    distribClusterMaxKeyMap.remove(currCluster->maxKey);
                                    currCluster->maxKey = bestMatch->first;
                                    distribClusterMaxKeyMap.insert(currCluster->maxKey,currCluster);
                                    updateRange = true;
                                }

                                if(updateRange)
                                {
                                    currCluster->range = distanceCache->value(currCluster->minKey)->value(currCluster->maxKey);
                                }

                            }

                        }
                    }while(bestMatch != nullptr && bestMatch->second <= v);
                    qInfo() << QString("Created a Distribution cluster with size %1").arg(currCluster->data->size());
                    progressCnt++;
                    ui->progressBar->setValue(progressCnt/fullSize);
                }

            }


            if(forceAddSingleDistribClusters() == true)
            {
                ui->progressBar->setValue(0);
                ui->progressBar->setTextVisible(true);
                ui->progressBar->setFormat("Eliminiere Einzelwertcluster...");
                //FIXME USE PROGRESSBAR
                QList<DataContainer*> singleElementClusters;

                for(int i = 0; i< distribClusterRefKeyMap.size(); i++)
                {
                    int sze = distribClusterRefKeyMap.value(distribClusterRefKeyMap.keys().at(i))->data->size();
                    if(sze == 1)
                    {
                        DataContainer* key = distribClusterRefKeyMap.keys().at(i);
                        singleElementClusters.append(key);
                    }
                }




                ui->progressBar->setValue(10);
                while(!singleElementClusters.isEmpty())
                {
                    DataContainer* singleKey = singleElementClusters.takeFirst();
                    double minDist = nan("");
                    DataContainer* refKeyMinDist = nullptr;
                    QHash<DataContainer*,Distribution::Distribution_cluster_t*>::iterator clusterIt = distribClusterRefKeyMap.begin();
                    while(clusterIt != distribClusterRefKeyMap.end())
                    {
                        DataContainer * key = clusterIt.key();
                        if(key != singleKey)
                        {
                            Distribution::Distribution_cluster_t* localCluster = distribClusterRefKeyMap.value(key);

                            double dist1 = distanceCache->value(localCluster->minKey)->value(singleKey);
                            double dist2 = distanceCache->value(localCluster->maxKey)->value(singleKey);
                            if(isnan(minDist) || dist1 < minDist || dist2 < minDist)
                            {
                                if(dist1 < dist2)
                                {
                                    minDist = dist1;
                                }else{
                                    minDist = dist2;
                                }
                                refKeyMinDist = key;
                            }
                        }
                        clusterIt++;
                    }

                    Distribution::Distribution_cluster_t* bestCluster = distribClusterRefKeyMap.value(refKeyMinDist);
                    Distribution::Distribution_cluster_t* oldCluster = distribClusterRefKeyMap.value(singleKey);
                    if(bestCluster->data->size() == 1)
                    {
                        singleElementClusters.removeAll(bestCluster->refKey);
                    }
                    if(!bestCluster->data->contains(singleKey))
                    {
                        bestCluster->data->append(singleKey);
                    }else{
                        qWarning() << "Tried adding a single cluster, but already contained the key of the new member!";
                    }


                    //calc new min/max & range
                    double currDist = distanceCache->value(bestCluster->refKey)->value(singleKey);
                    double oldMinDist = distanceCache->value(bestCluster->refKey)->value(bestCluster->minKey);
                    double oldMaxDist = distanceCache->value(bestCluster->refKey)->value(bestCluster->maxKey);

                    if(currDist > realV)
                    {
                        realV = currDist;
                    }

                    bool updateRange = false;
                    if(currDist < oldMinDist)
                    {
                        distribClusterMinKeyMap.remove(bestCluster->minKey);
                        bestCluster->minKey = singleKey;
                        distribClusterMinKeyMap.insert(bestCluster->minKey,bestCluster);

                        updateRange = true;
                    }
                    if(currDist > oldMaxDist)
                    {
                        distribClusterMaxKeyMap.remove(bestCluster->maxKey);
                        bestCluster->maxKey = singleKey;
                        distribClusterMaxKeyMap.insert(bestCluster->maxKey,bestCluster);
                        updateRange = true;
                    }

                    if(updateRange)
                    {
                        bestCluster->range = distanceCache->value(bestCluster->minKey)->value(bestCluster->maxKey);
                    }


                    distribClusterMaxKeyMap.remove(singleKey);
                    distribClusterMinKeyMap.remove(singleKey);
                    distribClusterRefKeyMap.remove(singleKey);
                    delete oldCluster->data;
                    delete oldCluster;

                }

                ui->progressBar->setValue(100);

            }
            QHash<DataContainer*,unsigned int> clusterObservations;
            Distribution* distr = nullptr;
            if(distrMap->contains(segment))
            {
                distr = distrMap->value(segment);
            }
            QList<Distribution::Distribution_cluster_t*>* clusterLst = nullptr;
            if(distrClusterMap == nullptr)
            {
                distrClusterMap = new QMap<Distribution*,QList<Distribution::Distribution_cluster_t*>*>();
            }
            if(distrMap->contains(segment) && distrClusterMap->contains(distr))
            {
                QList<Distribution::Distribution_cluster_t *> * lst = distrClusterMap->value(distrMap->value(segment));
                for(Distribution::Distribution_cluster_t* c : *lst)
                {
                    delete c;
                }
                lst->clear();
                clusterLst = lst;
            }else{
                clusterLst = new QList<Distribution::Distribution_cluster_t*>();
            }

            for(DataContainer* key : distribClusterRefKeyMap.keys())
            {
                clusterLst->append(distribClusterRefKeyMap.value(key));
                clusterObservations.insert(key,distribClusterRefKeyMap.value(key)->data->size());
            }
            if(distrClusterMap == nullptr)
            {
                distrClusterMap = new QMap<Distribution*,QList<Distribution::Distribution_cluster_t*>*>();
            }
            Distribution* tmp = Bucket::genDistrib(clusterObservations,metric,realV,ui->interpolatableCheckbox->isChecked());
            if(distr == nullptr)
            {
                distr = tmp;
            }else{
                *distr = Distribution(*tmp);
                delete tmp;
            }
            distrClusterMap->insert(distr,clusterLst);
            ui->progressBar->setFormat("Fertig");
            return distr;
        }
    }
}

QPair<DataContainer*,double>* MainWindow::calcDistribBestWaveHelper(DataContainer *currWave,QList<DataContainer*> data,QHash<DataContainer*,QHash<DataContainer*,double>*>* localDistanceChache)
{
    if(localDistanceChache == nullptr)
    {
        qCritical() << "Distancecache was nullptr can not get best wave";
        return nullptr;
    }
    QHash<DataContainer*,QHash<DataContainer*,double>*>::iterator currWaveMapIt = localDistanceChache->find(currWave);
    if(currWaveMapIt ==  localDistanceChache->end())
    {
        qCritical() << "Found no distances for a datacontainer ignoring it in the distribution";
        return nullptr;
    }else{
        QHash<DataContainer *, double> *currWaveMap = currWaveMapIt.value();
        double bestDist = nan("");
        DataContainer* bestCont = nullptr;
        for(DataContainer* otherWave : data)
        {
            if( currWave != otherWave)
            {
                QHash<DataContainer *, double>::iterator distIt = currWaveMap->find(otherWave);
                if(distIt == currWaveMap->end())
                {
                    qCritical() << "Found no distance for Datacontainer-combination ignoring it in the distribution";
                    continue;
                }else{
                    double dist = distIt.value();
                    if(isnan(dist))
                    {
                        qCritical() << "Distance was NaN for Datacontainer-combination ignoring it in the distribution";
                        continue;
                    }else if(isnan(bestDist) || dist < bestDist){
                        bestDist = dist;
                        bestCont = otherWave;
                    }
                }
            }else{
                continue;
            }
        }
        if(bestCont != nullptr && !isnan(bestDist))
        {
            return new QPair<DataContainer*,double>(bestCont,bestDist);
        }else{
            return nullptr;
        }
    }
}

void MainWindow::on_minValSpinbox_valueChanged(double arg1)
{
    if(ui->maxValSpinbox->value() <= arg1)
    {
        ui->minValSpinbox->setValue(ui->maxValSpinbox->value() - 0.1);
    }
}

void MainWindow::on_maxValSpinbox_valueChanged(double arg1)
{
    if(ui->minValSpinbox->value() >= arg1)
    {
        ui->maxValSpinbox->setValue(ui->minValSpinbox->value() + 0.1);
    }
}

void MainWindow::on_redistModeOne_clicked()
{
    ui->dropDataSlider->setEnabled(false);
    getD();
    getS();
}

void MainWindow::on_redistModeAll_clicked()
{
    ui->dropDataSlider->setEnabled(true);
    getD();
    getS();
}

void MainWindow::on_redistModeCase_clicked()
{
    ui->dropDataSlider->setEnabled(false);
    getD();
    getS();
}

void MainWindow::on_redistModeFill_clicked()
{
    ui->dropDataSlider->setEnabled(true);
    getD();
    getS();
}

void MainWindow::plotEQCs(QList<EquivalenceClass *> *lst,int colorIdxOffset)
{
    if(lst != nullptr && !lst->isEmpty())
    {
        if(anonData != nullptr && (lst  == nullptr || lst->isEmpty()))//TODO optimize graphs can be
        {//reset pen if no results and make visible
            QPen defaultPen = QPen(Qt::blue);
            defaultPen.setStyle(Qt::PenStyle::SolidLine);

            for(QHash<DataContainer*,QCPGraph*> *hash: *anonData)
            {
                for(QCPGraph* g : hash->values())
                {
                    g->setPen(defaultPen);
                    g->setVisible(true);
                }
            }
            if(droppedGraphs != nullptr)
            {
                droppedGraphs->clear();
            }
        }

        if(result == nullptr)
        {
            result = new QHash<EquivalenceClass*,QPair<QCPGraph*,QCPGraph*>>();
        }

        QList<QColor> colorLst = generateColors(colorIdxOffset + lst->size());//TODO optimize potential
        for(int i = 0; i < lst->size(); i++)
        {


            QCoreApplication::processEvents(QEventLoop::AllEvents,10);
            EquivalenceClass* eqc = lst->at(i);
            QColor color = colorLst.at(colorIdxOffset +i);
            if(eqc != nullptr && eqc->isValid())
            {

                if(!eqc->calcHull(ui->hullStepsizeSpinbox->value()))//always refresh hull
                {
                    qCritical() << QString("EQC %1 was not plotted, as no hull could be calculated!").arg(i);
                    continue;
                }

                QList<QPair<DataPoint *, DataPoint *> *> hull = eqc->getHull();

                //setup eqc-design
                QPen memberPen = QPen(color);
                memberPen.setStyle(Qt::PenStyle::DashLine);
                QPen dropPen = QPen(color);
                dropPen.setStyle(Qt::PenStyle::DotLine);
                QColor fillColor = color;
                fillColor.setAlpha(51);//20% opaque filling 100% opaque border & elements
                QCPGraph *lowerG = ui->graphPlotWidget->addGraph();
                lowerG->setPen(QPen(color));
                lowerG->setBrush(QBrush(fillColor));
                lowerG->setLineStyle(QCPGraph::LineStyle::lsLine);
                lowerG->setScatterStyle(QCPScatterStyle::ssNone);
                QCPGraph *upperG = ui->graphPlotWidget->addGraph();
                upperG->setPen(QPen(color));
                upperG->setBrush(QBrush(fillColor));
                upperG->setLineStyle(QCPGraph::LineStyle::lsLine);
                upperG->setScatterStyle(QCPScatterStyle::ssNone);
                lowerG->setChannelFillGraph(upperG);
                upperG->setChannelFillGraph(lowerG);

                if(droppedGraphs == nullptr)
                {
                    droppedGraphs = new QHash<DataContainer*,QCPGraph*>();
                }
                for(DataContainer* cont : eqc->getDroppedData())
                {

                    QCPGraph * contG =  nullptr;
                    QHash<DataContainer*,QCPGraph*>::iterator gIt = droppedGraphs->find(cont);
                    if(gIt == droppedGraphs->end())
                    {
                        for(QHash<DataContainer*,QCPGraph*>* hash : *anonData)
                        {
                            if(hash->contains(cont))
                            {
                                contG = hash->value(cont);
                                break;
                            }
                        }
                        if(contG != nullptr)
                        {
                            droppedGraphs->insert(cont,contG);
                        }else{
                            qCritical() << "Invalid dropped graph / not found in data!";
                        }
                    }else{
                        contG = gIt.value();
                    }
                    contG->setPen(dropPen);

                    if((ui->graphplotDetailsCheckbox->checkState() != Qt::Unchecked && eqc->getKReal() <= 1) || ui->graphplotDetailsCheckbox->checkState() == Qt::Checked)
                    {
                        contG->setVisible(false);
                    }else{
                        contG->setVisible(true);
                    }
                }


                for(DataContainer* cont : eqc->getRawData())
                {
                    QCPGraph * contG =  nullptr;
                    for(QHash<DataContainer*,QCPGraph*>* hash : *anonData)
                    {
                        if(hash->contains(cont))
                        {
                            contG = hash->value(cont);
                            break;
                        }
                    }
                    if(contG != nullptr)
                    {
                        contG->setPen(memberPen);
                        if(ui->graphplotDetailsCheckbox->checkState() != Qt::Unchecked && eqc->getKReal() <= 1)
                        {
                            contG->setVisible(false);
                        }else{
                            contG->setVisible(true);
                        }
                    }else{
                        qCritical() << "Invalid member graph / not found in data!";
                    }

                }
                if(ui->graphplotDetailsCheckbox->checkState() != Qt::Unchecked && eqc->getKReal() <= 1)
                {
                    qInfo() << "Single element EQC ignored for plotting";
                    continue;
                }

                for(QPair<DataPoint *, DataPoint *> * pair: hull)//generate graphs from hull
                {
                    if(pair->first->isValidValue() && pair->second->isValidValue())
                    {
                        double x1 = pair->first->getTime().toMSecsSinceEpoch() / 1000.0;
                        double y1 = pair->first->getValue();
                        lowerG->addData(x1,y1);

                        double x2 = pair->second->getTime().toMSecsSinceEpoch() / 1000.0;
                        double y2 = pair->second->getValue();
                        upperG->addData(x2,y2);

                    }else{
                        qInfo() << "Ingoring a Hull-value as not all bounds are valid!";
                    }
                }
                if(result->contains(eqc))
                {
                    QPair<QCPGraph *, QCPGraph *> oldHull = result->value(eqc);
                    ui->graphPlotWidget->removeGraph(oldHull.first);
                    ui->graphPlotWidget->removeGraph(oldHull.second);
                    result->remove(eqc);
                }

                if(ui->interpolatableCheckbox->isChecked() == false)
                {
                    lowerG->setLineStyle(QCPGraph::LineStyle::lsStepLeft);
                    upperG->setLineStyle(QCPGraph::LineStyle::lsStepLeft);
                }else{
                    lowerG->setLineStyle(QCPGraph::LineStyle::lsLine);
                    upperG->setLineStyle(QCPGraph::LineStyle::lsLine);
                }
                result->insert(eqc,QPair(lowerG,upperG));

            }else{
                qWarning() << QString("EQC %1 was not plotted, as it was nullptr or invalid").arg(i);
                QMessageBox msg;
                msg.setText(QString("Die EQC %1 wurde nicht anonymisiert, sie war leer oder ungültig!").arg(i));
                msg.exec();
            }

        }

        ui->graphPlotWidget->replot();
        ui->graphPlotWidget->rescaleAxes();

    }else{
        qWarning() << "No EQCs were plotted, the list was empty or nullptr";
        if(automaticEval == false)
        {
            QMessageBox msg;
            msg.setText("Es wurden keine Daten anonymisiert, die Liste war leer oder ungültig!");
            msg.exec();
        }
    }
}

QList<QColor> MainWindow::generateColors(unsigned int num)
{
    double golden_ratio_conjugate = 0.618033988749895;
    double h = rand();//random seed
    QList<QColor> ret;

    for(unsigned int i = 0;i < num ; i++ )
    {
        h += golden_ratio_conjugate;
        int tmp = (int) h;
        h = h - tmp;//discard everyting before .
        double s = COLOR_S;
        double v = COLOR_V;
        double r, g, b;

        int h_i = (h*6);
        double f = h*6 - h_i;
        double p = v * (1 - s);
        double q = v * (1 - f*s);
        double t = v * (1 - (1 - f) * s);

        switch(h_i)
        {
        case 0:
            r = v;
            g = t;
            b = p;
            break;
        case 1:
            r = q;
            g = v;
            b = p;
            break;
        case 2:
            r = p;
            g = v;
            b = t;
            break;
        case 3:
            r = p;
            g = q;
            b = v;
            break;
        case 4:
            r = t;
            g = p;
            b = v;
            break;
        case 5:
            r = v;
            g = p;
            b = q;
            break;
        default:
            r = 0;
            g = 0;
            b = 0;
            qWarning() << "Error converting HSV to RGB";
        }
        int ri,gi,bi;
        ri = r*255;
        gi = g*255;
        bi = b*255;
        ret.append(QColor(ri,gi,bi));
    }
    return ret;
}


void MainWindow::on_genDistribButton_clicked()
{

    genDistrib();
    bool vis = (anonData->size() > 1);

    ui->bucketSizeCurrSegmentLabel->setVisible(vis);
    ui->bucketSizeCurrSegmentOutput->setVisible(vis);
    ui->segmentSelectLabel->setVisible(vis);
    ui->segmentSelectScrollbar->setVisible(vis);

}

void MainWindow::plotDistrib(Distribution *distr,int segment,bool recalc)
{
    if(distr != nullptr)
    {
        Qt::CheckState checkState = ui->extendedDistribCheckbox->checkState();

        int numCurves = 0;
        if(!ui->extendedOptionsCheckbox->isChecked() || !ui->sepDistrCheckbox->isChecked())
        {
            if(anonData != nullptr)
            {
                numCurves = 0;
                for(QHash<DataContainer*,QCPGraph*> *hash : *anonData)
                {
                    numCurves +=hash->size();
                }
            }
        }else{
            if(distrData != nullptr)
            {
                numCurves = distrData->size();
            }
        }
        double v = getV();
        double realV = distr->getV();
        if( v == realV)
        {
            ui->distribPlotWidget->setToolTip(QString("Verteilung von %1 Kurven mit V=%2!").arg(numCurves).arg(v));
        }else{
            ui->distribPlotWidget->setToolTip(QString("Verteilung von %1 Kurven mit Ziel V=%2, reales V=%3.").arg(numCurves).arg(v).arg(realV));
        }

        QHash<DataContainer*,QHash<DataContainer*,double>*>* distanceCache;
        if(distribDistanceChache == nullptr || distribDistanceChache->isEmpty())
        {
            if(anonDistanceCache->contains(segment))
            {
                distanceCache = anonDistanceCache->value(segment);
            }else{
                qCritical() << "No Anon-Distancecache found for segment " + QString::number(segment);
            }
        }else{
            distanceCache = distribDistanceChache;
        }



        bool single = false;
        if(!distrMap->contains(segment))
        {
            qCritical() << "Invalid segment-distribution, segment: " + QString::number(segment);
            return;
        }

        Distribution* distr = distrMap->value(segment);

        if(!distrClusterMap->contains(distr))
        {
            qCritical() << "Invalid segment-cluster, segment: " + QString::number(segment);
            return;
        }
        if(!boxplots.contains(distr))
        {
            boxplots.insert(distr, new QList<QPair<QCPStatisticalBox*,QCPGraph*>>());
        }
        if(!barplots.contains(distr))
        {
            barplots.insert(distr, new QList<QCPGraph*>());
        }
        QList<Distribution::Distribution_cluster_t*> clusterList(*distrClusterMap->value(distr));//copy to work on and remove clusters as needed
        QList<QColor> colors = generateColors(clusterList.size() +1);
        for(Distribution* currDistr : boxplots.keys())
        {
            QList<QPair<QCPStatisticalBox*,QCPGraph*>>* currBxPlts = boxplots.value(currDistr);
            for(QPair<QCPStatisticalBox*,QCPGraph*> pair : *currBxPlts)
            {
                QCPStatisticalBox* bx = pair.first;
                QCPGraph* g = pair.second;
                if(currDistr == distr)//show selected distribution in correct mode
                {
                    if(recalc == true )
                    {
                        ui->distribPlotWidget->removePlottable(bx);
                        ui->distribPlotWidget->removeGraph(g);
                    }else if(checkState != Qt::CheckState::Unchecked)
                    {
                        bx->setVisible(true);
                        g->setVisible(true);
                        if(checkState == Qt::PartiallyChecked)
                        {
                            bx->setKeyAxis(ui->distribPlotWidget->xAxis);
                            bx->setValueAxis(ui->distribPlotWidget->yAxis2);
                            g->setKeyAxis(ui->distribPlotWidget->xAxis);
                            g->setValueAxis(ui->distribPlotWidget->yAxis);
                        }else if(checkState == Qt::Checked)
                        {
                            bx->setKeyAxis(ui->distribPlotWidget->xAxis2);
                            bx->setValueAxis(ui->distribPlotWidget->yAxis2);
                            g->setKeyAxis(ui->distribPlotWidget->xAxis2);
                            g->setValueAxis(ui->distribPlotWidget->yAxis);
                        }
                    }else{
                        bx->setVisible(false);
                        g->setVisible(false);
                    }
                }else{//hide all other distrib-plots
                    bx->setVisible(false);
                    g->setVisible(false);
                }
            }
        }

        for(Distribution* currDistr : barplots.keys())
        {
            QList<QCPGraph*>* currBarPlts = barplots.value(currDistr);
            for(QCPGraph* g : *currBarPlts)
            {
                if(currDistr == distr)
                {//show barplots for current distribution if in correct mode
                    if(recalc == true)
                    {
                        g->setVisible(false);
                        ui->distribPlotWidget->removeGraph(g);
                    }else if(checkState != Qt::CheckState::PartiallyChecked)
                    {
                        g->setVisible(true);
                        g->setKeyAxis(ui->distribPlotWidget->xAxis);
                        g->setValueAxis(ui->distribPlotWidget->yAxis);
                    }else{
                        g->setVisible(false);
                    }
                }else{//hide all other plots
                    g->setVisible(false);

                }
            }
        }

        if(checkState == Qt::Unchecked)
        {
            ui->distribPlotWidget->xAxis->setLabel("Clusterabstand");
            ui->distribPlotWidget->yAxis->setLabel("Beobachtungen");
            ui->distribPlotWidget->yAxis2->setVisible(false);
            ui->distribPlotWidget->xAxis2->setVisible(false);
            ui->distribPlotWidget->yAxis2->setRangeReversed(false);
        }else if(checkState == Qt::PartiallyChecked)
        {
            ui->distribPlotWidget->xAxis->setLabel("Äußerer Clusterabstand");
            ui->distribPlotWidget->yAxis->setLabel("Beobachtungen(X)");
            ui->distribPlotWidget->yAxis2->setLabel("Innerer Clusterabstand");
            ui->distribPlotWidget->yAxis2->setVisible(true);
            ui->distribPlotWidget->xAxis2->setVisible(false);
            ui->distribPlotWidget->yAxis2->setRangeReversed(false);
        }else{
            ui->distribPlotWidget->xAxis->setLabel("Äußerer Clusterabstand");
            ui->distribPlotWidget->yAxis->setLabel("Beobachtungen(X/Balken)");
            ui->distribPlotWidget->yAxis2->setLabel("Innerer Clusterabstand");
            ui->distribPlotWidget->xAxis2->setLabel("Äußerer Clusterabstand");
            ui->distribPlotWidget->yAxis2->setVisible(true);
            ui->distribPlotWidget->xAxis2->setVisible(true);
            ui->distribPlotWidget->yAxis2->setRangeReversed(true);
        }

        if(recalc == true )
        {
            boxplots.value(distr)->clear();
            barplots.value(distr)->clear();
        }

        bool calcBoxplots = boxplots.value(distr)->isEmpty();
        bool calcBarplots = barplots.value(distr)->isEmpty();
        double nextX1 = 0;
        int lineWidth = 1;
        Distribution::Distribution_cluster_t* nextCluster = nullptr;
        Distribution::Distribution_cluster_t* currCluster = nullptr;
        int i = 0;
        while(!clusterList.isEmpty())//TODO optimise (calc only when needed)
        {
            if(currCluster == nullptr || nextCluster == nullptr)
            {
                currCluster = clusterList.takeFirst();
            }else if(nextCluster != nullptr)
            {
                clusterList.removeAll(nextCluster);
                currCluster = nextCluster;
            }

            if(currCluster->data->size() == 1)
            {
                single = true;
            }
            QColor brushCol(colors.at(i));
            brushCol.setAlpha(128);
            QColor penCol(colors.at(i));

            double dist = nan("");

            for(Distribution::Distribution_cluster_t* cluster : clusterList)//Find closest cluster
            {

                double d1 = distanceCache->value(cluster->maxKey)->value(currCluster->minKey);
                double d2 = distanceCache->value(cluster->maxKey)->value(currCluster->maxKey);
                double d = (d1 < d2) ? d1 : d2;
                if(isnan(dist) || d < dist )
                {
                    dist = d;
                    nextCluster = cluster;
                }
            }
            if(isnan(dist))
            {
                dist = 0;
            }
            double x1 = nextX1;
            double y = currCluster->data->size();
            double x2 = x1 + currCluster->range;
            nextX1 = x2 + dist;

            if((checkState == Qt::Checked || checkState == Qt::Unchecked) && (recalc == true || calcBarplots))
            {
                QCPGraph *g = ui->distribPlotWidget->addGraph(ui->distribPlotWidget->xAxis,ui->distribPlotWidget->yAxis);
                g->setVisible(true);
                g->setKeyAxis(ui->distribPlotWidget->xAxis);
                g->setValueAxis(ui->distribPlotWidget->yAxis);
                g->setScatterStyle(QCPScatterStyle::ssNone);
                g->setLineStyle(QCPGraph::LineStyle::lsLine);
                g->setBrush(QBrush(brushCol));
                g->setPen(QPen(penCol,lineWidth));
                g->addData(x1,0);
                g->addData(x1,y);
                g->addData(x2,y);
                g->addData(x2,0);
                for(int i = ceil(x2) ; i < floor(x1) ; i++)
                {
                    g->addData(i,currCluster->data->size());
                }
                g->setVisible(true);
                barplots.value(distr)->append(g);
            }else if((checkState == Qt::Checked || checkState == Qt::PartiallyChecked) && (recalc == true || calcBoxplots))
            {
                QCPAxis* keyAx = checkState == Qt::PartiallyChecked ? ui->distribPlotWidget->xAxis : ui->distribPlotWidget->xAxis2;
                QCPAxis* valAx = ui->distribPlotWidget->yAxis2;
                QCPStatisticalBox* currBox = new QCPStatisticalBox(keyAx,valAx);
                brushCol.setAlpha(64);
                currBox->setBrush(brushCol);
                currBox->setPen(penCol);

                QList<double> currDat;
                QList<DataContainer*> conts = *currCluster->data;
                while(!conts.isEmpty())
                {
                    DataContainer* cont1 = conts.takeFirst();
                    //FIXME optimise!
                    double dist = nan("");
                    for(DataContainer* cont2 : conts)
                    {
                        if(cont1 != cont2)
                        {
                            double currDist = distanceCache->value(cont1)->value(cont2);
                            if(isnan(dist) || currDist < dist)
                            {
                                dist = currDist;
                            }
                        }
                    }
                    if(isnan(dist))
                    {
                        dist = 0;
                    }
                    currDat.append(dist);
                }
                std::sort(currDat.begin(),currDat.end());

                double x3 = x1 + currCluster->range/2.0f;
                if(!currDat.empty())
                {
                    double min = currDat.at(0);
                    double lowerQuart = calcQuantil(0.25,currDat);
                    double median =  calcQuantil(0.5,currDat);
                    double upperQuart =  calcQuantil(0.75,currDat);
                    double max = currDat.at(currDat.size() -1);
                    if(currDat.size() <= 2)
                    {
                        lowerQuart = median - (MIN_BOXPLOT_HEIGHT/2.0f);
                        upperQuart = median + (MIN_BOXPLOT_HEIGHT/2.0f);;
                    }
                    currBox->addData(x3,min,lowerQuart,median,upperQuart,max,currDat.toVector());
                    currBox->setWidth(currCluster->range);
                }


                QCPGraph *g = ui->distribPlotWidget->addGraph(keyAx,ui->distribPlotWidget->yAxis);
                g->setScatterStyle(QCPScatterStyle::ssCross);
                g->setLineStyle(QCPGraph::LineStyle::lsNone);
                g->setBrush(QBrush(brushCol));
                g->setPen(QPen(penCol,lineWidth*5));
                g->addData(x3,y);

                boxplots.value(distr)->append(QPair<QCPStatisticalBox*,QCPGraph*>(currBox,g));
            }

            i++;
        }
        ui->distribPlotWidget->rescaleAxes(true);
        ui->distribPlotWidget->replot();
        if(single == true)
        {
            qWarning() << "The Distribution contains single curves!";
            QMessageBox msg;
            msg.setText("Die Verteilung enthält einzelne Kurven.\nD.h. es kann genau eine Äquivalenzklasse erzeugt werden!\nPassen Sie ggf. den Parameter V an.");
            msg.exec();
        }
    }else{
        qWarning() << "Nullptr distribution";
        QMessageBox::warning(this,"Ungültige Verteilung!","Die Verteilung war ungültig und wurde nicht gezeichnet!\nVersuchen Sie andere Parameter.");
    }
}

void MainWindow::calcMinMax()
{
    if(anonData != nullptr && !anonData->isEmpty())
    {
        minDomVal = nan("");
        maxDomVal = nan("");
        QList<DataContainer*> contLst;
        for(QHash<DataContainer*,QCPGraph*> *hash: *anonData)
        {
            contLst.append(hash->keys());
        }
        for(DataContainer* cont : contLst)
        {
            for(DataPoint* pt : cont->getData().values())
            {

                QCoreApplication::processEvents(QEventLoop::AllEvents,2);
                if(isnan(minDomVal) || minDomVal > pt->getValue())
                {
                    minDomVal = pt->getValue();
                }
                if(isnan(maxDomVal) || maxDomVal < pt->getValue())
                {
                    maxDomVal = pt->getValue();
                }
            }
        }
        if(!isnan(minDomVal) && !isnan(maxDomVal) && minDomVal <= maxDomVal)
        {
            ui->minValSpinbox->setValue(minDomVal);
            ui->maxValSpinbox->setValue(maxDomVal);
        }else{
            ui->minValSpinbox->setValue(DEFAULT_MIN_DOMVAL);
            ui->maxValSpinbox->setValue(DEFAULT_MAX_DOMVAL);
        }
        ui->minValSpinbox->setEnabled(true);
        ui->maxValSpinbox->setEnabled(true);
    }
}

void MainWindow::on_replotButton_clicked()
{
    if(this->eqcMap != nullptr && !this->eqcMap->isEmpty())
    {
        int offset = 0;
        for(QList<EquivalenceClass*>* lst : eqcMap->values())
        {
            plotEQCs(lst,offset);
            offset += lst->size();
        }
    }
}


void MainWindow::on_extendedDistribCheckbox_stateChanged(int )
{
    int idx = getCurrentSegmentIdx();
    if(distrMap->contains(idx))
    {
        plotDistrib(distrMap->value(idx),idx,false);
    }else{
        qCritical() << "Invalid distribution index: " + QString::number(idx);
        QMessageBox::warning(this,"Fehler!","Ungültiger Verteilungsindex!\nDas sollte nicht passieren...");
    }
}

void MainWindow::on_bucketTreeButton_clicked()
{
    genBucketTree();
    if(ui->bSlider->maximum() > 0)
    {
        ui->bSlider->setValue(1);
        getB();
    }
}

void MainWindow::on_bSlider_valueChanged(int )
{
    getB();
}




void MainWindow::on_fSlider_valueChanged(int )
{
    getF();
}


void MainWindow::on_eqcLstWidget_itemChanged(QListWidgetItem *item)
{
    if(eqcVisMap == nullptr || visEqcMap == nullptr || itemStateMap == nullptr || itemStateMap->find(item) == itemStateMap->end())
    {
        updateLegend();
    }
    if(item->checkState() != itemStateMap->value(item))
    {
        updateEqcVis(item);
    }
}

void MainWindow::on_evaluationButton_clicked()
{
    //FIXME improvement rather ugly, can be split into smaller modules... and UI elements not hard coded params

    QString title = this->windowTitle();
    bool evalDuration = false;
    int repeats = 10;
    bool repeatHull = false;
    bool repeatK = false;
    bool repeatB = false;

    bool evalRest = true;
    bool evalRestMultiDim = false;

    int defaultV = 16830;
    double defaultF = 0.4;
    int defaultK = 3;
    int defaultB = 1;
    double defaultD = INFINITY;
    double defaultS = 1.0;
    int defaultH = 500;
    bool defaultGreed = true;

    //FIXME iterate ILossRes !!
    ui->infoLossResSpinbox->setValue(500);

    //Current evaluation => more datapoints  split&not split (hull) for publcation (params for AWF & AWP) , later larger hull for SPO2
    QList<int> hList = {8000,4000,2000,1000,500,250,125};
    QList<int> vList ={16830};
    QList<double> fList ={0.4};
    QList<int> bList ={1,2,3};
    QList<int> kList ={3,4,5,6,7,8,9};
    QList<double> dList ={1.0};
    QList<double> sList ={1.0};//FIXME add more params after fixing S
    QList<bool> greedList ={true};

    QString targetDir = QFileDialog::getExistingDirectory(this,"Wählen Sie ein Zielverzeichnis für den Export.",QDir::homePath())+ QDir::separator();
    QFileInfo fi(targetDir);
    if(targetDir.isEmpty() || targetDir =="/" ||  !fi.isWritable())
    {
        QMessageBox::information(this,"Kein Exportverzeichnis gewählt!","Es wurde kein (gültiges) Verzeichnis für den Export der Daten gewählt!");
        return;
    }

    QString userPrefix = QInputDialog::getText(this,"Export-Prefix:","");

    automaticEval = true;
    if(anonData == nullptr || anonData->isEmpty())
    {
        loadData();
    }
    this->setEnabled(false);
    this->setToolTip("Das UI ist während der automatischen Auswertung deaktiviert!");

    QString statFle = userPrefix + QDateTime::currentDateTime().toString("yyyy-MM-dd_hhmmsszzz");
    ui->redistModeAll->setChecked(true);//FIXME ALL MODES?

    bool first = true;
    time_t durationSum = 0;
    QDateTime start = QDateTime::currentDateTime();
    if(evalDuration)
    {
        int totalRepeats = 0;
        int totalCount = 0;
        if(repeatHull == true)
        {
            totalRepeats += repeats *hList.size();
        }
        if(repeatK == true)
        {
            totalRepeats += repeats *kList.size();
        }
        if(repeatB == true)
        {
            totalRepeats += repeats *bList.size();
        }

        if(repeatHull)
        {
            evalResetParams(defaultV,defaultF,defaultB,defaultK,defaultD,defaultS,defaultGreed,defaultH);

            int paramI =0;
            for(int h: hList)
            {
                paramI++;
                ui->hullStepsizeSpinbox->setValue(h);
                for(int i = 0; i< repeats; i++)
                {
                    QDateTime durStart = QDateTime::currentDateTime();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    anonymize();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    QString  evalPrefix = QString("-v%1-f%2-k%3-b%4-d%5-s%6-h%7-").arg(ui->distrSensSlider->value()).arg(ui->fSlider->value()).arg(ui->kSlider->value()).arg(ui->bSlider->value()).arg(ui->dropDataSlider->value()).arg(ui->sSlider->value()).arg(ui->hullStepsizeSpinbox->value());

                    bool savePlot = true;
                    bool saveDistrib = false;

                    exportEval(targetDir,userPrefix+"runtime-varyH-",evalPrefix,"runtime-varyH-"+statFle,first,savePlot,saveDistrib);
                    first = false;

                    durationSum += durStart.msecsTo(QDateTime::currentDateTime());
                    totalCount++;
                    time_t estimate = (durationSum / totalCount  ) * (totalRepeats-(totalCount));
                    estimate = estimate /60000.0;

                    this->setWindowTitle(QString("H %7/%8, Wiederholung %1/%2, total: %6/%5  seit %3, noch ca. %4 min ").arg(i+1).arg(repeats).arg(start.toString(Qt::ISODate)).arg(estimate).arg(totalRepeats).arg(totalCount).arg(paramI).arg(hList.size()));
                }
            }
        }

        if(repeatK)
        {
            evalResetParams(defaultV,defaultF,defaultB,defaultK,defaultD,defaultS,defaultGreed,defaultH);
            first = true;
            int paramI = 0;
            for(int k: kList)
            {
                paramI++;
                ui->kSlider->setValue(k);
                for(int i = 0; i< repeats; i++)
                {
                    QDateTime durStart = QDateTime::currentDateTime();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    anonymize();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    QString evalPrefix = QString("-v%1-f%2-k%3-b%4-d%5-s%6-h%7-").arg(defaultV).arg(defaultF*100.0).arg(defaultK).arg(defaultB).arg(defaultD).arg(defaultS*1000.0).arg(ui->hullStepsizeSpinbox->value());

                    bool savePlot = true;
                    bool saveDistrib = false;

                    exportEval(targetDir,userPrefix+"runtime-varyK-",evalPrefix,"runtime-varyK-"+statFle,first,savePlot,saveDistrib);
                    first = false;

                    durationSum += durStart.msecsTo(QDateTime::currentDateTime());
                    totalCount++;
                    time_t estimate = (durationSum / totalCount  ) * (totalRepeats-(totalCount));
                    estimate = estimate /60000.0;


                    this->setWindowTitle(QString("K %7/%8, Wiederholung %1/%2, total: %6/%5  seit %3, noch ca. %4 min ").arg(i+1).arg(repeats).arg(start.toString(Qt::ISODate)).arg(estimate).arg(totalRepeats).arg(totalCount).arg(paramI).arg(kList.size()));
                }
            }
        }
        if(repeatB)
        {
            evalResetParams(defaultV,defaultF,defaultB,defaultK,defaultD,defaultS,defaultGreed,defaultH);
            first = true;
            int paramI = 0;
            for(int b: bList)
            {
                paramI++;
                ui->bSlider->setValue(b);
                for(int i = 0; i< repeats; i++)
                {
                    QDateTime durStart = QDateTime::currentDateTime();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    anonymize();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    QString evalPrefix = QString("-v%1-f%2-k%3-b%4-d%5-s%6-h%7-").arg(defaultV).arg(defaultF*100.0).arg(defaultK).arg(defaultB).arg(defaultD).arg(defaultS*1000.0).arg(ui->hullStepsizeSpinbox->value());

                    bool savePlot = true;
                    bool saveDistrib = false;

                    exportEval(targetDir,userPrefix+"runtime-varyB-",evalPrefix,"runtime-varyB-"+statFle,first,savePlot,saveDistrib);
                    first = false;

                    durationSum += durStart.msecsTo(QDateTime::currentDateTime());
                    totalCount++;
                    time_t estimate = (durationSum / totalCount  ) * (totalRepeats-(totalCount));
                    estimate = estimate /60000.0;


                    this->setWindowTitle(QString("B %7/%8,  Wiederholung %1/%2, total: %6/%5  seit %3, noch ca. %4 min ").arg(i+1).arg(repeats).arg(start.toString(Qt::ISODate)).arg(estimate).arg(totalRepeats).arg(totalCount).arg(paramI).arg(bList.size()));
                }
            }
        }
    }


    evalResetParams(defaultV,defaultF,defaultB,defaultK,defaultD,defaultS,defaultGreed,defaultH);//FIXME suboptimal, as unneededed trees generated...

    if(evalRest == true)
    {

        first = true;
        int lastV = 0;
        start = QDateTime::currentDateTime();
        durationSum = 0;
        if(evalRestMultiDim)
        {
            double totalSteps = vList.size() * fList.size() * bList.size() * kList.size() * dList.size() * sList.size() * greedList.size();
            int stepCount = 1;
            QString title = this->windowTitle();
            this->setWindowTitle(QString("%1 von %2 Parameterkombinationen").arg(stepCount).arg(totalSteps));
            for(int v : vList)
            {
                QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                ui->distrSensSlider->setValue(v);
                genDistrib();
                for(double f : fList)
                {
                    int fInt = f*100.0;
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    ui->fSlider->setValue(fInt);
                    genBucketTree();
                    for(int b : bList)
                    {
                        ui->bSlider->setValue(b);
                        for(int k : kList)
                        {
                            ui->kSlider->setValue(k);
                            for(double d : dList)
                            {
                                int dInt = d*1000.0;
                                if(isinf(d))
                                {
                                    ui->dropDataSlider->setValue(ui->dropDataSlider->maximum());
                                }else{
                                    ui->dropDataSlider->setValue(dInt);
                                }
                                QCoreApplication::processEvents(QEventLoop::AllEvents,1);

                                for(double s : sList )
                                {
                                    int sInt = s* 1000.0;
                                    ui->sSlider->setValue(sInt);
                                    QCoreApplication::processEvents(QEventLoop::AllEvents,1);
                                    for(bool greed : greedList)
                                    {
                                        ui->greedyCheckbox->setChecked(greed);
                                        QDateTime durStart = QDateTime::currentDateTime();
                                        QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                                        anonymize();
                                        QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                                        QString evalPrefix = "";
                                        if(greed == true)
                                        {
                                            evalPrefix = "greedy";
                                        }else{
                                            evalPrefix = "considerate";
                                        }
                                        evalPrefix = evalPrefix + QString("-v%1-f%2-k%3-b%4-d%5-s%6-h%7-").arg(v).arg(fInt).arg(k).arg(b).arg(dInt).arg(sInt).arg(ui->hullStepsizeSpinbox->value());

                                        bool savePlot = !result->isEmpty();
                                        bool saveDistrib = lastV != v;
                                        lastV = v;

                                        exportEval(targetDir,userPrefix,evalPrefix,statFle,first,savePlot,saveDistrib);
                                        first = false;
                                        durationSum += durStart.msecsTo(QDateTime::currentDateTime());
                                        stepCount++;
                                        time_t estimate = (durationSum / (stepCount)  ) * (totalSteps -stepCount);
                                        estimate = estimate /60000.0;
                                        this->setWindowTitle(QString("%1 von %2 Parameterkombinationen seit %3, noch ca. %4 min").arg(stepCount).arg(totalSteps).arg(start.toString(Qt::ISODate)).arg(estimate));

                                    }
                                }
                            }
                        }
                    }
                }
            }

        }else{
            double totalSteps = vList.size() + fList.size() + bList.size() + kList.size() + dList.size() + sList.size() + greedList.size();
            int stepCount = 1;
            this->setWindowTitle(QString("%1 von %2 Parameterkombinationen").arg(stepCount).arg(totalSteps));


            if(kList.length() > 1)
            {
                first = true;
                evalResetParams(defaultV,defaultF,defaultB,defaultK,defaultD,defaultS,defaultGreed,defaultH);
                for(int k: kList)
                {
                    ui->kSlider->setValue(k);
                    QDateTime durStart = QDateTime::currentDateTime();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    anonymize();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    QString evalPrefix = "";
                    if(ui->greedyCheckbox->isChecked() == true)
                    {
                        evalPrefix = "greedy";
                    }else{
                        evalPrefix = "considerate";
                    }
                    evalPrefix = evalPrefix + QString("-v%1-f%2-k%3-b%4-d%5-s%6-h%7-").arg(ui->distrSensSlider->value()).arg(ui->fSlider->value()).arg(ui->kSlider->value()).arg(ui->bSlider->value()).arg(ui->dropDataSlider->value()).arg(ui->sSlider->value()).arg(ui->hullStepsizeSpinbox->value());
                    evalPrefix = evalPrefix + "-varyK-";
                    bool savePlot = !result->isEmpty();
                    bool saveDistrib = lastV != defaultV;
                    lastV = defaultV;

                    exportEval(targetDir,userPrefix,evalPrefix,"varyK-"+statFle,first,savePlot,saveDistrib);
                    first = false;

                    durationSum += durStart.msecsTo(QDateTime::currentDateTime());
                    stepCount++;
                    time_t estimate = (durationSum / (stepCount)  ) * (totalSteps -stepCount);
                    estimate = estimate /60000.0;
                    this->setWindowTitle(QString("%1 von %2 Parameterkombinationen seit %3, noch ca. %4 min (K)").arg(stepCount).arg(totalSteps).arg(start.toString(Qt::ISODate)).arg(estimate));

                }
            }

            if(fList.length() > 1)
            {
                first = true;
                evalResetParams(defaultV,defaultF,defaultB,defaultK,defaultD,defaultS,defaultGreed,defaultH);
                for(double f: fList)
                {
                    ui->fSlider->setValue(f*100);
                    genBucketTree();
                    QDateTime durStart = QDateTime::currentDateTime();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    anonymize();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    QString evalPrefix = "";
                    if(ui->greedyCheckbox->isChecked() == true)
                    {
                        evalPrefix = "greedy";
                    }else{
                        evalPrefix = "considerate";
                    }
                    evalPrefix = evalPrefix + QString("-v%1-f%2-k%3-b%4-d%5-s%6-h%7-").arg(ui->distrSensSlider->value()).arg(ui->fSlider->value()).arg(ui->kSlider->value()).arg(ui->bSlider->value()).arg(ui->dropDataSlider->value()).arg(ui->sSlider->value()).arg(ui->hullStepsizeSpinbox->value());
                    evalPrefix = evalPrefix + "-varyF-";
                    bool savePlot = !result->isEmpty();
                    bool saveDistrib = lastV != defaultV;
                    lastV = defaultV;

                    exportEval(targetDir,userPrefix,evalPrefix,"varyF-"+statFle,first,savePlot,saveDistrib);
                    first = false;
                    stepCount++;

                    durationSum += durStart.msecsTo(QDateTime::currentDateTime());
                    time_t estimate = (durationSum / (stepCount)  ) * (totalSteps -1);
                    estimate = estimate /60000.0;
                    this->setWindowTitle(QString("%1 von %2 Parameterkombinationen seit %3, noch ca. %4 min (F)").arg(stepCount).arg(totalSteps).arg(start.toString(Qt::ISODate)).arg(estimate));

                }
            }

            if(bList.length() > 1)
            {
                first = true;
                evalResetParams(defaultV,defaultF,defaultB,defaultK,defaultD,defaultS,defaultGreed,defaultH);
                for(double b: bList)
                {
                    ui->bSlider->setValue(b);
                    QDateTime durStart = QDateTime::currentDateTime();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    anonymize();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    QString evalPrefix = "";
                    if(ui->greedyCheckbox->isChecked() == true)
                    {
                        evalPrefix = "greedy";
                    }else{
                        evalPrefix = "considerate";
                    }
                    evalPrefix = evalPrefix + QString("-v%1-f%2-k%3-b%4-d%5-s%6-h%7-").arg(ui->distrSensSlider->value()).arg(ui->fSlider->value()).arg(ui->kSlider->value()).arg(ui->bSlider->value()).arg(ui->dropDataSlider->value()).arg(ui->sSlider->value()).arg(ui->hullStepsizeSpinbox->value());
                    evalPrefix = evalPrefix + "-varyB-";
                    bool savePlot = !result->isEmpty();
                    bool saveDistrib = lastV != defaultV;
                    lastV = defaultV;

                    exportEval(targetDir,userPrefix,evalPrefix,"varyB-"+statFle,first,savePlot,saveDistrib);
                    first = false;
                    stepCount++;

                    durationSum += durStart.msecsTo(QDateTime::currentDateTime());
                    time_t estimate = (durationSum / (stepCount)  ) * (totalSteps -1);
                    estimate = estimate /60000.0;
                    this->setWindowTitle(QString("%1 von %2 Parameterkombinationen seit %3, noch ca. %4 min (B)").arg(stepCount).arg(totalSteps).arg(start.toString(Qt::ISODate)).arg(estimate));

                }
            }

            if(dList.length() > 1)
            {
                first = true;
                evalResetParams(defaultV,defaultF,defaultB,defaultK,defaultD,defaultS,defaultGreed,defaultH);
                for(double d: dList)
                {
                    ui->dropDataSlider->setValue(d*1000);
                    QDateTime durStart = QDateTime::currentDateTime();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    anonymize();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    QString evalPrefix = "";
                    if(ui->greedyCheckbox->isChecked() == true)
                    {
                        evalPrefix = "greedy";
                    }else{
                        evalPrefix = "considerate";
                    }
                    evalPrefix = evalPrefix + QString("-v%1-f%2-k%3-b%4-d%5-s%6-h%7-").arg(ui->distrSensSlider->value()).arg(ui->fSlider->value()).arg(ui->kSlider->value()).arg(ui->bSlider->value()).arg(ui->dropDataSlider->value()).arg(ui->sSlider->value()).arg(ui->hullStepsizeSpinbox->value());
                    evalPrefix = evalPrefix + "-varyD-";
                    bool savePlot = !result->isEmpty();
                    bool saveDistrib = lastV != defaultV;
                    lastV = defaultV;

                    exportEval(targetDir,userPrefix,evalPrefix,"varyD-"+statFle,first,savePlot,saveDistrib);
                    first = false;
                    stepCount++;

                    durationSum += durStart.msecsTo(QDateTime::currentDateTime());
                    time_t estimate = (durationSum / (stepCount)  ) * (totalSteps -1);
                    estimate = estimate /60000.0;
                    this->setWindowTitle(QString("%1 von %2 Parameterkombinationen seit %3, noch ca. %4 min (D)").arg(stepCount).arg(totalSteps).arg(start.toString(Qt::ISODate)).arg(estimate));

                }
            }

            if(sList.length() > 1)
            {
                first = true;
                evalResetParams(defaultV,defaultF,defaultB,defaultK,defaultD,defaultS,defaultGreed,defaultH);
                for(double s: sList)
                {
                    ui->dropDataSlider->setValue(s*1000);
                    QDateTime durStart = QDateTime::currentDateTime();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    anonymize();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    QString evalPrefix = "";
                    if(ui->greedyCheckbox->isChecked() == true)
                    {
                        evalPrefix = "greedy";
                    }else{
                        evalPrefix = "considerate";
                    }
                    evalPrefix = evalPrefix + QString("-v%1-f%2-k%3-b%4-d%5-s%6-h%7-").arg(ui->distrSensSlider->value()).arg(ui->fSlider->value()).arg(ui->kSlider->value()).arg(ui->bSlider->value()).arg(ui->dropDataSlider->value()).arg(ui->sSlider->value()).arg(ui->hullStepsizeSpinbox->value());
                    evalPrefix = evalPrefix + "-varyS-";
                    bool savePlot = !result->isEmpty();
                    bool saveDistrib = lastV != defaultV;
                    lastV = defaultV;

                    exportEval(targetDir,userPrefix,evalPrefix,"varyS-"+statFle,first,savePlot,saveDistrib);
                    first = false;
                    stepCount++;

                    durationSum += durStart.msecsTo(QDateTime::currentDateTime());
                    time_t estimate = (durationSum / (stepCount)  ) * (totalSteps -1);
                    estimate = estimate /60000.0;
                    this->setWindowTitle(QString("%1 von %2 Parameterkombinationen seit %3, noch ca. %4 min (S)").arg(stepCount).arg(totalSteps).arg(start.toString(Qt::ISODate)).arg(estimate));

                }
            }

            if(greedList.length() > 1)
            {
                first = true;
                evalResetParams(defaultV,defaultF,defaultB,defaultK,defaultD,defaultS,defaultGreed,defaultH);
                for(bool g: greedList)
                {
                    ui->greedyCheckbox->setChecked(g);
                    QDateTime durStart = QDateTime::currentDateTime();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    anonymize();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    QString evalPrefix = "";
                    if(ui->greedyCheckbox->isChecked() == true)
                    {
                        evalPrefix = "greedy";
                    }else{
                        evalPrefix = "considerate";
                    }
                    evalPrefix = evalPrefix + QString("-v%1-f%2-k%3-b%4-d%5-s%6-h%7-").arg(ui->distrSensSlider->value()).arg(ui->fSlider->value()).arg(ui->kSlider->value()).arg(ui->bSlider->value()).arg(ui->dropDataSlider->value()).arg(ui->sSlider->value()).arg(ui->hullStepsizeSpinbox->value());
                    evalPrefix = evalPrefix + "-varyGreed-";
                    bool savePlot = !result->isEmpty();
                    bool saveDistrib = lastV != defaultV;
                    lastV = defaultV;

                    exportEval(targetDir,userPrefix,evalPrefix,"varyG-"+statFle,first,savePlot,saveDistrib);
                    first = false;
                    stepCount++;

                    durationSum += durStart.msecsTo(QDateTime::currentDateTime());
                    time_t estimate = (durationSum / (stepCount)  ) * (totalSteps -1);
                    estimate = estimate /60000.0;
                    this->setWindowTitle(QString("%1 von %2 Parameterkombinationen seit %3, noch ca. %4 min (Greed)").arg(stepCount).arg(totalSteps).arg(start.toString(Qt::ISODate)).arg(estimate));

                }
            }

            if(hList.length() > 1)
            {
                first = true;
                evalResetParams(defaultV,defaultF,defaultB,defaultK,defaultD,defaultS,defaultGreed,defaultH);
                for(int h: hList)
                {
                    ui->hullStepsizeSpinbox->setValue(h);
                    QDateTime durStart = QDateTime::currentDateTime();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    anonymize();
                    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
                    QString evalPrefix = "hull";

                    evalPrefix = evalPrefix + QString("-v%1-f%2-k%3-b%4-d%5-s%6-h%7-").arg(ui->distrSensSlider->value()).arg(ui->fSlider->value()).arg(ui->kSlider->value()).arg(ui->bSlider->value()).arg(ui->dropDataSlider->value()).arg(ui->sSlider->value()).arg(ui->hullStepsizeSpinbox->value());
                    evalPrefix = evalPrefix + "-varyHull-";
                    bool savePlot = !result->isEmpty();
                    bool saveDistrib = lastV != defaultV;
                    lastV = defaultV;

                    exportEval(targetDir,userPrefix,evalPrefix,"varyH-"+statFle,first,savePlot,saveDistrib);
                    first = false;
                    stepCount++;

                    durationSum += durStart.msecsTo(QDateTime::currentDateTime());
                    time_t estimate = (durationSum / (stepCount)  ) * (totalSteps -1);
                    estimate = estimate /60000.0;
                    this->setWindowTitle(QString("%1 von %2 Parameterkombinationen seit %3, noch ca. %4 min (Hull)").arg(stepCount).arg(totalSteps).arg(start.toString(Qt::ISODate)).arg(estimate));

                }
            }
        }

    }

    this->setWindowTitle(title);
    this->setToolTip("");
    this->setEnabled(true);
    automaticEval = false;

}

void MainWindow::exportData(QString dir)
{
    if(eqcMap != nullptr)
    {
        ui->exportButton->setEnabled(true);
        QString targetDir;
        if(dir.isEmpty())
        {
            targetDir = QFileDialog::getExistingDirectory(this,"Wählen Sie ein Zielverzeichnis für den Export.",QDir::homePath())+ QDir::separator();
        }else{
            targetDir = dir;
        }
        QFileInfo fi(targetDir);
        if(targetDir.isEmpty() || targetDir =="/" ||  !fi.isWritable())
        {
            QMessageBox::warning(this,"Kein Exportverzeichnis gewählt!","Es wurde kein (gültiges) Verzeichnis für den Export der Daten gewählt!");
            return;
        }else{
            QString prefix = QInputDialog::getText(this,"Export-Prefix:","");
            targetDir += prefix;//FIXME  SECURITY not good practice...
            ui->progressBar->setFormat("Exportiere Daten...");

            QDateTime currDT = QDateTime::currentDateTime();
            bool first = true;
            for(int seg : eqcMap->keys())
            {

                ui->progressBar->setFormat(QString("Exportiere Segment %1 / %2").arg(seg).arg(eqcMap->size()));
                QList<EquivalenceClass*>* eqcSegLst = eqcMap->value(seg);
                exportDataSegment(first,targetDir, eqcSegLst,seg,currDT);
                first = false;
                QApplication::processEvents(QEventLoop::AllEvents,10);

            }
            exportRestorePlot();
            if(currentSegmentHighlight != nullptr)
            {
                currentSegmentHighlight->first->setVisible(false);
                currentSegmentHighlight->second->setVisible(false);
                ui->graphPlotWidget->replot();
            }

            QString plotFleName(targetDir  + currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_PLOT_SUFFIX+".pdf");
            if(ENGLISH_EXPORT_PLOTS == true)
            {
                ui->graphPlotWidget->xAxis->setLabel("Time(s)");
                ui->graphPlotWidget->replot();
            }
            ui->graphPlotWidget->savePdf(plotFleName);
            if(ENGLISH_EXPORT_PLOTS == true)
            {
                ui->graphPlotWidget->xAxis->setLabel("Zeit(s)");
                ui->graphPlotWidget->replot();
            }
            if(currentSegmentHighlight != nullptr)
            {
                currentSegmentHighlight->first->setVisible(true);
                currentSegmentHighlight->second->setVisible(true);
                ui->graphPlotWidget->replot();
            }
        }
    }else{
        qWarning() << "No EQCs to export!";
        if(automaticEval == false)
        {
            QMessageBox::warning(this,"Fehler beim Export!","Es sind keine Äquivalenzklassen zum exportieren vorhanden!");
        }
        ui->exportButton->setEnabled(false);
    }
}

void MainWindow::exportDataSegment(bool first, QString targetDir, QList<EquivalenceClass*>* eqcSegLst, int segIdx, QDateTime currDT)
{

    if(result != nullptr)
    {
        //show only segment
        if(currentSegmentHighlight != nullptr)
        {
            currentSegmentHighlight->first->setVisible(false);
            currentSegmentHighlight->second->setVisible(false);
        }
        for(int i = 0;i< ui->graphPlotWidget->graphCount() ; i++)//FIXME REMOVE THIS Not needed if the curve issue (some curve not assigned) is solved in genAll!
        {
            ui->graphPlotWidget->graph(i)->setVisible(false);
        }
        for(EquivalenceClass* eqc : result->keys())
        {

            QPair<QCPGraph *, QCPGraph *> hull = result->value(eqc);
            bool vis = eqcSegLst->contains(eqc);//is in current segment

            for(QHash<DataContainer*,QCPGraph*>* hash : *anonData)
            {
                for(DataContainer* cont : eqc->getRawData())
                {
                    if(hash->contains(cont))
                    {
                        hash->value(cont)->setVisible(vis);//hide all graphs not in current segment
                    }
                }
                for(DataContainer* cont : eqc->getDroppedData())
                {
                    if(hash->contains(cont))
                    {
                        hash->value(cont)->setVisible(vis);//hide all graphs not in current segment
                    }
                }
            }
            hull.first->setVisible(vis);
            hull.second->setVisible(vis);
        }
        ui->graphPlotWidget->rescaleAxes(true);
        if(ENGLISH_EXPORT_PLOTS == true)
        {
            ui->graphPlotWidget->xAxis->setLabel("Time(s)");
        }
        ui->graphPlotWidget->replot();
        QString plotFleName(targetDir  + QString::number(segIdx) + "-"+currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_SEGPLOT_SUFFIX+".pdf");
        ui->graphPlotWidget->savePdf(plotFleName);
        if(ENGLISH_EXPORT_PLOTS == true)
        {
            ui->graphPlotWidget->xAxis->setLabel("Zeit(s)");
        }
        ui->graphPlotWidget->replot();
    }
    Qt::CheckState extState = ui->extendedDistribCheckbox->checkState();
    ui->extendedDistribCheckbox->setCheckState(Qt::Unchecked);
    QString simpleDistrFleName(targetDir  + QString::number(segIdx) + "-"+ currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_DISTRFLE_INFIX+"simple.pdf");
    if(ENGLISH_EXPORT_PLOTS == true)
    {
        ui->distribPlotWidget->xAxis->setLabel("Outer Clusterdistance");
        ui->distribPlotWidget->yAxis->setLabel("Observations");
        ui->distribPlotWidget->replot();
    }
    ui->distribPlotWidget->savePdf(simpleDistrFleName,500,500);//TODO autoscale/make configurable (+pdf/svg etc)
    ui->extendedDistribCheckbox->setCheckState(Qt::PartiallyChecked);
    if(ENGLISH_EXPORT_PLOTS == true)
    {
        ui->distribPlotWidget->xAxis->setLabel("Outer Clusterdistance");
        ui->distribPlotWidget->yAxis->setLabel("Observations(X)");
        ui->distribPlotWidget->yAxis2->setLabel("Inner Clusterdistance");
        ui->distribPlotWidget->replot();
    }
    QString mediumDistrFleName(targetDir  + QString::number(segIdx) + "-"+ currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_DISTRFLE_INFIX+"medium.pdf");
    ui->distribPlotWidget->savePdf(mediumDistrFleName,500,500);
    ui->extendedDistribCheckbox->setCheckState(Qt::Checked);
    if(ENGLISH_EXPORT_PLOTS == true)
    {
        ui->distribPlotWidget->xAxis->setLabel("Outer Clusterdistance");
        ui->distribPlotWidget->yAxis->setLabel("Observations(X/Bars)");
        ui->distribPlotWidget->yAxis2->setLabel("Inner Clusterdistance");
        ui->distribPlotWidget->xAxis2->setLabel("Outer Clusterdistance");
        ui->distribPlotWidget->replot();
    }
    QString extDistrFleName(targetDir  + QString::number(segIdx) + "-"+ currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_DISTRFLE_INFIX+"extended.pdf");
    ui->distribPlotWidget->savePdf(extDistrFleName,500,500);
    ui->extendedDistribCheckbox->setCheckState(extState);


    QFile statFle(targetDir  + currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_STATFLE_SUFFIX+EXPORT_FILETYPE);
    if(statFle.open(QIODevice::Append) == true)
    {
        QString statStr  ="";
        if(first)
        {
            statStr = QString("Eqc-Num%1"
                                "Seg-Num%1"
                               "Color%1"
                              "Real size%1"
                              "Target size%1"
                              "Min k%1"
                              "Real k%1"
                              "Max t%1"
                              "Real t%1"
                              "f%1"
                              "b%1"
                              "Implied c%1"
                              "d%1"
                              "s%1"
                              "v%1"
                              "Dom min Val%1"
                              "Eqc min Val%1"
                              "Dom max Val%1"
                              "Eqc max Val%1"
                              "Max Infoloss%1"
                              "Mean Infoloss%1"
                              "Min Infoloss%1"
                              "Median relative error %1"
                              "Hull Stepsize%1"
                              "Segment Count%1"
                              "Total duration\n").arg(EXPORT_SEP);
        }

        ui->progressBar->setFormat("Exportiere EQCs");

        QList<QColor> colors = generateColors(result->size() +1);
        for(EquivalenceClass* eqc : *eqcSegLst)
        {

            int idx = getEQCIdx(eqc);
            if(idx < 0)
            {
                qCritical() << "Invalid EQC-Index in exportData";
                if(automaticEval == false)
                {
                    QMessageBox::warning(this,"Fehler beim Export!","Ungültige ID für eine Äquivalenzklasse in Segment "+QString::number(segIdx));
                }
            }
            QFile eqcFle(targetDir  + currDT.toString("yyyy-MM-dd_hhmmsszzz")+ QString("EQC%1").arg(idx)+EXPORT_FILETYPE);
            if(eqcFle.open(QIODevice::WriteOnly) == true)
            {
                QString eqcStr = QString("% EQC %1\n% Members:\n").arg(QString::number(idx));

                for (DataContainer* container: eqc->getRawData())
                {
                    QFile src = QFile(container->getSource());
                    eqcStr += QString("% %1\n").arg(src.fileName());
                }
                eqcStr += QString("Time %1 Lower %1 Upper\n").arg(EXPORT_SEP);
                if(!eqc->hasHull())
                {
                    eqc->calcHull(eqc->getHull_stepsize());
                }
                QList<QPair<DataPoint *, DataPoint *> *> hull = eqc->getHull();
                for(QPair<DataPoint *, DataPoint *>* pair : hull)
                {
                    double time = static_cast<double>(pair->first->getTime().toMSecsSinceEpoch()) / 1000.0;
                    double lower = pair->first->getValue();
                    double upper = pair->second->getValue();
                    eqcStr += QString("%1 %4 %2 %4 %3\n").arg(QString::number(time, 'f', 6)).arg(lower).arg(upper).arg(EXPORT_SEP);
                }

                QTextStream eqcOut(&eqcFle);
                eqcOut << eqcStr;
                eqcFle.close();
                qInfo() << QString("Wrote hull of eqc %1").arg(idx);

                int realSize = (eqc->getKReal() + eqc->getDroppedData().size());

                InformationlossMetric::InformationLoss_t loss;
                if(resultLoss.find(eqc) == resultLoss.end())//FIXME adapt to have better resolution
                {
                    loss = eqc->getInfoMetric()->calcLoss(eqc->getHull(),eqc->getMinValue(),eqc->getMaxValue(),eqc->getInfoLossRes());//TODO improve was already calculated before, cache this!
                    resultLoss.insert(eqc,loss);
                }else{
                    loss = resultLoss.value(eqc);
                }

                statStr += QString::number(idx) + EXPORT_SEP + QString::number(segIdx) + EXPORT_SEP + colors.at(idx).name() + EXPORT_SEP +
                        QString::number(realSize)       + EXPORT_SEP + QString::number(eqc->getTargetSize()) + EXPORT_SEP +
                        QString::number(eqc->getKMin()) + EXPORT_SEP + QString::number(eqc->getKReal()) + EXPORT_SEP +
                        QString::number(eqc->getTMax()) + EXPORT_SEP + QString::number(eqc->getTReal()) + EXPORT_SEP +
                        QString::number(resF) + EXPORT_SEP  + QString::number(resB) + EXPORT_SEP + QString::number(resC) +
                        EXPORT_SEP +QString::number(resD) + EXPORT_SEP + QString::number(resS) + EXPORT_SEP + QString::number(resV) + EXPORT_SEP +
                        QString::number(resDomMinVal) + EXPORT_SEP + QString::number(eqc->getMinValue()) + EXPORT_SEP +
                        QString::number(resDomMaxVal) + EXPORT_SEP + QString::number(eqc->getMaxValue()) + EXPORT_SEP +
                        QString::number(loss.maxLoss) + EXPORT_SEP + QString::number(loss.averageLoss) + EXPORT_SEP +
                        QString::number(loss.minLoss) + EXPORT_SEP + QString::number(eqc->calcMRE()) + EXPORT_SEP + QString::number(resHulLStepSze)+ EXPORT_SEP +
                        QString::number(anonData->size()) + EXPORT_SEP + QString::number(resDuration) + "\n";
            }

        }
        ui->progressBar->setValue(ui->progressBar->maximum());
        QTextStream out(&statFle);
        out << statStr;
        statFle.close();
        ui->progressBar->setFormat("Exportieren abgeschlossen!");
    }else{
        qCritical() << "Stats exportfile could not be opened for writing!";
        if(automaticEval == false)
        {
            QMessageBox::warning(this,"Fehler beim Export!","Die Stats-Exportdatei konnte nicht zum Schreiben geöffnet werden!");
        }
    }
}

void MainWindow::exportRestorePlot()
{
    if(result != nullptr)
    {
        if(currentSegmentHighlight != nullptr)
        {
            currentSegmentHighlight->first->setVisible(true && anonData->size() > 1);
            currentSegmentHighlight->second->setVisible(true && anonData->size() > 1);
        }
        for(EquivalenceClass* eqc : result->keys())
        {

            QPair<QCPGraph *, QCPGraph *> hull = result->value(eqc);
            QListWidgetItem * item = eqcVisMap->value(eqc,nullptr);
            bool vis = false;
            if(item != nullptr)
            {
                vis = itemStateMap->value(item,Qt::Unchecked) == Qt::Checked;
            }else{//no legend entry => no way to disable it yet as segment was not yet selected!
                vis = true;
            }

            for(QHash<DataContainer*,QCPGraph*>* hash : *anonData)
            {
                for(DataContainer* cont : eqc->getRawData())
                {
                    if(hash->contains(cont))
                    {
                        hash->value(cont)->setVisible(vis);
                    }
                }
                if(ui->graphplotDetailsCheckbox->checkState() == Qt::Checked)
                {
                    vis = false;//dont show dropped in this checked mode
                }
                for(DataContainer* cont : eqc->getDroppedData())
                {
                    if(hash->contains(cont))
                    {
                        hash->value(cont)->setVisible(vis);
                    }
                }
            }
            hull.first->setVisible(vis);
            hull.second->setVisible(vis);
        }
        ui->graphPlotWidget->rescaleAxes();
        ui->graphPlotWidget->replot();
    }else if(anonData != nullptr && !anonData->isEmpty())
    {
        for(QHash<DataContainer*,QCPGraph*>* hash : *anonData)
        {
            for(QCPGraph* g : hash->values())
            {
                g->setVisible(true);

            }
        }
        ui->graphPlotWidget->rescaleAxes();
        ui->graphPlotWidget->replot();
    }

    Qt::CheckState checkState = ui->extendedDistribCheckbox->checkState();
    if(checkState == Qt::Unchecked)
    {
        ui->distribPlotWidget->xAxis->setLabel("Clusterabstand");
        ui->distribPlotWidget->yAxis->setLabel("Beobachtungen");
        ui->distribPlotWidget->yAxis2->setVisible(false);
        ui->distribPlotWidget->xAxis2->setVisible(false);
        ui->distribPlotWidget->yAxis2->setRangeReversed(false);
    }else if(checkState == Qt::PartiallyChecked)
    {
        ui->distribPlotWidget->xAxis->setLabel("Äußerer Clusterabstand");
        ui->distribPlotWidget->yAxis->setLabel("Beobachtungen(X)");
        ui->distribPlotWidget->yAxis2->setLabel("Innerer Clusterabstand");
        ui->distribPlotWidget->yAxis2->setVisible(true);
        ui->distribPlotWidget->xAxis2->setVisible(false);
        ui->distribPlotWidget->yAxis2->setRangeReversed(false);
    }else{
        ui->distribPlotWidget->xAxis->setLabel("Äußerer Clusterabstand");
        ui->distribPlotWidget->yAxis->setLabel("Beobachtungen(X/Balken)");
        ui->distribPlotWidget->yAxis2->setLabel("Innerer Clusterabstand");
        ui->distribPlotWidget->xAxis2->setLabel("Äußerer Clusterabstand");
        ui->distribPlotWidget->yAxis2->setVisible(true);
        ui->distribPlotWidget->xAxis2->setVisible(true);
        ui->distribPlotWidget->yAxis2->setRangeReversed(true);
    }
    ui->distribPlotWidget->rescaleAxes();
    ui->distribPlotWidget->replot();

}

void MainWindow::exportEvalSegment(QString targetDir, bool savePlot, bool saveDistr, bool first, QString userPrefix, QString statPrefix, QString datPrefix, QList<EquivalenceClass *> *eqcLst, int segIdx, QDateTime currDT)
{
    if(savePlot == true)
    {
        if(result != nullptr)
        {
            //show only segment
            if(currentSegmentHighlight != nullptr)
            {
                currentSegmentHighlight->first->setVisible(false);
                currentSegmentHighlight->second->setVisible(false);
            }
            for(int i = 0;i< ui->graphPlotWidget->graphCount() ; i++)//FIXME REMOVE THIS Not needed if the curve issue (some curve not assigned) is solved in genAll!
            {
                ui->graphPlotWidget->graph(i)->setVisible(false);
            }
            for(EquivalenceClass* eqc : result->keys())
            {

                QPair<QCPGraph *, QCPGraph *> hull = result->value(eqc);
                bool vis = eqcLst->contains(eqc);//is in current segment

                for(QHash<DataContainer*,QCPGraph*>* hash : *anonData)
                {
                    for(DataContainer* cont : eqc->getRawData())
                    {
                        if(hash->contains(cont))
                        {
                            hash->value(cont)->setVisible(vis);//hide all graphs not in current segment
                        }
                    }
                    for(DataContainer* cont : eqc->getDroppedData())
                    {
                        if(hash->contains(cont))
                        {
                            hash->value(cont)->setVisible(vis);//hide all graphs not in current segment
                        }
                    }
                }
                hull.first->setVisible(vis);
                hull.second->setVisible(vis);
            }
            ui->graphPlotWidget->rescaleAxes(true);
            if(ENGLISH_EXPORT_PLOTS == true)
            {
                ui->graphPlotWidget->xAxis->setLabel("Time(s)");
            }
            ui->graphPlotWidget->replot();
            QString plotFleName(targetDir  +userPrefix+ "-Seg"+QString::number(segIdx) + "-"+currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_SEGPLOT_SUFFIX+".pdf");
            ui->graphPlotWidget->savePdf(plotFleName);
            if(ENGLISH_EXPORT_PLOTS == true)
            {
                ui->graphPlotWidget->xAxis->setLabel("Zeit(s)");
                ui->graphPlotWidget->replot();
            }

        }
    }
    if(saveDistr == true)
    {
        Qt::CheckState extState = ui->extendedDistribCheckbox->checkState();
        ui->extendedDistribCheckbox->setCheckState(Qt::Unchecked);
        if(ENGLISH_EXPORT_PLOTS == true)
        {
            ui->distribPlotWidget->xAxis->setLabel("Outer Clusterdistance");
            ui->distribPlotWidget->yAxis->setLabel("Observations");
            ui->distribPlotWidget->replot();
        }
        QString simpleDistrFleName(targetDir  + userPrefix + "-Seg"+QString::number(segIdx) + "-"+ currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_DISTRFLE_INFIX+"simple.pdf");
        ui->distribPlotWidget->savePdf(simpleDistrFleName,500,500);//TODO autoscale/make configurable (+pdf/svg etc)
        ui->extendedDistribCheckbox->setCheckState(Qt::PartiallyChecked);
        if(ENGLISH_EXPORT_PLOTS == true)
        {
            ui->distribPlotWidget->xAxis->setLabel("Outer Clusterdistance");
            ui->distribPlotWidget->yAxis->setLabel("Observations(X)");
            ui->distribPlotWidget->yAxis2->setLabel("Inner Clusterdistance");
            ui->distribPlotWidget->replot();
        }
        QString mediumDistrFleName(targetDir  +userPrefix+ "-Seg"+ QString::number(segIdx) + "-"+ currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_DISTRFLE_INFIX+"medium.pdf");
        ui->distribPlotWidget->savePdf(mediumDistrFleName,500,500);
        ui->extendedDistribCheckbox->setCheckState(Qt::Checked);
        if(ENGLISH_EXPORT_PLOTS == true)
        {
            ui->distribPlotWidget->xAxis->setLabel("Outer Clusterdistance");
            ui->distribPlotWidget->yAxis->setLabel("Observations(X/Bars)");
            ui->distribPlotWidget->yAxis2->setLabel("Inner Clusterdistance");
            ui->distribPlotWidget->xAxis2->setLabel("Outer Clusterdistance");
            ui->distribPlotWidget->replot();
        }
        QString extDistrFleName(targetDir +userPrefix+ "-Seg"+ QString::number(segIdx) + "-"+ currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_DISTRFLE_INFIX+"extended.pdf");
        ui->distribPlotWidget->savePdf(extDistrFleName,500,500);
        ui->extendedDistribCheckbox->setCheckState(extState);
    }

    QFile statFle(targetDir + statPrefix + EXPORT_STATFLE_SUFFIX+EXPORT_FILETYPE);
    QIODevice::OpenModeFlag mode = QIODevice::Append;
    if(first == true)
    {
        mode = QIODevice::WriteOnly;
    }
    if(statFle.open(mode) == true)
    {
        QString statStr = "";
        if(first == true)
        {
            statStr = QString("Eqc-Cnt%1"
                              "Seg-Idx%1"
                                   "Colors%1"
                                  "Real sizes%1"
                                  "Target sizes%1"
                                  "Min k%1"
                                  "Real ks%1"
                                  "Mean k%1"
                                  "Max t%1"
                                  "Real ts%1"
                                  "Mean t%1"
                                  "f%1"
                                  "b%1"
                                  "Implied c%1"
                                  "d%1"
                                  "s%1"
                                  "v%1"
                                  "Dom min Val%1"
                                  "Eqc min Vals%1"
                                  "Dom max Val%1"
                                  "Eqc max Vals%1"
                                  "Max Infolosses%1"
                                  "Avg Infolosses%1"
                                  "Min Infolosses%1"
                                  "Mean Avg Infoloss%1"
                                  "Median relative errors%1"
                                  "Mean median relative error%1"
                                  "Hull Stepsize%1"
                                  "Info Loss Stepsize%1"
                                  "Segment Count%1"
                                  "Total duration\n").arg(EXPORT_SEP);
        }

        ui->progressBar->setFormat("Exportiere EQCs");

        double meanK = 0;
        double meanAvgIL = 0;
        double meanT = 0;
        double meanMre = 0;
        QString colorsTpl ="(";
        QString realSzeTpl ="(";
        QString targetSzeTpl ="(";
        QString realKTpl ="(";
        QString realTTpl ="(";
        QString maxILTpl ="(";
        QString meanILTpl ="(";
        QString minILTpl ="(";
        QString minValsTpl ="(";
        QString maxValsTpl ="(";
        QString mreTpl ="(";
        int k = 0;
        double t = -1;
        QList<QColor> colors = generateColors(result->size() +1);
        for(EquivalenceClass* eqc : *eqcLst)
        {
            //k & t should be the same everywhere
            meanK += eqc->getKReal();
            meanT += eqc->getTReal();

            if(k == 0)
            {
                k = eqc->getKMin();
            }

            if(t == -1)
            {
                t = eqc->getTMax();
            }

            int idx = getEQCIdx(eqc);
            if(idx < 0)
            {
                qCritical() << "Invalid EQC-Index in exportEval";
            }
            QFile eqcFle(targetDir  +userPrefix + "-" + datPrefix + currDT.toString("yyyy-MM-dd_hhmmsszzz")+ QString("EQC%1").arg(idx)+EXPORT_FILETYPE);
            if(eqcFle.open(QIODevice::WriteOnly) == false)
            {
                qWarning() << "EQC-Hull could not be writte (Write error!)";
            }
            QString eqcStr = QString("Time%1Lower%1Upper\n").arg(EXPORT_SEP);
            if(!eqc->hasHull())
            {
                eqc->calcHull(eqc->getHull_stepsize());
            }
            QList<QPair<DataPoint *, DataPoint *> *> hull = eqc->getHull();
            for(QPair<DataPoint *, DataPoint *>* pair : hull)
            {
                double time = static_cast<double>(pair->first->getTime().toMSecsSinceEpoch()) / 1000.0;
                double lower = pair->first->getValue();
                double upper = pair->second->getValue();
                eqcStr += QString::number(time, 'f', 6) + EXPORT_SEP + QString::number(lower)  +EXPORT_SEP + QString::number(upper) +"\n";
            }

            QTextStream eqcOut(&eqcFle);
            eqcOut << eqcStr;
            eqcFle.close();
            qInfo() << QString("Wrote hull of eqc %1").arg(idx);

            InformationlossMetric::InformationLoss_t loss;
            if(resultLoss.find(eqc) == resultLoss.end())//FIXME adapt to have better resolution
            {
                loss = eqc->getInfoMetric()->calcLoss(eqc->getHull(),eqc->getMinValue(),eqc->getMaxValue(),resilR);//TODO improve was already calculated before, cache this!
                resultLoss.insert(eqc,loss);
            }else{
                loss = resultLoss.value(eqc);
            }
            meanAvgIL += loss.averageLoss;


            colorsTpl += colors.at(idx).name()+EXPORT_TUPLE_SEP;

            int realSize = (eqc->getKReal() + eqc->getDroppedData().size());
            realSzeTpl += QString::number(realSize) + EXPORT_TUPLE_SEP;

            targetSzeTpl += QString::number(eqc->getTargetSize()) + EXPORT_TUPLE_SEP;

            realKTpl += QString::number(eqc->getKReal()) + EXPORT_TUPLE_SEP;

            realTTpl += QString::number(eqc->getTReal()) + EXPORT_TUPLE_SEP;

            minValsTpl += QString::number(eqc->getMinValue()) + EXPORT_TUPLE_SEP;
            maxValsTpl += QString::number(eqc->getMaxValue()) + EXPORT_TUPLE_SEP;

            double mre = eqc->calcMRE();
            meanMre += mre;
            mreTpl += QString::number(mre) + EXPORT_TUPLE_SEP;

            maxILTpl += QString::number(loss.maxLoss) + EXPORT_TUPLE_SEP;
            meanILTpl += QString::number(loss.averageLoss) + EXPORT_TUPLE_SEP;
            minILTpl += QString::number(loss.minLoss) + EXPORT_TUPLE_SEP;

        }
        if(eqcLst->size() > 0)
        {
            meanMre = meanMre / (double) eqcLst->size();
            meanK = meanK/(double)eqcLst->size();
            meanT = meanT/(double)eqcLst->size();
            meanAvgIL = meanAvgIL/(double)eqcLst->size();
            int ind = colorsTpl.lastIndexOf(EXPORT_TUPLE_SEP);
            colorsTpl = colorsTpl.replace(ind,ind+1,")");
            ind = realSzeTpl.lastIndexOf(EXPORT_TUPLE_SEP);
            realSzeTpl =  realSzeTpl.replace(ind,ind+1,")");
            ind = targetSzeTpl.lastIndexOf(EXPORT_TUPLE_SEP);
            targetSzeTpl =  targetSzeTpl.replace(ind,ind+1,")");
            ind = realKTpl.lastIndexOf(EXPORT_TUPLE_SEP);
            realKTpl =  realKTpl.replace(ind,ind+1,")");
            ind = realTTpl.lastIndexOf(EXPORT_TUPLE_SEP);
            realTTpl =  realTTpl.replace(ind,ind+1,")");
            ind = maxILTpl.lastIndexOf(EXPORT_TUPLE_SEP);
            maxILTpl =  maxILTpl.replace(ind,ind+1,")");
            ind = meanILTpl.lastIndexOf(EXPORT_TUPLE_SEP);
            meanILTpl = meanILTpl.replace(ind,ind+1,")");
            ind = minILTpl.lastIndexOf(EXPORT_TUPLE_SEP);
            minILTpl =  minILTpl.replace(ind,ind+1,")");
            ind = minValsTpl.lastIndexOf(EXPORT_TUPLE_SEP);
            minValsTpl =  minValsTpl.replace(ind,ind+1,")");
            ind = maxValsTpl.lastIndexOf(EXPORT_TUPLE_SEP);
            maxValsTpl = maxValsTpl.replace(ind,ind+1,")");
            ind = mreTpl.lastIndexOf(EXPORT_TUPLE_SEP);
            mreTpl = mreTpl.replace(ind,ind+1,")");
        }else{
            colorsTpl ="()";
            realSzeTpl ="()";
            targetSzeTpl ="()";
            realKTpl ="()";
            realTTpl ="()";
            maxILTpl ="()";
            maxILTpl ="()";
            meanILTpl ="()";
            minILTpl ="()";
            minValsTpl ="()";
            maxValsTpl ="()";
            mreTpl ="()";
        }

        statStr += QString::number(eqcLst->size()) + EXPORT_SEP + QString::number(segIdx)+ EXPORT_SEP + colorsTpl+ EXPORT_SEP +
                realSzeTpl      + EXPORT_SEP + targetSzeTpl+ EXPORT_SEP +
                QString::number(k) + EXPORT_SEP + realKTpl + EXPORT_SEP + QString::number(meanK) + EXPORT_SEP +
                QString::number(t) + EXPORT_SEP + realTTpl + EXPORT_SEP + QString::number(meanT) + EXPORT_SEP +
                QString::number(resF) + EXPORT_SEP  + QString::number(resB) + EXPORT_SEP + QString::number(resC) +
                EXPORT_SEP +QString::number(resD) + EXPORT_SEP + QString::number(resS) + EXPORT_SEP + QString::number(resV) + EXPORT_SEP +
                QString::number(resDomMinVal) + EXPORT_SEP + minValsTpl + EXPORT_SEP +
                QString::number(resDomMaxVal) + EXPORT_SEP + maxValsTpl + EXPORT_SEP +
                maxILTpl + EXPORT_SEP + meanILTpl + EXPORT_SEP + minILTpl + EXPORT_SEP + QString::number(meanAvgIL)
                + EXPORT_SEP + mreTpl + EXPORT_SEP +QString::number(meanMre)  + EXPORT_SEP + QString::number(resHulLStepSze)+ EXPORT_SEP +
                QString::number(resilR)+ EXPORT_SEP +QString::number(anonData->size()) + EXPORT_SEP + QString::number(resDuration) + "\n";
        ui->progressBar->setValue(ui->progressBar->maximum());
        QTextStream out(&statFle);
        out << statStr;
        statFle.close();
        ui->progressBar->setFormat("Exportieren abgeschlossen!");
    }else{
        qCritical() << "Stats exportfile could not be opened for writing!";
    }
}

void MainWindow::exportEval(QString dir,QString userPrefix,QString datPrefix,QString statPrefix,bool first,bool savePlot,bool saveDistrib)
{
    if(eqcMap != nullptr)
    {
        QDateTime currDT = QDateTime::currentDateTime();
        QString targetDir;
        QFileInfo fi(dir);
        if(dir.isEmpty() || dir =="/" ||  !fi.isWritable())
        {
            qFatal( "Invalid export dir in evaluation!");
        }else{
            targetDir = dir;
        }

        ui->progressBar->setFormat("Exportiere Daten...");
        ui->progressBar->setValue(0);

        if(savePlot == true )
        {
            if(currentSegmentHighlight != nullptr)
            {
                currentSegmentHighlight->first->setVisible(false);
                currentSegmentHighlight->second->setVisible(false);

                ui->graphPlotWidget->replot();
            }
            QString plotFleName(targetDir  + userPrefix + "-" + currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_PLOT_SUFFIX+".pdf");
            ui->graphPlotWidget->savePdf(plotFleName);
            if(currentSegmentHighlight != nullptr)
            {
                currentSegmentHighlight->first->setVisible(true);
                currentSegmentHighlight->second->setVisible(true);

                ui->graphPlotWidget->replot();
            }
        }
        for(int seg : eqcMap->keys())
        {
            QList<EquivalenceClass*>* eqcSegLst = eqcMap->value(seg);
            exportEvalSegment(targetDir,savePlot,saveDistrib,first,userPrefix,statPrefix,datPrefix,eqcSegLst,seg,currDT);
            first = false;
        }
        exportRestorePlot();
    }else{
        qWarning() << "No EQCs to export!";
        if(automaticEval == false)
        {
            QMessageBox::warning(this,"Fehler beim Export!","Es sind keine Äquivalenzklassen zum exportieren vorhanden!");
        }
        ui->exportButton->setEnabled(false);
    }
}


void MainWindow::anonymize()
{
    if(result != nullptr)
    {
        for(EquivalenceClass* eqc : result->keys())
        {
            ui->graphPlotWidget->removeGraph(result->value(eqc).first);
            ui->graphPlotWidget->removeGraph(result->value(eqc).second);
            delete eqc;
        }
        result->clear();
        resultLoss.clear();
        resDuration = 0;

        QPen defaultPen(Qt::blue);
        QList<QCPGraph*> gLst;
        for(QHash<DataContainer*,QCPGraph*> *hash : *anonData)
        {
            gLst.append(hash->values());
        }
        for(QCPGraph* g: gLst)
        {
            g->setPen(defaultPen);
        }
        ui->graphPlotWidget->replot();
    }

    ui->progressBar->setValue(0);//FIXME make more sensible loadingbar!
    ui->progressBar->setTextVisible(true);
    ui->progressBar->setFormat("Bereite Anonymisierung vor...");

    double k = ui->kSlider->value();
    double t = getT();
    unsigned int b = getB();
    int ilR = ui->infoLossResSpinbox->value();
    resilR = ilR;
    resB = b;
    //Interpolatable already used when loading the data.
    //extended options below
    double d = getD();
    resD = d;
    double s = getS();
    resS = s;
    double h = ui->hullStepsizeSpinbox->value();
    resHulLStepSze = h;
    resDomMinVal = minDomVal;
    resDomMaxVal = maxDomVal;
    bool greedy = getGreedy();

    double maxCost = nan("");
    for(BucketTree* tree : treeMap.values())//FIXME optimize cache this
    {
        double localMC = tree->getMaxCost(b);
        if(isnan(maxCost) || maxCost < localMC)
        {
            maxCost = localMC;
        }
    }
    resC = maxCost;


    ui->plotLabel->setText(QString("Anonymisierte Daten (v:%1;f:%2;b:%3;d:%4;s:%5;h:%6;minV:%7;maxV:%8;gr.:%9;k:%10;i:%11):").arg(resV).arg(resF).arg(resB).arg(resD).arg(resS).arg(resHulLStepSze).arg(resDomMinVal).arg(resDomMaxVal).arg(resGreedy).arg(k).arg(resilR));

    ui->progressBar->setValue(10);
    DistributionDistanceMetric *distribDistMetric = getCurrentDistribDistanceMetric();
    InformationlossMetric *infoLossMetric = getCurrentInfoLossMetric();
    generationMode_t mode = getMode();
    resMode = mode;
    resGreedy = greedy;
    //sampling strategy is used in calcDistrib not needed later


    ui->progressBar->setValue(20);

    if(distrMap == nullptr || distrMap->isEmpty())
    {
        ui->progressBar->setFormat("Fehlgeschlagen keine Verteilungen vorhanden!");
        return;
    }else{
        int i = 0;
        for(Distribution* currDistr : distrMap->values())
        {
            if(!currDistr->isValid())
            {
                ui->progressBar->setFormat(QString("Fehlgeschlagen ungültige Verteilung für Segment %1").arg(i));
                return;
            }
            i++;
        }

        if(result != nullptr && !result->isEmpty())//clear old EQCs if existing
        {
            for(EquivalenceClass* eqc : result->keys())
            {
                QPair<QCPGraph *, QCPGraph *> hull = result->value(eqc);
                ui->graphPlotWidget->removeGraph(hull.first);
                ui->graphPlotWidget->removeGraph(hull.second);
                result->remove(eqc);
            }
            ui->graphPlotWidget->replot();
            ui->graphPlotWidget->rescaleAxes();
            result->clear();
        }

        if(eqcLst == nullptr)
        {
            eqcLst = new QList<EquivalenceClass*>();
        }else{
            //already deleted earlier
            eqcLst->clear();
        }

        if(eqcMap == nullptr)
        {
            eqcMap = new QMap<int,QList<EquivalenceClass*>*>();
            inverseEqcMap = new QMap<EquivalenceClass*,int>();
        }else{
            clearEqcMap();
        }
        int totalFullFillCnt = 0;
        time_t segDuration = 0;
        i = 0;
        for(QHash<DataContainer*,QCPGraph*> *hash : *anonData)
        {

            QApplication::processEvents(QEventLoop::AllEvents,10);
            if(!distrMap->contains(i))
            {
                QMessageBox::warning(this,"Fehler!",QString("Keine Verteilung für Segment %1 gefunden.\n Dieses wird nicht anonymisert!").arg(i));
                continue;
            }
            Distribution* distr = distrMap->value(i);
            QMap<DataContainer*,unsigned int> virtBucket;
            DataContainer* rightMostKey = distr->getClosestKey(hash->keys().at(0));
            DataContainer* oldRightMostKey = nullptr;
            do{
                oldRightMostKey = rightMostKey;
                rightMostKey = distr->getNextKey(rightMostKey);
            }while(rightMostKey != oldRightMostKey);

            ui->progressBar->setValue(30);
            DataContainer* refKey = EquivalenceClassSizeTree::calcRefKey(distr,hash->size(),rightMostKey,false);
            virtBucket.insert(refKey,hash->size());
            ui->progressBar->setValue(50);

            double minVal = ui->minValSpinbox->value();
            double maxVal = ui->maxValSpinbox->value();
            EquivalenceClassSizeTree* eqcSizeTree = new EquivalenceClassSizeTree(t,distribDistMetric,distr);//empty tree
            unsigned int eqcSizeTreeDepth = 0;
            ui->progressBar->setValue(70);

            if(treeMap.isEmpty())
            {
                resetEqcUILst();
                delete eqcSizeTree;
                qWarning() << "Buckettree was invalid. Aborting anonymization.";
                if(automaticEval == false)
                {
                    QMessageBox msg;
                    msg.setText("Der Eimerbaum war ungültig. Anonymisierung wird abgebrochen.\n Laden Sie die Daten erneut.");
                    msg.exec();
                }
                ui->progressBar->setValue(0);
                return;
            }else{
                for(BucketTree* tree : treeMap)
                {
                    tree->reset();
                }
                ui->progressBar->setValue(100);
                //Disable s for most modes and move to advanced settings
                //introduces maxCostSlider OR calculate

                QDateTime start = QDateTime::currentDateTime();
                ui->progressBar->setValue(0);
                ui->progressBar->setFormat(QString("Anonymisiere seit %1... das kann lange dauern. Holen Sie sich schonmal einen Tee.").arg(start.toString(Qt::ISODate)));
                ui->progressBar->setEnabled(false);
                //variables used in the switch


                EquivalenceClass *currEqc = nullptr;
                if(!treeMap.contains(i))
                {
                    qCritical() << "Bucketree not found in Map!";
                    QMessageBox::warning(this,"Fehler!","Ein Eimerbaum ist verloren gegangen...\nDas sollte nicht passieren!");
                    continue;
                }

                if(!anonDistanceCache->contains(i))
                {
                    qCritical() << "Non Anon-Distancecache found for the Segment!";
                    QMessageBox::warning(this,"Fehler!","Ein Abstandscache ist verloren gegangen...\nDas sollte nicht passieren!");
                    continue;
                }
                QHash<DataContainer*,QHash<DataContainer*,double>*>* currDistCache = anonDistanceCache->value(i);


                switch(mode)
                {
                case generationMode_ONE:
                {
                    do{
                        currEqc = EquivalenceClassSizeTree::generate(minVal,maxVal,treeMap.value(i),distr,dataDistMetric,distribDistMetric,infoLossMetric,k,t,b,h,ilR,eqcSizeTree,&eqcSizeTreeDepth,greedy,currDistCache);
                        if(currEqc != nullptr)
                        {
                            eqcLst->append(currEqc);
                            inverseEqcMap->insert(currEqc,i);
                            currEqc->calcSpecs();
                        }
                    }while (currEqc != nullptr);
                }
                    break;
                case generationMode_ALL:
                {
                    QList<EquivalenceClass*>* tmp = EquivalenceClassSizeTree::generateAll(minVal,maxVal,treeMap.value(i),distr,dataDistMetric,distribDistMetric,infoLossMetric,k,t,b,h,s,ilR,eqcSizeTree,&eqcSizeTreeDepth,greedy,d,currDistCache);
                    if(tmp != nullptr)
                    {
                        eqcLst->append(*tmp);
                        if(!tmp->isEmpty())
                        {
                            eqcMap->insert(i,tmp);
                            for(EquivalenceClass* eqc : *tmp)
                            {
                                inverseEqcMap->insert(eqc,i);
                            }
                        }else{
                            qInfo() << "No EQCs anonymized in segment "+QString::number(i);
                        }
                    }
                    break;
                }
                case generationMode_CASE:
                    //FIXME WORK HERE implement case centered!
                    //popup on case-centered (select case)
                    qCritical() << "Case centered mode not implemented yet!";
                    if(automaticEval == false)
                    {
                        QMessageBox::warning(this,"Fehler!","Der gewählte Modus (Fall zentriert) ist noch nicht umgesetzt!");
                    }
                    delete eqcSizeTree;
                    return;
                    break;
                case generationMode_FILL:
                    //FIXME WORK HERE implement Fill!
                    qCritical() << "Filling mode is not implemented yet!";
                    if(automaticEval == false)
                    {
                        QMessageBox::warning(this,"Fehler!","Der gewählte Modus (Auffüllen) ist noch nicht umgesetzt!");
                    }
                    delete eqcSizeTree;
                    return;
                    break;
                default:
                    break;
                }

                if(eqcMap != nullptr && eqcMap->contains(i))
                {
                    int cnt = 0;
                    QList<EquivalenceClass *> *eqcSegLst = eqcMap->value(i);
                    if(!eqcSegLst->empty() )
                    {
                        for(EquivalenceClass* eqc : *eqcSegLst)
                        {
                            if(eqc->fullFillsConstraints())
                            {
                                cnt++;
                            }
                            qInfo() << QString("EQC %1 has %2-Anonymity and %3-closeness").arg(i).arg(eqc->getKReal()).arg(eqc->getTReal());
                            InformationlossMetric::InformationLoss_t loss;
                            if(resultLoss.find(eqc) == resultLoss.end())//FIXME adapt to have better resolution
                            {
                                loss = eqc->getInfoMetric()->calcLoss(eqc->getHull(),eqc->getMinValue(),eqc->getMaxValue(),resilR);//TODO improve was already calculated before, cache this!
                                resultLoss.insert(eqc,loss);
                            }else{
                                loss = resultLoss.value(eqc);
                            }
                            qInfo() <<  QString("EQC %1 with real K=%2  real T=%3, dropped curves %4, average loss %5, min loss %6 , max loss %7.").arg(i).arg(eqc->getKReal()).arg(eqc->getTReal()).arg(eqc->getDroppedData().size()).arg(loss.averageLoss).arg(loss.minLoss).arg(loss.maxLoss);
                        }

                        totalFullFillCnt += cnt;
                        qInfo() << QString("Anonymisation of segment %4 finished with %1 of %2 Eqc fullfilling %3 - anonymity ").arg(cnt).arg(eqcLst->size()).arg(k).arg(i);


                        plotEQCs(eqcSegLst,eqcLst->size() - eqcSegLst->size() +1);

                        ui->graphPlotWidget->setToolTip(QString("%1 Äquivalenzklassen in %2 Segmente(n)").arg(eqcLst->size()).arg(anonData->size()));
                    }
                }
                i++;
                segDuration += start.msecsTo(QDateTime::currentDateTime());
                resDuration += segDuration;
                qInfo() << QString("Finished anonymization of %1 segments in %2 ms. %3 \%").arg(hash->size()).arg(segDuration).arg((double)i /(double)anonData->size());
                ui->progressBar->setFormat(QString("Anonymisierung von %1 / %2 Segmenten abgeschlossen.").arg(i).arg(anonData->size()));
                ui->progressBar->setValue((double)i /(double) anonData->size());

            }
        }


        resetEqcUILst();
        updateLegend();
        if(automaticEval == false)
        {
            QMessageBox::information(nullptr,"Anonymisierung abgeschlossen!",QString("Die Anonymisierung wurde beendet %1 von %2 Äquivalenzklassen in %3 Segmenten erfüllen %4 - Anonymität.").arg(totalFullFillCnt).arg(eqcLst->size())
                                     .arg(eqcMap->size()).arg(k));
        }
        ui->progressBar->setValue(100);
        ui->progressBar->setFormat(QString("Anonymisierung nach %1 s abgeschlossen.").arg((int)round(resDuration/1000.0)));
        ui->progressBar->setEnabled(true);

        enableWidget(ui->kSlider,false);
        enableWidget(ui->kSliderLabel,false);
        enableWidget(ui->kSliderOutput,false);
        enableWidget(ui->anonymizationButton,false);
        enableWidget(ui->bSlider,false);
        enableWidget(ui->bLabel,false);
        enableWidget(ui->bucketSizeLabel,false);
        enableWidget(ui->bucketSizeOutput,false);
        enableWidget(ui->bOutput,false);
        enableWidget(ui->dropDataLabel,false);
        enableWidget(ui->dropDataOutput,false);
        enableWidget(ui->dropDataSlider,false);
        enableWidget(ui->distrSensLabel,false);
        enableWidget(ui->distrSensOutput,false);
        enableWidget(ui->distrSensSlider,false);
        enableWidget(ui->redistModeLabel,false);
        enableWidget(ui->redistModeOne,false);
        enableWidget(ui->redistModeAll,false);//FIXME other modes!
        enableWidget(ui->sSliderLabel,false);
        enableWidget(ui->sSliderOutput,false);
        enableWidget(ui->sSlider,false);
        enableWidget(ui->greedyCheckbox,false);
        enableWidget(ui->hullLabel,false);
        enableWidget(ui->hullStepsizeSpinbox,false);
        enableWidget(ui->anonymizationButton,false);
        enableWidget(ui->bucketSizeCurrSegmentLabel,false);
        enableWidget(ui->bucketSizeCurrSegmentOutput,false);
        enableWidget(ui->extendedDistribCheckbox,false);

        enableWidget(ui->exportButton,true);//newly enabled widgets
        enableWidget(ui->replotButton,true);
        enableWidget(ui->graphplotDetailsCheckbox,true);
    }
}

void MainWindow::loadData()
{
    //FIXME improvment set default hull res after load!
    //according to data density and length or using fourier

    dataDistMetric = nullptr;
    ui->plotLabel->setText("Daten:");
    ui->graphPlotWidget->setToolTip("");
    deleteAnonData();
    ui->graphPlotWidget->clearGraphs();
    anonData = new QList<QHash<DataContainer*,QCPGraph*>*>();
    QHash<DataContainer *, QCPGraph *> *waveLst = loadWaves("Zu anonymisierende Daten laden. Eine Datei pro Kurve.",true);
    anonData->append(waveLst);
    ui->kSlider->setMinimum(2);
    ui->kSlider->setMaximum(waveLst->size());
    ui->kSlider->setEnabled(waveLst->size() >= 2);


    enableWidget(ui->interpolatableCheckbox,false);
    enableWidget(ui->loadButton,false);

    disableWidget(ui->kSlider);
    disableWidget(ui->kSliderLabel);
    disableWidget(ui->kSliderOutput);
    disableWidget(ui->bucketSizeCurrSegmentLabel);
    disableWidget(ui->bucketSizeCurrSegmentOutput);
    disableWidget(ui->graphplotDetailsCheckbox);
    disableWidget(ui->fSlider);
    disableWidget(ui->fOutput);
    disableWidget(ui->fLabel);
    disableWidget(ui->bSlider);
    disableWidget(ui->bLabel);
    disableWidget(ui->bOutput);
    disableWidget(ui->bucketSizeLabel);
    disableWidget(ui->bucketSizeOutput);
    disableWidget(ui->bucketSizeCurrSegmentLabel);
    disableWidget(ui->minValLabel);
    disableWidget(ui->minValSpinbox);
    disableWidget(ui->maxValLabel);
    disableWidget(ui->maxValSpinbox);
    disableWidget(ui->dropDataLabel);
    disableWidget(ui->dropDataOutput);
    disableWidget(ui->dropDataSlider);
    disableWidget(ui->distrSensLabel);
    disableWidget(ui->distrSensOutput);
    disableWidget(ui->distrSensSlider);
    disableWidget(ui->redistModeLabel);
    disableWidget(ui->redistModeOne);//FIXME other modes!
    disableWidget(ui->redistModeAll);
    disableWidget(ui->greedyCheckbox);
    disableWidget(ui->sSliderLabel);
    disableWidget(ui->sSliderOutput);
    disableWidget(ui->sSlider);
    disableWidget(ui->distribSingleValueClusterCheckbox);
    disableWidget(ui->extendedDistribCheckbox);
    disableWidget(ui->hullLabel);
    disableWidget(ui->hullStepsizeSpinbox);
    disableWidget(ui->replotButton);
    disableWidget(ui->anonymizationButton);
    disableWidget(ui->exportButton);

    bool  enableDistr = false;
    if(!ui->extendedOptionsCheckbox->isChecked() || !ui->sepDistrCheckbox->isChecked())
    {        enableDistr = anonData != nullptr && !anonData->isEmpty();
        if(anonData != nullptr && !anonData->isEmpty()  == false)
        {
            ui->genDistribButton->setToolTip("Zu anonymisierende Daten müssen geladen sein, daraus wird mit diesen Einstellungen die Verteilung generiert!");
        }
        calcDistances();
    }else{
        enableDistr = anonData != nullptr && !anonData->isEmpty() && distrData != nullptr && !distrData->isEmpty();

        if(anonData != nullptr && !anonData->isEmpty()  == false)
        {
            ui->genDistribButton->setToolTip("Zu anonymisierende Daten müssen geladen sein!");//TODO not realy true...
        }else if(distrData != nullptr && !distrData->isEmpty() == false)
        {
            ui->genDistribButton->setToolTip("Daten für die Verteilung müssen geladen sein, wenn die Verteilung nicht aus den zu anonymisierenden Daten generiert werden soll!");
        }
    }

    getD();
    getS();
    if(enableDistr)
    {
        enableWidget(ui->genDistribButton,true);//newly enabled widgets
    }else{
        disableWidget(ui->genDistribButton);
    }
    calcMinMax();//FIXME use progressbar
    if(anonData == nullptr || anonData->isEmpty())
    {
        qWarning() << "Waves could not be loaded!";
        if(automaticEval == false)
        {
            QMessageBox msg;
            msg.setText("Es konnten keine Daten geladen werden. Versuchen Sie es erneut.");
            msg.exec();
        }
    }else{

        enableWidget(ui->minValLabel,true);//newly enabled widgets
        enableWidget(ui->minValSpinbox,true);
        enableWidget(ui->maxValLabel,true);
        enableWidget(ui->maxValSpinbox,true);
        enableWidget(ui->distribSingleValueClusterCheckbox,true);
        enableWidget(ui->splitButton,true);
        enableWidget(ui->distanceList,false);
        enableWidget(ui->infoLossList,false);
        enableWidget(ui->distrSensLabel,true);
        enableWidget(ui->distrSensOutput,true);
        enableWidget(ui->distrSensSlider,true);


        disableWidget(ui->kSlider);
        disableWidget(ui->kSliderLabel);
        disableWidget(ui->kSliderOutput);
        disableWidget(ui->bucketSizeCurrSegmentLabel);
        disableWidget(ui->bucketSizeCurrSegmentOutput);
        disableWidget(ui->fSlider);
        disableWidget(ui->fOutput);
        disableWidget(ui->fLabel);
        disableWidget(ui->bSlider);
        disableWidget(ui->bLabel);
        disableWidget(ui->bOutput);
        disableWidget(ui->dropDataLabel);
        disableWidget(ui->dropDataOutput);
        disableWidget(ui->dropDataSlider);
        disableWidget(ui->redistModeLabel);
        disableWidget(ui->redistModeOne);
        disableWidget(ui->redistModeAll);//FIXME other modes!
        disableWidget(ui->greedyCheckbox);
        disableWidget(ui->sSliderLabel);
        disableWidget(ui->sSliderOutput);
        disableWidget(ui->sSlider);
        disableWidget(ui->extendedDistribCheckbox);
        disableWidget(ui->hullLabel);
        disableWidget(ui->hullStepsizeSpinbox);
        disableWidget(ui->replotButton);
        disableWidget(ui->exportButton);

        int stepSize = calcHullStepsize();
        if(stepSize >0)
        {
            ui->hullStepsizeSpinbox->setValue(stepSize);
        }else{
            ui->hullStepsizeSpinbox->setValue(DEFAULT_HULL_STEPSIZE_MS);
        }
        qInfo() << "Waves loaded successfully!";
        if(automaticEval == false)
        {
            QMessageBox msg;
            msg.setText("Die Daten wurden erfolgreich geladen.");
            msg.exec();
        }else{
            stepSize = QInputDialog::getInt(this,"Genauigkeit der Hülle","Schrittgröße ms",stepSize,1);
            if(stepSize >0)
            {
                ui->hullStepsizeSpinbox->setValue(stepSize);
            }else{
                ui->hullStepsizeSpinbox->setValue(DEFAULT_HULL_STEPSIZE_MS);
            }
        }
    }

}

int MainWindow::calcHullStepsize()
{
    if(anonData == nullptr || anonData->isEmpty())
    {
        return -1;
    }else{

        int range = calcDataRange();
        if(range >= 0)
        {
            return (int) ((range/1000.0) * HULL_STEPSIZE_FACTOR);
        }else{
            return -2;
        }
    }
}

int MainWindow::calcDataRange()
{
    QDateTime start;
    QDateTime end;
    QList<DataContainer*> contLst;
    for(QHash<DataContainer*,QCPGraph*>*hash : *anonData)
    {
        contLst.append(hash->keys());
    }
    for(DataContainer* cont : contLst)
    {
        const QDateTime localStart = cont->getStart();
        const QDateTime localEnd = cont->getEnd();
        if(!start.isValid() || start.msecsTo(localStart) < 0)
        {
            start = localStart;
        }
        if(!end.isValid() || end.msecsTo(localEnd) > 0)
        {
            end = localEnd;
        }
    }
    if(start.isValid() && end.isValid())
    {
        return  (start.msecsTo(end));
    }else{
        return -1;
    }
}

void MainWindow::genDistrib()
{
    //FIXME use progressbar
    double v = getV();
    resV = v;
    clearDistribs();
    if(distrMap == nullptr)
    {
        distrMap = new QMap<int,Distribution*>();
    }
    for(int i = 0; i< anonData->size(); i++)
    {
        Distribution* distr = calcDistrib(dataDistMetric,i,v);
        distrMap->insert(i,distr);
        plotDistrib(distr,i,true);
    }
    on_segmentSelectScrollbar_valueChanged(ui->segmentSelectScrollbar->value());//correcting visualization


    bool enableNextBtn = false;
    if(!ui->extendedOptionsCheckbox->isChecked() || !ui->sepDistrCheckbox->isChecked())
    {
        enableNextBtn = anonData != nullptr && !anonData->isEmpty() && dataDistMetric != nullptr;
        if(anonData != nullptr && !anonData->isEmpty()  == false)
        {
            ui->anonymizationButton->setToolTip("Zu anonymisierende Daten müssen geladen sein!");
            ui->bucketTreeButton->setToolTip("Zu anonymisierende Daten müssen geladen sein!");
        }
    }else{
        enableNextBtn = anonData != nullptr && !anonData->isEmpty() && distrData != nullptr && !distrData->isEmpty() && dataDistMetric != nullptr;

        if(anonData != nullptr && !anonData->isEmpty()  == false)
        {
            ui->anonymizationButton->setToolTip("Zu anonymisierende Daten müssen geladen sein!");//TODO not realy true...
            ui->bucketTreeButton->setToolTip("Zu anonymisierende Daten müssen geladen sein!");
        }else if(distrData != nullptr && !distrData->isEmpty() == false)
        {
            ui->anonymizationButton->setToolTip("Daten für die Verteilung müssen geladen sein, wenn die Verteilung nicht aus den zu anonymisierenden Daten generiert werden soll!");
            ui->bucketTreeButton->setToolTip("Daten für die Verteilung müssen geladen sein, wenn die Verteilung nicht aus den zu anonymisierenden Daten generiert werden soll!");
        }
    }
    if(enableNextBtn)
    {
        enableWidget(ui->minValLabel,false);
        enableWidget(ui->minValSpinbox,false);
        enableWidget(ui->maxValLabel,false);
        enableWidget(ui->maxValSpinbox,false);
        enableWidget(ui->distribSingleValueClusterCheckbox,false);
        enableWidget(ui->distrSensLabel,false);
        enableWidget(ui->distrSensOutput,false);
        enableWidget(ui->distrSensSlider,false);
        if(ui->splitButton->isEnabled())
        {
            enableWidget(ui->splitButton,false);
        }
        enableWidget(ui->distanceList,false);
        enableWidget(ui->genDistribButton,false);
        if(automaticEval == false)
        {
            enableWidget(ui->segmentSelectLabel,false);
            enableWidget(ui->segmentSelectScrollbar,false);
        }


        enableWidget(ui->fSlider,true);//newly enabled widgets
        enableWidget(ui->fOutput,true);
        enableWidget(ui->fLabel,true);
        enableWidget(ui->bucketTreeButton,true);
        enableWidget(ui->extendedDistribCheckbox,true);

        disableWidget(ui->bucketSizeCurrSegmentLabel);
        disableWidget(ui->bucketSizeCurrSegmentOutput);
        disableWidget(ui->graphplotDetailsCheckbox);
        disableWidget(ui->bSlider);
        disableWidget(ui->bLabel);
        disableWidget(ui->bOutput);
        disableWidget(ui->bucketSizeCurrSegmentLabel);
        disableWidget(ui->bucketSizeCurrSegmentOutput);
        disableWidget(ui->bucketSizeLabel);
        disableWidget(ui->bucketSizeOutput);
        disableWidget(ui->dropDataLabel);
        disableWidget(ui->dropDataOutput);
        disableWidget(ui->dropDataSlider);
        disableWidget(ui->redistModeLabel);
        disableWidget(ui->redistModeOne);//FIXME other modes!
        disableWidget(ui->redistModeAll);
        disableWidget(ui->greedyCheckbox);
        disableWidget(ui->sSliderLabel);
        disableWidget(ui->sSliderOutput);
        disableWidget(ui->sSlider);
        disableWidget(ui->hullLabel);
        disableWidget(ui->hullStepsizeSpinbox);
        disableWidget(ui->replotButton);
        disableWidget(ui->exportButton);

        ui->distribLabel->setText(QString("Verteilung(v:%1):").arg(resV));
    }else{
        ui->distribLabel->setText(QString("Verteilung:"));
    }
    getF();
    ui->bucketTreeButton->setEnabled(enableNextBtn);

}

void MainWindow::genBucketTree()
{
    if(anonData != nullptr && distrMap != nullptr && !distrMap->isEmpty() && dataDistMetric != nullptr)
    {
        ui->progressBar->setFormat("Erzeuge Eimerbäume, das kann einige Zeit dauern!");
        ui->progressBar->setValue(0);
        ui->progressBar->setTextVisible(true);
        resF = getF();
        ui->usedFOutput->setText(QString("Gewähltes f: %2").arg(resF));
        int i = 0;
        bool enableNextBtn = true;
        for(QHash<DataContainer*,QCPGraph*> *hash : *anonData)
        {
            if(distrMap->contains(i))
            {
                BucketTree *tree = BucketTree::genTree(hash->keys(),distrMap->value(i),dataDistMetric,resF);
                enableNextBtn  = enableNextBtn && tree != nullptr;
                if(enableNextBtn == false)
                {
                    qWarning() << "A buckettree could not be generated. Aborting at i:"+QString::number(i);
                    QMessageBox::warning(this,"Fehler!","Ein Eimerbaum konnte nicht erzeugt werden.\nAbbruch.");
                    clearTreeMap();
                    break;
                }else{
                    treeMap.insert(i,tree);
                    i++;
                }
            }else{
                qCritical() << "No distribution found for segment " + QString::number(i);//TODO error in UI?
            }
        }
        ui->anonymizationButton->setToolTip("");
        ui->bucketTreeButton->setToolTip("");
        if(enableNextBtn == true)
        {

            enableWidget(ui->minValLabel,false);
            enableWidget(ui->minValSpinbox,false);
            enableWidget(ui->maxValLabel,false);
            enableWidget(ui->maxValSpinbox,false);
            enableWidget(ui->distribSingleValueClusterCheckbox,false);
            disableWidget(ui->splitButton);
            enableWidget(ui->distanceList,false);//FIXME check distLst enable!
            if(automaticEval == false)
            {
                enableWidget(ui->bucketSizeCurrSegmentLabel,false);
                enableWidget(ui->bucketSizeCurrSegmentOutput,false);
                enableWidget(ui->segmentSelectLabel,false);
                enableWidget(ui->segmentSelectScrollbar,false);
            }
            enableWidget(ui->fSlider,false);
            enableWidget(ui->fOutput,false);
            enableWidget(ui->fLabel,false);
            enableWidget(ui->extendedDistribCheckbox,false);
            enableWidget(ui->bucketTreeButton,false);
            enableWidget(ui->distrSensLabel,false);
            enableWidget(ui->distrSensOutput,false);
            enableWidget(ui->distrSensSlider,false);


            enableWidget(ui->anonymizationButton,true);//newly enabled widgets
            enableWidget(ui->bucketSizeCurrSegmentLabel,true);
            enableWidget(ui->bucketSizeCurrSegmentOutput,true);
            enableWidget(ui->bSlider,true);
            enableWidget(ui->bLabel,true);
            enableWidget(ui->bucketSizeLabel,true);
            enableWidget(ui->bucketSizeOutput,true);
            enableWidget(ui->bOutput,true);
            enableWidget(ui->dropDataLabel,true);
            enableWidget(ui->dropDataOutput,true);
            enableWidget(ui->dropDataSlider,true);
            enableWidget(ui->redistModeLabel,true);
            enableWidget(ui->redistModeOne,true);
            enableWidget(ui->redistModeAll,true);//FIXME other modes!
            enableWidget(ui->sSliderLabel,true);
            enableWidget(ui->sSliderOutput,true);
            enableWidget(ui->sSlider,true);
            enableWidget(ui->greedyCheckbox,true);
            enableWidget(ui->hullLabel,true);
            enableWidget(ui->hullStepsizeSpinbox,true);
            enableWidget(ui->anonymizationButton,true);
            enableWidget(ui->kSlider,true);
            enableWidget(ui->kSliderLabel,true);
            enableWidget(ui->kSliderOutput,true);


            disableWidget(ui->graphplotDetailsCheckbox);
            disableWidget(ui->replotButton);
            disableWidget(ui->exportButton);
        }
        getB();
        ui->progressBar->setValue(100);
        ui->progressBar->setFormat("Eimerbaum erzeugt!");
    }else{

        ui->usedFOutput->setText(QString(""));
        ui->progressBar->setFormat("Eimer baum konnte nicht erzeugt werden! %p");
        ui->progressBar->setValue(100);
        if(automaticEval == false)
        {
            QMessageBox::warning(this,"Eimerbaumfehler!","Der Eimberbaum konnte nicht erstellt werden, da keine Daten geladen, die Verteilung oder die Abstandsmetrik ungültig sind.");
        }
        ui->anonymizationButton->setToolTip("Der Eimerbaum muss erstellt sein für die Anonymisierung.");
        ui->bucketTreeButton->setToolTip("");
        if(!treeMap.isEmpty())
        {
            clearTreeMap();
        }//FIXME  UX ! widgets disable on failure? also elsewhere!
        getB();
    }

}

void MainWindow::on_splitButton_clicked()
{
    if(anonData != nullptr  && !anonData->isEmpty())
    {
        if(anonData->size() != 1)
        {
            qWarning() << "Tried to split segmented data! Aborting.";
            QMessageBox::warning(this,"Achtung!","Die aktuellen Kurven sind bereits segmentiert!\nEin nochmaliges Aufteilen ist nicht empfohlen.\nLaden Sie die Ursprungsdaten erneut um die Daten anders zu segmentieren.");
        }else{
            SplitDialog* diag = new SplitDialog(anonData,ui->graphPlotWidget,this);
            connect(diag,&SplitDialog::segmentsApplied,this,&MainWindow::calcSegDistances);//FIXME improvment only calculate distances in same segments! reduces the load hugely- idea see calcDistances
            diag->exec();
            disconnect(diag,&SplitDialog::segmentsApplied,this,&MainWindow::calcSegDistances);
            bool enableSegSel = anonData->size() > 1;
            if(enableSegSel)
            {
                disableWidget(ui->splitButton);
                enableWidget(ui->segmentSelectLabel,true);
                enableWidget(ui->segmentSelectScrollbar,true);
                ui->segmentSelectScrollbar->setMinimum(0);
                ui->segmentSelectScrollbar->setMaximum(anonData->size() -1);
                ui->segmentSelectScrollbar->setValue(0);
                on_selectedSegmentChanged(0);
            }
            delete diag;
        }
    }else{
        qWarning() << "Invalid Curves for splitting";
        QMessageBox::warning(this,"Fehler!","Die Kurven sind invalide und können nicht Segmentiert werden.");
    }
}

void MainWindow::on_segmentSelectScrollbar_valueChanged(int value)
{
    on_selectedSegmentChanged(value);
    updateLegend();
}

void MainWindow::on_selectedSegmentChanged(int segment)
{
    if(segment >= 0 && segment < anonData->size())
    {
        updateSegmentHighlight(segment);
        if(distrMap != nullptr && !distrMap->isEmpty())
        {
            if(distrMap->contains(segment))
            {
                plotDistrib(distrMap->value(segment),segment,false);
            }else{
                qCritical() << "Distribution for the current segment not found! SegmentIdx: "+QString::number(segment);
            }
        }
        getB();
    }
}

void MainWindow::on_savePlotsButton_clicked()
{
    QString targetDir = QFileDialog::getExistingDirectory(this,"Wählen Sie ein Zielverzeichnis für das Speichern.",QDir::homePath())+ QDir::separator();
    QFileInfo fi(targetDir);
    if(targetDir.isEmpty() || targetDir =="/" ||  !fi.isWritable())
    {
        QMessageBox::information(this,"Kein Verzeichnis gewählt!","Es wurde kein (gültiges) Verzeichnis für  das Speichern gewählt!");
        return;
    }
    //show only segment
    if(currentSegmentHighlight != nullptr)
    {
        currentSegmentHighlight->first->setVisible(false);
        currentSegmentHighlight->second->setVisible(false);
    }
    int segIdx = getCurrentSegmentIdx();
    QDateTime currDT = QDateTime::currentDateTime();
    if(ENGLISH_EXPORT_PLOTS == true)
    {
        ui->graphPlotWidget->xAxis->setLabel("Time(s)");
        ui->graphPlotWidget->replot();
    }
    ui->graphPlotWidget->savePdf(targetDir+currDT.toString("yyyy-MM-dd_hhmmsszzz")+"plot.pdf");
    if(ENGLISH_EXPORT_PLOTS == true)
    {
        ui->graphPlotWidget->xAxis->setLabel("Zeit(s)");
    }
    ui->graphPlotWidget->replot();



    for(int i = 0;i< ui->graphPlotWidget->graphCount() ; i++)//FIXME REMOVE THIS Not needed if the curve issue (some curve not assigned) is solved in genAll!
    {
        ui->graphPlotWidget->graph(i)->setVisible(false);
    }
    if(result != nullptr)//FIXME improvement make this possible without anonymizing (i.e. export each segment raw)
    {
        for(EquivalenceClass* eqc : result->keys())
        {

            QPair<QCPGraph *, QCPGraph *> hull = result->value(eqc);
            bool vis = eqcMap->value(segIdx)->contains(eqc);//is in current segment

            for(QHash<DataContainer*,QCPGraph*>* hash : *anonData)
            {
                for(DataContainer* cont : eqc->getRawData())
                {
                    if(hash->contains(cont))
                    {
                        hash->value(cont)->setVisible(vis);//hide all graphs not in current segment
                    }
                }
                for(DataContainer* cont : eqc->getDroppedData())
                {
                    if(hash->contains(cont))
                    {
                        hash->value(cont)->setVisible(vis);//hide all graphs not in current segment
                    }
                }
            }
            hull.first->setVisible(vis);
            hull.second->setVisible(vis);
        }
    }

    ui->graphPlotWidget->rescaleAxes(true);
    if(ENGLISH_EXPORT_PLOTS == true)
    {
        ui->graphPlotWidget->xAxis->setLabel("Time(s)");
    }
    ui->graphPlotWidget->replot();
    QString plotFleName(targetDir  + QString::number(segIdx) + "-"+currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_SEGPLOT_SUFFIX+".pdf");
    ui->graphPlotWidget->savePdf(plotFleName);
    if(ENGLISH_EXPORT_PLOTS == true)
    {
        ui->graphPlotWidget->xAxis->setLabel("Zeit(s)");
    }


    Qt::CheckState extState = ui->extendedDistribCheckbox->checkState();
    if(extState == Qt::Unchecked )
    {
        QString simpleDistrFleName(targetDir  + QString::number(segIdx) + "-"+ currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_DISTRFLE_INFIX+"simple.pdf");
        if(ENGLISH_EXPORT_PLOTS == true)
        {
            ui->distribPlotWidget->xAxis->setLabel("Outer Clusterdistance");
            ui->distribPlotWidget->yAxis->setLabel("Observations");
            ui->distribPlotWidget->replot();
        }
        ui->distribPlotWidget->savePdf(simpleDistrFleName,500,500);//TODO autoscale/make configurable (+pdf/svg etc)
    }else if(extState == Qt::PartiallyChecked)
    {

        if(ENGLISH_EXPORT_PLOTS == true)
        {
            ui->distribPlotWidget->xAxis->setLabel("Outer Clusterdistance");
            ui->distribPlotWidget->yAxis->setLabel("Observations(X)");
            ui->distribPlotWidget->yAxis2->setLabel("Inner Clusterdistance");
            ui->distribPlotWidget->replot();
        }
        QString mediumDistrFleName(targetDir  + QString::number(segIdx) + "-"+ currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_DISTRFLE_INFIX+"medium.pdf");
        ui->distribPlotWidget->savePdf(mediumDistrFleName,500,500);
    }else{
        if(ENGLISH_EXPORT_PLOTS == true)
        {
            ui->distribPlotWidget->xAxis->setLabel("Outer Clusterdistance");
            ui->distribPlotWidget->yAxis->setLabel("Observations(X/Bars)");
            ui->distribPlotWidget->yAxis2->setLabel("Inner Clusterdistance");
            ui->distribPlotWidget->xAxis2->setLabel("Outer Clusterdistance");
            ui->distribPlotWidget->replot();
        }
        QString extDistrFleName(targetDir  + QString::number(segIdx) + "-"+ currDT.toString("yyyy-MM-dd_hhmmsszzz")+ EXPORT_DISTRFLE_INFIX+"extended.pdf");
        ui->distribPlotWidget->savePdf(extDistrFleName,500,500);
    }

    exportRestorePlot();
    QMessageBox::information(this,"Gespeichert","Die Ansicht wurde erfolgreich gespeichert!");
}


void MainWindow::evalResetParams(int defaultV,double defaultF,int defaultB,int defaultK,double defaultD,double defaultS,bool defaultGreed,int defaultH)
{
    ui->hullStepsizeSpinbox->setValue(defaultH);
    ui->distrSensSlider->setValue(defaultV);
    genDistrib();
    QCoreApplication::processEvents(QEventLoop::AllEvents,10);
    int fInt = defaultF*100.0;
    ui->fSlider->setValue(fInt);
    genBucketTree();
    ui->bSlider->setValue(defaultB);
    ui->kSlider->setValue(defaultK);
    int dInt = defaultD*1000.0;
    if(isinf(defaultD))
    {
        ui->dropDataSlider->setValue(ui->dropDataSlider->maximum());
    }else{
        ui->dropDataSlider->setValue(dInt);
    }
    int sInt = defaultS* 1000.0;
    ui->sSlider->setValue(sInt);
    ui->greedyCheckbox->setChecked(defaultGreed);
}

void MainWindow::enableWidget(QWidget *wid, bool highlight)
{
    if(wid != nullptr)
    {
        wid->setEnabled(true);
        if(highlight == true)
        {
            wid->setStyleSheet(HIGHLIGHT_STYLE);
        }else{
            wid->setStyleSheet("");
        }
        wid->update();
    }
}

void MainWindow::disableWidget(QWidget *wid)
{
    if(wid != nullptr)
    {
        wid->setEnabled(false);
        wid->setStyleSheet("");
        wid->update();
    }
}


