#include "bucket.h"

Distribution *Bucket::genDistrib(QList<Bucket *> *buckets)
{
    if(buckets == nullptr || buckets->isEmpty())
    {
        return nullptr;
    }else{
        QHash<DataContainer*,unsigned int> distribVals;
        double v = nan("");
        for(Bucket* b : *buckets)
        {
            distribVals.insert(b->getRefKey(),b->getFullCount());
            if(isnan(v) || v < b->getMaxDist())
            {
                v = b->getMaxDist();
            }
        }
        Distribution* distr = buckets->at(0)->getDistribution();
        return new Distribution(distr->getType(),distr->isInterpolateable(),distribVals,distr->getDistMetric(),v);
    }
}

Distribution *Bucket::genDistrib(QHash<DataContainer *, unsigned int> virtBuckets, DataDistanceMetric* metric, double v,bool interpolatable)
{
    if(virtBuckets.isEmpty() || metric == nullptr || isnan(v))
    {
        return nullptr;
    }else{
        return new Distribution(Distribution::DISTRIBUTION_TYPE_VIRTUAL,interpolatable,virtBuckets,metric,v);
    }
}

double Bucket::calcMaxDist()
{
    if(data.isEmpty())
    {
        return nan("");
    }else{
        double ret = nan("");
        for(double dist : data.values())
        {
            if(isnan(ret) || ret < dist)
            {
                ret = dist;
            }
        }
        return ret;
    }
}



Bucket::Bucket(DataContainer *refValue, double maxCost, DataDistanceMetric *metric, Distribution *distr,bool copyDistribData)
{
    this->refKey = refValue;
    this->metric = metric;
    QHash<DataContainer*,unsigned int> rawData;
    if(copyDistribData == true)
    {
        rawData = distr->getRawData();
    }
    this->distr = new Distribution(distr->getType(),distr->isInterpolateable(),rawData,distr->getDistMetric(),distr->getV());
    this->costLimit = maxCost;

    if(isValid())
    {
        if(distr->getClosestKey(refValue) != refValue)//add if not already present
        {
            distr->addProp(refValue,1);
        }
    }
    this->maxDist = calcMaxDist();
}

Bucket::Bucket(double maxCost, DataDistanceMetric *metric, Distribution *distr,bool copyDistribData)
{
    this->refKey = nullptr;
    this->metric = metric;
    QHash<DataContainer*,unsigned int> rawData;
    if(copyDistribData == true)
    {
        rawData = distr->getRawData();
    }
    this->distr = new Distribution(distr->getType(),distr->isInterpolateable(),rawData,distr->getDistMetric(),distr->getV());
    this->costLimit = maxCost;
    this->maxDist = calcMaxDist();
}

Bucket::Bucket(Bucket *copy,bool copyData)
{
    this->refKey = copy->getRefKey();
    this->metric = copy->getMetric();
    QHash<DataContainer*,unsigned int> rawData;
    Distribution* copyDistr =  copy->getDistribution();
    if(copyData == true)
    {
        QHash<DataContainer *, unsigned int> tmp = copyDistr->getRawData();
        rawData = tmp;
        data = copy->getData();
        fullCount = data.size();
    }
    this->distr = new Distribution(copyDistr->getType(),copyDistr->isInterpolateable(),rawData,copyDistr->getDistMetric(),copyDistr->getV());
    this->costLimit = copy->getCostLimit();
    this->maxDist = calcMaxDist();
}

Bucket::~Bucket()
{
    delete distr;
}

bool Bucket::isValid()
{
    return  (refKey != nullptr && distr != nullptr && !isnan(costLimit));
}

bool Bucket::add(DataContainer *val, QHash<DataContainer *, QHash<DataContainer *, double> *> *groundDistanceCache,bool dontChangeDistrib)
{
    double semDist = 0;
    if(val != nullptr && !data.contains(val) && fullFillsConstraints(val,&semDist,groundDistanceCache) == true)
    {
        if(refKey == nullptr || (!data.keys().contains(refKey) && !takenData.keys().contains(refKey)))//refkey not set or was removed
        {
            refKey = val;
        }
        fullCount++;
        if(semDist > maxDist)
        {
            maxDist = semDist;
        }
        data.insert(val,semDist);
        if(dontChangeDistrib == false)
        {
            distr->addProp(refKey,1);
        }
        return true;
    }else{
        return false;
    }

}

bool Bucket::remove(DataContainer *val)
{
    if(val != nullptr && distr->isValid() && data.contains(val))
    {
        bool dist = distr->removeOccurence(val);
        bool dat = data.remove(val);
        if(dat == true)
        {
            fullCount--;
        }
        maxDist = calcMaxDist();
        if(refKey == nullptr || (!data.keys().contains(refKey) && !takenData.keys().contains(refKey)))//refkey not set or was removed
        {
            if(!data.isEmpty())
            {
                refKey = data.keys().at(0);
            }else if(!takenData.isEmpty())
            {
                refKey = takenData.keys().at(0);
            }else{
                qWarning() << "Bucket is empty setting refKey to nullptr";
                refKey = nullptr;
            }
        }
        return  (dist && dat);
    }else{
        return false;
    }
}


void Bucket::reset()
{
    QList<DataContainer*> takenKeys = takenData.keys();
    QList<double> takenVals = takenData.values();
    for(int i = 0; i < takenKeys.size(); i++)
    {
        double dist = takenVals.at(i);
        if(dist > maxDist)
        {
            maxDist = dist;
        }
        data.insert(takenKeys.at(i),dist);
    }
}

bool Bucket::revert()
{
    if(lastTakenData.isEmpty())
    {
        return false;
    }else{
        for(DataContainer* dat: lastTakenData)
        {
            double dist = takenData.value(dat);
            data.insert(dat,dist);
            if(dist > maxDist)
            {
                maxDist = dist;
            }
            takenData.remove(dat);
        }
    }
    // FIXME: missing return value. Is returning true correct here?
    return true;
}

void Bucket::clear()
{
    clearData();
    clearTaken();
}

void Bucket::clearData()
{
    maxDist = nan("");
    data.clear();
}

void Bucket::clearTaken()
{
    takenData.clear();
}

uint32_t Bucket::getFullCount()
{
    return fullCount;
}

int Bucket::size()
{
    return data.size();
}


bool Bucket::fullFillsConstraints(DataContainer *val, double *maxCost, QHash<DataContainer *, QHash<DataContainer *, double> *> *groundDistanceCache)
{
    if(val == nullptr)
    {
        return false;
    }else{
        double localCalcMaxCost = calcMaxCost(groundDistanceCache);
        //double localSemDist = metric->semanticDistance(*val,*refKey);
        //double localTimeDist = metric->timeDistance(*val,*refKey); //FIXME further constraints?
        //double localValDist = metric->valueDistance(*val,*refKey);
        if(maxCost != nullptr)
        {
            *maxCost = localCalcMaxCost;
        }
        if(!isinf(costLimit))
        {
            bool ret = (localCalcMaxCost <= this->costLimit);
            if(ret == false)
            {
                qDebug() << QString("Bucket -- Cost is %1 , maxCost is %2").arg(localCalcMaxCost).arg(costLimit);
            }
            return ret;
        }else{
            qDebug() << QString("Bucket -- MaxCost is infinity");
            return true;
        }
    }
}

QHash<DataContainer *, double> Bucket::getData() const
{
    return data;
}

double Bucket::getCostLimit() const
{
    return costLimit;
}



DataDistanceMetric *Bucket::getMetric() const
{
    return metric;
}

Distribution* Bucket::getDistribution() const
{
    return distr;
}

DataContainer *Bucket::at(int i)
{
    if(i >= data.size() || i < 0 )
    {
        return nullptr;
    }else{
        return data.keys().at(i);
    }
}

void Bucket::take(DataContainer *cont)
{
    QHash<DataContainer*,double>::const_iterator dat = data.find(cont);
    if(dat != data.end())
    {
        lastTakenData.append(cont);
        takenData.insert(cont,dat.value());
        data.remove(cont);
    }
}

void Bucket::resetLastTaken()
{
    lastTakenData.clear();
}


bool Bucket::setRefKey(DataContainer *newRefKey)
{
    if(newRefKey == nullptr || !data.contains(newRefKey))
    {
        return false;
    }else{
        refKey = newRefKey;
        return true;
    }
}

DataContainer *Bucket::getRefKey() const
{
    return refKey;
}

double Bucket::calcMaxCost(QList<DataContainer *> data, unsigned int fullCount, Distribution *distr, QHash<DataContainer *, QHash<DataContainer *, double> *> *groundDistanceCache )
{
    DataContainer *leastKey = distr->getLeastPropKey();
    DataContainer *mostKey = distr->getMostPropKey();
    //find the key where you can get the largest error by assuming all obervations in the bucket are from that key

    DataContainer* maxCostKey = nullptr;
    if(leastKey == nullptr && mostKey == nullptr)
    {
        return nan("");
    }else if(leastKey != nullptr && mostKey == nullptr)
    {
        maxCostKey = (DataContainer*) leastKey;
    }else if(leastKey == nullptr && mostKey != nullptr)
    {
        maxCostKey = (DataContainer*) mostKey;
    }else{
        uint64_t diffLeast = abs(distr->getObservations(leastKey) - fullCount);
        uint64_t diffMost = abs(distr->getObservations(mostKey) - fullCount);
        if(diffLeast < diffMost)
        {
            maxCostKey = (DataContainer*) mostKey;
        }else{
            maxCostKey = (DataContainer*)  leastKey;
        }
    }
    double cost = 0;
    for(DataContainer* key : data)
    {
        double grDist;//FIXME move to helper
        if(groundDistanceCache != nullptr)
        {
            QHash<DataContainer*,QHash<DataContainer*,double>*>::iterator keyIt = groundDistanceCache->find(key);
            if(keyIt != groundDistanceCache->end())
            {//already calculated something for this value
                QHash<DataContainer*,double>::iterator maxCostKeyIt = keyIt.value()->find(maxCostKey);
                if(maxCostKeyIt != keyIt.value()->end())
                {
                    grDist = maxCostKeyIt.value();
                }else{
                    grDist = distr->getDistMetric()->semanticDistance(*maxCostKey,*key);
                    keyIt.value()->insert(maxCostKey,grDist);
                }
            }else{
                grDist = distr->getDistMetric()->semanticDistance(*maxCostKey,*key);
                QHash<DataContainer*,double>* maxCostKeyCache = new QHash<DataContainer*,double>();
                maxCostKeyCache->insert(maxCostKey,grDist);
                groundDistanceCache->insert(key,maxCostKeyCache);
            }

        }else{
            grDist = distr->getDistMetric()->semanticDistance(*maxCostKey,*key);
        }
        if(!isnan(grDist))
        {
            uint32_t flow = distr->getObservations(key);
            cost += grDist * flow;
        }else{
            qDebug() << QString("Bucket -- ground distance was NaN ignoring container number %1 ").arg(data.indexOf(key));
        }
    }
    return cost;//FIXME CHECK THIS!!!!

}

double Bucket::calcMaxCost(QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    DataContainer *leastKey = distr->getLeastPropKey();
    DataContainer *mostKey = distr->getMostPropKey();
    //find the key where you can get the largest error by assuming all obervations in the bucket are from that key

    DataContainer* maxCostKey = nullptr;
    if(leastKey == nullptr && mostKey == nullptr)
    {
        return INFINITY;
    }else if(leastKey != nullptr && mostKey == nullptr)
    {
        maxCostKey = (DataContainer*) leastKey;
    }else if(leastKey == nullptr && mostKey != nullptr)
    {
        maxCostKey = (DataContainer*) mostKey;
    }else{
        uint64_t diffLeast = abs(distr->getObservations(leastKey) - getFullCount());
        uint64_t diffMost = abs(distr->getObservations(mostKey) - getFullCount());
        if(diffLeast < diffMost)
        {
            maxCostKey = (DataContainer*) mostKey;
        }else{
            maxCostKey = (DataContainer*)  leastKey;
        }
    }
    double cost = 0;
    QList<DataContainer*> keys = data.keys();
    for(DataContainer* key : keys )
    {
        double grDist;//FIXME move to helper
        if(groundDistanceCache != nullptr)
        {
            bool genCache = false;
            QHash<DataContainer*,QHash<DataContainer*,double>*>::iterator keyIt = groundDistanceCache->find(key);
            if(keyIt != groundDistanceCache->end())
            {//already calculated something for this value
                QHash<DataContainer*,double>::iterator maxCostKeyIt = keyIt.value()->find(maxCostKey);
                if(maxCostKeyIt != keyIt.value()->end())
                {
                    grDist = maxCostKeyIt.value();
                    genCache = false;
                }else{
                    genCache = true;
                }
            }else{
                genCache = true;
            }

            if(genCache == true)
            {
                if(maxCostKey != key)
                {
                    grDist = distr->getDistMetric()->semanticDistance(*maxCostKey,*key);
                    QHash<DataContainer*,double>* maxCostKeyCache = new QHash<DataContainer*,double>();
                    maxCostKeyCache->insert(maxCostKey,grDist);
                    groundDistanceCache->insert(key,maxCostKeyCache);
                }else{
                    grDist = 0;
                }
            }
        }else{
            grDist = distr->getDistMetric()->semanticDistance(*maxCostKey,*key);
        }
        if(!isnan(grDist))
        {
            uint32_t flow = distr->getObservations(key);
            cost += grDist * flow;
        }else{
            qDebug() << QString("Bucket -- ground distance was NaN ignoring container number %1 ").arg(data.keys().indexOf(key));
        }
    }
    cost = cost/(double)data.size(); //normalize???
    return cost;//FIXME CHECK THIS!!!!


}

double Bucket::getMaxDist()
{
    return maxDist;
}

bool Bucket::isUninitalized()
{
    return (refKey == nullptr);
}

bool Bucket::isEmpty()
{
    return (refKey != nullptr && data.size() == 0 && takenData.size() > 0);
}





