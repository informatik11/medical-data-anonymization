#ifndef SPLITDIALOG_H
#define SPLITDIALOG_H

#include "datacontainer.h"
#include "qcustomplot.h"
#include "wavesegment.h"
#include "euclideanfrechetdatadistancemetric.h"


#include <QDialog>

#define HULL_STEPSIZE_FACTOR 0.125
namespace Ui {
class SplitDialog;
}

class SplitDialog : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief SplitDialog Behaves undefined if rawCurves or parentPlot is nullptr!
     * @param rawCurves the curves in the parentplot that need to be split, should only have one hash. Undefined results (but somewhat sensible) otherwise!
     * @param parentPlot  the parent plot in the mainwindow, for updating the curves when needed
     * @param parent if it is not nullptr the parent-Widget is disabled during the execution of this dialog.
     */
    explicit SplitDialog(QList<QHash<DataContainer*,QCPGraph*>*>*  rawCurves,QCustomPlot* parentPlot,QWidget *parent = nullptr);
    ~SplitDialog();
signals:
    /**
     * @brief segmentsApplied gets emitted  when the segments are applied, to the mainwindow. I.e. the distances need to be recalculated.
     */
    void segmentsApplied();

private slots:
    void on_splitButton_clicked();

    //Selection inspired by viewer !
    void onSelectMove(QMouseEvent *event);//Could be done as in viewer via qcpselectionlayer... is better but more complcated!

    void onSelectPress(QMouseEvent *event);

    void onSelectRelease(QMouseEvent *event);

    void on_buttonBox_clicked(QAbstractButton *button);


    void on_frechetButton_toggled(bool checked);

    void on_visualButton_toggled(bool checked);

    void on_fixedButton_toggled(bool checked);

    void on_segCntSpinbox_valueChanged(int arg1);

    void on_wndSzeSpinbox_valueChanged(int arg1);

    void on_stepSzeSpinbox_valueChanged(int arg1);

    void on_intervalSpinbox_valueChanged(int arg1);

private:
    typedef enum SplitMode_t{
        SPLIT_MODE_FIXED = 0,
        SPLIT_MODE_VISUAL = 1,
        SPLIT_MODE_FRECHET = 2
    }SplitMode_t ;

    /**
     * @brief convertWave converts the given container to a graph in the given plot
     * @param seg has to have the data already calculated!
     * @param plot
     * @return empty nulltr
     */
    static QCPGraph* convertWave(DataContainer* seg,QCustomPlot* plot);//FIXME unify with MainWindow::convertWave!

    /**
     * @brief convertWave converts the given segment to a container and a graph in the given plot
     * @param seg has to have the data already calculated!
     * @param plot
     * @return empty if an error occured
     */
    static QPair<DataContainer*,QCPGraph*> convertWave(WaveSegment* seg,QCustomPlot* plot);//FIXME unify with MainWindow::convertWave!
    /**
     * @brief applyData transferes the data to the mainwindow, overwriting the existing data and graphs
     */
    void applyData();

    /**
     * @brief exportData exports all segments and ends the selection in visual mode
     */
    void exportData();

    /**
     * @brief exportSegment
     * @param seg
     * @param dir
     * @param segmentId
     * @param waveId
     * @return success or failure
     */
    bool exportSegment(WaveSegment* seg,QString dir,int segmentId,int waveId);

    /**
     * @brief fixedSplit
     * Splitting at a fixed distance
     */
    void fixedSplit();

    /**
     * @brief frechetSplit
     * splitting with a frechet-window approach
     */
    void frechetSplit();

    /**
     * @brief visualSplit
     * splitting at the selected points
     */
    void visualSplit();

    /**
     * @brief calcDataRange
     * @return the range of the loaded data in ms, -1 if start/end were invalid
     */
    int calcDataRange();

    /**
     * @brief clearSegments
     * Deletes everything in the segments variable
     * @param delSegs delets the segments or just the lists
     */
    void clearSegments( bool delSegs = true);

    /**
     * @brief clearSelectGraphs
     * Deletes everything in the selectGraphs variable
     */
    void clearSelectGraphs();

    /**
     * @brief addSplitGraph add two lines indicating segments and marks the area inbetween like the selection
     * Does nothing on invalid DT
     * @param start
     * @param end
     */
    void addSplitGraph(QDateTime start,QDateTime end);

    /**
     * @brief splitWave
     * @param wave
     * @param start
     * @param end
     * @return nullptr if wave was nullptr or any parameter is invalid
     */
    WaveSegment* splitWave(Wave* wave,QDateTime start,QDateTime end);

    /**
     * @brief calcMinMaxDt calculates and sets the min and max time of all data!
     */
    void calcMinMaxDt();

    Ui::SplitDialog *ui;
    ///Data from the mainwindow
    QList<QHash<DataContainer*,QCPGraph*>*>*  rawCurves = nullptr;
    ///Plot from the mainwindow
    QCustomPlot* parentPlot = nullptr;


    ///The ordered list of the results. I.e. the first list are all curves of the first segment etc.
    QList<QList<WaveSegment*>*>* segments;

    QDateTime maxStart;
    QDateTime minEnd;

    QDateTime visCurrStart;
    QDateTime visCurrEnd;

    ///The graphs for the selection via visualmode, first is the lower, second the upper
    QList<QPair<QCPGraph*,QCPGraph*>*> selectGraphs ;


    bool selectFinished = false;
    bool selecting = false;

    QWidget* parent = nullptr;
    int range = 0;
    int stepSze = 0;
    int wndSze = 0;
    int segCnt = 0;
    int intervalSze = 0;

};

#endif // SPLITDIALOG_H
