#include "earthmoverdistributiondistancemetric.h"

EarthMoverDistance::EarthMoverDistance()
{

    name = "Euclidean Frechet Distribution Distance";
}


double EarthMoverDistance::error()
{
    return 0;
}


double EarthMoverDistance::distance(Distribution* d1, Distribution* d2, bool ignoreNonExist)
{
    if(!d1->isValid() || !d2->isValid())
    {
        return nan("");
    }else{
        QHash<DataContainer *, unsigned int> distr1Data = d1->getRawData();
        QHash<DataContainer *, unsigned int> distr2Data = d2->getRawData();
        double ret = 0;
        int normalizationFactor = 0;
        for(int i = 0;i < distr1Data.size(); i++)
        {
            QHash<DataContainer *, unsigned int>::iterator d2It = distr2Data.find(distr1Data.keys().at(i));
            if( d2It == distr2Data.end() && ignoreNonExist == true)
            {
                continue;//do nothing
            }else if(d2It == distr2Data.end() && ignoreNonExist == false){
                double prop = d1->getProp(distr1Data.keys().at(i))*0.5;
                normalizationFactor++;
                ret += prop;// prop should never be negative
            }else{//key in both distr

                double prop1 = d1->getProp(distr1Data.keys().at(i));
                double prop2 = d2->getProp(distr1Data.keys().at(i));
                normalizationFactor +=2;
                ret += std::abs((double)(prop1 - prop2));//needed as if not existing, the value is interpolated if possible, this is not wanted here!
                distr2Data.remove(distr1Data.keys().at(i));//remove all that are in both distributions and only handle those which are only in d2 later
            }
        }
        if(ignoreNonExist == false)//only those unique to d2 are remaining
        {
            for(int i = 0;i < distr2Data.size() -1; i++)
            {
                double prop = d2->getProp(distr2Data.keys().at(i))*0.5;// prop should never be negative
                normalizationFactor ++;
                ret += prop;
            }
        }
        if(normalizationFactor > 1)
        {
            normalizationFactor--;
        }
        ret /= (double)normalizationFactor;
        return ret;
    }

}
