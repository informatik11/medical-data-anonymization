#include "datapoint.h"

DataPoint::DataPoint(QDateTime time,double value,bool valid)
{
    this->time = time;
    this->value = value;
    this->m_isValid = valid;
}

QDateTime DataPoint::getTime() const
{
    return time;
}

double DataPoint::getValue() const
{
    return value;
}

bool DataPoint::isValidValue() const
{
    return m_isValid;
}

bool DataPoint::operator==(DataPoint x)
{
    return (this->getTime() == x.getTime() &&  this->getValue() == x.getValue());
}

