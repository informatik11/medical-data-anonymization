QT       += core gui

RC_ICONS += icon.ico
ICON += icon.ico

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

CONFIG += c++2a

DEFINES += EVAL_MODE=true

QMAKE_CXXFLAGS += -fopenmp
QMAKE_LFLAGS += -fopenmp
#QMAKE_CXXFLAGS += -fsanitize=memory

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    PolyFit.cpp \
    bucket.cpp \
    buckettree.cpp \
    constantvalue.cpp \
    datacontainer.cpp \
    datapoint.cpp \
    distribution.cpp \
    earthmoverdistributiondistancemetric.cpp \
    equivalenceclass.cpp \
    equivalenceclasssizetree.cpp \
    euclideanfrechetdatadistancemetric.cpp \
    extendedgenerallossmetric.cpp \
    frechet/clustering.cpp \
    frechet/config.cpp \
    frechet/curve.cpp \
    frechet/dynamic_time_warping.cpp \
    frechet/frechet.cpp \
    frechet/interval.cpp \
    frechet/jl_transform.cpp \
    frechet/point.cpp \
    frechet/simplification.cpp \
    informationlossmetric.cpp \
    main.cpp \
    mainwindow.cpp \
    polynomial.cpp \
    qcustomplot.cpp \
    splitdialog.cpp \
    tangent.cpp \
    wave.cpp \
    wavesegment.cpp

HEADERS += \
    PolyFit.h \
    bucket.h \
    buckettree.h \
    constantvalue.h \
    datacontainer.h \
    datadistancemetric.h \
    datapoint.h \
    distribution.h \
    distributiondistancemetric.h \
    earthmoverdistributiondistancemetric.h \
    equivalenceclass.h \
    equivalenceclasssizetree.h \
    euclideanfrechetdatadistancemetric.h \
    extendedgenerallossmetric.h \
    frechet/clustering.hpp \
    frechet/config.hpp \
    frechet/coreset.hpp \
    frechet/curve.hpp \
    frechet/dynamic_time_warping.hpp \
    frechet/frechet.hpp \
    frechet/grid.hpp \
    frechet/interval.hpp \
    frechet/jl_transform.hpp \
    frechet/point.hpp \
    frechet/random.hpp \
    frechet/simplification.hpp \
    frechet/types.hpp \
    informationlossmetric.h \
    mainwindow.h \
    polynomial.h \
    qcustomplot.h \
    splitdialog.h \
    tangent.h \
    wave.h \
    wavesegment.h

FORMS += \
    mainwindow.ui \
    splitdialog.ui

TRANSLATIONS += \
    NANNI_Anonymizer_de_DE.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
