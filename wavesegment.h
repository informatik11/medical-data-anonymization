#ifndef WAVESEGMENT_H
#define WAVESEGMENT_H

#include <QObject>
#include <QDateTime>
#include "wave.h"
#include "datapoint.h"
class WaveSegment : public DataContainer
{
public:

    /**
     * @brief WaveSegment creating a new wave-segment which goes from start to end
     * @param parentWave
     */
    WaveSegment(Wave* parentWave);

    /**
     * @brief WaveSegment creates a new wave-segment with the given parameters
     * @param parentWave
     * @param start
     * @param end
     */
    WaveSegment(Wave* parentWave, QDateTime start, QDateTime end);


    virtual ~WaveSegment();


    /**
     * @brief setStart sets the start time it is valid and recalculates the contained data if it is a valid segment
     * @param value
     * @return true if the starttime was set, i.e. if \c isValidInitalization returns true
     */
    bool setStart(const QDateTime &value);


    /**
     * @brief setEnd sets the end time and recalculates the contained data if it is a valid segment
     * @param value
     * @return true if the endtime was set, i.e. if \c isValidInitalization returns true
     */
    bool setEnd(const QDateTime &value);

    /**
     * @brief getUnit
     * @return same as parentwave
     */
    QString getUnit();

    /**
     * @brief getParentWave
     * @return
     */
    Wave* getParentWave() const;


    /**
     * @brief isValid The segment should be discarded if not valid, as no variables are set!
     * @return
     */
    bool isValid() const;



public slots:

    /**
     * @brief dataPointRemoved refreshes the data-list from the wave.
     * Should be connected to the corresponding dataPointRemoved signal in Wave
     */
    void dataPointRemoved(DataPoint* dp);

    /**
     * @brief dataPointAdded refreshes the data-list from the wave.
     * Should be connected to the corresponding dataPointAdded signal in Wave
     */
    void dataPointAdded(DataPoint* dp);

    /**
     * @brief calcData calculates and sets the contained datapoints with the current start &  end
     */
    void  calcData();

private:

    /**
     * @brief isValidInitalization checks if parentWave is not a nullptr, dates (in wave and given) are valid, start is before end and the time-intervall is within the time-intervall of the wave
     * @param parentWave
     * @param start
     * @param end
     * @return true iff all constraints are fullfilled
     */
    bool isValidInitalization(Wave* parentWave,QDateTime start, QDateTime end);



    /// The Wave this segment is a part of
    Wave* parentWave = nullptr;
    bool m_isValid = false;

};

#endif // WAVESEGMENT_H
