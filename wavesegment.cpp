#include "wavesegment.h"

WaveSegment::WaveSegment(Wave *parentWave)
{
    bool valid = isValidInitalization(parentWave,parentWave->getStart(),parentWave->getEnd());
    if(valid)
    {
        this->parentWave = parentWave;
        this->start = parentWave->getStart();
        this->end = parentWave->getEnd();
        calcData();
    }
    m_isValid = valid;
}

WaveSegment::WaveSegment(Wave *parentWave, QDateTime start, QDateTime end)
{
    bool valid = isValidInitalization(parentWave,start,end);
    if(valid)
    {
        this->parentWave = parentWave;
        this->start = start;
        this->end = end;
        calcData();
    }
    m_isValid = valid;
}

WaveSegment::~WaveSegment()
{
}


bool WaveSegment::setStart(const QDateTime &value)
{
    bool valid = isValidInitalization(parentWave,value,end);
    if(valid)
    {
        start = value;
        calcData();
    }
    return valid;
}

bool WaveSegment::setEnd(const QDateTime &value)
{
    bool valid = isValidInitalization(parentWave,start,value);
    if(valid)
    {
        end = value;
        calcData();
    }
    return valid;
}

QString WaveSegment::getUnit()
{
    return getParentWave()->getUnit();
}

Wave *WaveSegment::getParentWave() const
{
    return parentWave;
}

bool WaveSegment::isValidInitalization(Wave *parentWave, QDateTime start, QDateTime end)
{
    return (parentWave != nullptr
            && start.isValid() && end.isValid() && start.msecsTo(end) > 0
            && parentWave->getStart().isValid() && parentWave->getEnd().isValid() && parentWave->getStart().msecsTo(end) > 0
            && parentWave->getStart().msecsTo(start) >= 0  && parentWave->getEnd().msecsTo(end) <= 0);
}

void WaveSegment::calcData()
{
    const QMap<QDateTime, DataPoint *> parentDat = parentWave->getData();
    for(QDateTime dt : parentDat.keys())
    {
        if( dt.msecsTo(start) <= 0 && dt.msecsTo(end) >= 0)
        {
            data.insert(dt,parentDat.value(dt));
        }else if(dt.msecsTo(end) < 0 ) //As the keys are orderd in a map can this be done
        {
            break;
        }
    }
}

bool WaveSegment::isValid() const
{
    return m_isValid;
}

void WaveSegment::dataPointRemoved(DataPoint *dp)
{
    QDateTime key = reverseFind(dp);
    if(key.isValid())
    {
        data.remove(key);
    }
}

void WaveSegment::dataPointAdded(DataPoint *dp)
{    QDateTime key = reverseFind(dp);
     if(!key.isValid())
     {
         data.insert(dp->getTime(),dp);
     }
}
