#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>
#include "qcustomplot.h"

#include "equivalenceclasssizetree.h"
#include "euclideanfrechetdatadistancemetric.h"
#include "earthmoverdistributiondistancemetric.h"
#include "extendedgenerallossmetric.h"
#include "wave.h"
#include "bucket.h"
#include "splitdialog.h"

#define HIGHLIGHT_STYLE "background-color: rgba(255,0,0,10%);"

#define DEFAULT_INFOLOSS_INDEX 0
#define DEFAULT_DISTANCE_INDEX 0
#define DEFAULT_GREEDY true
#define DEFAULT_GEN_MODE generationMode_ALL
#define DEFAULT_D INFINITY
#define DEFAULT_V_FACTOR 0.4 //0-1
#define DEFAULT_S_FACTOR 0.2 //0-1
#define DEFAULT_DISTRIB_SINGLE_VAL true
#define DEFAULT_DROP_ENABLED false
#define DEFAULT_MIN_DOMVAL 0
#define DEFAULT_MAX_DOMVAL 100

#define HULL_STEPSIZE_FACTOR 0.125
#define QUANTILE_PRECISION 0.01f
#define MIN_BOXPLOT_HEIGHT 10

#define EXPORT_FILETYPE  ".csv"
#define EXPORT_SEP ";"
#define EXPORT_TUPLE_SEP ","
#define EXPORT_STATFLE_SUFFIX "stats"
#define EXPORT_PLOT_SUFFIX "plot"
#define EXPORT_SEGPLOT_SUFFIX "plotSeg"
#define EXPORT_DISTRFLE_INFIX "distr_"

#define ENGLISH_EXPORT_PLOTS true
#define AXIS_PADDING_PX 10



#define COLOR_S 0.5
#define COLOR_V 0.95
#define SEP ';'
#define DECIMAL_POINT ','

#define EPOCH_OFFSET_MS 0 // all time starts at this (ignoring all starttimes)


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private slots:
    void on_anonymizationButton_clicked();

    void on_kSlider_valueChanged(int value);

    void on_extendedOptionsCheckbox_stateChanged(int);

    void on_loadButton_clicked();

    void on_exportButton_clicked();

    void on_dropDataSlider_valueChanged(int);

    void on_sSlider_valueChanged(int);

    void on_distrSensSlider_valueChanged(int);

    void on_loadDistribButton_clicked();

    void on_distanceList_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

    void on_sepDistrCheckbox_stateChanged(int);

    void on_minValSpinbox_valueChanged(double arg1);

    void on_maxValSpinbox_valueChanged(double arg1);

    void on_redistModeOne_clicked();

    void on_redistModeAll_clicked();

    void on_redistModeCase_clicked();

    void on_redistModeFill_clicked();

    void on_genDistribButton_clicked();

    void on_replotButton_clicked();

    void on_extendedDistribCheckbox_stateChanged(int);

    void on_bucketTreeButton_clicked();

    void on_bSlider_valueChanged(int);

    void on_fSlider_valueChanged(int);

    void on_eqcLstWidget_itemChanged(QListWidgetItem *item);

    void on_evaluationButton_clicked();

    void on_splitButton_clicked();

    void on_segmentSelectScrollbar_valueChanged(int value);

    /**
     * @brief on_selectedSegmentChanged updates the marked region in the plot and the plotted distribution
     * @param segment the index of the segment. Does nothing if the index is invalid
     */
    void on_selectedSegmentChanged(int segment);

    void on_savePlotsButton_clicked();


private:

    /**
     * @brief m_rescaleAxis rescales the axis of the given plot with a padding. Does replot!
     * @param plot
     */
   // void m_rescaleAxis(QCustomPlot* plot); //FIXME NOT IMPLEMENTED

    /**
     * @brief evalResetParams reseting the parameters for the single-dimensional evaluation
     * @param defaultV
     * @param defaultF
     * @param defaultB
     * @param defaultK
     * @param defaultD
     * @param defaultS
     * @param defaultGreed
     * @param defaultH
     */
    void evalResetParams(int defaultV,double defaultF,int defaultB,int defaultK,double defaultD,double defaultS,bool defaultGreed,int defaultH);
    /**
     * @brief enableWidget enables the given widget and sets the style accordingly
     * @param wid
     * @param highlight highlights the button if true, otherwise disables the highlight
     */
    void enableWidget(QWidget* wid,bool highlight = true);

    /**
     * @brief enableWidget enables the given widget and sets the style accordingly
     * @param wid
     */
    void disableWidget(QWidget* wid);

    //FIXME improvment own export-class!
    /**
     * @brief exportData Helperfunction for button
     * @param dir opens a selection dialog if the dir is empty
     */
    void exportData(QString dir ="");

    /**
     * @brief exportDataSegment Helper function for segment-wise exportData
     * @param first true for the first segment
     * @param targetDir
     * @param eqcSegLst all eqcs in the segment
     * @param segIdx the index of the segment
     * @param currDT the timestamp for the filename (should be consistent for all current exports)
     */
    void exportDataSegment(bool first,QString targetDir,QList<EquivalenceClass*>* eqcSegLst,int segIdx,QDateTime currDT);

    /**
     * @brief exportRestorePlot restores the plot after segmentwise export
     */
    void exportRestorePlot();


    /**
     * @brief exportDataSegment Helper function for segment-wise exportEval
     * @param targetDir
     * @param savePlot
     * @param saveDistr
     * @param first true iff this is the first segment
     * @param userPrefix a user chosen prefix for all files
     * @param statPrefix the prefix for the current statsFile
     * @param datPrefix the prefix for the current dataFile
     * @param eqcLst all eqcs in the segment
     * @param segIdx the index of the segment
     * @param currDT the timestamp for the filename (should be consistent for all current exports)
     */
    void exportEvalSegment(QString targetDir,bool savePlot,bool saveDistr,bool first,QString userPrefix, QString statPrefix,QString datPrefix,QList<EquivalenceClass*>* eqcLst,int segIdx,QDateTime currDT);

    //FIXME improvment merge eval&export segment

    /**
     * @brief exportEval Helperfunction for automatic evaluation. Appends to a single file
     * @param dir opens a selection dialog if the dir is empty
     * @param userPrefix a user chosen prefix for all files
     * @param datPrefix the parameters, set as a prefix for the images and hulls
     * @param statPrefix the prefix for the statsFle
     * @param first adds header on the first entry, appends if it is not the first entry. Otherwise writes normally (i.e. overwrite)
     * @param savePlot saves the plot as PDF only if this is true
     * @param saveDistrib saves the distribution as PDFs only if this is true
     */
    void exportEval(QString dir,QString userPrefix,QString datPrefix,QString statPrefix,bool first,bool savePlot,bool saveDistrib);//FIXME improvment plot prefix? not just user+time

    /**
     * @brief calcMRE calculates the median relative error for the given eqcs
     * @param eqc
     * @return NaN if an error occured
     */
    double calcMRE(EquivalenceClass* eqc);
    /**
     * @brief anonymize Helperfunction for button and automatic evaluation
     */
    void anonymize();

    /**
     * @brief loadData Helperfunction for button and automatic evaluation
     */
    void loadData();

    /**
     * @brief calcHullStepsize estimates a good stepsize for the hull based on the loaded data. Must be adapted based on the actual resolution and frequency.
     * Is only dependent on the range!
     * @return -1 if no anonData is present, -2 if no range could be calculated
     */
    int calcHullStepsize();

    /**
     * @brief calcDataRange
     * @return the range of the loaded data in ms, -1 if start/end were invalid
     */
    int calcDataRange();

    /**
     * @brief genDistrib Helperfunction for button and automatic evaluation
     */
    void genDistrib();

    /**
     * @brief genBucketTree Helperfunction for button and automatic evaluation
     */
    void genBucketTree();

    /**
     * @brief calcMinMax calculating the minimal and maximal observed value and sets it as the domain border. Can be adapted via the corresponding spinboxes
     * Sets the corresponding variables.
     * Does nothing if anondata is null or empty
     */
    void calcMinMax();

    /**
     * @brief plotDistrib plots the given distribution in the corresponding plotwidget
     * Opens a warning message if it is unlikely that a anonymization will be successfull with the given distribution
     * @param distr does nothing if nullptr
     * @param segment the number of the segment
     * @param recalc if this is true the old graphs are deleted and newly calculated
     */
    void plotDistrib(Distribution* distr,int segment,bool recalc = true);

    /**
     * @brief plotEQCs adds the hulls of the eqcs to the plot and colors all graphs within the eqc in the same color as the eqc
     * Stores the graphs in the corresponding map (result)
     * @param lst
     * @param the number of colors that came before. i.e index of the first unused color. must be >0
     */
    void plotEQCs(QList<EquivalenceClass*>* lst,int colorIdxOffset);

    /**
     * @brief generateColors creates the needed number of colors using the conjugate of the golden ration and some randomness. Credit: https://martin.ankerl.com/2009/12/09/how-to-create-random-colors-programmatically/
     * step with golden ration conjugate steps through the color-space. the more stept there are, the more similar the values become
     * @param num
     * @return
     */
    QList<QColor> generateColors(unsigned int num);



    /**
     * @brief calcDistrib calculates the distribution if the distances are cached.
     * It is calculated by grouping the waves together according to the set (or default) v value.
     * If single value clusters are merged into the nearest clusters, the realV of the distribution might be larger then the given v
     * @param metric
     * @param segment
     * @param v maximum distance between waves to be considered equal
     * @return nullptr if an error occured
     */
    Distribution* calcDistrib(DataDistanceMetric* metric,int segment,double v);
    //FIXME improvment add distanccache as parameter, so there is no explicit copy & restore in this method

    /**
     * @brief calcDistribBestWaveHelper loads the best distance for the given wave
     * @param currWave
     * @param data
     * @param localDistanceChache
     * @return a pair of the best DataContainer, with its distance. or nullptr if an error occured.
     */
    QPair<DataContainer*,double>* calcDistribBestWaveHelper(DataContainer* currWave,QList<DataContainer*> data,QHash<DataContainer*,QHash<DataContainer*,double>*>* localDistanceChache);


    /**
     * @brief calcDistances calculates the distances between all distribution data waves (or data waves, if no distribution waves are loaded)
     * Sets the slider for v accordingly
     * Does nothing to the distanceCache if the corresponding data is not yet loaded, however the sliders etc are disabled in this case
     * @param segments is true when called with segmented data
     */
    void calcDistances(bool segments = false);

    /**
     * @brief clearDistanceCaches distrib and anonData cache are cleared.
     */
    void clearDistanceCaches();

    /**
     * @brief calcSegDistances
     * Helper for signal-slot construction
     */
    void calcSegDistances();

    /**
     * @brief calcDistanceHelper does the calculation for calcDistances on the given data. also sets min/maxDist
     * @param dat
     * @param anonData calculates the min and max distances and switches the used cache. alternative is distribdata
     * @param segment the index of the segment where the distances are calculated
     */
    void calcDistanceHelper(QList<DataContainer*> dat,bool anonData = true,int segment = 0);

    /**
     * @brief loadWaves opens a selection dialog with the given titel and parses the selected files as waves
     * Uses the progressbar
     * Loads at most as many datapoints + header as specified in the maxDataPoints spinbox
     * @param title
     * @param plot should the parsed waves be added to the plot
     * @return nullptr if an error occured, otherwise a mapping of waves to graphs, all graphs are nullptr if plot is false
     */
    QHash<DataContainer *,QCPGraph*> * loadWaves(QString title,bool plot);

    /**
     * @brief convertWave converts the given data to a wave
     * NaN values are added as invalid Datapoints
     * @param data
     * @param source
     * @param patId only for the name
     * @param type
     * @param unit
     * @param startDt only for the name. Must be valid!
     * @param isInterpolatable
     * @param plot true if the data should also be added to the plot, does not replot the widget!
     * @return the populated wave as the first and the corresponding graph as the second element in the pair. Wave might be invalid, graph is nullptr if plot is false.
     */
    QPair<DataContainer*,QCPGraph*> convertWave(QMap<double,double> data, QString source, QString patId, QString type, QString unit, QDateTime startDt, bool isInterpolatable, bool plot = true);

    /**
     * @brief parseLine reads the given line and adds it to the given lis
     * @param values
     * @param line
     * @return true on success
     */
    bool parseLine(QMap<double,double> *values, QString line);

    /// Encoding the mode of the eqc generation
    typedef enum generationMode_t{
        generationMode_ALL = 0,///Distribute across all eqcs, redistribute the remaining (unfinished eqcs) in the best eqcs or drop
        generationMode_ONE = 1,///Fill one eqc, then the next until , redistribute the remaining (unfinished eqcs) in the best eqcs or drop
        generationMode_FILL = 2,///Fill one eqc, until a threshold is reached, then open another and do round robin until the loss increases to much again... , redistribute the remaining (unfinished eqcs) in the best eqcs or drop
        generationMode_CASE = 3///Fill one eqc around a given case. Drop all other data.
    } generationMode_t;

    /**
     * @brief getCurrentInfoLossMetric
     * @return the selected informationlossmetric, if extended options are enabled, otherwise the default
     */
    InformationlossMetric *getCurrentInfoLossMetric();


    /**
     * @brief getCurrentDistribDistanceMetric is the same as data distance, just a different interface!
     * @return the selected distancemetric, if extended options are enabled, otherwise the default
     */
    DistributionDistanceMetric *getCurrentDistribDistanceMetric();

    /**
     * @brief getCurrentDataDistanceMetric is the same as distribution distance, just a different interface!
     * @return the selected distancemetric, if extended options are enabled, otherwise the default
     */
    DataDistanceMetric *getCurrentDataDistanceMetric();

    /**
     * @brief getMode
     * @return the selected generation mode, if extended options are enabled, otherwise the default
     */
    generationMode_t getMode();

    /**
     * @brief getGreedy
     * @return the selected greedy mode, if extended options are enabled, otherwise the default
     */
    bool getGreedy();

    /**
     * @brief setVisibilityExtendedOptions enables or disables the visibility of the extended options according to the checkbox
     */
    void setVisibilityExtendedOptions();



    /**
     * @brief T calculates the T-value, as the slider can only handle integer values. Also sets the outputLabel
     * @return always 1, as not needed with bucketree
     */
    double getT();

    /**
     * @brief getS calculates the S-value, as the slider can only handle integer values. Also sets the outputLabel, tooltip and disables/enables all elements if needed
     * @return the value in the slider, NaN if S-is not available in the current mode
     */
    double getS();

    /**
     * @brief getF calculates the f-Value, as the slider can only handle integer values. Also sets the outputLabel
     * @return the value in the slider, scaled accordingly. NaN if the slider is disabled!
     */
    double getF();

    /**
     * @brief getB calculates the B-value. Also sets the outputLabel
     * @return the value in the slider
     */
    unsigned int getB();


    /**
     * @brief getD calculates the D-value, as the slider can only handle integer values. Also sets the outputLabel
     * @return the value in the (maxValue - value)/10.0, the default value if the extended options are disabled, NaN if the option is disabled in the selected mode
     */
    double getD();

    /**
     * @brief getV calculates the V-value, as the slider can only handle integer values. Also sets the outputLabel
     * The value can only be calculated after data has been loaded. As the borders of the slider depend on the distances between curves
     * @return the value, the default value if extended options are off, NaN if no data has been loaded yet
     */
    double getV();

    /**
     * @brief dropEnabled
     * @return true if data-dropping is enabled
     */
    bool dropEnabled();


    /**
     * @brief forceAddSingleDistribClusters
     * @return the value of the corrsponding checkbox or the default value otherwise
     */
    bool forceAddSingleDistribClusters();

    /**
     * @brief calcQuantil
     * @param factor
     * @param data
     * @return
     */
    double calcQuantil(double factor,QList<double> data);

    /**
     * @brief updateEqcVis updates the eqc according to the state of the item
     * @param item
     */
    void updateEqcVis(QListWidgetItem* item);

    /**
     * @brief updateLegend creates or updates all items and lists for the list of legend, corresponding to the currently selected segment
     */
    void updateLegend();

    /**
     * @brief clearEqcMap deletes the eqcs and clears the map. does not delete it. Also delets the inverseEqcMap
     */
    void clearEqcMap();

    /**
     * @brief resetEqcUILst
     */
    void resetEqcUILst();

    /**
     * @brief deleteAnonData
     */
    void deleteAnonData();

    /**
     * @brief clearTreeMap
     */
    void clearTreeMap();

    /**
     * @brief clearDistribs clears everythin distribution related.
     */
    void clearDistribs();

    /**
     * @brief clearDistribMap clears the distribution map and deletes the distributions
     */
    void clearDistribMap();

    /**
     * @brief clearDistribClusters deletes the clusters and clears the mapping. does NOT delete the mapping or distribution
     */
    void clearDistribClusters();

    /**
     * @brief clearDistribBoxPlots
     */
    void clearDistribBoxPlots();

    /**
     * @brief clearDistribBarPlots
     */
    void clearDistribBarPlots();

    /**
     * @brief getCurrentSegmentIdx
     * @return id of the current segment
     */
    int getCurrentSegmentIdx();

    /**
     * @brief updateSegmentHighlight
     * @param segment the current segment MUST BE VALID is not checked!
     */
    void updateSegmentHighlight(int segment);

    /**
     * @brief getEQCIdx
     * @param eqc should not be null !
     * @return the number of the equivalence class (over all segments), -1 on nullptr (eqc or eqcLst), -2 if the eqc was not found
     */
    int getEQCIdx(EquivalenceClass* eqc);

    /**
     * @brief getSegmentIdx
     * @param eqc should not be null !
     * @return the number of the segment the given eqc is in., -1 on nullptr (eqc or eqcLst), -2 if the eqc was not found
     */
    int getSegmentIdx(EquivalenceClass* eqc);

    ///Mapping index in the list to corresponding infoloss, indizes must be consequitive
    const QMap<int,InformationlossMetric*> infolossMap{{0,new ExtendedGeneralLossMetric()}};//TODO map from item instead! (sorting?)
    ///Mapping index in the list to corresponding distanceMetric, indizes must be consequitive for datacontainers, same list as for distributions
    const QMap<int,DataDistanceMetric*> dataDistMap{{0,new EuclideanFrechetDataDistanceMetric()}};//TODO map from item instead! (sorting?)
    ///Mapping index in the list to corresponding distanceMetric, indizes must be consequitive for distributions, same list as for datacontainers
    const QMap<int,DistributionDistanceMetric*> distribDistMap{{0,new EarthMoverDistance()}};//TODO map from item instead! (sorting?)

    ///Mapping the index of the segemtns in anonData to the corresponding bucketTree
    QMap<int,BucketTree*> treeMap;

    ///The loaded and parsed data to be anonymized. each list is a segment. Only one list is present if there was no split performend
    QList<QHash<DataContainer*,QCPGraph*>*>* anonData = nullptr;
    ///The loaded and parsed data for the calculation of the distributions. The anonData is used if no dedicated distrData is loaded
    QList<DataContainer*>* distrData = nullptr;

    ///Contains all eqcs
    QList<EquivalenceClass*>*  eqcLst = nullptr;

    ///The anonymized data, mapping each eqc to the graphs of its hull
    QHash<EquivalenceClass*,QPair<QCPGraph*,QCPGraph*>>* result =  nullptr;

    ///The anonymized data, mapping each eqc to its item.
    QHash<EquivalenceClass*,QListWidgetItem*>* eqcVisMap =  nullptr;
    ///The anonymized data, mapping each  item to its eqc.
    QHash<QListWidgetItem*,EquivalenceClass*>* visEqcMap =  nullptr;

    QHash<QListWidgetItem*,Qt::CheckState>* itemStateMap = nullptr;

    ///Saving the losses from being calculated multiple times for the same eqcs.
    QHash<EquivalenceClass*,InformationlossMetric::InformationLoss_t> resultLoss;

    //<<---Storing the factors of the anonymization for the export, they might change--->

    ///The resolution of the infoLoss
    int resilR = 0;

    ///The duration of the calculation for the result
    time_t resDuration = 0;
    ///The buckettree depth used in the result
    unsigned int resB = 0;
    ///The drop-factor used in the result
    double resD = 0;
    ///The sensitivity used in the result
    double resS = 0;
    ///The distribution sensitivity used in the result
    double resV = 0;
    ///The minimum bucket weighing factor used in the result
    double resF = 0;
    ///The implied maximum cost for the buckettree (implied by f,b and the data) used in the result
    double resC = 0;

    ///The minimum domain value used in the result
    double resDomMinVal = 0;

    ///The maximum domain value used in the result
    double resDomMaxVal = 0;

    ///The resultion of the hull-stepsize, used in the result. This might differ from the one used to plot the result!
    int resHulLStepSze = 0;
    ///The mode in which the result was generated.
    generationMode_t resMode  = generationMode_ALL;

    ///Was the result calculated greedy or not.
    bool resGreedy = true;

    ///The dropped data for each container. mapping each dropped curve to its graph
    QHash<DataContainer*,QCPGraph*>* droppedGraphs =  nullptr;

    ///The distances between the data, mapping each wave to a map, which maps a wave to a distance
    /// Identified by the index of the segment.
    /// (wave1->[wave2->n1,wave3->n2]) has to be read as distance between wave 1 and 2 is n1 , between 1 and 3 is n2
    QMap<int,QHash<DataContainer*,QHash<DataContainer*,double>*>*>* anonDistanceCache = nullptr;



    ///The distances between the distribution data, mapping each wave to a map, which maps a wave to a distance. The same for every segment!
    /// (wave1->[wave2->n1,wave3->n2]) has to be read as distance between wave 1 and 2 is n1 , between 1 and 3 is n2
    QHash<DataContainer*,QHash<DataContainer*,double>*>* distribDistanceChache = nullptr;


    ///The smalles distance between loaded curves for anonymization, identified by the index of the segment
    QMap<int,double> minAnonDist;
    ///The largest distance between loaded curves for anonymization, identified by the index of the segment
    QMap<int,double> maxAnonDist;


    ///The smalles distance between loaded curves for distribution
    double minDistribDist = nan("");
    ///The largest distance between loaded curves for distribution
    double maxDistribDist = nan("");

    ///The smalles value of the loaded curves
    double minDomVal = nan("");
    ///The largest value of the laoded curves
    double maxDomVal = nan("");


    ///One distribution for every segment, maps the index of the segment in anonData to the distribution
    QMap<int,Distribution*>* distrMap = nullptr;

    ///The mapping the distribution for every segment to the clusters of the distribution
    QMap<Distribution*,QList<Distribution::Distribution_cluster_t*>*>* distrClusterMap = nullptr;

    ///Stores all boxplots for the extended distribution visualization for every distribution  (every segment)
    QMap<Distribution*,QList<QPair<QCPStatisticalBox*,QCPGraph*>>*> boxplots;


    ///Stores all barplots for the default distribution visualization, for every segment/distribution
    QMap<Distribution*,QList<QCPGraph*>*> barplots;

    ///The result of the anonymization, mapping each segment idx to the list of its equivalence classes
    QMap<int,QList<EquivalenceClass*>*>* eqcMap = nullptr;

    /// Mapping an eqc to its segment idx
    QMap<EquivalenceClass*,int>* inverseEqcMap = nullptr;

    ///The datadistance metric used for the buckettree AND the anonymization itself.
    DataDistanceMetric *dataDistMetric = nullptr;

    ///lower,upper graph around the selected segment
    QPair<QCPGraph*,QCPGraph*> *currentSegmentHighlight = nullptr;

    Ui::MainWindow *ui = nullptr;

    ///Gets set to disable all popups
    bool automaticEval = false;
};
#endif // MAINWINDOW_H
