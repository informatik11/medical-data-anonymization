#include "distribution.h"



Distribution::Distribution(Distribution &copy)
{
    Distribution(copy,true);
}

Distribution::Distribution(Distribution::Distribution_type_t type, bool p_isInterpolateable,QHash<DataContainer *, uint32_t> distr,DataDistanceMetric* metric,double v)
{
    distrib = new QHash<DataContainer*,uint32_t>();
    this->type = type;
    if(!distr.isEmpty() && metric != nullptr)
    {
        for(DataContainer* key : distr.keys())
        {
            uint32_t val = distr.value(key);
            distrib->insert(key,val);
        }
        for(uint32_t obs : distr.values())
        {
            totalObservations += obs;
        }
    }else{
        totalObservations = 0;
    }
    this->v = v;
    this->distMetric = metric;
    m_isInterpolateable = p_isInterpolateable;
}

Distribution::Distribution(Distribution &copy, bool copyData)
{
    this->type = copy.getType();
    this->v = copy.getV();
    this->m_isInterpolateable = copy.isInterpolateable();
    this->distMetric = copy.getDistMetric();

    distrib = new QHash<DataContainer*,uint32_t>();
    if(copyData == true)
    {
        QHash<DataContainer *, unsigned int> rawData = copy.getRawData();
        for(DataContainer* key : rawData.keys())
        {
            uint32_t val = rawData.value(key);
            distrib->insert(key,val);
        }
        for(uint32_t obs : rawData.values())
        {
            totalObservations += obs;
        }
    }else{
        totalObservations = 0;
    }
}

Distribution::~Distribution()
{
    delete distrib;
}

void Distribution::reset()
{
    if(distrib != nullptr)
    {
        distrib->clear();
    }
    totalObservations = 0;
}

bool Distribution::isValid()
{
    return (distrib != nullptr && !distrib->isEmpty() && type != DISTRIBUTION_TYPE_INVALID);
}

Distribution::Distribution_type_t Distribution::getType()
{
    return type;
}

double Distribution::getClosestProp(DataContainer *key)
{
    DataContainer* closest = getClosestKey(key);
    if( closest != nullptr)
    {
        return distrib->value(closest);
    }else{
        return -1;
    }
}

double Distribution::getNextProp(DataContainer *key)
{
    DataContainer* next = getNextKey(key);
    if( next != nullptr)
    {
        return distrib->value(next);
    }else{
        return -1;
    }
}

double Distribution::getPreviousProp(DataContainer *key)
{
    DataContainer* prev = getPreviousKey(key);
    if( prev != nullptr)
    {
        return distrib->value(prev);
    }else{
        return -1;
    }
}

DataContainer *Distribution::getClosestKey(DataContainer *key)
{
    if(key == nullptr)
    {
        return nullptr;
    }else{
        QList<DataContainer*> keys =  distrib->keys();
        double minDistance = nan("");
        DataContainer*  closestKey = nullptr;
        for(DataContainer* loopKey : keys)
        {
            double distance = abs(loopKey - key);
            if(isnan(minDistance) || distance < minDistance)
            {
                minDistance = distance;
                closestKey = loopKey;
            }
        }
        return closestKey;
    }
}

DataContainer *Distribution::getNextKey(DataContainer *key)
{
    if(key == nullptr)
    {
        return nullptr;
    }else{
        QList<DataContainer*> keys =  distrib->keys();
        double minDistance = nan("");
        DataContainer*  closestKey = nullptr;
        for(DataContainer* loopKey : keys)
        {
            double distance = (loopKey - key);
            if((distance >= 0) && (isnan(minDistance) || distance < minDistance))//find closest neighbor which is larger
            {
                minDistance = distance;
                closestKey = loopKey;
            }
        }
        return closestKey;
    }
}

DataContainer* Distribution::getPreviousKey(DataContainer* key)
{
    if(key == nullptr)
    {
        return nullptr;
    }else{
        QList<DataContainer*> keys =  distrib->keys();
        double minDistance = nan("");
        DataContainer*  closestKey = nullptr;
        for(DataContainer* loopKey : keys)
        {
            double distance = (loopKey - key);

            if((distance <= 0) && (isnan(minDistance) || distance < minDistance))//find closest neighbor which is smaller
            {
                minDistance = distance;
                closestKey = loopKey;
            }
        }
        return closestKey;
    }
}

double Distribution::getProp(DataContainer*key)
{

    QHash<DataContainer*, uint32_t>::iterator it= distrib->find(key);
    if(it == distrib->end())
    {    if(!m_isInterpolateable)
        {
            return -1;
        }else{
            DataContainer* closest = getClosestKey(key);
            DataContainer* secondClosest = getNextKey(closest);
            if(closest == secondClosest || closest == nullptr)
            {
                secondClosest = getPreviousKey(closest);
            }


            if(closest == secondClosest || closest == nullptr)
            {

                double closestProp = getProp(closest);//FIXME check recursion
                double secondClosestProp = getProp(secondClosest);
                return ((closestProp + secondClosestProp)/2);
            }else{
                return -1;
            }
        }
    }else{
        return ((double)it.value() / (double)totalObservations);
    }
    return -1;
}

int64_t Distribution::getObservations(DataContainer*key)
{
    QHash<DataContainer*, uint32_t>::iterator it = distrib->find(key);
    if(it == distrib->end())
    {    if(!m_isInterpolateable)
        {
            return -1;
        }else{
            DataContainer* closest = getClosestKey(key);
            DataContainer* secondClosest = getNextKey(closest);
            if(closest == secondClosest || closest == nullptr)
            {
                secondClosest = getPreviousKey(closest);
            }


            if(closest == secondClosest || closest == nullptr)
            {

                double closestProp = getObservations(closest);//FIXME check recursion
                double secondClosestProp = getObservations(secondClosest);
                return round((closestProp + secondClosestProp)/2.0);
            }else{
                return -1;
            }
        }
    }else{
        return it.value();
    }
    return -1;
}

bool Distribution::addProp(DataContainer *key, uint32_t occurences)
{
    unsigned int localOcc = 0;
    bool ret = false;
    if(distrib->contains(key))
    {
        localOcc = distrib->value(key);
        ret = true;
    }
    localOcc += occurences;
    distrib->insert(key,localOcc);
    totalObservations += occurences;
    return ret;
}

bool Distribution::removeProp(DataContainer *key,bool removeClosest)
{
    if(!distrib->isEmpty())
    {
        if(removeClosest == true)
        {
            key = getClosestKey(key);
        }
        return (distrib->remove(key) > 0);
    }else{
        return false;
    }
}

bool Distribution::addOccurence(DataContainer* key,bool useClosest)
{
    if(useClosest)
    {
        DataContainer* closest = getClosestKey(key);
        if(closest != nullptr)
        {
            key = closest;
        }
    }
    if(key == nullptr)
    {
        return false;
    }
    if(!distrib->contains(key))
    {
        distrib->insert(key,1);
        totalObservations++;
        return false;
    }else{
        distrib->insert(key,distrib->value(key) +1);
        totalObservations++;;
        return true;
    }
}

bool Distribution::removeOccurence(DataContainer *key, bool useClosest)
{
    if(useClosest)
    {
        key = getClosestKey(key);
    }
    if(key == nullptr)
    {
        return false;
    }
    if(distrib->contains(key))
    {
        const uint32_t oldObs = distrib->value(key);
        if(oldObs <= 1)
        {
            if(oldObs == 1)
            {
                totalObservations--;
            }
            distrib->remove(key);
        }else{
            distrib->insert(key,oldObs -1);
            totalObservations--;
        }
        return true;
    }else{
        return false;
    }
}

bool Distribution::setProp(DataContainer*key, uint32_t occurences)
{
    if(distrib->contains(key))
    {
        removeProp(key);
        addProp(key,occurences);
        return true;
    }else{
        return false;
    }
}

Distribution* Distribution::merge(Distribution* distr1, Distribution* distr2)
{
    if(distr1 == nullptr || distr2 == nullptr || !distr1->isValid() || !distr2->isValid()//not valid
            || distr1->getType() != distr2->getType()//not the same distribution type
            || distr1->getDistMetric() != distr2->getDistMetric())
    {
        return nullptr;
    }else{

        QHash<DataContainer*,uint32_t> distr3Data;
        QHash<DataContainer*, uint32_t> distr1Data = distr1->getRawData();
        QHash<DataContainer*, uint32_t> distr2Data = distr2->getRawData();

        for(int i = 0; i < distr1Data.size() ; i++)
        {
            DataContainer* currKey = distr1Data.keys().at(i);
            uint32_t currObservations = distr1Data.value(currKey);
            QHash<DataContainer*, uint32_t>::iterator distr2ValIt = distr2Data.find(currKey);
            if(distr2ValIt != distr2Data.end())
            {
                currObservations += distr2ValIt.value();
                distr2Data.remove(currKey);//remove, and only add keys that do not exist in distr1 later
            }
            distr3Data.insert(currKey,currObservations);
        }

        for(int i = 0; i <distr2Data.size() ; i++)
        {
            distr3Data.insert(distr2Data.keys().at(i),distr2Data.value(distr2Data.keys().at(i)));
        }

        double distr3V = fmax(distr1->getV(),distr2->getV());
        return new Distribution(distr1->getType(),(distr1->isInterpolateable() && distr2->isInterpolateable()),distr3Data,distr1->getDistMetric(),distr3V);
    }
}

bool Distribution::operator==(Distribution x)
{
    bool typeCheck= (this->type == x.type); //same types
    if(typeCheck == false || this->getRawData().size() != x.getRawData().size()
            || this->distMetric != x.getDistMetric()
            || this->getV() != x.getV() )
    {
        return false;
    }else{
        QHash<DataContainer*, uint32_t> d1 = this->getRawData();
        QHash<DataContainer*, uint32_t> d2 = x.getRawData();
        for(int i = 0;i < d1.size(); i++)
        {
            DataContainer*  currKey = d1.keys().at(i);
            QHash<DataContainer*, uint32_t>::const_iterator d2It = d2.find(currKey);
            if(d2It == d2.end() && d1.value(currKey) > 0)//different in a non 0 oberservation
            {
                return false;
            }else{
                if(d1.value(currKey) != d2It.value())
                {
                    return false;
                }else{
                    d2.remove(currKey);//remove all identical values
                }
            }
        }
        if(d2.size() == 0)
        {
            return true;
        }else{
            for(int i = 0;i< d2.size(); i++)
            {
                if(d2.values().at(i) > 0)
                {
                    return false;//different in a non 0 oberservation
                }
            }
            return true;//only 0 oberservation values left
        }
    }
}

Distribution Distribution::operator+(Distribution x)
{
    Distribution* tmp = merge(this,&x);
    Distribution ret = Distribution(*tmp,true);
    delete tmp;
    return ret;
}

QHash<DataContainer*, uint32_t> Distribution::getRawData()
{
    return *distrib;
}

bool Distribution::isInterpolateable() const
{
    return m_isInterpolateable;
}

uint32_t Distribution::getTotalObservations() const
{
    return totalObservations;
}

DataContainer* Distribution::getLeastPropKey()
{
    DataContainer* min = nullptr;
    if(isValid())
    {
        QList<DataContainer*> keyLst = distrib->keys();
        for(DataContainer* key : keyLst)
        {
            if(min == nullptr || distrib->value(min) > distrib->value(key))
            {
                min = key;
            }
        }
    }
    return min;
}

DataContainer*Distribution::getMostPropKey()
{
    DataContainer* max = nullptr;
    if(isValid())
    {
        QList<DataContainer*> keyLst = distrib->keys();
        for(DataContainer* key : keyLst)
        {
            if(max == nullptr || distrib->value(max) < distrib->value(key))
            {
                max = key;
            }
        }
    }
    return max;
}

double Distribution::getV() const
{
    return v;
}

DataDistanceMetric *Distribution::getDistMetric() const
{
    return distMetric;
}
