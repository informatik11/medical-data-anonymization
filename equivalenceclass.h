#ifndef EQUIVALENCECLASS_H
#define EQUIVALENCECLASS_H

#include <QList>
#include <QPair>
#include <QObject>
#include <algorithm>
#include <float.h>

#include "wavesegment.h"//FIXME not needed, but min/max include weird...
#include "datapoint.h"
#include "datadistancemetric.h"
#include "distribution.h"
#include "informationlossmetric.h"
#include "bucket.h"
#include "buckettree.h"
#include "distributiondistancemetric.h"


#define DISTR_IGNORE_NON_EXIST false

#define DEFAULT_HULL_STEPSIZE_MS (50)

/**
 * @brief The EquivalenceClass class holds all data that is clustered into an equivalence class. Together with all metrics and distributions.
 * Can not be instatiated directly,  generate  generateExist or  generateAll in the friend-class EquivalenceClassSizeTree must be used.
 * If the intention is to release a particular wave, but anonymize it with as little information loss as possible  generateAlong in EquivalenceClassSizeTree is the method you are looking for.
 * All of those generation methods can fail with the given constraints and data!
 */
class EquivalenceClass
{
    /**
     * EquivalenceClassSizeTree generates the equivalence classes
     */
    friend class EquivalenceClassSizeTree;
private:

    /**
     * @brief EquivalenceClass creates an empty eqc with the given paramters
     * @param k minimal k i.e. size
     * @param t maximal t, i.e. distance to popDistr. The abs of it is taken
     * @param h the hull stepsize in ms. The default is used if this is NaN. but this parameter is important for the calculation of the hull and infoloss
     * @param popDistr the distribution of the sensitve attribute
     * @param distM distancemetric for datacontainers
     * @param distrDistM distance metric for distributions
     * @param infoM
     * @param targetSize how large should the eqc be if it is not already filled
     * @param maxValue
     * @param minValue
     * @param infoLossRes the information loss resolation (how tight it is sampled)
     */
     EquivalenceClass(unsigned int k, double t, double h,Distribution* popDistr,DataDistanceMetric* distM,DistributionDistanceMetric* distrDistM,InformationlossMetric* infoM,unsigned int targetSize,double maxValue,double minValue,int infoLossRes);

     /**
      * @brief EquivalenceClass
      * @param copy
      */
     EquivalenceClass(EquivalenceClass *copy);

public:

     /**
      * @brief calcMRE calculates the median relative error
      * @return NaN if an error occured, e.g. the hull was not calculated
      */
     double calcMRE();

    /**
     * @brief merge merges the given equivalence classes into a new one. Is needed for SABRE. The parameters are deleted on success
     * It calculates the new eqc with all measures needed.
     * Does not delete the given eqcs.
     * Keeps the stricter constraints, i.e. larger kMin, smaller tMax.
     * Merges the eqc distributions, merging the population distribution if they are not equal
     * The metrics are taken from the first eqc! (I.e. Distance (datacontainer and distribution) and Infoloss metric)
     * Targetsizes are added.
     * MinValue and MaxValue is the min and max of the values of the classes.
     * Min of hullstepsizes
     * Min of infoLossRes
     * @param eqc1
     * @param eqc2
     * @return A new equivalence class, as t-closeness is transitive if eqc1 and eqc2 fullfill t closeness this does to. If eqc1 and eqc2 are k-anonymized this is k*2 anonymized.
     */
    static EquivalenceClass* merge(EquivalenceClass* eqc1, EquivalenceClass* eqc2);


    /**
     * @brief split splits the given eqc according to the given parameters.
     * If a split is not possible with the given paramteres  eqc is unchanged
     * @param eqc the eqc that will be split. This is also one of the results.
     * @param k minimum size
     * @param t maximum distance to the distribution in  eqc
     * @param maxTolerance maximum distance on the time-axis between merged points
     * @param droppable can the split delete points to improve the result. They are nevetheless assigned to the closes cluster, but as "droppedPoints".
     * @return nullptr if the constraints cant be met and  eqc can not be split with this constraints, in this case  eqc is unchanged.
     * Otherwise the return and  eqc are the newly generated equivalence classes. All there attributes are updated to the
     */
    static EquivalenceClass* split(EquivalenceClass* eqc,int k, double t,double maxTolerance, bool droppable = false);//FIXME implement


    /**
     * @brief generateAlong generates an equivalence class along a reference containers.
     * The intention is to release a specific wave and anonymize it with as little information loss as possible, i.e. the best data w.r.t. this wave is picked.
     * This will most likely result in a very bad overall result if other eqcs are generated, but if you want to release one or just a few waves, this might be the way to go!
     * @param data the DataContainer that need to be anonymized. Used segments are removed from data, to avoid overlapping clusters if this function is called repeatedly.
     * @param reference the segment to anonymize around
     * @param popDistr  the distribution in the population. Is the reference for t-closeness
     * @param k the minimal number of containers in the eqc
     * @param t the maximum distance of the distribution in the eqc to the population eqc
     * @param maxTolerance the maximum distance between points on the time-axis to be considered close.
     * @param maxSemanticDistance how far away can sensitive attributes be to fall into the same bucket for t-closeness
     * @return
     */
    static EquivalenceClass* generateAlong(QList<DataContainer*>* data,DataContainer* reference,Distribution* popDistr,int k, double t,double maxTolerance,double maxSemanticDistance,bool droppabale = false);//FIXME implement & adapt params!!


    /**
     * @brief isValid
     * @return true if either kMin > 1  or  (tMax < 1 and popDistr is Valid)
     */
    bool isValid();

    /**
     * @brief fullFillsConstraints
     * @return true iff the eqc is k anonymized and t close
     */
    bool fullFillsConstraints();

    /**
     * @brief isKAnonymized
     * @return true iff kReal >= kMin && kMin > 1
     */
    bool isKAnonymized();

    /**
     * @brief isTClose
     * @return true iff tReal <= tMax && tMax < 1
     */
    bool isTClose();


    /**
     * @brief addDataContainer adds the given element to the eqc
     * @param element
     * @param drop should the given container be dropped and only be added for late reuse
     */
    void add(DataContainer* element,bool drop = false);

    /**
     * @brief remove removes the given element from the eqc, if it is in it. otherwise it does nothing
     * @param element
     */
    void remove(DataContainer* element);

    /**
     * @brief addDataContainer adds the given element to the eqc
     * @param elements
     * @param drop should the given container be dropped and only be added for late reuse
     */
    void add(QList<DataContainer*> elements,bool drop = false);


    //FIXME WORKE HERE continue with merge, getParentWaves?? , getDataPoints (all datapoints in eqc), return buckets for generate, isEmpty

    /**
     * @brief hasHull
     * @return true iff the hull was generated. But can be old nevertheless
     */
    bool hasHull();

    /**
     * @brief getDistMetric
     * @return the distancemetric for containers
     */
    DataDistanceMetric *getDistMetric();

    /**
     * @brief setDistMetric
     * @param value ignores nullptr
     */
    void setDistMetric(DataDistanceMetric *value);

    /**
     * @brief getKMin
     * @return
     */
    unsigned int getKMin() const;

    /**
     * @brief getKReal
     * @return
     */
    unsigned int getKReal() const;

    /**
     * @brief getTMax
     * @return
     */
    double getTMax() const;

    /**
     * @brief getTReal
     * @return
     */
    double getTReal() const;

    /**
     * @brief getDistrPopulation
     * @return
     */
    Distribution *getDistrPopulation() const;

    /**
     * @brief getDistrEqc
     * @return
     */
    Distribution *getDistrEqc() const;

    /**
     * @brief getRawData
     * @return
     */
    QList<DataContainer *> getRawData() const;

    /**
     * @brief removeDroppedContainer removes the given container from the dropped-data list
     * @param dat
     * @return true if it was removed, false if the given container was not found
     */
    bool removeDroppedContainer(DataContainer* dat);

    /**
     * @brief getDroppedData
     * @return
     */
    QList<DataContainer*> getDroppedData() const;

    /**
     * @brief setInfoMetric ignores nullptrs
     * @param value
     */
    void setInfoLossMetric(InformationlossMetric *value);

    /**
     * @brief getInfoMetric
     * @return
     */
    InformationlossMetric* getInfoMetric() const;

    /**
     * @brief getTargetSize
     * @return
     */
    unsigned int getTargetSize() const;

    /**
     * @brief setTargetSize
     * @param value
     */
    void setTargetSize(unsigned int value);


    /**
     * @brief takeGreedy Takes the best containers, w.r.t. the information loss of the eqc,  from the bucket and inserts them in the eqc.
     * The number of containers taken is proportional to the distribution.
     * @param bucket
     * @return number of containers that were added to the eqc
     */
    int takeGreedy(Bucket* bucket);//FIXME how to do relaxation? Document & make configurable?


    /**
    * @brief takeConsiderate Takes containers such that the information loss is decreased, but does not have to be the best from the bucket and inserts them in the eqc.
    * The number of containers taken is roughly proportional to the distribution.
    * @param bucket
     * @param groundDistanceCache drastically reduces the computational load if distances only have to be calculated once in a tree. Not used if it is a nullptr
    * @return the number of containers taken. -1 if an error occured.
    */
    int takeConsiderate(Bucket* bucket);//FIXME how to do relaxation? Document & make configurable?
    //TODO take* can be done via higher order with takeOne* as param


    /**
     * @brief takeOneGreedy Takes one container from the bucket and adds it to the eqc.
     * Ignores the proportionality constraint!
     * Takes it greedy w.r.t. the informationloss in eqc
     * @param bucket
     * @return true if one was taken, false otherwise
     */
    bool takeOneGreedy(Bucket* bucket);


    /**
     * @brief takeOneConsiderate Takes one container from the bucket and adds it to the eqc.
     * Ignores the proportionality constraint!
     * Takes it considerate w.r.t. the informationloss in eqc, i.e. tries to keep the information loss the same, but not always take the best.
     * @param bucket
     * @return true if one was taken, false otherwise
     */
    bool takeOneConsiderate(Bucket* bucket);
    //TODO takeOne* can be done via higher order with getBest/similar as param

    /**
    * @brief takeGreedy Takes the best containers, w.r.t. the information loss of the bucket and inserts them in the current eqc.
    * The number of containers taken is proportional to the distribution.
    * Only redistributes the rawData, not dropped data, as data is only dropped after redistributen
    * @param eqc where the taken containers will be added
    * @param maxInfoLoss should be smaller then dropThresh
    * @param d If this is infinity no points are dropped! Can not be NaN!
    * @return the number of containers taken. -1 if an error occured.
    */
    int takeGreedy(Bucket *bucket,double maxInfoLoss,double d );

    /**
    * @brief takeConsiderate Takes containers such that the information loss is decreased, but does not have to be the best from the bucket and inserts them in current eqc.
    * The number of containers taken is proportional to the distribution.
    * Only redistributes the rawData, not dropped data, as data is only dropped after redistributen
    * @param eqc where the taken containers will be added
    * @param maxInfoLoss should be smaller then dropThresh
    * @param d If this is infinity no points are dropped! Can not be NaN!
    * @return the number of containers taken. -1 if an error occured.
    */
    int takeConsiderate(Bucket *bucket,double maxInfoLoss,double d );

    /**
     * @brief takeGreedy Takes the best containers, w.r.t. the information loss of the eqc,  from the eqc and inserts them in the given eqc.
     * Is used to redistribute  an unfinished eqc.
     * The number of containers taken is proportional to the distribution.
     * Only redistributes the rawData, not dropped data, as data is only dropped after redistributen
     * @param eqc where the taken containers will be added
     * @param maxInfoLoss
     * @param dropThresh If this is infinity no points are dropped! Can not be NaN!
     * @return the number of containers taken. -1 if an error occured.
     */
    int takeGreedy(EquivalenceClass *eqc,double maxInfoLoss,double d = INFINITY);

    /**
    * @brief takeConsiderate Takes containers such that the information loss is decreased, but does not have to be the best  from the eqc and inserts them in the given eqc.
    * The number of containers taken is roughly proportional to the distribution.
    * Only redistributes the rawData, not dropped data, as data is only dropped after redistributen
    * @param eqc where the taken containers will be added
    * @param maxInfoLoss
     * @param dropThresh If this is infinity no points are dropped! Can not be NaN!
    * @return the number of containers taken. -1 if an error occured.
    */
    int takeConsiderate(EquivalenceClass *eqc,double maxInfoLoss, double d = INFINITY);

    /**
     * @brief calcSpecs calculates the real t and k for the EQC
     */
    void calcSpecs();

    /**
     * @brief calcHull calculates the hull of the eqc. VERY expensive!
     * Does nothing if start or stop are invalid
     * @param stepSize how large the steps for calculating the hull will be, in ms
     * @return true iff the calculation succeeded. False otherwise
     */
    bool calcHull(time_t stepSize);

    /**
     * @brief calcHull calculates the hull of the eqc. VERY expensive!
     * Does nothing if start or stop are invalid. Stepsize is the h-param of the eqc
     * @return true iff the calculation succeeded. False otherwise
     */
    bool calcHull();

    /**
     * @brief getMaxValue
     * @return the maximal value of the domain. NOT the maximal value of the Eqc!
     */
    double getMaxValue() const;

    /**
     * @brief getMinValue
     * @return the minmal value of the domain. NOT the minmal value of the Eqc!
     */
    double getMinValue() const;

    /**
     * @brief getHull
     * @return
     */
    QList<QPair<DataPoint *, DataPoint *> *> getHull() const;

    /**
     * @brief getDistribDistMetric
     * @return the distancemetric for distributions
     */
    DistributionDistanceMetric *getDistribDistMetric() const;

    /**
     * @brief setDistribDistMetric
     * @param value ignores nullptr
     */
    void setDistribDistMetric(DistributionDistanceMetric *value);

    /**
     * @brief getHull_stepsize
     * @return h, the DEFAULT_HULL_STEPSIZE is used if it is NaN. In ms.
     */
    double getHull_stepsize() const;

    /**
     * @brief getStartTime
     * @return
     */
    QDateTime getStartTime() const;

    /**
     * @brief getEndTime
     * @return
     */
    QDateTime getEndTime() const;

    int getInfoLossRes() const;

private:





    /**
     * @brief takeOneConsiderateHelper Takes one container from the bucket and adds it to the eqc.
     * Ignores the proportionality constraint!
     * Takes it considerate w.r.t. the informationloss in eqc, i.e. tries to keep the information loss the same, but not always take the best.
     * @param bucket
     * @param overwriteRevert clears the revertlist if true, just adds it to the current revert if false.
     * Should be true if used independently, false if used in a wrapperr
     * @return true if one was taken, false otherwise
     */
    bool takeOneConsiderateHelper(Bucket* bucket,bool overwriteRevert = true);


    /**
     * @brief takeOneGreedyHelper Takes one container from the bucket and adds it to the eqc
     * Ignores the proportionality constraint!
     * Takes it greedy w.r.t. the informationloss
     * @param bucket
     * @param overwriteRevert clears the revertlist if true, just adds it to the current revert if false.
     * Should be true if used independently, false if used in a wrapper
     * @return true if one was taken, false otherwise
     */
    bool takeOneGreedyHelper(Bucket* bucket,bool overwriteRevert = true);


    //FIXME Improvement merge functions getBest Bucket&eqc and getSimilar bucket&eqc !

    /**
     * @brief getBest Finds the best container from the bucket the eqc w.r.t. the informationloss.
     * It only considers the average information loss.
     * @param bucket
     * @return the container with the smalles distance to the reference value and the corresponding info loss, nullptr if there is none
     */
    QPair<DataContainer*,InformationlossMetric::InformationLoss_t>* getBest(Bucket* bucket);


    /**
     * @brief getSimilar Finds a container which decreases information loss for the eqc, does not have to be the best.
     * If none decreases the information loss the best is taken.
     * It only considers the average information loss.
     * @param bucket
     * @return the container with a s distance  smaller then the average to the reference value and the corresponding info loss, nullptr if there is none
     */
    QPair<DataContainer*,InformationlossMetric::InformationLoss_t>* getSimilar(Bucket* bucket);//FIXME never possible to decrease? just dont increase to much? configurable??!!!


    /**
     * @brief getBest Finds the best container for the eqc w.r.t. the informationloss.
     * It only considers the average information loss.
     * @param eqc
     * @return the container with the smalles distance to the reference value and the corresponding info loss, nullptr if there is none
     */
    QPair<DataContainer*,InformationlossMetric::InformationLoss_t>* getBest(EquivalenceClass *eqc);

    /**
     * @brief getSimilar Finds a container which decreases information loss for the eqc, does not have to be the best.
     * If none decreases the information loss the best is taken.
     * It only considers the average information loss.
     * @param eqc
     * @return the container with a s distance  smaller then the average to the reference value and the corresponding info loss, nullptr if there is none
     */
    QPair<DataContainer*,InformationlossMetric::InformationLoss_t>* getSimilar(EquivalenceClass *eqc);

    /**
     * @brief addDataContainer adds the given element to the eqc. returns the new information loss
     * @param element
     * @param p_calcSpecs should the specs be (re)calculated. This is expensive and should be done only when needed!
     * Ignored for dropped data. The Hull is NOT calculated.
     * @param drop should the given container be dropped and only be added for late reuse
     */
    void addHelper(DataContainer* element,bool p_calcSpecs = true,bool drop = false);


    /// Earliest time, where data is present, the hull can be smaller, e.g. if only one container is larger!
    QDateTime startTime;

    /// Latest time, where data is present, the hull can be smaller, e.g. if only one container is larger!
    QDateTime endTime;

    /// Stores the hull of the equivalence class, first min then max DataPoint !
    QList<QPair<DataPoint*, DataPoint* > *> hulldata;

    /// Stores all segments contained in the equivalence class
    QList<DataContainer* > rawData;

    /// Stores all points that were dropped by the anonymization, but would have been in this equivalence class, if droppable would have been false
    QList<DataContainer* > droppedData;

    /// Measures the distance between containers
    DataDistanceMetric *distMetric = nullptr;

    /// Measures the distance between Distributions
    DistributionDistanceMetric *distribDistMetric = nullptr;

    /// Measures the information loss in this eqc
    InformationlossMetric* infoMetric = nullptr;

    /// How large should the eqc if possible
    unsigned int targetSize = 0;

    /// The minimum number of elements in the eqc to be k-anonymized
    unsigned int kMin = 0;

    /// The real number of elements in the eqc
    unsigned int kReal = 0;

    /// The maximum distance of the SA-distribution of the eqc to the distribution of the population
    double tMax = nan("");

    /// The real distance to the population distribution
    double tReal = nan("");

    /// The distribution of the SA in the population
    Distribution *distrPopulation = nullptr;

    /// The distribution of the SA in the eqc
    Distribution *distrEqc = nullptr;

    /// The maximal value for this type. Important for InformationLoss
    double maxValue = nan("");

    /// The minimal value for this type. Important for InformationLoss
    double minValue = nan("");

    /// For calculating the hull. Important for the precision of the hull and infoloss
    double hull_stepsize = nan("");

    /// The resolution of the sampling for calcuation of the information loss in ms.
    int infoLossRes = 50;

signals:

};

#endif // EQUIVALENCECLASS_H
