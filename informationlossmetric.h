#ifndef INFORMATIONLOSSMETRIC_H
#define INFORMATIONLOSSMETRIC_H

#include "datacontainer.h"
#include "datapoint.h"



/**
 * @brief The InformationlossMetric class defining the interface of how to calculate the
 */
class InformationlossMetric
{
public:
    //FIXME cache results?!
    // E.g. store last N results and recalculate on changes
    typedef struct InformationLoss_t{
        double averageLoss;
        double minLoss;
        double maxLoss;
    }InformationLoss_t ;



    /**
     * @brief calcLoss calculates the informationloss in the eqc, based on its hull
     * @param eqcHull
     * @param eqcMinVal the minimal (smallest) value in the EQC. Should not be NaN, must be smaller then eqcMaxVal
     * @param  eqcMaxVal the maximal (largest) value in the EQC. Should not be NaN, must be larger then eqcMinVal
     * @param  resolution how the hull is sampled. In ms. if it is 0 the data points of the hull are the only measurements
     * @return all values NaN if an error occured
     */
   virtual InformationLoss_t calcLoss(QList<QPair<DataPoint *, DataPoint *> *> eqcHull,double eqcMinVal,double eqcMaxVal,unsigned int resolution = 0) = 0;

    /**
     * @brief calcLoss calculates the informationloss eqc,based on its hull, with the additional container
    * @param eqcHull
     * @param eqcMinVal the minimal (smallest) value in the EQC. Should not be NaN, must be smaller then eqcMaxVal
     * @param  eqcMaxVal the maximal (largest) value in the EQC. Should not be NaN, must be larger then eqcMinVal
     * @param add
     * @param  resolution how the hull is sampled. In ms. if it is 0 the data points of the hull are the only measurements
     * @return
     */
   virtual InformationLoss_t calcLoss(QList<QPair<DataPoint *, DataPoint *> *> eqcHull,double eqcMinVal,double eqcMaxVal,DataContainer add,unsigned int resolution = 0) = 0;
    /**
     * @brief getName
     * @return
     */
   virtual QString getName() const;

protected:
    /// The name of the informationloss, for visualization purposes
    QString name = "Informationloss Baseclass";
};

#endif // INFORMATIONLOSSMETRIC_H

