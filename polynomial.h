#ifndef POLYNOMIAL_H
#define POLYNOMIAL_H
#include <QDebug>

#include "datacontainer.h"
#include "PolyFit.h"



/**
 * @brief The Polynomial class stores all data for a polynomial.
 * The maximal order of it can be set and the data can be changed by changing it in the corresponding datacontainer and calling the dataChanged slot afterwards.
 *
 */
class Polynomial : public DataContainer
{
public:
    /**
     * @brief Polynomial creates a new polynomial of the given order with the given flucuation handling
     * @param refData what data should be used to create the fit
     * @param refZero the reference point for normalising the time. This must be choosen carefully avoid overflows for the fit.
     * @param order what order should the polynomial have
     * @param parent
     */
    Polynomial(DataContainer* refData,DataPoint* refZero,uint8_t order = 5,QObject *parent = nullptr);

    /**
     * @brief getCoefficients
     * @return  a pair of an array and an uint8_t. if the array is nullptr an error occured. The uint8t is the realOrder. The array is list of all coeffcients in ascending order, i.e. 0th element => c_0 , 1st element => c_1 * x ...
     */
    QPair<long double*, uint8_t> getCoefficients();

    /**
     * @brief getCoefficient
     * @param order
     * @return the coefficient for the given order, NaN if there is none.
     */
    long double getCoefficient(uint8_t order);

    /**
     * @brief setCoefficient tries to set the coefficient of the polynomial.
     * If increaseOrder is true it can be usefull to check the order before and after and maybe recalculate the polynomial with the higher order to get a better fit!
     * @param order what coefficient should be set
     * @param coeff the value
     * @param increaseOrder  should the order of the polynomial be increased if order > maxOrder ? otherwise the function will ignore the set if the order is to large.
     * @return true if the coefficient was set succesfully, false e.g. if the order was larger then maxOrder and increaseOrder was false.
     */
    bool setCoefficient(uint8_t order, long double coeff, bool increaseOrder = false);


    /**
     * @brief getRealOrder
     * @return order of the largest coefficient which is not zero or NaN; -1 if all coefficients are zero or NaN; otherwise a the 0 <= order <= 255
     */
    int16_t getRealOrder();

    /**
     * @brief getMaxOrder
     * @return
     */
    uint8_t getMaxOrder() const;

    /**
     * @brief setMaxOrder sets the maximal order of the coefficient.
     * Deletes all larger coefficients with a larger order, or recalculates.
     * @param newMaxOrder
     * @param recalc should the coefficients be recalculated with the new order ?
     */
    void setMaxOrder(uint8_t newMaxOrder,bool recalc = true);

    /**
     * @brief getFluct
     * @return
     *
    Wave_Trend_Fluctuation_t getFluct() const;
    */

    /**
     * @brief setFluct sets the flucuation handling and ALWAYS recalculates. So if you also want to change the maxOrder, do this first!
     * @param newFluct
     *
    void setFluct(Wave_Trend_Fluctuation_t newFluct);
    */

    /**
     * @brief valueAt
     * @param x
     * @return the value at the given position, or NaN if the polynomial is not valid or another error occured!
     */
    double valueAt(QDateTime x);//FIXME check cast (long double)

    /**
     * @brief similarity calculates a similarity score based on R2Adj.
     * I.e. it calculates the R2Adj (omega^2) for the ref-points of the one polynomial the the coeffcients of the other and vise versa.
     * Those two omega^2_1 and omega^2_2 is added up and provides a sense of similarity
     * @param p1
     * @param p2
     * @return NaN if an error occured! Otherwise large ~ similar
     */
    static double similarity(Polynomial* p1, Polynomial* p2);//FIXME check theory!


    /**
     * @brief isValid
     * @return  true if the polynomial can be used, false if an error occured while fiting! Then all data will be NaN!
     */
    bool isValid() const;

    /**
     * @brief getRefZero
     * @return
     */
    DataPoint getRefZero() const;

    /**
     * @brief setRefZero sets the refzero. The polynomial MUST be recalculated after this.
     * However reCalc provides a parameter to do this later, e.g. after also setting the maxOrder.
     * The recalculation should not be forgotten! This would make the whole polynomial uncomperable.
     * @param newRefZero
     * @param reCalc should the fit be recalculated now or is it done later by another function? DONT FORGET TO RECALC!
     */
    void setRefZero(DataPoint *newRefZero, bool reCalc = true);

    /**
     * @brief getWeights
     * @return
     */
    long double **getWeights() const;

    /**
     * @brief getRefData
     * @return
     */
    DataContainer *getRefData();

    /**
     * @brief getHorizon
     * @return number of datapoints used for fitting
     */
    int getHorizon();

public slots:

    /**
     * @brief dataChanged
     * Should be called when the datacontainer is changed, recalculates the polynomial. So it should ONLY be called after all changes are finished.
     * It also copies the data and recalculates start-end.
     */
    void dataChanged();

private:

    /**
     * @brief calcPolynomial helper function calculating the coefficients with the current configuartion
     * Using NASA-PolyFit
     * Could be improved by using the error array and many other parameters!
     * @return true on success, false on failure
     */
    bool calcPolynomial();//TODO use weights?!
    //FIXME long double only needed for x. not for coeff and maaabye not for y (see calculation)

    ///! Mapping the order of the coeffient to its value. I.e. (1,2) , (2,3.1) => 2*x + 3.1*x^2
    QMap<uint8_t,long double> coeff;

    ///! Stores the raw datacontainer that is used to calcualte the polynomial
    DataContainer* refData = nullptr;

    ///! The referencepoint for normalizing the time
    DataPoint* refZero = nullptr;

    ///! How the fit handles fluctuations
   // Wave::Wave_Trend_Fluctuation_t fluct;

    ///! The maximum order of the polynomial
    uint8_t maxOrder = 0;

    ///! Is the current polynomial valid. if not all points will be NaN
    bool valid = false;

    ///! The weights within the calculation of the polynomial, is reused for similarity
    long double ** weights = nullptr;
};

#endif // POLYNOMIAL_H
