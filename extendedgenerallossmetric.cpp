#include "extendedgenerallossmetric.h"

ExtendedGeneralLossMetric::ExtendedGeneralLossMetric()
{
    name = "Extended General Loss Metric";
}

InformationlossMetric::InformationLoss_t ExtendedGeneralLossMetric::calcLoss(QList<QPair<DataPoint *, DataPoint *> *> eqcHull,double eqcMinVal,double eqcMaxVal,unsigned int resolution)
{
    InformationLoss_t ret ={nan(""),nan(""),nan("")};
    if(eqcHull.isEmpty()){
        ret ={0,0,0};
        return ret;
    }else if( isnan(eqcMaxVal)|| isnan(eqcMaxVal)  || eqcMinVal > eqcMaxVal){
        return ret;
    }else{
        ret ={0,0,0};
        unsigned int ptCount = 1;
        if(resolution == 0)
        {
            for(QPair<DataPoint*,DataPoint*>* p : eqcHull)
            {
                lossHelper(p,eqcMaxVal,eqcMinVal,&ret,&ptCount);
            }
        }else{
            QDateTime startTime = eqcHull.first()->first->getTime();
            QDateTime endTime = eqcHull.last()->first->getTime();
            QDateTime currTime = startTime;
            QList<DataPoint*> lowerDat;
            QList<DataPoint*> upperDat;
            for(QPair<DataPoint*,DataPoint*>* p : eqcHull)
            {
                lowerDat.append(p->first);
                upperDat.append(p->second);
            }
            Wave low = Wave("Hull-lower","",Wave::WaveType_t::WAVE_TYPE_UNKNOWN,"",&lowerDat,"",true);
            Wave high = Wave("Hull-upper","",Wave::WaveType_t::WAVE_TYPE_UNKNOWN,"",&upperDat,"",true);
            while(currTime < endTime)
            {
                QPair<DataPoint*,DataPoint*>* p = new QPair<DataPoint*,DataPoint*>();
                p->first=new DataPoint(currTime,low.valueAt(currTime));
                p->second=new DataPoint(currTime,high.valueAt(currTime));
                //FIXME add slider to UI for selection / param to eval
                //FIXME use resoltuin in mainwindow
                lossHelper(p,eqcMaxVal,eqcMinVal,&ret,&ptCount);
                currTime = currTime.addMSecs(resolution);
                delete p->first;
                delete p->second;
                delete p;
            }
        }
        return ret;
    }

}

InformationlossMetric::InformationLoss_t ExtendedGeneralLossMetric::calcLoss(QList<QPair<DataPoint *, DataPoint *> *> eqcHull,double eqcMinVal,double eqcMaxVal, DataContainer add,unsigned int resolution)
{

    InformationLoss_t ret ={nan(""),nan(""),nan("")};
    if(eqcHull.isEmpty()){
        ret ={0,0,0};
        return ret;
    }else if( isnan(eqcMaxVal)|| isnan(eqcMaxVal)  || eqcMinVal > eqcMaxVal){
        return ret;
    }else{
        ret ={0,0,0};
        unsigned int ptCount = 1;
        if(resolution == 0)
        {
            for(QPair<DataPoint*,DataPoint*>* p : eqcHull)
            {
                lossHelper(p,eqcMaxVal,eqcMinVal,&ret,&ptCount,add);
            }
        }else{
            QDateTime startTime = eqcHull.first()->first->getTime();
            QDateTime endTime = eqcHull.last()->first->getTime();
            QDateTime currTime = startTime;
            QList<DataPoint*> lowerDat;
            QList<DataPoint*> upperDat;
            for(QPair<DataPoint*,DataPoint*>* p : eqcHull)
            {
                lowerDat.append(p->first);
                upperDat.append(p->second);
            }
            Wave low = Wave("Hull-lower","",Wave::WaveType_t::WAVE_TYPE_UNKNOWN,"",&lowerDat,"",true);
            Wave high = Wave("Hull-upper","",Wave::WaveType_t::WAVE_TYPE_UNKNOWN,"",&upperDat,"",true);
            while(currTime < endTime)
            {
                QPair<DataPoint*,DataPoint*>* p = new QPair<DataPoint*,DataPoint*>();
                p->first=new DataPoint(currTime,low.valueAt(currTime));
                p->second=new DataPoint(currTime,high.valueAt(currTime));
                lossHelper(p,eqcMaxVal,eqcMinVal,&ret,&ptCount,add);
                currTime = currTime.addMSecs(resolution);
                delete p->first;
                delete p->second;
                delete p;
            }
        }
        return ret;
    }

}

void ExtendedGeneralLossMetric::lossHelper(QPair<DataPoint*,DataPoint*>* p,double eqcMaxVal,double eqcMinVal, InformationLoss_t* ret, unsigned int* ptCount)
{
    double val = abs(p->first->getValue() - p->second->getValue()) / (eqcMaxVal - eqcMinVal);
    if(val < ret->minLoss)
    {
        ret->minLoss = val;
    }
    if(val > ret->maxLoss)
    {
        ret->maxLoss = val;
    }
    ret->averageLoss = ret->averageLoss + (val - ret->averageLoss)/(*ptCount);
    (*ptCount)++;

}

void ExtendedGeneralLossMetric::lossHelper(QPair<DataPoint *, DataPoint *> *p, double eqcMaxVal, double eqcMinVal, InformationlossMetric::InformationLoss_t *ret, unsigned int *ptCount, DataContainer add)
{
    double addVal ;
    if(add.isInterpolateable())
    {
        addVal = add.valueAt(p->first->getTime(),HOLD_VAL_LOSS);
    }else{
        addVal = add.findClosest(p->first->getTime())->getValue();//FIXME time error??
    }
    if(isnan(addVal))
    {
        qWarning() << "Skipping invalid value for informationloss calculation!";
        return;
    }
    double minVal = fmin(p->first->getValue(), addVal);
    double maxVal = fmax(p->second->getValue(), addVal);
    double val = abs(maxVal - minVal) / (eqcMaxVal - eqcMinVal);
    if(val < ret->minLoss)
    {
        ret->minLoss = val;
    }
    if(val > ret->maxLoss)
    {
        ret->maxLoss = val;
    }
    ret->averageLoss = ret->averageLoss + (val - ret->averageLoss)/ (*ptCount);
    (*ptCount)++;
}
