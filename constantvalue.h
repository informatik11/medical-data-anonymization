#ifndef CONSTANTVALUE_H
#define CONSTANTVALUE_H

#include "datacontainer.h"
#include <QObject>

/**
 * @brief The ConstantValue class represents all none-wave type values, i.e. a single datapoint like age, zipcode etc.
 */
class ConstantValue : public DataContainer
{
public:
    ConstantValue();

    //FIXME WORK HERE
    // Override Data var (+fcts?)
    //custom type
    // NaN before start & after end, else the value
};

#endif // CONSTANTVALUE_H
