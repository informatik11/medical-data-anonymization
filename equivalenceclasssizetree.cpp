#include "equivalenceclasssizetree.h"

QList<EquivalenceClassSizeTree::EqcSizeTree_node_t *> EquivalenceClassSizeTree::EqcSizeTree_node_t::getSibilings()
{
    return parentTree->root->getNodesByDepth(getDepth());
}

QList<EquivalenceClassSizeTree::EqcSizeTree_node_t *> EquivalenceClassSizeTree::EqcSizeTree_node_t::getNodesByDepth(unsigned int depth)
{
    QList<EqcSizeTree_node_t*> ret;
    if(depth == 0)
    {
        ret.append(this);
        return ret;
    }else{
        if(!isLeaf())
        {
            QList<EqcSizeTree_node_t*> lSib = left->getNodesByDepth(depth -1);
            QList<EqcSizeTree_node_t*> rSib = right->getNodesByDepth(depth -1);
            for(EqcSizeTree_node_t* node : lSib)
            {
                ret.append(node);
            }
            for(EqcSizeTree_node_t* node : rSib)
            {
                ret.append(node);
            }
        }
        return ret;
    }
}

unsigned int EquivalenceClassSizeTree::EqcSizeTree_node_t::getMaxLevelSize()
{
    if(parentNode != nullptr)
    {
        unsigned int currSize = 0;
        if(parentNode->left != nullptr)
        {
            currSize = max(currSize,parentNode->left->calcSize());
        }
        if(parentNode->right != nullptr)
        {
            currSize = max(currSize, parentNode->right->calcSize());
        }
        return currSize;
    }else{
        return calcSize();//is the root
    }
}

unsigned int EquivalenceClassSizeTree::EqcSizeTree_node_t::getMaxLevelSize(int depth)
{
    if(depth > 0)
    {
        unsigned int currSize = 0;
        if(left != nullptr)
        {
            currSize = max(currSize,left->getMaxLevelSize(depth -1));
        }
        if(right != nullptr)
        {
            currSize = max(currSize,right->getMaxLevelSize(depth -1));
        }
        return currSize;
    }else if(depth == 0)
    {
        return calcSize(); // saves one recursion per leaf-branch compared to calling getMaxLevelSize()
    }else{
        return 0;
    }
}

unsigned int EquivalenceClassSizeTree::EqcSizeTree_node_t::calcSize()
{
    if(nodeDistrib != nullptr && nodeDistrib->isValid())
    {
        return nodeDistrib->getTotalObservations();
    }else{
        unsigned int size = 0;
        QList<unsigned int> localValues = bucketSizes.values();
        for(unsigned int val : localValues )
        {
            size += val;
        }
        return size;
    }
}

QString EquivalenceClassSizeTree::EqcSizeTree_node_t::toString()
{
    QString subsizes;
    QList<unsigned int> vals = bucketSizes.values();
    for(unsigned int val : vals)
    {
        subsizes += QString("%1,").arg(val);
    }
    QString currStr = QString("S:%1(%2)").arg(calcSize()).arg(subsizes);
    QString lStr;
    if(left != nullptr)
    {
        lStr = "-L-"+left->toString();
    }else{
        lStr = "--<EMPTY>";
    }
    QString rStr;
    if(right != nullptr)
    {
        rStr = "-R-"+right->toString();
    }else{
        rStr = "<EMPTY>";
    }
    return currStr + lStr + rStr;//FIXME better visualization
}

bool EquivalenceClassSizeTree::EqcSizeTree_node_t::constraintHolds(unsigned int maxSize)
{

    return (calcSize() <= maxSize);
}

bool EquivalenceClassSizeTree::EqcSizeTree_node_t::constraintHoldsAlways(unsigned int maxSize)
{
    if(constraintHolds(maxSize))
    {
        if(isLeaf())
        {
            return true;
        }else{
            return (left->constraintHoldsAlways(maxSize) &&  right->constraintHoldsAlways(maxSize));
        }
    }else{
        return false;
    }
}

QList<EquivalenceClassSizeTree::EqcSizeTree_node_t *> EquivalenceClassSizeTree::EqcSizeTree_node_t::getLeafNodes()
{
    QList<EqcSizeTree_node_t*> ret;
    if(isLeaf())
    {
        ret.append(this);
        return ret;
    }else{
        QList<EqcSizeTree_node_t*> leftLeafs = left->getLeafNodes();
        QList<EqcSizeTree_node_t*> rightLeafs = right->getLeafNodes();

        for(EqcSizeTree_node_t* node : leftLeafs)
        {
            ret.append(node);
        }

        for(EqcSizeTree_node_t* node : rightLeafs)
        {
            ret.append(node);
        }

        return ret;
    }
}

QList<EquivalenceClassSizeTree::EqcSizeTree_node_t *> EquivalenceClassSizeTree::EqcSizeTree_node_t::getNodes(int depth)
{
    QList<EqcSizeTree_node_t *>  ret;
    if(depth < 0)
    {
        return ret;
    }else if(depth == 0){
        ret.append(this);
        return ret;
    }else{//still need to recurse
        if(isLeaf())
        {
            return ret;
        }else{
            QList<EqcSizeTree_node_t *> lRet = left->getNodes(depth -1);
            QList<EqcSizeTree_node_t *> rRet = right->getNodes(depth -1);
            if(lRet.isEmpty() ||  rRet.isEmpty())//if depth cant be reached in one branch nothing is returned!
            {
                return ret;
            }else{
                for(EqcSizeTree_node_t* node : lRet)
                {
                    ret.append(node);
                }

                for(EqcSizeTree_node_t* node : rRet)
                {
                    ret.append(node);
                }

                return ret;
            }
        }
    }
}

void EquivalenceClassSizeTree::EqcSizeTree_node_t::deleteTree(bool deleteDistr)
{
    if(left != nullptr )
    {
        left->deleteTree();
        delete left;
    }
    if(right != nullptr)
    {
        right->deleteTree();
        delete right;
    }
    if(deleteDistr == true)
    {
        delete nodeDistrib;
    }
}

unsigned int EquivalenceClassSizeTree::EqcSizeTree_node_t::getDepth()
{
    if(parentNode == nullptr)
    {
        return 0;
    }else{
        return parentNode->getDepth() + 1;
    }
}

int64_t EquivalenceClassSizeTree::EqcSizeTree_node_t::getMinDepth(unsigned int maxSize)
{

    return getMinDepthHelper(maxSize,getDepth());
}

int64_t EquivalenceClassSizeTree::EqcSizeTree_node_t::getMinDepthHelper(unsigned int maxSize, int currDepth)
{
    if (bucketSizes.isEmpty()){
        return -1;
    }else{
        if(calcSize() <= maxSize){
            return currDepth;
        }else if(left != nullptr && right != nullptr){
            int leftDepth = left->getMinDepthHelper(maxSize,currDepth +1);
            if(leftDepth < 0)
            {
                return leftDepth;
            }//Calc right only if left is OK

            int rightDepth = right->getMinDepthHelper(maxSize,currDepth +1);
            if(rightDepth < 0)
            {
                return rightDepth;
            }
            //both branches OK
            return max(leftDepth,rightDepth);

        }else{
            return -2;
        }
    }
}


EquivalenceClassSizeTree::EquivalenceClassSizeTree(double c, DistributionDistanceMetric *metric, Distribution *popDistr)
{
    root = nullptr;
    this->b = c;
    this->metric = metric;
    this->popDistr = popDistr;

}

EquivalenceClassSizeTree::EquivalenceClassSizeTree(EqcSizeTree_node_t *root, double c, DistributionDistanceMetric *metric, Distribution *popDistr)
{
    root->parentTree = this;
    this->root = root;
    this->b = c;
    this->metric = metric;
    this->popDistr = popDistr;
}

EquivalenceClass *EquivalenceClassSizeTree::generate(double minValue, double maxValue, BucketTree *data, Distribution *popDistr, DataDistanceMetric *dataDistMetric, DistributionDistanceMetric* distribDistMetric, InformationlossMetric *infoMetric, unsigned int k, double t, unsigned int b, double h,int infoLossRes, EquivalenceClassSizeTree *sizeTree, unsigned int *sizeTreeDepth, bool greedy, QHash<DataContainer *, QHash<DataContainer *, double> *> *groundDistanceCache)
{
    if(k < 2 || isnan(t) || isnan(h) || h <= 0 || t <= 0 || data == nullptr || dataDistMetric == nullptr || distribDistMetric == nullptr || data->isEmpty() || isnan(minValue) || isnan(maxValue) || maxValue <= minValue)
    {
        return nullptr;
    }else{
        unsigned int minDepth = data->getMinDepth();
        if(minDepth < b)
        {
            qCritical() << QString("The target depth  (%1) can not be reached in the buckettree. Aborting anonymization").arg(b);
            QMessageBox msg;
            msg.setText(QString("Die Zieltiefe (%1) kann im aktuellen Eimerbaum nicht erreicht werden.\nBreche Anonymisierung ab!").arg(b));
            msg.exec();
            return nullptr;
        }
        QList<Bucket*> buckets = data->getBuckets(b);
        double c = data->getMaxCost(b);


        QList<EquivalenceClassSizeTree::EqcSizeTree_node_t*> nodes = handleSizeTree(sizeTree,sizeTreeDepth,data,c,dataDistMetric,distribDistMetric,popDistr,popDistr->getV(),groundDistanceCache);

        if(nodes.isEmpty())
        {
            return nullptr;
        }else{

            int emptyCount = 0;

            for(Bucket* bucket : buckets)
            {
                if(bucket->isEmpty())
                {
                    emptyCount++;
                }
            }
            if(emptyCount == buckets.size())
            {
                qInfo() << "All buckets where empty, stopping generation!";
                return nullptr;
            }

            unsigned int maxTargetSize = 0;
            for(EquivalenceClassSizeTree::EqcSizeTree_node_t* node : nodes)
            {
                unsigned int sze = node->calcSize();
                if(maxTargetSize < sze)
                {
                    maxTargetSize = sze;
                }
            }

            EquivalenceClass* eqc = new EquivalenceClass(k,t,h,popDistr,dataDistMetric,distribDistMetric,infoMetric,maxTargetSize,maxValue,minValue,infoLossRes);

            for(Bucket* bucket : buckets)
            {
                if(greedy == true)
                {
                    eqc->takeGreedy(bucket);
                }else{
                    eqc->takeConsiderate(bucket);
                }
            }
            return eqc;
        }
    }
}

QList<EquivalenceClass *> *EquivalenceClassSizeTree::generateAll(double minValue, double maxValue, BucketTree *data, Distribution *popDistr, DataDistanceMetric *dataDistMetric, DistributionDistanceMetric* distrDistMetric, InformationlossMetric *infoMetric, int k, double t, unsigned int b, double h, double s, int infoLossRes,EquivalenceClassSizeTree *sizeTree, unsigned int *sizeTreeDepth, bool greedy, double d, QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    if(k < 2 || isnan(t) || t <= 0  || isnan(d)|| d <= 0 ||  isnan(s) || s <= 0|| dataDistMetric == nullptr || distrDistMetric == nullptr || data == nullptr || data->isEmpty() || isnan(minValue) || isnan(maxValue) || maxValue <= minValue)
    {
        return nullptr;
    }else{
        QList<EquivalenceClass*>* openEqcLst = new QList<EquivalenceClass*>();//All eqcs which do not yet fullfill the constraints
        QList<EquivalenceClass*>* finEqcLst = new QList<EquivalenceClass*>();//All finished eqcs which already fullfill the constraints

        unsigned int minDepth = data->getMinDepth();
        if(minDepth < b)
        {
            delete openEqcLst;
            delete finEqcLst;
            qCritical() << QString("The target depth  (%1) can not be reached in the buckettree. Aborting anonymization").arg(b);
            QMessageBox msg;
            msg.setText(QString("Die Zieltiefe (%1) kann im aktuellen Eimerbaum nicht erreicht werden.\nBreche Anonymisierung ab!").arg(b));
            msg.exec();
            return nullptr;
        }
        QList<Bucket*> buckets = data->getBuckets(b);
        double c = data->getMaxCost(b);

        QList<EquivalenceClassSizeTree::EqcSizeTree_node_t*> nodes = handleSizeTree(sizeTree,sizeTreeDepth,data,c,dataDistMetric,distrDistMetric,popDistr,popDistr->getV(),groundDistanceCache);
        if(nodes.isEmpty())
        {
            delete finEqcLst;
            delete openEqcLst;
            return nullptr;
        }else{
            for(EquivalenceClassSizeTree::EqcSizeTree_node_t* node : nodes)
            {
                openEqcLst->append(new EquivalenceClass(k,t,h,popDistr,dataDistMetric,distrDistMetric,infoMetric,node->calcSize(),maxValue,minValue,infoLossRes));
            }
        }

        QHash<EquivalenceClass*,bool> finishedMap; // a eqc is mapped to true, if nothing was added from any bucket. Then it can be ignored in the future . If all are true, the loop is broken.
        for(EquivalenceClass* eqc : *openEqcLst)
        {
            finishedMap.insert(eqc,false);
        }
        int currEqcInd = 0;
        while(!openEqcLst->isEmpty() && !data->isLevelEmpty(b))//Fill in round robin until buckets are empty or all EQCs filled
        {
            if(currEqcInd >= openEqcLst->size())
            {
                currEqcInd = 0;
            }
            EquivalenceClass* eqc = openEqcLst->at(currEqcInd);
            if(finishedMap.value(eqc) == true)
            {
                for(EquivalenceClass* eqc : *openEqcLst)
                {
                    if(finishedMap.value(eqc) == true)
                    {
                        currEqcInd = openEqcLst->indexOf(eqc);//there was nothing added last time, there wont be anything to add this time
                        continue;
                    }
                }
                break;//there are no eqcs, which are not finished (i.e. all open eqcs had a chance to get more containers, but couldnt)
            }
            int num = 0;
            for(Bucket* bucket: buckets)//FIXME break if all buckets are empty
            {
                if(greedy == true)
                {
                    num +=  eqc->takeGreedy(bucket,c,d);
                }else{
                    num += eqc->takeConsiderate(bucket,c,d);
                }
                if(eqc->infoMetric->calcLoss(eqc->getHull(),eqc->getMinValue(),eqc->getMaxValue(),eqc->getInfoLossRes()).averageLoss > s)//create new eqc if sensitivity level reached
                {
                    qInfo() << "Created a new EQC, as the information loss was larger then S!";
                    unsigned int ts1 = eqc->getTargetSize();
                    eqc->setTargetSize(eqc->getKReal());
                    unsigned int ts2 = eqc->getKReal() - ts1;
                    openEqcLst->append(new EquivalenceClass(k,t,h,popDistr,dataDistMetric,distrDistMetric,infoMetric,ts2,maxValue,minValue,infoLossRes));
                }
            }
            if(num == 0){//Nothing added for all buckets.
                finishedMap.insert(eqc,true);
            }

            if(eqc->fullFillsConstraints() == true)
            {
                finEqcLst->append(eqc);
                openEqcLst->removeAll(eqc);
                if(openEqcLst->isEmpty() && !data->isLevelEmpty(b))//Create "trash" eqc
                {
                    unsigned int targetSize = 0;
                    for(Bucket* bucket : buckets)
                    {
                        targetSize += bucket->size();
                    }
                    openEqcLst->append(new EquivalenceClass(k,t,h,popDistr,dataDistMetric,distrDistMetric,infoMetric,targetSize,maxValue,minValue,infoLossRes));
                    currEqcInd = 0;
                    continue;

                }
                continue;
            }else{
                currEqcInd++;
            }
        }

        if(!openEqcLst->isEmpty())//Some EQCs dont fullfill the constraints redistribute their content
        {

            //PHASE 1: Redistribute between unfinished EQCs
            if(openEqcLst->size() > 1)
            {
                QList<EquivalenceClass*> redistrEqcLst = *openEqcLst;
                while(!openEqcLst->isEmpty())
                {
                    EquivalenceClass* eqc = openEqcLst->first();
                    redistrEqcLst.removeAll(eqc);
                    int currEqc = 0;
                    int added = 0;//how many containers were added after a fulll cycle through all eqcs
                    bool firstCycle  = true;
                    if(!redistrEqcLst.isEmpty())
                    {
                        do{
                            EquivalenceClass* targetEqc = redistrEqcLst.at(currEqc);
                            if(greedy == true)
                            {
                                added += eqc->takeGreedy(targetEqc,s,d);
                            }else{
                                added += eqc->takeConsiderate(targetEqc,s,d);
                            }

                            if(currEqc >= redistrEqcLst.size())
                            {
                                currEqc = 0;
                                if(firstCycle == true)
                                {
                                    firstCycle = false;
                                }
                            }
                        }while((firstCycle || added > 0) && !eqc->getRawData().isEmpty() );//still able to redistribute
                        if(eqc->getRawData().isEmpty())//fully redistributed
                        {
                            delete eqc;
                            openEqcLst->removeAll(eqc);
                        }
                    }else{
                        break;
                    }
                }
            }

            //PHASE 2: Into finished EQCs
            if(!finEqcLst->isEmpty() && !openEqcLst->isEmpty())
            {
                QList<EquivalenceClass*> openCopy = *openEqcLst;
                for(EquivalenceClass* eqc : openCopy)
                {
                    int currEqc = 0;
                    int added = 0;//how many containers were added after a fulll cycle through all eqcs
                    bool firstCycle  = true;
                    do{
                        EquivalenceClass* targetEqc = finEqcLst->at(currEqc);
                        if(greedy == true)
                        {
                            added += eqc->takeGreedy(targetEqc,s,d);
                        }else{
                            added += eqc->takeConsiderate(targetEqc,s,d);
                        }

                        if(currEqc >= finEqcLst->size())
                        {
                            currEqc = 0;
                            added = 0;
                            if(firstCycle == true)
                            {
                                firstCycle = false;
                            }
                        }
                    }while((firstCycle || added > 0) && !eqc->getRawData().isEmpty() );//still able to redistribute
                    if(eqc->getRawData().isEmpty())//fully redistributed
                    {
                        delete eqc;
                        openEqcLst->removeAll(eqc);
                    }
                }
            }

            //PHASE 3: Drop data or redistribute without infoLossConstraint
            if(!finEqcLst->isEmpty() && !openEqcLst->isEmpty())
            {
                QList<EquivalenceClass*> openCopy = *openEqcLst;
                for(EquivalenceClass* eqc : openCopy)
                {
                    int currEqc = 0;
                    int added = 0;//how many containers were added after a fulll cycle through all eqcs
                    bool firstCycle  = true;

                    do{
                        EquivalenceClass* targetEqc = finEqcLst->at(currEqc);
                        if(greedy == true)
                        {
                            added += eqc->takeGreedy(targetEqc,INFINITY,d);
                        }else{
                            added += eqc->takeConsiderate(targetEqc,INFINITY,d);
                        }

                        if(currEqc >= finEqcLst->size())
                        {
                            currEqc = 0;
                            added = 0;
                            if(firstCycle == true)
                            {
                                firstCycle = false;
                            }
                        }
                    }while((firstCycle || added > 0) && !eqc->getRawData().isEmpty() );//still able to redistribute
                    if(eqc->getRawData().isEmpty())//fully redistributed
                    {
                        delete eqc;
                        openEqcLst->removeAll(eqc);
                    }
                }
            }

            //FIXME improvement make shorter (merge phases / helper function)
            //FIXME improvment drop strategy (drop at end, drop fast, dont drop)
            if(!data->isLevelEmpty(b))//Drop the remaining data = assign to closest eqc as dropped
            {
                for(Bucket* bucket: buckets)
                {
                    if(bucket->isEmpty())
                    {
                        continue;
                    }else{
                        for(DataContainer* cont : bucket->getData().keys())
                        {
                            double bestLoss = nan("");
                            EquivalenceClass* bestEqc = nullptr;
                            for(EquivalenceClass* eqc : *finEqcLst)
                            {
                                double loss = eqc->getInfoMetric()->calcLoss(eqc->getHull(),eqc->getMinValue(),eqc->getMaxValue(),cont,eqc->getInfoLossRes()).averageLoss;
                                if(isnan(bestLoss) || loss < bestLoss)
                                {
                                    bestEqc = eqc;
                                    bestLoss = loss;
                                }
                            }
                            if(bestEqc != nullptr)
                            {
                                bestEqc->add(cont,true);
                            }else{
                                bestLoss = nan("");

                                QList<EquivalenceClass*> openCopy = *openEqcLst;
                                for(EquivalenceClass* eqc : openCopy)
                                {
                                    double loss = eqc->getInfoMetric()->calcLoss(eqc->getHull(),eqc->getMinValue(),eqc->getMaxValue(),cont,eqc->getInfoLossRes()).averageLoss;
                                    if(isnan(bestLoss) || loss < bestLoss)
                                    {
                                        bestEqc = eqc;
                                        bestLoss = loss;
                                    }
                                }

                                bestEqc->add(cont,true);
                            }
                        }
                    }
                }
            }
        }
        bool empty = true;//FIXME DEBUG REMOVE THIS!
        for(Bucket* bucket: buckets)
        {
            if(!bucket->getData().isEmpty())
            {
                empty = false;
            }
        }
        if(!empty || openEqcLst->size() > 0)
        {
            qDebug() << "Not realy empty!!! Found bugy curve!";
        }
        return finEqcLst;
    }
}

EquivalenceClass *EquivalenceClassSizeTree::generateExist(QList<DataContainer *> *data, QList<EquivalenceClass *> *exEqcs, Distribution *popDistr, int k, double t, double maxTolerance, double maxSemanticDistance, bool droppabale)
{
    qFatal("Unimplemented GenerateExist in EQCSizeTree");
    //FIXME IMPLEMENT
    //FIXME ADAPT API
}

QList<EquivalenceClassSizeTree::EqcSizeTree_node_t *> EquivalenceClassSizeTree::getSibilings(unsigned int depth)
{
    return root->getNodesByDepth(depth);
}

unsigned int EquivalenceClassSizeTree::getMaxLevelSize(int depth)
{
    if(!isEmpty())
    {
        return root->getMaxLevelSize(depth);
    }else{
        return 0;
    }
}

QPair<EquivalenceClassSizeTree *, unsigned int> EquivalenceClassSizeTree::genTree(QHash<DataContainer *, unsigned int> buckets, double c, DataDistanceMetric *dataDistMetric,DistributionDistanceMetric* distribDistMetric, Distribution *popDistr,double v,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    QPair<EquivalenceClassSizeTree *, unsigned int> ret;
    if(!isnan(c) && c >= 0  && distribDistMetric != nullptr && dataDistMetric != nullptr && popDistr != nullptr && popDistr->isValid() && !buckets.isEmpty() && !isnan(v) && v >= 0)
    {
        EqcSizeTree_node_t* root = new EqcSizeTree_node_t;
        root->bucketSizes = buckets;
        root->nodeDistrib = Bucket::genDistrib(buckets,dataDistMetric,v);
        root->parentNode = nullptr;

        EquivalenceClassSizeTree* tree = new EquivalenceClassSizeTree(root,c,distribDistMetric,popDistr);
        bool localCache = false;
        if(groundDistanceCache == nullptr)
        {
            localCache = true;
            groundDistanceCache = new QHash<DataContainer*,QHash<DataContainer*,double>*>();
        }
        int maxLeafSize = tree->splitLeafsUntil(groundDistanceCache);
        if(localCache == true)
        {
            delete groundDistanceCache;
        }
        if(maxLeafSize < 0)
        {
            tree->deleteTree();
            delete root;
            delete tree;
            ret.first = nullptr;
            return ret;
        }else{
            ret.first = tree;
            ret.second = maxLeafSize;
            return ret;
        }
    }else{
        ret.first = nullptr;
        return ret;
    }
}

QPair<EquivalenceClassSizeTree *, unsigned int> EquivalenceClassSizeTree::genTree(BucketTree* bTree, double c, DataDistanceMetric *dataDistMetric,DistributionDistanceMetric* distribDistMetric,Distribution *popDistr,double v,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{

    unsigned int depth = bTree->getMinDepth(c);
    QList<Bucket*> buckets = bTree->getBuckets(depth);
    QHash<DataContainer*, unsigned int> virtBuckets;
    for(Bucket* bucket: buckets)
    {
        virtBuckets.insert(bucket->getRefKey(),bucket->getFullCount());
    }
    return genTree(virtBuckets,c,dataDistMetric,distribDistMetric,popDistr,v,groundDistanceCache);
}


bool EquivalenceClassSizeTree::constraintHolds()
{
    return root->constraintHolds(b);
}

bool EquivalenceClassSizeTree::constraintHoldsAlways()
{
    return root->constraintHoldsAlways(b);
}

bool EquivalenceClassSizeTree::insert(QHash<DataContainer*,unsigned int> buckets,EqcSizeTree_node_t *parent,Distribution* nodeDistrib)
{
    EqcSizeTree_node_t *p=new EqcSizeTree_node_t;
    p->bucketSizes = buckets;
    p->left = nullptr;
    p->right = nullptr;
    p->nodeDistrib = nodeDistrib;
    p->parentTree = parent->parentTree;
    if(isEmpty())
    {
        root = p;
        return true;
    }else{
        if(parent->left == nullptr)
        {
            parent->left = p;
            return true;
        }else if(parent-> right == nullptr)
        {
            parent->right = p;
            return true;
        }else{
            return false;
        }
    }
}


QString EquivalenceClassSizeTree::toString()
{
    return root->toString();
}

int EquivalenceClassSizeTree::splitLeafsUntil(unsigned int sizeThres,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    if(!isEmpty())
    {
        return splitLeafsUntil(root,sizeThres,groundDistanceCache);
    }else{
        return -1;
    }
}

int EquivalenceClassSizeTree::splitLeafsUntil(EqcSizeTree_node_t *node, unsigned int sizeThres,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    if(node->isLeaf())
    {
        return splitUntil(node,sizeThres,groundDistanceCache);
    }else{
        return max(splitLeafsUntil(node->left,sizeThres,groundDistanceCache), splitLeafsUntil(node->right,sizeThres,groundDistanceCache));
    }
}

int EquivalenceClassSizeTree::splitLeafsUntil(EqcSizeTree_node_t *node,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    if(node->isLeaf())
    {
        return splitUntil(node,groundDistanceCache);
    }else{
        return max(splitLeafsUntil(node->left,groundDistanceCache), splitLeafsUntil(node->right,groundDistanceCache));
    }
}

int EquivalenceClassSizeTree::splitLeafsUntil(QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    if(!isEmpty())
    {
        return splitLeafsUntil(root,groundDistanceCache);
    }else{
        return -1;
    }
}

int EquivalenceClassSizeTree::splitUntil(EqcSizeTree_node_t *node, unsigned int sizeThres,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    unsigned int oldSize = node->calcSize();
    unsigned int currSize = splitNode(node,true,groundDistanceCache);
    if(currSize <= 0)
    {
        return -1;
    }else if(currSize >= sizeThres && currSize < oldSize)
    {
        return max(splitUntil(node->left,sizeThres,groundDistanceCache),splitUntil(node->right,sizeThres,groundDistanceCache));
    }else{//Thresh reached or not reachable
        return currSize;
    }
}

int EquivalenceClassSizeTree::splitUntil(EqcSizeTree_node_t *node,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    unsigned int oldSize = node->calcSize();
    unsigned int currSize = splitNode(node,true,groundDistanceCache);
    if(currSize <= 0)
    {
        return -1;
    }else if(currSize >= oldSize)//no further split possible for this t
    {
        return currSize;
    }else{
        unsigned int leftSize = splitUntil(node->left,groundDistanceCache);
        unsigned int rightSize = splitUntil(node->right,groundDistanceCache);
        return min(leftSize,rightSize);
    }
}

int EquivalenceClassSizeTree::splitUntil(unsigned int sizeThres,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    if(!isEmpty())
    {
        return splitUntil(root,sizeThres,groundDistanceCache);
    }else{
        return -1;
    }
}

int EquivalenceClassSizeTree::splitUntil(QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    if(!isEmpty())
    {
        return splitUntil(root,groundDistanceCache);
    }else{
        return -1;
    }
}

QList<EquivalenceClassSizeTree::EqcSizeTree_node_t *> EquivalenceClassSizeTree::getLeafNodes()
{
    return root->getLeafNodes();
}

QList<EquivalenceClassSizeTree::EqcSizeTree_node_t *> EquivalenceClassSizeTree::getNodes(int depth)
{
    return root->getNodes(depth);
}

void EquivalenceClassSizeTree::deleteTree(bool delDistr)
{
    root->deleteTree(delDistr);
}

int64_t EquivalenceClassSizeTree::getDepth()
{
    return root->getDepth();
}

int64_t EquivalenceClassSizeTree::getMinDepth(unsigned int maxSize)
{
    if(root != nullptr)
    {
        return root->getMinDepth(maxSize);
    }else{
        return -2;
    }
}
bool EquivalenceClassSizeTree::isValid()
{
    return (!isnan(b) && b >= 0  && popDistr != nullptr && popDistr->isValid() && metric != nullptr);
}

double EquivalenceClassSizeTree::getC()
{
    return b;
}


Distribution *EquivalenceClassSizeTree::getPopDistribution()
{
    return popDistr;
}

DistributionDistanceMetric *EquivalenceClassSizeTree::getMetric()
{
    return metric;
}

DataContainer *EquivalenceClassSizeTree::calcRefKey(Distribution *distr, unsigned int size, DataContainer *currRefKey, bool next)
{
    if(distr == nullptr || size == 0 || currRefKey == nullptr)
    {
        return nullptr;
    }
    unsigned int cnt;
    if(size % 2 != 0) //is uneven
    {
        cnt = size/2;
    }else{
        cnt = ceil(size/2);
    }

    DataContainer* key = currRefKey;
    while(cnt > 0)
    {
        cnt--;
        if(next == true)
        {
            key = distr->getNextKey(key);
        }else{
            key = distr->getPreviousKey(key);
        }
    }

    return key;

}

QList<EquivalenceClassSizeTree::EqcSizeTree_node_t *> EquivalenceClassSizeTree::handleSizeTree(EquivalenceClassSizeTree *sizeTree, unsigned int *depth, BucketTree *data, double c, DataDistanceMetric *dataDistMetric,DistributionDistanceMetric* distribDistMetric, Distribution *popDistr,double v,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    QList<EquivalenceClassSizeTree::EqcSizeTree_node_t*> ret;
    QPair<EquivalenceClassSizeTree*, unsigned int> pair;
    if(data == nullptr || data->isEmpty() || isnan(c) || c < 0 || dataDistMetric == nullptr || distribDistMetric == nullptr || isnan(v) || v < 0 || popDistr == nullptr || !popDistr->isValid())
    {
        return ret;
    }else{
        //FIXME improvement use actual MaxCost, not upperbound maxCost
        if(sizeTree != nullptr || sizeTree->isEmpty())
        {
            pair = EquivalenceClassSizeTree::genTree(data,c,dataDistMetric,distribDistMetric,popDistr,v,groundDistanceCache);
            if(pair.first == nullptr)
            {
                return  ret;
            }else if(sizeTree->isEmpty())
            {
                *sizeTree = *pair.first;
            }

        }

        unsigned int maxSizeTreeDepth = pair.first->getDepth();
        if(depth != nullptr || *depth > maxSizeTreeDepth )
        {
            *depth = maxSizeTreeDepth;
        }

        ret = pair.first->getSibilings(*depth);

        return ret;
    }
}

int64_t EquivalenceClassSizeTree::splitNode(EqcSizeTree_node_t *node, bool overwriteChildren,QHash<DataContainer*,QHash<DataContainer*,double>*>* groundDistanceCache)
{
    EquivalenceClassSizeTree* tree = node->parentTree;

    if(node != nullptr && tree != nullptr && tree->isValid())
    {
        QList<unsigned int> sizes = node->bucketSizes.values();

        if(sizes.contains(0))//invalid size!
        {
            return -3;
        }else if(sizes.contains(1))//No further split possible!
        {
            return node->calcSize();
        }else{
            if(overwriteChildren == true || (node->left == nullptr  && node->right == nullptr))
            {
                QHash<DataContainer*,unsigned int> leftBuckets;//dichotomize according to SABRE
                QHash<DataContainer*,unsigned int> rightBuckets;
                Distribution* distr =  node->nodeDistrib;
                QList<DataContainer*> keys =  node->bucketSizes.keys();
                for(DataContainer* key : keys)
                {
                    unsigned int sze = node->bucketSizes.value(key);
                    unsigned int lSze = (unsigned int) round(sze * 0.5);
                    unsigned int rSze = sze - lSze;

                    leftBuckets.insert(calcRefKey(distr,lSze,key,false),lSze);
                    rightBuckets.insert(calcRefKey(distr,rSze,key,true),rSze);
                    //FIXME ERROR ?  check this!
                }
                //FIXME improvment correct v?
                Distribution* lDistr = Bucket::genDistrib(leftBuckets,node->nodeDistrib->getDistMetric(),node->nodeDistrib->getV());
                if(lDistr == nullptr || !lDistr->isValid())
                {
                    delete lDistr;
                    return -4;
                }else{//valid distribution

                    QList<EqcSizeTree_node_t *> sibilings = node->getSibilings();
                    Distribution* distr = node->parentTree->root->nodeDistrib;
                    double maxCost  = 0;// = U @sabre
                    for(EqcSizeTree_node_t* s : sibilings){
                        QHash<DataContainer*,unsigned int> buckSzes = s->bucketSizes;
                        unsigned int cnt = 0;
                        QList<unsigned int> values = buckSzes.values();
                        for(unsigned int i :values)
                        {
                            cnt += i;
                        }
                        double localCost = Bucket::calcMaxCost(buckSzes.keys(),cnt,distr,groundDistanceCache);//FIXME CHECK THIS!!!!!!!!!!!!!
                        if(!isnan(localCost))
                        {
                            maxCost += localCost;
                        }else{
                            qCritical() << "Cost was NaN in EQCSizeTree!!";
                            return -4;
                        }
                    }
                    double lDistance = tree->getMetric()->distance(lDistr,tree->getPopDistribution(),DISTR_IGNORE_NON_EXIST);
                    double totalLDist = lDistance + maxCost;
                    if( totalLDist > tree->getC())//node doesnt fullfill constraint (theorem 4 SABRE)
                    {
                        delete lDistr;
                        return node->calcSize();
                    }else{//left node fullfills constraint
                        //FIXME improvment correct v?
                        Distribution* rDistr = Bucket::genDistrib(rightBuckets,node->nodeDistrib->getDistMetric(),node->nodeDistrib->getV());
                        if(rDistr == nullptr || !rDistr->isValid())
                        {
                            delete rDistr;
                            return -4;
                        }else{//valid distribution
                            double rDistance = tree->getMetric()->distance(rDistr,tree->getPopDistribution(),DISTR_IGNORE_NON_EXIST);
                            double totalRDist = rDistance + maxCost;
                            //FIXME WORK HERE exit if two sibilings? Normalise distance to 0-1
                            //FIXME WORK HERE split/ cost wrong
                            if(totalRDist > tree->getC())//node doesnt fullfill constraint (theorem 4 SABRE)
                            {
                                delete rDistr;
                                return node->calcSize();
                            }else{//Both nodes fullfill constraint
                                EqcSizeTree_node_t* left = new EqcSizeTree_node_t;
                                left->bucketSizes = leftBuckets;
                                left->nodeDistrib = lDistr;
                                left->parentNode = node;
                                left->parentTree = tree;
                                EqcSizeTree_node_t* right = new EqcSizeTree_node_t;
                                right->bucketSizes = rightBuckets;
                                right->nodeDistrib = rDistr;
                                right->parentNode = node;
                                right->parentTree = tree;
                                if(left == nullptr || right == nullptr)
                                {
                                    qDebug() << "DEBUGGING HERE!!";
                                }
                                node->left = left;
                                node->right = right;
                                return min(left->calcSize(),right->calcSize());
                            }
                        }
                    }
                }
            }else{
                return -2;
            }
        }
    }else{
        return -1;
    }
}
