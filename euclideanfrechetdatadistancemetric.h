#ifndef EUCLIDEANFRECHETDATADISTANCEMETRIC_H
#define EUCLIDEANFRECHETDATADISTANCEMETRIC_H

#include "datadistancemetric.h"
#include "math.h"
#include "frechet/frechet.hpp"

#include <error.h>
#include <QDebug>

/**
 * @brief The EuclideanFrechetDataDistanceMetric class implements DistanceMetric with frechet distance with an euclidean gorund distance for datacontainers and -points
 */
class EuclideanFrechetDataDistanceMetric : public DataDistanceMetric
{
public:
    EuclideanFrechetDataDistanceMetric();

    /**
     * @brief error how precise is the distance
     * @return
     */
    double error() override;


    /**
     * @brief valueDistance distance only on the value-axis, basically just abs(delta val)
     * @param p1
     * @param p2
     * @return the distance between two datapoints on the time axis
     */
    double valueDistance(DataPoint p1,DataPoint p2) override;

    /**
      * @brief valueDistance calculates the distance between the datapoint, both on value-axis. I.e. the minimum of left to right and right to left, if they are of unequal length.
      * @param w1
      * @param w2
      * @return the distance between all  datapoints in the given containers
      */
    double valueDistance(DataContainer d1, DataContainer d2) override;

    /**
     * @brief timeDistance distance only on the time-axis, basically just abs(delta t)
     * @param p1
     * @param p2
     * @return the distance between two datapoints on the time axis
     */
    double timeDistance(DataPoint p1,DataPoint p2) override;

    /**
      * @brief timeDistance calculates the distance between the datapoint, both on time-axis. I.e. the minimum of left to right and right to left, if they are of unequal length.
      * @param w1
      * @param w2
      * @return the distance between all  datapoints in the given containers
      */
    double timeDistance(DataContainer d1, DataContainer d2) override;

    /**
     * @brief distance distance both on time and value-axis
     * @param p1
     * @param p2
     * @return the distance between two datapoints
     */
    double distance(DataPoint p1,DataPoint p2) override;

    /**
      * @brief distance calculates the distance between the datapoint, both on time and value-axis. I.e. the minimum of left to right and right to left, if they are of unequal length.
      * @param w1
      * @param w2
      * @return the distance between all  datapoints in the given containers
      */
    double distance(DataContainer d1, DataContainer d2) override;

    //TODO all *distance(Datacontainer...) methods can be done by one higher order function which gets the *distance(DataPoint..:) method as a param.


    /**
     * @brief semanticDistance Calculates the discrete frechet distance, assuming the containers are aligned.
     * Will ingore all Data before / after the shorter curve has ended
     * Expensive to calculate.
     * @param d1
     * @param d2
     * @return the distance between the containers. NaN if their times do not overlap. Ignoring all non overlapping areas for the similarity analyis
     */
    double semanticDistance(DataContainer d1, DataContainer d2) override;


};

#endif // EUCLIDEANFRECHETDATADISTANCEMETRIC_H
