#include "datacontainer.h"

DataContainer::DataContainer(bool isInterpolatable, QString source)
{
    m_isInterpolateable = isInterpolatable;
    mSource = source;
}

DataContainer::DataContainer(QMap<QDateTime , DataPoint *> data, bool isInterpolatable, QString source)
{

    this->data = data;
    m_isInterpolateable = isInterpolatable;
    mSource = source;
    calcStartEnd();

}

DataContainer::DataContainer(DataContainer *cpy)
{
    this->data = cpy->getData();
    m_isInterpolateable = cpy->isInterpolateable();
    mSource = cpy->getSource();
    calcStartEnd();
}

double DataContainer::slopeAt(QDateTime key)
{
    const DataPoint* base = findClosest(key);
    const DataPoint* base2;
    if(base != nullptr)
    {
        base2 = findClosestNeighbor(base->getTime());
    }
    if( !key.isValid() || base == nullptr || !base->isValidValue() ||base2 == nullptr || !base2->isValidValue())
    {
        return  nan("");
    }else{
        if(base->getTime().msecsTo(base2->getTime()) > 0)
        {
            return (base->getValue() - base2->getValue())/(base->getTime().toMSecsSinceEpoch() - base2->getTime().toMSecsSinceEpoch());
        }else{
            return (base2->getValue() - base->getValue())/(base2->getTime().toMSecsSinceEpoch() - base->getTime().toMSecsSinceEpoch());
        }
    }
}

double DataContainer::valueAt(QDateTime key,bool stayInRange)
{
    DataPoint *dp = find(key);
    if( dp != nullptr && dp->isValidValue())
    {
        return dp->getValue();
    }else if (m_isInterpolateable == true){
        if(stayInRange == true)
        {
            if(key.msecsTo(start) > 0)
            {
                key = start;
            }else if(key.msecsTo(end) < 0)
            {
                key = end;
            }
        }
        Tangent tang(findClosestNeighbor(key),this->slopeAt(key));
        if(tang.isValid())
        {
            return tang.valueAt(key);
        }else{
            return nan("");//invalid interpolation
        }
    }else{
        if(CONF_HOLD_VALUE == true)
        {
            return findPrevious(key)->getValue();//holds old value as long as there is no new one
        }else{
            return findClosest(key)->getValue();
        }
    }
}
bool DataContainer::isInterpolateable() const
{
    return m_isInterpolateable;
}

QString DataContainer::getSource() const
{
    return mSource;
}

void DataContainer::calcStartEnd()
{
    QList<QDateTime> times = data.keys();
    QDateTime smallest = QDateTime();
    QDateTime largest = QDateTime();
    for(QDateTime curr : times)
    {
        if(!smallest.isValid() || (curr.isValid() && curr.msecsTo(smallest) > 0  ))
        {
            smallest = curr;
        }
        if(!largest.isValid() || (curr.isValid() && curr.msecsTo(smallest) < 0  ))
        {
            largest = curr;
        }
    }
    start = smallest;
    end = largest;

}

int DataContainer::indexOf(DataPoint *val)
{
    if(val != nullptr)
    {
        QList<DataPoint*> datLst = data.values();
        for(int i=0;i< datLst.size();i++)
        {
            if(datLst.at(i)  == val )
            {
                return i;
            }
        }
    }
    return -1;
}

QDateTime DataContainer::reverseFind(DataPoint *dp)
{
    int i = indexOf(dp);
    if(i != -1)
    {
        return data.keys().at(i);
    }else{
        return QDateTime();
    }
}

bool DataContainer::isValidData()
{
    QList<DataPoint *> values = data.values();
    QList<QDateTime> keys = data.keys();
    for(int i=0;i<values.size();i++)
    {
        DataPoint* val = values.at(i);
        QDateTime key = keys.at(i);
        if(val != nullptr && key.isValid() && val->getTime().isValid() && val->getTime().msecsTo(key) == 0)
        {
            continue;
        }else{
            return false;
        }
    }
    return true;
}

DataPoint *DataContainer::findClosestHelper(QDateTime key, bool retKey)
{
    if(!key.isValid())
    {
        return nullptr;
    }else{
        QList<QDateTime> keys = data.keys();
        if(retKey == false)
        {
            keys.removeAll(key);
        }
        QDateTime closest = QDateTime();
        for(QDateTime keyIt : keys)
        {

            QApplication::processEvents(QEventLoop::AllEvents,1);
            if(!closest.isValid() || (keyIt.isValid() && abs(keyIt.msecsTo(key)) < abs(closest.msecsTo(key))))
            {
                closest = keyIt;
            }
        }
        if(closest.isValid())
        {
            return data.value(closest);
        }else{
            return nullptr;
        }
    }
}
Curve DataContainer::toFrechetCurve()
{
    Points pts = Points(2);
    QList<QDateTime> keys = data.keys();
    for(int i = 0 ; i< keys.size() ; i++)
    {
        Point tmp(2);
        tmp.set(0,keys.at(i).toMSecsSinceEpoch());//FIXME OVERFLOW /precision ?
        tmp.set(1,data.value(keys.at(i))->getValue());
        pts.add(tmp);
    }
    Curve ret(pts);
    return ret;
}

DataPoint *DataContainer::findNext(QDateTime key)
{
    if(!key.isValid())
    {
        return nullptr;
    }else{
        QList<QDateTime> keys = data.keys();
        QDateTime closest = QDateTime();
        for(QDateTime keyIt : keys)
        {
            if(!closest.isValid() || (keyIt.isValid() && keyIt.msecsTo(key) > 0 &&  abs(keyIt.msecsTo(key)) < abs(closest.msecsTo(key))))
            {
                closest = keyIt;
            }
        }
        if(closest.isValid())
        {
            return data.value(closest);
        }else{
            return nullptr;
        }
    }
}

DataPoint *DataContainer::findPrevious(QDateTime key)
{
    if(!key.isValid())
    {
        return nullptr;
    }else{
        QList<QDateTime> keys = data.keys();
        QDateTime closest = QDateTime();
        for(QDateTime keyIt : keys)
        {
            if(!closest.isValid() || (keyIt.isValid() && keyIt.msecsTo(key) < 0 &&  abs(keyIt.msecsTo(key)) < abs(closest.msecsTo(key))))
            {
                closest = keyIt;
            }
        }
        if(closest.isValid())
        {
            return data.value(closest);
        }else{
            return nullptr;
        }
    }
}

DataPoint *DataContainer::findClosestNeighbor(QDateTime key)
{
    return findClosestHelper(key,false);
}



DataPoint *DataContainer::findClosest(QDateTime key)
{
    return findClosestHelper(key,true);
}

bool DataContainer::remove(DataPoint *dp)
{
    if(dp != nullptr)
    {
        QMap<QDateTime,DataPoint*>::iterator localFind = data.find(dp->getTime());
        if(localFind != data.end())
        {
            bool ret =  data.remove(localFind.key()) > 0;
            calcStartEnd();
            return ret;
        }else{
            return false;
        }
    }else{
        return false;
    }
}

DataPoint *DataContainer::find(QDateTime key)
{
    QMap<QDateTime,DataPoint*>::iterator it = data.find(key);
    if(it == data.end())
    {
        return nullptr;
    }else{
        return it.value();
    }
}

unsigned int DataContainer::getSize()
{
    return data.size();
}

const QMap<QDateTime,DataPoint*> DataContainer::getData()
{
    return data;
}


const QDateTime DataContainer::getStart()
{
    return start;
}

const QDateTime DataContainer::getEnd()
{
    return end;
}

DataPoint *DataContainer::getNext(DataPoint *current)
{
    int index = indexOf(current);
    if(index == -1 || index +1 >= data.size())
    {
        return nullptr;
    }else{
        return data.values().at(index+1);
    }
}

const DataPoint *DataContainer::getPrev(DataPoint *current)
{
    int index = indexOf(current);
    if(index == -1 || index -1 >= 0)
    {
        return nullptr;
    }else{
        return data.values().at(index-1);
    }
}
